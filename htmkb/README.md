# HTMKB: misusing HTML editors for rich data entry

HTMKB is an attempt at a no-code Git-friendly personal database solution with simple templating where all the field contents are written in HTML.

It was designed with [SeaMonkey](https://www.seamonkey-project.org/)'s HTML editor in mind.

It was _not_ designed to be _particularly_ flexible, just to keep the data in it decently easy to maintain.

## Basic Workflow

* `htmkb-init`: Essentially a quickstart.
* `htmkb-checkout`: Applies templates to files in `json/` to generate the `html/` directory.
* `htmkb-commit`: Converts files in the `html/` directory to files in `json/`.

## Attributes

HTMKB uses hidden attributes in the HTML to bind the JSON and HTML together.

HTMKB maintains a "current object" as it walks the HTML tag tree.

Attributes are processed roughly in the order given.

### `<html htmkb-template="...">`

The `htmkb-template` attribute is automatically added on checkout to ensure that the template can be found from the HTML file.

It is equivalent to a field called `"template"` in the JSON object.

### `htmkb-no-commit="1"`

If `htmkb-no-commit` is set on a tag, regardless of value, commit ignores this tag and its contents.

This is useful when information is duplicated.

### `htmkb-xref="..."` / `htmkb-xref-fixed="..."`

The `htmkb-xref` attribute:

1. Implicitly causes this tag to be ignored on commit.
2. Changes the current object to the root of the JSON file referred to by the given field of the previous current object.

The `htmkb-xref-fixed` is similar, but rather than looking up a field, it just provides a hardcoded object ID (say, for globals).

These are processed early because of the `htmkb-xref="..." htmkb-array="..."` pattern.

### `htmkb-object="..."`

The `htmkb-object` attribute is processed before all other attributes that would index a field.

It indexes a field (creating an object there if necessary).

### `htmkb-array="..."`

The `htmkb-array` attribute is processed before all other attributes that would index a field.

It indexes a field (creating an array there if necessary).

### `htmkb-href="..."` / `htmkb-src="..."` / `htmkb-href-fixed="..."` / `htmkb-src-fixed="..."`

The `htmkb-href` attribute sets the `href` attribute to point to the page of the JSON file referred to by the given field.

`htmkb-src` is the same, but sets the `src` attribute. Same basic idea.

The `-fixed` variants of these don't do a field lookup, instead referring to a page directly.

## `htmkb-array-element=""`

The `htmkb-array-element` attribute existing reacts interestingly between commit and checkout.

The current object is assumed to be an array.

Tags with this attribute are _deleted entirely_ during checkout and then templated once for each array element.

If the attribute is set to exactly `"non-empty"`, then, during checkout, if no elements are created, a dummy element will be inserted with an empty object.

For internal tracking reasons, the value `"active"` is special and causes this attribute to be a NOP during checkout.

During commit, having this attribute creates the array element.

### `htmkb-inner-text="..."`

The `htmkb-inner-text` attribute binds a field of the current object to the inner text of the tag it is attached to.

### `htmkb-inner-html="..."`

The `htmkb-inner-html` attribute binds a field of the current object to the inner HTML of the tag it is attached to.

On commit, the HTML is written as an array, with the elements joined by newline characters.

On checkout, the value may be an array (as in commit) or a string.
