<cue-pva-example-logo/>

Portable Vector Animation, not to be confused with anything else called Portable Vector Animation in past or future that might cause me to have to change the name, is an attempt to create a small-size vector / sprite animation format that, at least in theory, can work with any 3D software package.

While the current exporter is Blender-based, the format will work nicely with anything that can provide a series of individual frames made up of coloured/textured triangles.

The JavaScript renderer supports WebGL and to a lesser extent regular old 2D Canvas (though it requires DOMMatrix anyway, so the utility of this is limited).

The addon tries very hard to support all sorts of things you can do in Blender, like geometry nodes (ab)use. However, using these features is not necessarily file-size-efficient.

Still, using geometry nodes, effects like lighting can be achieved:

<cue-pva-example-city/>

The renderer's written in TypeScript, and is licensed under the MIT license - see [the unminified code](libpva.js) or [the minified code](libpva.min.js).

The library's relatively easy to add into a page for flexible control; or for simple looping animations, as shown above, an iframe wrapper can be used.

There's also [a more controllable viewer for testing and debugging](viewer.html), along with a [2D Canvas-only counterpart.](viewer.html?wtf)

## Goals

* Can be written with any 3D software package with minimal integration work, while allowing full use of automation tools such as Geometry Nodes etc.
	* _Caveat: This implies that it's not acceptable for PVA to require "too much" information native to any specific tool. As such, interpolation is not supported. Expressions are supported insofaras their resolved values are "baked in"._
	* This avoids the problems with, say, Lottie.
* Minimal (but flexible) playback code.
	* For the web, the libpva.min.js code is <how-big-is-pva/>.
	* Playback code can seek arbitrarily without issues, so can easily be synced to audio/etc.
	* Scale factor allows for supersample-based antialiasing via CSS.
	* Written in modern TypeScript, aims for rough compatibility with 2010-2014 browsers. (This is because the canvas texture transforms require DOMMatrix and the alternative is WebGL.)
* Uses Deflate for the heavy lifting.
	* Format is arranged for Deflate-friendliness.
	* For better compression, decompress and apply zopfli --zlib.
	* This helps to remove the need for editor-specific tricks and interpolation.
	* Doesn't assume your web server can ship gzipped files - a Deflate decompressor is included in the library.

## The code? The specification? The Blender addon?

[All here.](https://gitlab.com/20kdc/scrapheap/-/tree/main/pva)

## What about release stability?

Riight. That. Um. I don't actually have that in place at this time. Sorry! I'd probably need to move this whole thing into a separate repository and figure that out.

Though, at this point, the chance of a major change is extremely low.

## .blend file examples?

Alright! Note that these expect [an asset catalog to be loaded.](https://gitlab.com/20kdc/scrapheap/-/tree/main/blender-asset-catalog)

* [Loading Dots](loading.blend) ([Demo](demo.html#loading.pva))
