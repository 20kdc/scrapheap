## Why is this here?

~~So I can read it later, why else?~~

In case I need to explain everything to someone later.

<warning>

The VRChat SDK changes Unity version every so often. This guide may therefore become out of date.

</warning>

## Unity

First and foremost, you need a Unity account. These are free... so long as you don't meet a bunch of conditions that make you ineligible for Unity Personal. (Whose idea was it to tie VRChat content creation to the engine this heavily?)

Secondly, you need to [install Unity Hub for Linux.](https://docs.unity3d.com/hub/manual/InstallHub.html#install-hub-linux)

The current Unity version to use is presently listed at <https://creators.vrchat.com/sdk/upgrade/current-unity-version/>. The Unity version that's recommended seems to change every so often, and this guide will only occasionally be updated. This guide was _last updated for_ 2022.3.22f1.

<warning>

You need to install build support for the platforms you wish to target:

* Windows/Proton: 'Windows Build Support (Mono)'
* Android/Quest: 'Android Build Support' (OpenJDK and Android SDK/NDK tools not required)
* iOS: _Untested, but probably 'iOS Build Support'_

If you don't install these, they can be installed later by pressing the cog icon on the right of the install entry (in the Installs menu in Unity Hub) and pressing 'Add modules'.

</warning>

## VPM/Creator Companion Substitute

Ok, so, for all practical purposes, pretend VPM and the Creator Companion don't exist (they won't work properly).

Use [vrc-get](https://github.com/vrc-get/vrc-get) and/or [ALCOM](https://github.com/vrc-get/vrc-get/tree/master/vrc-get-gui) (if you want a GUI) instead. I've had issues getting ALCOM to start Unity but that's fine since you can just use Unity Hub for the launch anyway.

## Template Projects

The VRChat [project template repositories](https://vcc.docs.vrchat.com/guides/using-project-template-repos) auto-download the VRChat SDK by themselves, and they make good, well, template projects.

## The SDK Patches

<warning>

This section may or may not be out of date. Pay attention to it if you get any of the mentioned issues.

</warning>

So there's [a forum thread](https://ask.vrchat.com/t/error-uploading-avatar-from-unity-linux-2022-3-6f1/21530) going over an interesting issue you'll have with uploads.

For the programmers in the audience, simply put, VRChat thought they were being clever by mucking around in the guts of Mono APIs using reflection, didn't bother to silence errors in this non-essential code, and you get to clean up the mess.

In your project, `Packages/com.vrchat.base/Editor/VRCSDK/Dependencies/VRChat/API/VRCTools.cs` will contain a function called `IncreaseSendBuffer`. Simply remove the contents (between `{}`) of the function.

## The World Building Issue

So you might get the error `AssetBundle was not built` if you're trying to make worlds.

Thanks to <https://blog.tlfoxhuman.net/2023/12/26/how-to-upgrade-to-unity2022-on-linux/>, there is an answer to how this happened. In short, case sensitivity.

As such, install `ciopfs`, rename `/tmp/DefaultCompany` to `/tmp/DefaultCompany-cs` and run `ciopfs /tmp/DefaultCompany-cs /tmp/DefaultCompany`.

I personally used this script (simply deletes the directory entirely):

```
#!/bin/sh
umount /tmp/DefaultCompany
rm -rf /tmp/DefaultCompany
rm -rf /tmp/DefaultCompany-cs
mkdir /tmp/DefaultCompany
mkdir /tmp/DefaultCompany-cs
ciopfs /tmp/DefaultCompany-cs /tmp/DefaultCompany
```

_It's worth mentioning that <https://github.com/BefuddledLabs/LinuxVRChatSDKPatch> appears to have a neater, less awkward solution to this issue._

## Mobile Emission Shader Issues

See <https://feedback.vrchat.com/sdk-bug-reports/p/standard-lite-emission-broken-in-unity-2022>.

## Offline Testing

This might be possible with something along the lines of:

`flatpak run com.valvesoftware.Steam -applaunch 438100 "--url=create?roomId=6706523883&hidden=true&name=BuildAndRun&url=file:///Z%3a%5chome%2ft20kdc%2f.local%2fshare%2fVRChat%2fVRChat%2fWorlds%2fscene-StandaloneWindows64-VRCDefaultWorldScene.vrcw" --no-vr --enable-debug-gui --enable-sdk-log-levels --enable-udon-debug-logging`

The biggest problem is figuring out what about it doesn't work, as VRChat doesn't seem to document this...

## Random Bits

* Resources on VRChat creation:
	* <https://vrc.school/> provides a lot of Unity information the official guide lacks.
	* The VRChat creator's guide at <https://creators.vrchat.com>.
	* <https://vrcfury.com/> is a common addon used for VRChat avatar creation, patching a lot of usability holes in the toolset. In particular, it's useful for managing the expressions menu while installing locomotion replacements, and it trivializes the process of adding a blendshape toggle.
* In Edit -> Project Settings, there is a UdonSharp section. This is where you disable 'Listen for client exceptions' (which UdonSharp will request you do).
