## Prologue

So I have a few Java projects.

* [isle-tooling](https://gitlab.com/20kdc/scrapheap/-/tree/main/isle-tooling)
* [gabien-common](https://github.com/20kdc/gabien-common/)
* [R48](https://github.com/20kdc/gabien-app-r48/) (primary consumer of `gabien-common`)
* [c3ds-projects](https://github.com/20kdc/c3ds-projects)

I rely heavily on the tooling provided by IDEs for a productive development environment.

This is actually why I hate C#, because it has such a rotten ecosystem that basic tooling we take for granted in the Java world is broken or downright unusable in the C# world.

<tangent-Rant>

Between proprietary debuggers and third-class implementations, the C# Language Server ecosystem exists mainly to provide the appearance of fair play. The comments of [OmniSharp/omnisharp-roslyn#2241](https://github.com/OmniSharp/omnisharp-roslyn/issues/2241) are extremely insightful and [dotnet/sdk#22247](https://github.com/dotnet/sdk/issues/22247) is an example of how Microsoft are willing to try 'pushing the boundaries'. Anecdote: _I have never had working C# hot reload since .NET Core._

</tangent-Rant>

But for IDEs to provide this tooling, they need a _high level of integration._ The ability to alter a function at runtime does not come from nowhere; it must be _provided._

That integration comes with a need for a tight grip on the build and run process.

Parallel to this: As far back as the days of C, there have been build systems; this is why Makefiles have such a long heritage.

Build systems and IDEs tend to either conflict or have a very uneasy alliance.

The most common fix is the IDE being able to read the build system's files (Cargo, MSBuild, Maven). The second most common fix is the build system being able to generate project files (CMake, Gradle).

The problem is when builds do, essentially, anything non-trivial at all ever.

I have never seen any build plugin not created by the authors of the build system that has not horribly broken down at most a few years later. (Have you ever tried to build an old Minecraft Forge-based mod? What about an old Android project?) Yet as long as the build system _acts consistently in regards to its outputs,_ a shell script usually stays decently stable. Why is this?

The answer is that _most build system authors can't be trusted at all_ and _some can only be trusted so far._ This is for the exact same reason that you're better off vendoring ffmpeg on Linux.

The fact of the matter is that certain major build systems, like certain libraries, are both _treated as if they are stable software_ and _do not act like stable software, nor do they pretend to._

These major build systems end up in distributions and are _quasi-stable_ for any given version of the distribution, but then fail when moved to another version. Make no mistake, though, Windows & Mac aren't immune to this problem; in fact, it's even worse. Substituting 'distribution' for 'whatever the user happened to download that day' doesn't actually change anything.

## The Good, The Bad, And The Ugly

Of the build systems I've dealt with, the nicest are Cargo and Maven.

Both of these seem to be pretty zealous about _not breaking things,_ with the only hitch being Maven increasing its Java version requirement.

Both Cargo and Maven have build plugin/script mechanisms.

* Maven: My biggest concern is that Maven's is too complicated to reasonably follow without going through some kind of Maven course. However, if you stick to StackOverflow copypasta, you can do _100% of things you would ever need to do to any IDE-compatible Java SE project._
	* The 'IDE-compatible' part of this is important. Generated source code is _not_ IDE-compatible, except *maybe* annotation processors but they have their own issues. I'll get into what I replaced them with later.
* Cargo: I'll be honest: Rust has metaprogramming that's too good to actually make me care about Cargo's `build.rs` as anything but a slightly overcomplicated way of setting up native libraries. I appreciate that it's written in Rust so I'm not learning *another* language.

The worst are CMake and Gradle.

CMake's issue is being a complex bespoke language where bugs can be present which you have to fix at 3AM because someone told you you should 'just' compile a thing yourself and PR a change, except apparently you're missing pkg-config files even though the package is _clearly installed_ and now you don't know what to do and you give up.

Gradle's issues. Oh, _Gradle's issues._ Ok, so, the thing you need to know about Gradle is that Java doesn't have a built-in bytecode emitter API. _Why does that matter?_ Well, because it means they have to replace it with ObjectWeb ASM. That's fine, it's a good, solid library. Now I'm going to upgrade your JDK to a 'compatible' future version. Oh, your current Gradle broke. Ok, fine, just update Gradle -- _OH DEAR GOODNESS THE ENTIRE BUILD IS ON FIRE WHAT DID YOU **DO.**_

In addition to this, Gradle support in IDEs with more 'advanced' projects seems to involve the IDE running Gradle in some capacity. This tends to suck for your build/iteration times and general developer experience. When I only had 4GiB of RAM and was using IntelliJ IDEA, I was acutely aware of how bad it was.

As for ugly, I have a personal abomination of shell scripts I'm trying to turn into a somewhat less awful Java-based system to compile Android projects.

Why would I do that? Well:

1. Google _really_ want you to use Gradle.
	* You can't _get_ recent Android API JARs via Maven Central.
	* In fact, any tooling which isn't Android Studio and Gradle is effectively unsupported.
2. I went over why Gradle wasn't an option (build breaks).

How often is this system likely to break? Theoretically more often, practically less often. Compatibility with itself doesn't matter because it's built into a set of projects which don't advertise a stable versioning policy anyway.

## Principles

So, `umvn` & `gabien-do` ; this particular pair of partners in positive practices happened because of a [D8 bug with OpenJDK 21 compiles](https://stackoverflow.com/questions/77788162/android-project-no-longer-compiles-when-api-dependency-is-compiled-with-jdk-21) and my knowledge that the Maven toolchains plugin is awful to work with (Write and add a `toolchains.xml` file to your `.m2` directory. Where's that? What does it look like? If you're a Windows user, how is this supposed to be _less_ complicated than `setx`?). I later found out that `maven.compiler.executable` exists, but removing "install Maven" from the guidance for Windows users was worth it by itself.

I was finally at the breaking point where I wanted to simplify my build for external contributors.

My objectives, and the statement I really want to make about all of this:

1. A build system should be portable.
2. A build system does not have to be zero-setup; in fact, it is better if it goes wrong for everybody immediately, so that the build guide teaches the contributor how to fix the error, than for 'magic' to fail, leaving the contributor stranded.
3. There are two _layers_ of build system; a layer which mirrors the actions an IDE would perform in a consistent way (essentially a 'compile the entire project' command, and if necessary a dependency manager), and a layer which orchestrates the setup of the environment and the release process.
4. The compiler layer should be as universal as possible and should be shared between as many projects as possible.
	* It should be as complex as necessary; if the language supports the host platform, it should be capable of producing a shippable output file for the host platform from an arbitrarily complex project tree.
	* It should not support plugins; it should assume the orchestration layer will generate source code, native libraries etc. before the compile begins. 
	* Essentially, **assume the orchestration layer will perform any action an IDE would not perform.** The compiler layer *replaces the IDE,* including the IDE's compilation of several interdependent projects.
5. The orchestration layer should be _per-project custom code built using a compiler-layer build system, ideally the same system used for the rest of the project._
	* Under ideal circumstances, the orchestration layer would be 100% custom code; however if it is shared, it should be as stable as the project's underlying framework.
	* The idea here is that the orchestration layer can, among other things, **perform code generation, even in an IDE context.** This allows the orchestration layer to provide 'super-macro' capabilities in languages with limited or no macro support without interfering with IDE function.
	* Orchestration layers may end up invoking the orchestration layers of other projects if they're co-developed/vendored. **Absolutely make sure there's a decent plan for this if it could be relevant, but if the IDE and/or compiler layer can do everything for you in this regard then there's not that much to worry about.** (In Maven, for example, if adding `<module>../other</module>` is enough to vendor a dependency, don't worry too much. Also keep in mind that the point of making the orchestration layers work this way is to make them easy to tweak.)

## What I Actually Ended Up Doing, And How It's Going

What I ended up doing was this:

* As the compiler layer, I implemented microMVN, aka `umvn`. This is essentially a universal Java 8 'minimum viable subset of Maven'.
	* By using a subset of the Maven POM structure, POM parsing (the standard for Java dependency management) is inherent, saving code.
	* `umvn` is a single `.class` file in size, and can be run on any Java 8+ JRE, allowing for trivial redistribution. It does not use ObjectWeb ASM and should last as long as Java 8 execution support lasts (beyond which, there isn't a good way of fixing things anyway).
* As the orchestration layer, I implemented `gabien-do`, which uses `umvn` to compile itself along with a project-specific `build-script` directory and runs the resulting program. The framework only prescribes a consistent way of supplying command-line arguments to tools, along with supplying some key utilities. Scripts do not need to worry about 'phases' or 'tasks' or 'recipes'; release builds should be clean anyway.
* The two are tied together using a Python `venv`-style system.
* The initial build will likely fail immediately with an `env.JAVA_1_8_HOME`-related error. This is absolutely expected. The second build will fail due to lack of `gabien-natives` installation. This is also expected. Outside of Android (which, frankly, there is no way to make not a mess, though I'm working on improving things), these should be the only two requirements. This works _not_ because of 'magic,' but because _everything else required for the build has been shipped._
* Everything that can be is being moved to Java, so a potential contributor (who presumably knows Java) knows the language to fix any possible error that could occur.
* As much of as the build tooling as possible is being moved to Java 8 specifically, preventing any risk of JDK version conflicts.

Ultimately, this is going to be one of Those reworks, where it takes a while but it makes everything better.

At the very least, if I can make the release process work on Windows, that'd be nice. Before this big rework, the piling up issues lead to a haphazard shell-script and Lua-based build process, particularly around Android. That may seem very confusing given my statements around portability, but keep in mind: _Gradle wasn't an option, and Google really, **really** want you to use Gradle._ The state of Eclipse Android development is basically dying and/or dead. My Android projects that aren't meant to be cross-platform surrender to Gradle even knowing they'll be unbuildable in a few years.

But for a decently large project (`cloc` says ~60k LOC of Java, split across ~13 Maven projects depending on what you count) I needed to be able to maintain, shell scripts _kept it maintainable_ when it would have otherwise bitrotted.

I think that, if nothing else, shows why designing your build this way matters, because when throwing together shell scripts works better than the 'professional' option, _something is seriously wrong._

The main things I want to do are:

* Finish moving the GaBIEn/R48 build to be completely portable, including Android.
* Contributor onboarding tests in Docker containers; how difficult is it to go from blank container to Android dev build? Find pain points, etc. Moving `gabien-natives` to being downloaded from Pages would be nice.
	* Making the `gabien-natives` compile easy is sadly _completely impossible_ due to the variety of compile targets.

With that, I guess that's this post over? I think it did turn out a bit of a poorly-structured ramble this time. It's sort of a companion piece to the [microMVN Rationale](https://github.com/20kdc/gabien-common/blob/master/micromvn/RATIONALE.md) page, except that page looks at the problem from a narrow angle while this looks at the problem from a more broad angle, going over also the rationale of the `gabien-do` system and such.

<cue-signoff-2/>
