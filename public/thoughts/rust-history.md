## Prologue

You know, fun fact for you. This was originally just going to involve pulling some old compiler versions and seeing how the language changed.

```
t20kdc@Trevize:~/Downloads/rust-0.1$ make
 (...)
determined most recent snapshot: rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0curl: (6) Could not resolve host: dl.rust-lang.org
 (...)
subprocess.CalledProcessError: Command '['curl', '-o', 'dl/rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2.tmp', 'http://dl.rust-lang.org/stage0-snapshots/rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2']' returned non-zero exit status 6.
 (...)
make: *** [mk/stage0.mk:7: x86_64-unknown-linux-gnu/stage0/bin/rustc] Error 1
```

_Ok, this is going to be a blogpost about the journey too, huh._

A warning, also; I'm not really _happy_ with this post. I don't think it provides an in-depth experience of these older versions of Rust.

At best, it gives you an idea of how you might get one of them running so you can experience them for yourself, and it gives you _something_ to get your teeth into if you're curious.

**But ultimately, this is a blogpost about exploring a programming language's history written by someone who wasn't there for it, but just really curious.**

## Part 1: A Simple Idea

I've always been kind of fascinated by how projects develop.

I think people in general are fascinated this way, which is why blogposts about how projects develop are interesting.

So, to get to the point: I've semi-recently been using [Rust](https://rust-lang.org) a lot more.

<tangent-Tangent>

I'd have probably migrated the native code in some of my projects to Rust, if they weren't deep into "public domain or die trying" territory.

And if macOS didn't exist.

</tangent-Tangent>

And I've heard some things about Rust's history, but trying to actually poke at it is a little more... difficult.

Here are the materials that you might want to go through before/after reading this:

* A presentation: <https://www.youtube.com/watch?v=79PSagCD_AY>
	* This goes into a lot of detail about the processes, and about general trends, but it has less about the moment to moment language. It does still _have_ slides with code, but it's not exhaustive (for probably obvious reasons).
* Some personal blogs:
	* nikomatsakis: <https://smallcultfollowing.com/babysteps/blog/>
	* graydon: This is a very varied blog, so I'll link to a few particular posts.
		* "Rust release lightning talk": <https://graydon2.dreamwidth.org/214016.html>
		* "The Rust I Wanted Had No Future": <https://graydon2.dreamwidth.org/307291.html>
* The release notes: <https://github.com/rust-lang/rust/blob/master/RELEASES.md>
	* This gives you a very dry view of the changes, but not what the language actually looked like.
* The meeting minutes: <https://github.com/rust-lang/meeting-minutes/blob/master/Meetings-2011.md>
* An old repo of 'prehistory' <https://github.com/graydon/rust-prehistory/>
	* This is before the compiler was self-hosting, pre-0.1.
		* The positive here is that being before the compiler was self-hosting means you don't need an ancient proto-Rust compiler to build it.
		* The negative here is that it doesn't actually compile for some reason?
	* The documentation is basically more like ideas about what the language _will_ be. That's not to insult anyone, it's just a natural consequence of plucking from a point in time before the project really got 'official'.
* The actual release tags for `0.1`/etc.
* And most importantly, <https://forge.rust-lang.org/archive/release-history.html> -- which is a pretty good definitive resource for the older versions.
	* Contains actual, usable tarballs for 0.10 onwards.
	* Contains Windows installers down to 0.2.
	* Contains a source code tarball for 0.1.
		* The original announcement mentions a Windows installer too. It's gone due to a bug where it can damage the system PATH: <https://web.archive.org/web/20220815094335/https://mail.mozilla.org/pipermail/rust-dev/2012-January/001261.html>

## Part 2: Domain Change

So, surely it's possible to compile old Rust programs with all of this?

_Well..._ actually yes. But first, we need to play a guessing game.

See, `dl.rust-lang.org` isn't a thing. But `static.rust-lang.org` is a thing, so, do a little substitution and...!

```
t20kdc@Trevize:~/Downloads$ sha1sum rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2
798f019d0b32c7df2ad173877c6c52b90ae25462  rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2
```

Yay! Now excuse me while I stash this somewhere so I'll _never_ lose it... and copy into the `dl/` directory, aaaaaaand...

```
./src/rt/sync/sync.cpp: In static member function ‘static void sync::yield()’:
./src/rt/sync/sync.cpp:10:18: error: ‘int pthread_yield()’ is deprecated: pthread_yield is deprecated, use sched_yield instead [-Werror=deprecated-declarations]
   10 |     pthread_yield();
 (...)
cc1plus: all warnings being treated as errors
```

Oh come on!

...`-Werror` should be removed from C/C++ compilers. Ok, ok, I hear you saying "Wait, _what?_".

The problem is that `-Werror` makes literally existing as unchanging code on a disk a semver-incompatible change. Compilers find new excuses all the time to warn about things. I had to go remove these from `mk/platform.mk`.

Release tarballs, even indev ones, should _never_ use `-Werror`. It makes some sense inside your internal 'sanity check' scripts which a user can easily skip, but even then, if it's going to be buried inside a Makefile tree -- or _worse,_ CMake or autoconf -- just don't do it!

But alright, now that's dealt with...

```
./src/rt/rust_shape.h:110:1: error: expected unqualified-id before ‘alignof’
  110 | alignof() {
      | ^~~~~~~
```

This'll be a long day, huh? But, ok, let's just substitute `alignof` for `RSalignof` in `rust_shape.cpp` and `rust_shape.h` and trust it works out.

```
/home/t20kdc/Downloads/rust-0.1/src/llvm/include/llvm/ADT/IntervalMap.h:1980:47: error: expected primary-expression before ‘)’ token
 1980 |     Node[NewNode] = this->map->newNode<NodeT>();
      |
```

Ok, I don't think I'm going to be able to repair the entirety of LLVM to counter C++ language drift.

Remind me, how do people use C++ in production again? I don't think it's worse than Node, because in Node, _nothing_ is stable, but _yeesh._

## Part 3: Containers

![A graph. X axis: Containers. Y axis: More Containers. It looks somewhat thrown-together. Yes, this is meant to be a reference to an obscure meme that floated around Minecraft modding channels 8 years ago.](sapphire.svg)

Containers... are _exactly what we need for this situation._

The C++ language is not stable. Anyone who tells you it is stable is at best naive and at worst _lying._ It's so unstable that you effectively need a time machine to compile old code, as anyone following along at home has now likely surmised.

We need to set our target date to 2012, so...

* `podman run -it debian:squeeze`
* exit with `exit`
* `podman ps -a` to get the container name (here `tender_fermat`)
* `podman cp rust-0.1.tar.gz tender_fermat:/`
* `podman cp dl/rust-stage0-2012-01-18-8c97854-linux-x86_64-798f019d0b32c7df2ad173877c6c52b90ae25462.tar.bz2 tender_fermat:/`
* `podman start -ai tender_fermat` whenever we want to access it.

I won't bother describing the commands that amount to "put together the downloaded files the same way you already had to".

Anyhow, Debian have a policy which I'll affectionately call "please get off of this version". See <https://wiki.debian.org/DebianSqueeze>.

The TLDR is that you have to update your `/etc/apt/sources.list` to point at the "archive" servers and disable the signature timestamp checking.

You will still get `W: GPG error:` warnings. This is expected, I think you can ignore them but I spent so long messing around trying to get them to go away I don't even know.

The packages to install are `python3`, `curl`, `gcc`, `g++`, `make`, and `python`. That installs `python2` as `/usr/bin/python`, which seems to be _quite important_ for LLVM.

Installing `pandoc` would be a mistake; it's too old.

But in any case, a useful side-effect of this being such an old system is that ABI compatibility helps a lot here.

<tangent-Rant>

I know some people consider Linux's ABI compatibility bad, but it's fine for base libraries (C/C++ standard libraries, X11, Wayland, OpenGL, SDL2).

The key is to compile on an older distro than your test target. Use containers. 4-8 year gap typically works, and it's about the length of a Windows generation. _Don't use **anything** newer than that distro in your build; any problems it 'solves' are temporary fixes that will break tomorrow._

(I may expand on why in another post.)

</tangent-Rant>

It's now time to wait for LLVM and Rust to compile. What fun. Be sure to pass `-j` with some number higher than 1 to `make` when doing the Rust compile.

Finally, assuming all goes well, `podman cp tender_fermat:rust-0.1 ./rust-0.1` to finally pull out the ancient compiler tree, aaaand...

```
t20kdc@Trevize:~/Downloads/rust-0.1/x86_64-unknown-linux-gnu/stage3/bin$ ./rustc
usage: ./rustc [options] <input>
```

There's also `doc/rust.md`. _Actual documentation!_ Brilliant.

It can be converted with the command: `pandoc --standalone --toc --section-divs --number-sections --from=markdown --to=html --css=rust.css --output=doc/rust.html doc/rust.md doc/version.md doc/keywords.md` (since it wasn't done in the container and we *really* don't want to overwrite the successful build...)

You also should probably be reading `doc/tutorial.md`, even if you know Rust 1.0+.

## Part 4: `release-0.1 : January 2012`

So, first off, the `stage3` tree _isn't actually a full compiler tree, it's missing at least the intrinsics file._ I think it's just there to check the compiler can self-compile.

Use `stage2` instead.

Now for the good stuff...

Oh, _wow_ this feels weird.

* So crates had dedicated "crate files" (i.e. `test.rc`) along with a corresponding main source file (i.e. `test.rs`). The corresponding source file was automatically included. Doing `mod test;` leads to a duplicate function error.
* There used to be `prove` and `claim` expressions. `prove` seems to have been meant as a compile-time-error-friendly version of `assert!`. `claim` is somewhat similar to [`core::hint::assert_unchecked`](https://doc.rust-lang.org/core/hint/fn.assert_unchecked.html).
* In a truly _stunning_ moment of irony, the `concat_idents` macro was available for general use in this version, though it has limitations in where it works. Trying to use it in `let` for instance is doomed to fail mysteriously.
* Macro By Example is probably one of the oldest surviving features that never got renamed (though it did get reworked, as did basically everything).
* The equivalent to [`core::mem::forget`](https://doc.rust-lang.org/stable/core/mem/fn.forget.html), `unsafe::leak`, used to be, well, considered unsafe.
	* Ultimately, Rust's model changed to 'provably unreachable objects cannot hold meaningful references', which is what bit [the scoped threads API](https://doc.rust-lang.org/nomicon/leaking.html#threadscopedjoinguard). As it turns out, threads are _always_ reachable until it's made very certain they are unreachable.
		* For the record, when the question is 'the safety of code that accesses a parent stack frame,' I think the answer is always closures, as they have some particularly useful safety properties.
* `cargo` used to use `curl`, `git`, `tar`, `cp`, and `rustc` via command-line. Sadly, the repositories do not seem to exist to check what it was like.
* `rustdoc` used to just output Markdown, and wasn't very complete.
* What we now call `mpsc` used to be the `core::comm` module.
* `unwrap` was called `get`, and weirdly, required `T: copy` (not sure what's up with that, as it hasn't affected anything).

But the crown jewels are the differences in the type system.

* Ok, so the sigils are basically surface-level Rust history iceberg stuff.
	* `~`, aka `Box<>` (but not yet).
	* `@`, aka `Gc<>` (but not yet).
* Slices didn't exist. `[]` was basically another sigil, this time for a... slice-`Vec`-hybrid. Practically: Go's slices without slicing.
	* `str` is basically the same deal. Interestingly, this might be the _only_ surviving case of a non-primitive type with a lowercase name making it into Rust 1.0, though it got the same transformation as `[]` did (where the `Vec`-equivalent syntax became the slice-equivalent syntax).
* At this point, Rust had a lot of overlap with Go. The main claim Rust had that Go didn't was nullability and the fact that Rust's GC was _relatively_ optional. _But also,_ all the smart pointer types didn't exist, so...
* Traits used to be called interfaces, the `self` parameter was implicit, and if a closure is the last parameter to a function, then it can be written outside of the function.
	* Could be wrong about this, but it seems like interfaces were limited to only what was object-safe. Also, no default methods.
	* The last part of this change to happen was actually post-1.0: [the `Trait` -> `dyn Trait` migration.](https://doc.rust-lang.org/edition-guide/rust-2021/warnings-promoted-to-error.html)
* Newtypes are formally defined as "single-variant enums".
* Structs were originally "record types".
* Typestates look... interesting-ish, though they're very runtimey. Hmm.

### Some Examples

For this, `test.rc` is empty, the command-line is `bin/rustc test.rc`, and the examples are the contents of `test.rs`.

```
use std;

fn main(args: [str]) {
	let ab = 1;
	// Stable concat_idents?!?!
	std::io::println(#fmt("%d", #concat_idents(a, b)));
}
```

I was expecting to write more example code, but seeing that `libstd` contained both `json` and `sha1`, I have decided to instead link to them here:

* <https://github.com/rust-lang/rust/blob/release-0.1/src/libstd/json.rs>
* <https://github.com/rust-lang/rust/blob/release-0.1/src/libstd/sha1.rs>

And some code putting the JSON library into practice:

```
use std;

import core::option;
import core::result;

fn main(args: [str]) {
	let res = result::get(std::io::read_whole_file_str("test.json"));
	let j = option::get(std::json::from_str(res));
	std::io::println(std::json::to_str(j));
}
```

## Part 5: `release-0.3 : July 2012`

Ok, so, in _theory,_ we can go anywhere we like in Rust time. The time machine should work more or less just as well right up through to Rust 0.9, and presumably onwards; but after that, precompiled tarballs exist.

However, Rust 0.3 is a particularly good next step. Rust 0.2 feels like a very "fix the stuff we missed in Rust 0.1" release. Not that there's anything wrong with that, I've done it, just... There are only two things worth mentioning about Rust 0.2:

* Classes (you can go see those in Rust 0.3 anyway, but they're meh and disappear by Rust 0.4; lasted all of half a year before being axed).
* Introduction of compiler metadata, stored using EBML. Presumably used for inter-crate linking as it is later on.

This is the first version of Rust which still has online `rustdoc` documentation. Additional benefit is, we get to see what `rustdoc` was like in 2012.

* [core](https://doc.rust-lang.org/0.3/core/index.html)
* [std](https://doc.rust-lang.org/0.3/std/index.html)

And... it's fine? Like, it's fine. It's HTML with just enough styling to not look like baseline. I'm not sure I understand the use of `max-width` without centring, though.

Anyway, so, the standard library of Rust 0.3 is... big. I don't want to say _too_ big, but... a lot of scope creep clearly happened here.

In terms of the actual content... things get kind of weird here.

<tangent-Note>

While the [Rust 0.3 tutorial](https://doc.rust-lang.org/0.3/tutorial.html) and [manual](https://doc.rust-lang.org/0.3/rust.html) do not mention _unsoundness,_ it has the basic idea of 'isolate unsafe code' that makes up modern Rust. The term 'unsound' was at least used as far back as [issue #851, in August of 2011.](https://github.com/rust-lang/rust/issues/851)

</tangent-Note>

* [`core::dvec`](https://doc.rust-lang.org/0.3/core/dvec.html)
	* This is the closest thing to true `Vec` so far.
	* _Because and only because the `data` field is not protected in any way, it's unsound._ I'm pretty sure they just hadn't gotten around to yet.
* [`core::future`](https://doc.rust-lang.org/0.3/core/future.html)
	* Futures... It seems like the Rust async story in this era basically amounted to 'green threads, then let it be blocking' using the task/port/channel system.
* [`libcore/iter-trait.rs`](https://github.com/rust-lang/rust/blob/release-0.3/src/libcore/iter-trait.rs) is a funny one, as it illuminates the differences between `iface` and `trait` a little better.

In the interest of science, I tried compiling this one directly, too.

```
configure: error: Found Python 3.12.3, but LLVM requires Python 2.4-2.7
```

Alright, I get the point. To the container, backup `rust-stage0-2012-07-06-b5f5676-linux-x86_64-f1711216ab7b144306e4c3ce7d7e50e32eae762f.tar.bz2`, etc...

...The real irony is, I still need to automate the process of making _this container_ in an archivable format, and archive _that..._

Anyway:

* This time it didn't even output any binaries in stage3, just empty directories.
* `rustdoc` is now outputting entire HTML trees independently.
* `cargo` is, unsurprisingly, still not working, owing to being, you know, basically a prototype.
	* More intriguingly, there is a `sources.json` file in the tarball! Not sure if Cargo uses this by default (seems not, though), but hey, it's a way to get it running.

```
$ ./cargo sources
info: erickt (https://raw.github.com/erickt/rust-packages/master/packages.json) via curl
info: central (https://raw.github.com/mozilla/cargo-central/master/packages.json) via curl
info: elly (https://raw.github.com/elly/rust-packages/master/packages.json) via curl
```

Of these, only `erickt` is still an online package source, and no package source seems to actually work properly. This is expected, and those crates have long since updated away from this Rust version.

Now for the fun parts!

## Part 5.1: Classes

Ok, so, here's what I've learned about classes:

* Classes are pretty much a prototype of structs as we know them today.
* The manual on classes is incomplete.
* Classes _must_ have at least one field. Compiler will complain if they don't (`./test.rs:3:0: 10:1 error: a class must have at least one field`).
* There's a ???bug??? where you can redefine the constructor with different args. The original constructor is effectively deleted; it is not even checked for validity.
* Otherwise, they're mostly kind of like structs, except they don't use an interface for `drop` behaviour.
* Having `drop` makes a type uncopyable. The main difference to modern Rust is that there, you have to explicitly make a type copyable (via derive) _or_ explicitly make it droppable (via impl). Both at once is still not (thankfully) possible, as `error[E0184]` appears. Which is good, it's a useful sanity check.

```
import core::dvec::*;

class myclass {
	let tmp: int;
	new() {
		// doesn't initialize self.tmp, refers to nonexistent symbols; likely lost in parsing
		invalidcodewhichdoesntexist
	}
	new(tmp: int) {
		self.tmp = tmp;
		core::io::println("Also hi!");
	}
	drop {
		core::io::println("You dropped me!");
	}
}

fn main(args: ~[str]) {
	myclass(0);
}
```

```
$ ./test
Also hi!
You dropped me!
```

## Part 5.2: Typestates

They are assertions that live in function signatures but have to be explicitly checked. The main benefit of typestates is that you get to push the actual check where you want it (i.e. out of loop bounds), but honestly, this kind of feels like a responsibility that entered the optimizer ages ago.

What I will say is that they are _very borrow-checker like._ Here's the example I'll be using:

```
pure fn must_be_zero(x: int) -> bool {
	x == 0
}

fn thing_that_requires_zero(x: int) : must_be_zero(x) {
	core::io::println(#fmt("%d", x));
}
```

And a 'complicated' function that uses it:

```
fn main(args: ~[str]) {
	let mut tmp = 0;
	check must_be_zero(tmp);
	let mut p = 0;
	while p < 10 {
		thing_that_requires_zero(tmp);
		p += 1;
	}
	tmp += 1;
	check must_be_zero(tmp); // Panic here
	thing_that_requires_zero(tmp);
}
```

This program will encounter a task fail (read: panic) at the marked comment.

So far, so good. The utility is that if you were to remove that line, you get:

```
test.rs:17:1: 17:31 error: unsatisfied precondition constraint (for example, must_be_zero(tmp) - arising from test.rs:10:7: 10:25) for expression:
thing_that_requires_zero(tmp)
precondition:
must_be_zero(tmp) - arising from test.rs:10:7: 10:25
prestate:
!must_be_zero(tmp) - arising from test.rs:10:7: 10:25, !must_be_zero(tmp) - arising from test.rs:5:38: 5:55, !must_be_zero(tmp) - arising from test.rs:5:38: 5:55
```

This is despite the same call being perfectly valid during a loop (which I chose due to it not being perfectly linear control flow).

Basically, because the program mutably used `tmp`, all `check` statements made on it are invalidated until they are re-`check`'d. This is sort of like how `&` references interact with mutation and the borrow checker. These days, we use the type system directly for this, [and some even call it the typestate pattern.](https://cliffle.com/blog/rust-typestate/).

<tangent-Revival>

These typestates and the borrow checker use the same basic idea of control-flow based checks, and they both invalidate on mutation.

If you want something close to this pattern in modern Rust, you might be able to by representing the `check` result as something like: `struct Check<'a>(PhantomData<&'a SomeType>);` (add more `PhantomData` and lifetimes as appropriate). However, the method that consumes the `Check` can't mutate or consume the target. There are more expansive ideas, but I feel they'll evolve into some flavour of "What if: Newtypes, but _sets?_".

</tangent-Revival>

Unfortunately, being a feature of a prerelease compiler that never made it to 1.0, the implementation is also a bit unsound. Observe:

```
fn main(args: ~[str]) {
	let mut tmp = 0;
	check must_be_zero(tmp);
	if true {
		tmp = 1;
	}
	thing_that_requires_zero(tmp);
}
```

With that, a non-zero value has been laundered and supplied to `thing_that_requires_zero`.

It is worth noting that it is specifically the conditional construct here that breaks it. Related PRs from the time suggest that the typestate system was first used for detecting if a variable is initialized, which, at the time, wasn't really something that could be 'invalidated'.

It is also worth noting that the typestate feature [was removed 2-3 days after 0.3's release, around July 15th 2012,](https://github.com/rust-lang/rust/commit/41a21f053ced3df8fe9acc66cb30fb6005339b3e) and there was [a PR to remove it the month before.](https://github.com/rust-lang/rust/pull/2588), referencing a planning meeting (I'm not sure which, it's not one of the weeklies). [The next weekly meeting went over the details.](https://github.com/rust-lang/meeting-minutes/blob/master/weekly-meetings/2012-06-19.md#typestate)

## Part 6: `release-0.4 : October 2012`

This release is pretty much the beginning of the end for Old Rust. Just reading [the release notes](https://github.com/rust-lang/rust/blob/1.0.0-alpha.2/RELEASES.md#version-04-october-2012) make it clear what's going on here:

* [`alt` became `match`.](https://rust-lang.github.io/wg-async/vision/characters/barbara.html)
* "Explicit method self types" (this laid the groundwork for, among many other things, `&self` versus `&mut self`)
* Undocumented changes:
	* They were always called traits. My mistake, must have hit my head on something this morning...
	* Type names were always `CamelCase`? Ok, who suplexed a train directly into my head?
* Borrowed pointers being "more mature" actually means lifetimes start meaningfully showing up in this version.
	* I'm not sure if they were present before, but they definitely are now, though with a different syntax: `&a/MyType`.
	* [`src/rustc/middle/borrowck.rs` is real! I HAVE SEEN IT!](https://github.com/rust-lang/rust/blob/release-0.4/src/rustc/middle/borrowck.rs)
* `std::arc::ARC` is real! It's all uppercase, but it's real! I think that makes it the first non-sigil smart pointer?
	* At this point, `Clone` wasn't a trait, just a function that happened to be implemented.
* Typestate is gone.

Though, while the borrow checker seems to have been an omen of doom for most of the other ways of thinking about memory management in Rust, we're still missing some pieces before we can really say that this is the 'incremental improvement to 1.0 and nothing interesting happens anymore' stage...

* `Cell` exists, but doesn't mean what it means now. That pretty much arrived in [Rust 0.9](https://doc.rust-lang.org/0.9/std/cell/index.html); and `RefCell` arrived with it.
* Green threads, `Gc` pointers haven't been deleted yet.
* `.rc` files still exist.
* Judging by changes in 0.6, `&mut` was aliasable at some point.
* [There's a proto-Serde-ish-thingy here???](https://github.com/rust-lang/rust/blob/release-0.4/src/libstd/json.rs)

## Part 7: Some notes about 0.4 onwards

Reading Rust 0.4 code, it's pretty much "Rust but a bit _off._"

In fact, `json.rs` seems to be a great way to track the evolution of Rust, so let's just do that for a few versions. Honestly, there's not really much more to say as things get increasingly closer and closer to modern Rust.

* [Rust 0.4](https://github.com/rust-lang/rust/blob/release-0.4/src/libstd/json.rs)
	* No major differences up to 0.6.
* [Rust 0.7](https://github.com/rust-lang/rust/blob/release-0.7/src/libextra/json.rs)
	* Pushed to `libextra`. Also, suddenly `&mut self` as far as the eye can see.
* [Rust 0.10](https://github.com/rust-lang/rust/blob/0.10/src/libserialize/json.rs)
	* Pushed to `libserialize`.
* [Rust 0.11](https://github.com/rust-lang/rust/blob/0.11.0/src/libserialize/json.rs)
	* Sigils replaced.
	* By this point, there's no longer anything left in this file that indicates it ever came from old Rust, or was ever related to it.
	* Most _language-level_ changes from here on out fit one of these patterns:
		* Name bikeshedding
		* Syntax bikeshedding
		* Code organization bikeshedding
		* Deleting stuff
	* There is a pretty major exception, though...

## Part 8: DSTs

At this point, I think I've mined out all the really interesting stuff to talk about, except for one thing: DSTs.

In practice, this means studying what happened to `Vec` between 0.11.0 (as DST implementation began in 0.12.0) and 1.0.0-alpha (when DST implementation officially finished).

* Before: <https://github.com/rust-lang/rust/blob/0.11.0/src/libcollections/vec.rs>
	* `Vector` and `Collection` traits provide slice & length respectively.
* During: <https://github.com/rust-lang/rust/blob/0.12.0/src/libcollections/vec.rs>
	* Migration of slice functions to `Slice` op.
	* Notably, it's already possible to at least _index_ a slice via `Deref`, but I haven't checked the caveats, and `Vec` isn't doing it.
* After: <https://github.com/rust-lang/rust/blob/1.0.0-alpha/src/libcollections/vec.rs>
	* Slicing and related responsibilities are handed to `Slice` via `Deref`.
	* Funny thing that cropped up: Something I can only call "kind of evil", `DerefVec`, which just kind of assumes an immutable slice is actually an immutable `Vec`. Given it's not in stable it presumably was removed; what confuses me is why it was ever added.

The biggest clue as to what the big changes were is the result of trying to compile a pretty simple example of container-based slices:

```
struct MyContainer {
	contents: [i32, ..4]
}

impl Deref<[i32]> for MyContainer {
	fn deref(&self) -> &[i32] {
		&self.contents
	}
}

fn main() {
	let container = MyContainer { contents: [1, 2, 3, 4] };
	println!("hello {}", container[3]);
}
```

On 0.11.0:

```
$ LD_LIBRARY_PATH=lib bin/rustc test.rs
test.rs:5:12: 5:17 error: bare `[]` is not a type
test.rs:5 impl Deref<[i32]> for MyContainer {
                     ^~~~~
error: aborting due to previous error
```

This code works fine on 0.12.0. There are some far-reaching implications here, so let's see what happens if `impl Deref<[i32]> for MyContainer {` is replaced with `impl MyContainer {`.

```
test.rs:7:3: 7:17 error: mismatched types: expected `&[i32]` but found `&[i32, .. 4]` (expected vector but found vector)
```

So, among other things, there's no implicit conversion from a fixed-size array to the corresponding slice. That's fine, it can be made explicit:

```
impl MyContainer {
	fn deref<'a>(&'a self) -> &'a [i32] {
		self.contents.as_slice()
	}
}

fn main() {
	let container = MyContainer { contents: [1, 2, 3, 4] };
	println!("hello {}", container.deref()[3]);
}
```

From this, it's possible to get a pretty good idea of what happened.

In 0.11.0, it was not possible to even _refer_ to `[T]`.

* This lead to a situation where `Deref<[T]>` was impossible. `Deref` wants to put a `&` on anything going through it, which would create the (valid) `&[T]`, but that requires going *through* `[T]`, which can't exist.
	* `DerefMut` makes designing it this way more or less mandatory at this point in history. A possible "fix" would have been for `&` to carry an associated type `T`, but, hey, turns out `std::borrow::Borrow` doesn't exist yet either...
	* Ok, ok, I *say* it can't exist. It's more complicated than that.
		* [`rustc::middle::ty::sty`](https://doc.rust-lang.org/0.11.0/rustc/middle/ty/type.sty.html) would imply it's a `ty_vec(T, None)`.
		* The error is generated at [./librustc/middle/typeck/astconv.rs](https://github.com/rust-lang/rust/blob/0.11.0/src/librustc/middle/typeck/astconv.rs#L709).
		* The type that code generates (so the program can continue typechecking) is _a unique pointer to_ a slice.
		* It seems the simple fact of the matter is that they absolutely _do not_ want a bare `ty_vec` leaking into the rest of the compiler in 0.11.0.
* Conversion from fixed-size arrays to slices could only be done explicitly via the `Vector` trait and related traits.

All this talk about how the type is theoretically representable but isn't allowed gives me a great idea: What if I just remove the error?

```
checking whether GCC is new enough... no
configure: error:
The selected GCC C++ compiler is not new enough to build LLVM. Please upgrade
to GCC 4.7. You may pass --disable-compiler-version-checks to configure to
bypass these sanity checks.
configure: error: LLVM configure failed
```

...right. So the time machine's too far back to build this version. (I tried updating the time machine, and then it tried building a 32-bit version of Rust and then ran into LLVM link errors and IDK what happened.)

## Part 9: Conclusions

Rust went through a _long_ development process. And it's interesting to see it essentially becoming Go for a while, then the borrow checker shows up and suddenly it diverges.

It feels like at some point, Rust went through an eleventh-hour "make it shippable" push. Major changes seem to have been going on right up to 1.0.0.

Ultimately, I feel like being able to run these older versions emphasizes some of the rationale of why things are the way they are.

This is particularly accentuated by the compiler compiling itself through each of these changes. Having the `stage0` snapshot for Rust 0.1 allows, in theory, performing a series of builds that rebuild the compiler right through to the latest version.

One could even try and do this from, say, the last commit with the bootstrap compiler: <https://github.com/rust-lang/rust/tree/ef75860a0a72f79f97216f8aaa5b388d98da6480>.

Food for thought, but I don't think I will. This post is long enough as it is, and I think this might be the first time I've really written a long-form post just to _write one._

The experience is... well... I think I'll stick to smaller stuff in the future. It's a bit of a commitment.

<cue-signoff-2/>
