## Prologue

So. This is a website. It has blogposts on it, and special-case pages, and it tries to follow a consistent theme and structure for each page.

And "Don't Repeat Yourself" is a very important principle we should all follow.

Having a bunch of copy/pasted HTML headers/footers is not really good on that front.

I eventually made something to generate the RSS feed and convert some Markdown using `marked`, but this had... issues.

Not to mention, every time I wanted to write a tangent in a blogpost, I had to write:

```
<div class="div-tangent">

Note {

The mynt is a kind of exothermic anteplane entity.

}

</div>
```

...this was not particularly good.

So, obviously, the solution is a static site generator, right? ...right???

## GitLab CI

...GitLab CI is very awkward if you use many software stacks.

You have to setup each container as a separate job and then link them together with artifacts.

This is bad for iteration time, and I _really_ don't want to exceed my GitLab limits.

_GitHub's_ CI (and compatibles like Gitea & Forgejo) is a single base image that you work on with actions. This has its problems, but it means you're only running a single container, and downloading/caching typical SDKs is accounted for by Actions.

GitLab's CI _almost_ has its own solution to this, but the design pretty much forces you to either factor everything into a *lot* of separate containers (it seems there isn't a sensible way to create a Dockerfile to setup everything you need?) or figure out how to configure the cache the right way.

~~What this means in practice is that I have whatever the `oven/bun:1` container gives me, with some wiggle room of what I can get `apt-get` to get for me (though I am kind of just *hoping* that I've cached apt and cargo correctly).~~

What this means in practice is that I have whatever the `rust:latest` container gives me, because it turns out Debian Rust as accessed through `oven/bun:1` is too old to support the 2021 Edition of Rust. Oops.

This gets me:

* `bun` itself, which is basically `node`/`npm` with superpowers, which if _correctly utilized_ should save on NPM traffic
* Godot 3.x through a custom cached-download shell script

To be honest, the potential unreliability of the caching still bothers me.

On my personal homelab CI, I solved this problem by providing a read-only 'tooling' storage (see [the Forgejo documentation](https://forgejo.org/docs/next/user/actions/#jobsjob_idcontainervolumes) on this). This should cache everything except Cargo dependencies, which I may in future solve with a write-through Cargo registry, but for now I solve with CI caching. But all of this only works for private projects where I can provide that storage.

I'm certainly not going to provide a potentially world-accessible GitLab runner.

Why is any of this relevant? Well, because it means the "speed bump" to setup a given site generator in CI isn't non-existent.

Even ignoring that, though, there are problems.

## A Little History

So my original plan I tried months ago did not involve `marked`.

Basically, it amounted to writing my own static site generator.

It is worth noting, this is before I wrote `shcms`. I overengineered things to put post management directly in the generator, and basically just made a tangled mess.

By the time I gave up, I still hadn't added Rust to the CI.

Ultimately, I gave up and wrote `shcms`, which amounted to a way of managing the post conversion with shell scripts.

The use of `marked` was decently obvious, as `node` was already in the CI for PVA.

Everything else could be taped together with `echo` etc.

```
# (omitted: compiling pages-md-compiler...)

. ci/shcms
cms_post thoughts/example "Example Post" "Sat Nov 9 00:00:00 2024 +0000" "Example post."
# (omitted: more posts...)
cms_done
cms_page "index.html" isle-tooling/index "isle-tooling" content
# (omitted: more pages...)
```

The existence of `pages-md-compiler` and use of `content` at the end of `cms_page` here are anachronisms; that's how things work now as I write this post. But otherwise it was the same.

## What About: Jekyll

[Jekyll](https://jekyllrb.com/) has some good ideas. But I take issue with a few things:

* Gem-based themes will get in the way of trying to bend Jekyll into what I need it to be. This is fixable but it's a speed bump.
* Breaking all the links by using Jekyll's blogging system is a big no-no.
* The documentation helpfully talks about using `post_url` to protect post links from permalink style changes. _If I have to worry about permalink style changes, it isn't a permalink, is it?_
* Liquid is Yet Another Templating Language I Have To Learn.
* The documentation seems to be a bit cagey as to how I might implement shorthand for common 'mid-post tropes' not in Markdown/etc. It just says I can add my own Liquid tags using plugins?
* On the face of it, the plugins system sounds good, but this adds to the workload.

## Quick-fire points

Ok, so, having gotten this far, we can start to see the outline of why we might have problems.

You can use these to rule out various static site generators (and being for Pages, it had to be static).

To be clear, I am _very aware_ that I'm skipping over a lot of static site generators here.

<tangent-Example>

The box in which this text is sitting is the ideal sort of thing I'd want to template.

If you look at the source, it actually has all sorts of tricky stuff around it to improve the user experience when using Firefox's Reader Mode.

Hopefully, it also improves the screen reader experience somewhat. If it doesn't, I can always change how it works later.

</tangent-Example>

* I need to be able to template in arbitrary 'moments'.
	* This templating is specific to this site and can be as specific as reused elements on a single page, though where the template is located isn't really a concern to me.
* Whatever the system is, it mustn't make decisions 'in my name.'
	* Arbitrary decisions on exact filepaths and structure are not allowed.
		* This might rule out Hugo, which bases its "taxonomy" system on filepaths. [_It's documented that it isn't possible to override content sections._](https://gohugo.io/content-management/organization/)
	* Ideally, it would not disturb manual content, i.e. PVA testing.
	* It can't rename all assets to have silly names.

Basically, I just want a tool to both convert and template my markdown files, and maybe something could generate an RSS feed if it's feeling nice.

## What About: MDX

...Honestly, if MDX had... _any_ even vaguely acceptable tooling support whatsoever, I might have considered it. At first, anyway (see below).

Unironically, there's a way to integrate MDX with every single bundler as long as you're willing to mess around with its configuration a bit, and yet they don't have a simple CLI compiler. _Even though they have a Rust crate for the sole purpose of taking in MDX and outputting JS, they don't have a CLI compiler._ (For reasons that will be explained, I elected not to write one.)

Of the options, I found [Radish](https://radishjs.com/) was the closest to what I wanted, but it also wants to rename all your assets to `bleh-HASH.mu` or whatever (an attribute I assume it inherited from its use of `esbuild` at its core) and copy some code of questionable copyright status (Radish's metadata claims it's MIT-licensed but as far as I could tell there's no copyright notice?) into your output.

It doesn't seem to (?) have a simple "copy a file from the input to the output" option without messing with things.

<tangent-Note>

I don't _think_ "just copy all files from this directory 1:1" is an option? If it was, that and a way to define "steps" to write arbitrary files (ideally with an actual plan for XML) I think would complete Radish.

But, this isn't the blogpost to consider the future potential of Radish that someone who is better at Node Stuff than me might unlock. As it is, I've got a pretty good usecase lined up for it already in personal notes, since a personal notes site that was _already_ 100% static no-JS doesn't actually need to care about, say, JS making a reference to a file that got renamed.

</tangent-Note>

Now, sure, hypothetically, I could throw together a some-assembly-required Radish clone using [preact-render-to-string](https://github.com/preactjs/preact-render-to-string/) and get more or less what I wanted. In practice, most of the work is making sure MDX is setup, though, which means I have to start playing games with what the "main" program is (since MDX must be ready before the site is loaded).

The final nail in the coffin is that MDX [has a big fat Vercel mark on the bottom of its package docs,](https://mdxjs.com/packages/mdx/#license) and, well, let's just say Radish's author has posted a tweet specifically saying to switch hosting providers away from Vercel. With a _very_ good 'avoid everything this company touches'-tier reason.

<cw-Politics>

<https://mastodon.social/@jakelazaroff/113437307193328078>

...well, no offense to Jake Lazaroff (Radish's primary author), thank you for the warning, and now I'm not going to use anything involving Vercel... or MDX... but this kind of leaves Radish in an unfortunate place, since MDX-as-a-templating-language is kind of Radish's whole appeal.

<!-- ...Should it turn out that GitLab and GitHub have similar _issues,_ though, I don't really have a good plan. (Not to mention the other repositories I'd have to move.) -->

</cw-Politics>

## What actually worked?

Ultimately, I wrote what amounted to a Rust replacement for the header/footer `echo`s and a replacement for `marked`.

By writing that and _only that,_ I cleaned things up without disturbing the actually-fine-and-working infrastructure.

[MeltedMD](https://gitlab.com/20kdc/scrapheap/-/blob/main/klc/melted.md) is part of another project I still haven't managed to finish refactoring for release yet (though, the `klc` directory bears the code I have managed to clean up into an acceptable structure).

It's hyper-super-ultra-subject-to-change, but it works for my usecase (when I don't overengineer things).

MeltedMD exists to "melt" a Markdown AST (as provided by the [markdown crate](https://docs.rs/markdown/1.0.0-alpha.21/markdown/index.html)) into a form that's a lot easier to process, particularly for various kinds of output (i.e. not just HTML).

Rather than trying to integrate Markdown into a JSX environment, MeltedMD focuses more on the idea that the user is already making a homebrew document processor with all sorts of exceptional cases for specific documents, and tries to make sure the data is available to do it.

This is for various reasons, but the quick TLDR is:

* One alternative is requiring that the user learn Yet Another Templating Language. This tends to spiral out of control quickly.
	* Instead, by requiring the processor be implemented in the calling program, the user can use whatever language the processor is either written in or exposes for use.
		* In practice, for my purposes, this means Rust or TSX.
* Output formats such as XSL-FO don't have a 1:1 relationship between the document and a single element you can just dump paragraphs into.
	* For example, you may want to switch page sequences mid-document. In XSL-FO this involves ending your current flow and page sequence, then starting a new flow and a new page sequence.
	* If you try to build a format around being converted to all other formats without any per-format 'prodding', the user will inevitably have to do the prodding themselves. (I tried using LibreOffice/ODT for converting to both EPub and PDF. This is why you don't do that.)
	* You also can't assume different output formats should be fed the exact same instructions, or instructions 'trivially translated'. The conversion _must_ be customizable, particularly for multiple output formats.

How this plays out in practice is that the signature at the end of this blogpost is written as `<cue-signoff-2/>`.
This is a _cue;_ cues are a way to signal points in the document to the processor, for example the point where the page layout might change, or where a particularly fancy object might need to be inserted.
In theory, the processor could be changed to do whatever it needs to as the result of a cue. A templating language isn't so flexible.

Parallel to cues are _sigils_; they're in-text cues, i.e. `hello <somesigil/> world`. The lack of the `cue-` prefix would resolve ambiguous cases, but I haven't implemented this yet.
A good example of the flexibility of this system is that in the processor used for this blogpost, there's a sigil to measure the length of the `libpva.min.js` file: `<how-big-is-pva/>`.
The result: <how-big-is-pva/>.

Orthogonal to cues and sigils are tags and blocks; tags used for the usual things like `<u>underlining</u>` (empty tags are not a thing), and blocks used whenever something needs to group other commands. (What 'group' means exactly is completely up to the processor, intentionally.)

The idea is to simply _let there be_ special-case-flags. You can't fit absolutely everything into a perfectly clean model, and in the case of document formatting, _trying to_ just makes it harder to get what you originally wanted to do done. So instead, MeltedMD does what it can, and provides a broad structure full of intentional escape hatches for everything else.

And MeltedMD conversion is deliberately _not_ integrated into the library, because there's always some exceptional case somewhere where a pure "tree conversion" just isn't enough.

And, yeah. I think MeltedMD is hacky. But, it's good enough for what I need it to be.

Do I think you should use it? Absolutely not. But I think describing its ideology here might help in understanding the thought process behind the design.

...Which is probably going to become relevant as I move into trying to figure out how to release the program it was originally built for.

<cue-signoff-2/>
