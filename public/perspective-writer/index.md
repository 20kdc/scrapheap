```raw-html
<script>
	window.importJSON = () => {
		let data = null;
		try {
			data = JSON.parse(document.getElementById("json_area").value);
		} catch (ex) {
			alert("invalid JSON");
			return;
		}
		let dbr = indexedDB.open("/userfs");
		dbr.onsuccess = () => {
			let db = dbr.result;
			let tx = dbr.result.transaction("FILE_DATA", "readwrite");
			let os = tx.objectStore("FILE_DATA");
			os.put({
				"timestamp": new Date(),
				"mode": 33206,
				"contents": new Int8Array(new TextEncoder().encode(JSON.stringify(data)))
			}, "/userfs/bonfire2.jsonl");
			alert("success");
		};
		dbr.onerror = () => {
			alert("could not open /userfs database");
		};
	};
</script>
```

Perspective Writer exists for writing text presented with multiple speakers.

This can have uses in story-writing, personal journalling, meeting notes taking, etc.

_It's not meant to store data for a long period of time, and relying on its storage will risk it being lost. This applies doubly if you run multiple instances of Perspective Writer at one time. **Please don't do that.**_

Alternatively, you can compile the project yourself and install it wherever you want for offline use.
This process only requires [Godot Engine 3.x](https://godotengine.org/download/3.x/) and a copy of the [source code](https://gitlab.com/20kdc/scrapheap/-/tree/main/perspective-writer) (on that page, press 'Code', followed by one of the 'Download this directory' options).
The resulting project directory can be imported into Godot Engine.

By pressing the below button you acknowledge that Perspective Writer will store data in your browser.<br/>
This data is never uploaded. No data derived from that data is ever uploaded.<br/>
Outside of any possible access logging performed by GitLab Pages (which as far as I'm presently aware I cannot see, and which would not contain this data), Perspective Writer does not perform any analytics whatsoever.<br/>
Perspective Writer is not (and will never be) designed as a 'software as a service' product -- it's just an internal tool that it seems people find useful, so it's not internal anymore.<br/>
The browser was chosen for ease of access; you can get a completely offline version at any time by getting Godot 3.x and importing the project as described above.

[Run In-Browser](app.html)

Data can be exported as JSON at any time from within the application. You can import data using the panel below:

```raw-html
<p><textarea id="json_area"></textarea></p>
```

[Import JSON](javascript:importJSON()) (WARNING: You will lose your existing Perspective Writer data. You may need to have run Perspective Writer at least once to use this option.)
