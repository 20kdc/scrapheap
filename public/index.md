This was originally setup because libpva has built artifacts.

But it's also kind of grown as a general place for "immediately runnable 20kdc web stuff". Also a blog.

* [portable vector animations](pva/)
* [isle-tooling: LibreSprite to APNG plus a few other oddities](isle-tooling/)
* ['blog' for longform posts](blog.html)
* [perspective-writer: writing tool for text with multiple speakers](perspective-writer/)
