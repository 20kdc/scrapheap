/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Functions that do big IO stuff
 */
public class BigIOFunctions {
    public static void writeAPNG(Anim anim, File file, Compressor compressor) throws Exception {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            // begin actual write
            int frameCount = anim.frames.size();
            APNGWriter writer = new APNGWriter(dos, anim.width, anim.height, frameCount);
            Anim.Frame lastFrame = null;
            // Last frame may need to predict the first half of current frame's processing.
            // These predictions have to be pretty decently accurate, so they use the diff.
            // May as well reuse it.
            APNG.Frame cachedDiff = null;
            for (int fi = 0; fi < frameCount; fi++) {
                // frames
                Anim.Frame frame = anim.frames.get(fi);
                Anim.Frame nextFrame = fi < frameCount - 1 ? anim.frames.get(fi + 1) : null;
                // diff
                APNG.Frame diff;
                if (cachedDiff != null) {
                    diff = cachedDiff;
                    cachedDiff = null;
                } else {
                    diff = lastFrame != null ? genDiff(anim, lastFrame, frame) : genDiff(anim, frame);
                }
                // work out dispose op
                int bestDisposeOp = APNG.DISPOSE_NONE;
                Anim.Frame bestDisposeOpState = frame;
                // try dispose ops 
                if (nextFrame != null) {
                    // Since we'll need this anyway, cache it.
                    cachedDiff = genDiff(anim, frame, nextFrame);
                    int bestDisposeOpNFC = cachedDiff.cost();
                    Anim.Frame bkgFrame = simBackgroundOp(anim, frame, diff);
                    // gen diff...
                    APNG.Frame bkgDiff = genDiff(anim, bkgFrame, nextFrame);
                    int bkgCost = bkgDiff.cost();
                    if (bkgCost < bestDisposeOpNFC) {
                        cachedDiff = bkgDiff;
                        bestDisposeOp = APNG.DISPOSE_BACKGROUND;
                        bestDisposeOpState = bkgFrame;
                        bestDisposeOpNFC = bkgCost;
                    }
                    if (lastFrame != null) {
                        APNG.Frame prvDiff = genDiff(anim, lastFrame, nextFrame);
                        int prvCost = prvDiff.cost();
                        if (prvCost < bestDisposeOpNFC) {
                            cachedDiff = prvDiff;
                            bestDisposeOp = APNG.DISPOSE_PREVIOUS;
                            bestDisposeOpState = lastFrame;
                            bestDisposeOpNFC = prvCost;
                        }
                    }
                }
                // done, commit
                diff.disposeOp = bestDisposeOp;
                System.out.println(frame.name + " (" + diff.x + "," + diff.y + "," + diff.width + "," + diff.height + "," + diff.disposeOp + "," + diff.blendOp + ")");
                writer.writeFrame(diff, diff.asCompressedPixels(compressor));
                lastFrame = bestDisposeOpState;
            }
            writer.end();
        }
    }
    private static Anim.Frame simBackgroundOp(Anim anim, Anim.Frame frame, APNG.Frame diff) {
        // Make backgrounded frame
        Anim.Frame bkgFrame = Anim.Frame.newLinear("checkBackgroundOp", anim.width, anim.height, frame);
        // copy from src
        for (int i = 0; i < anim.height; i++)
            System.arraycopy(frame.pixels, frame.rows[i], bkgFrame.pixels, bkgFrame.rows[i], anim.width);
        // clear area covered by diff
        for (int i = 0; i < diff.height; i++) {
            int targBase = bkgFrame.rows[diff.y + i] + diff.x;
            Arrays.fill(bkgFrame.pixels, targBase, targBase + diff.width, 0);
        }
        return bkgFrame;
    }
    private static APNG.Frame genDiff(Anim anim, Anim.Frame a) {
        int[] data = new int[anim.width * anim.height];
        for (int i = 0; i < anim.height; i++)
            System.arraycopy(a.pixels, a.rows[i], data, i * anim.width, anim.width);
        return new APNG.Frame(data, 0, 0, anim.width, anim.height, a.delayNum, a.delayDiv, 0, 0);
    }
    private static APNG.Frame genDiff(Anim anim, Anim.Frame a, Anim.Frame b) {
        // test column/row differences
        boolean[] colsDiffer = new boolean[anim.width];
        boolean[] rowsDiffer = new boolean[anim.height];
        for (int testY = 0; testY < anim.height; testY++) {
            int aOfs = a.rows[testY];
            int bOfs = b.rows[testY];
            for (int testX = 0; testX < anim.width; testX++) {
                if (a.pixels[aOfs++] != b.pixels[bOfs++]) {
                    colsDiffer[testX] = true;
                    rowsDiffer[testY] = true;
                }
            }
        }
        // constrain rectangle
        int x = 0;
        int y = 0;
        int w = anim.width;
        int h = anim.height;
        // L
        while (w > 1 && !colsDiffer[x]) {
            x++;
            w--;
        }
        // U
        while (h > 1 && !rowsDiffer[y]) {
            y++;
            h--;
        }
        // R
        while (w > 1 && !colsDiffer[x + w - 1])
            w--;
        // D
        while (h > 1 && !rowsDiffer[y + h - 1])
            h--;
        // rectangle finalized, now we can finally begin actually generating the diff
        int[] aBuffer = new int[w * h];
        int[] finalDiff = new int[w * h];
        // copy all pixels to diff-relative coordinates
        for (int i = 0; i < h; i++) {
            System.arraycopy(a.pixels, a.rows[y + i] + x, aBuffer, w * i, w);
            System.arraycopy(b.pixels, b.rows[y + i] + x, finalDiff, w * i, w);
        }
        // if a pixel differs AND the new value has non-full alpha, we can't just blend in
        // otherwise, we *can*
        boolean replace = false;
        for (int i = 0; i < finalDiff.length; i++) {
            if (aBuffer[i] != finalDiff[i]) {
                if ((finalDiff[i] & 0xFF000000) != 0xFF000000) {
                    replace = true;
                    break;
                }
            }
        }
        // if we're not replacing then do per-pixel blend-based opt.
        if (!replace)
            for (int i = 0; i < finalDiff.length; i++)
                if (aBuffer[i] == finalDiff[i])
                    finalDiff[i] = 0;
        // finally export this
        return new APNG.Frame(finalDiff, x, y, w, h, b.delayNum, b.delayDiv, 0, replace ? 0 : 1);
    }

    /* -- LSSpritesheet -- */

    public static Anim readLSSpritesheet(File json, File png) throws Exception {
        JSONTokener jt = new JSONTokener(new InputStreamReader(new FileInputStream(json), StandardCharsets.UTF_8));
        JSONObject res = (JSONObject) jt.nextValue();
        JSONObject frames = res.getJSONObject("frames");
        LinkedList<String> frameKeys = new LinkedList<String>(frames.keySet());
        // listen to eurobeat while looking at this code {
        int sheetW;
        int sheetH;
        int[] sheet;
        if (png != null) {
            BufferedImage biPre = ImageIO.read(png);
            sheetW = biPre.getWidth();
            sheetH = biPre.getHeight();
            sheet = new int[sheetW * sheetH];
            biPre.getRGB(0, 0, sheetW, sheetH, sheet, 0, sheetW);
            biPre = null;
            System.gc();
        } else {
            // luckily, this is fine, because we don't do anything with content here
            sheetW = 0;
            sheetH = 0;
            sheet = new int[0];
        }
        // }
        // example:
        // move to inventory 0.ase
        // move to inventory 1.ase
        // ...
        // move to inventory 10.ase
        frameKeys.sort((a, b) -> {
            if (a.length() > b.length())
                return 1;
            if (a.length() < b.length())
                return -1;
            return a.compareTo(b);
        });
        Anim anim = null;
        for (String s : frameKeys) {
            JSONObject frame = frames.getJSONObject(s);
            JSONObject rect = frame.getJSONObject("frame");
            int x = rect.getInt("x");
            int y = rect.getInt("y");
            int w = rect.getInt("w");
            int h = rect.getInt("h");
            int duration = frame.getInt("duration");
            // and now actually instance
            if (anim == null)
                anim = new Anim(w, h);
            // blit into subFrame
            int[] rows = genRows(h, x + (y * sheetW), sheetW);
            // and make frame
            anim.frames.add(new Anim.Frame(s, sheet, rows, new RatioTimed(duration, 1000)));
        }
        return anim;
    }

    public static int[] genRows(int height, int base, int stride) {
        int[] rows = new int[height];
        for (int i = 0; i < height; i++)
            rows[i] = base + (i * stride);
        return rows;
    }

    public static SplitAFLResult splitAFL(Anim anim, int fps, File baseFile, boolean unix) throws Exception {
        // This is doubly important now that wildcards are in use.
        if (baseFile.isDirectory())
            throw new RuntimeException(baseFile + " already exists");
        baseFile.mkdirs();
        int idx = 0;
        double gifskiTime = 0;
        double gifskiAdvance = 1000.0d / fps;
        double inputTime = 0;
        SplitAFLResult frameFiles = new SplitAFLResult(baseFile);
        BufferedImage tmpBI = new BufferedImage(anim.width, anim.height, BufferedImage.TYPE_INT_ARGB);
        WritableRaster tmpWR = tmpBI.getRaster();
        int[] rowBuffer = new int[anim.width];
        for (Anim.Frame frame : anim.frames) {
            int instances = 0;
            // the div should be 1000 for from-GIF results so this will divide the divisor to 1
            // if not then we'll just have to hope.
            // there's no particularly flawless algorithm to use here
            double divDivved = frame.delayDiv / 1000d;
            inputTime += frame.delayNum / divDivved;
            while (gifskiTime < inputTime) {
                gifskiTime += gifskiAdvance;
                instances++;
            }
            if (instances > 0) {
                File[] targets = new File[instances];
                for (int i = 0; i < targets.length; i++) {
                    targets[i] = new File(baseFile, "f" + (idx++) + ".png");
                }
                // have to copy each individual row because of the memory problem
                for (int i = 0; i < anim.height; i++) {
                    System.arraycopy(frame.pixels, frame.rows[i], rowBuffer, 0, anim.width);
                    tmpWR.setDataElements(0, i, anim.width, 1, rowBuffer);
                }
                ImageIO.write(tmpBI, "PNG", targets[0]);
                System.out.println(frame.name + " -> " + targets[0]);
                if (unix) {
                    // reuse this one file for all targets
                    for (int i = 0; i < instances; i++)
                        frameFiles.files.add(targets[0]);
                } else {
                    // this sucks so much. blame Windows command line limits.
                    // I'm serious.
                    frameFiles.files.add(targets[0]);
                    for (int i = 1; i < targets.length; i++) {
                        Files.copy(targets[0].toPath(), targets[i].toPath(), StandardCopyOption.REPLACE_EXISTING);
                        frameFiles.files.add(targets[i]);
                    }
                }
            }
        }
        return frameFiles;
    }
    public static class SplitAFLResult {
        public final File baseDir;
        // Individual frames in-order
        public final LinkedList<File> files = new LinkedList<>();
        public final String wildcardGifski;
        public SplitAFLResult(File dir) {
            baseDir = dir;
            wildcardGifski = new File(dir, "f").toString() + "*.png";
        }
        public void delete() {
            for (File f : files)
                f.delete();
            baseDir.delete();
        }
    }
}
