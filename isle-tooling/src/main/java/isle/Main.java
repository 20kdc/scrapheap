/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import isle.BigIOFunctions.SplitAFLResult;

public class Main {
    public static void main(String[] args) throws Exception {
        Compressor compressor = Compressor.detect();
        if (args.length < 1) {
            showHelp();
        } else if (args[0].equals("splitafl") || args[0].equals("splitaflu")) {
            if (args.length != 6) {
                System.out.println("splitafl/splitaflu JSON PNG FPS OUTDIR OUTLIST");
                System.exit(1);
            }
            boolean unix = args[0].equals("splitaflu");
            System.out.println("Loading spritesheet...");
            Anim frames = BigIOFunctions.readLSSpritesheet(new File(args[1]), new File(args[2]));
            completeSplit(frames, args[3], args[4], args[5], unix);
        } else if (args[0].equals("splitapng") || args[0].equals("splitapngu")) {
            if (args.length != 5) {
                System.out.println("splitapng/splitapngu PNG FPS OUTDIR OUTLIST");
                System.exit(1);
            }
            boolean unix = args[0].equals("splitapngu");
            System.out.println("Loading APNG...");
            Anim frames = APNGReader.readAPNG(new File(args[1]));
            completeSplit(frames, args[2], args[3], args[4], unix);
        } else if (args[0].equals("aflgs") || args[0].equals("aflgsu")) {
            if (args.length < 4) {
                System.out.println("aflgs/aflgsu JSON PNG FPS GIFSKIARGS...");
                System.exit(1);
            }
            boolean unix = args[0].equals("aflgsu");
            System.out.println("Loading spritesheet...");
            Anim frames = BigIOFunctions.readLSSpritesheet(new File(args[1]), new File(args[2]));
            completeGS(frames, args, 3, unix);
        } else if (args[0].equals("apnggs") || args[0].equals("apnggsu")) {
            if (args.length < 3) {
                System.out.println("apnggs/apnggsu APNG FPS GIFSKIARGS...");
                System.exit(1);
            }
            boolean unix = args[0].equals("apnggsu");
            System.out.println("Loading APNG...");
            Anim frames = APNGReader.readAPNG(new File(args[1]));
            completeGS(frames, args, 2, unix);
        } else if (args[0].equals("apng")) {
            if (args.length != 4) {
                System.out.println("apng JSON PNG OUTAPNG");
                System.exit(1);
            }
            System.out.println("Loading spritesheet...");
            Anim frames = BigIOFunctions.readLSSpritesheet(new File(args[1]), new File(args[2]));
            System.out.println("Writing...");
            BigIOFunctions.writeAPNG(frames, new File(args[3]), compressor);
        } else if (args[0].equals("lst2apng")) {
            if (args.length != 5) {
                System.out.println("lst2apng LST BASE FPS OUTAPNG");
                System.exit(1);
            }
            int fps = Integer.parseInt(args[3]);
            File base = new File(args[2]);
            System.out.println("Loading images...");
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(args[1]), StandardCharsets.UTF_8));
            Anim frames = null;
            while (true) {
                String l = br.readLine();
                if (l == null)
                    break;
                if (l.equals(""))
                    continue;
                File ff = new File(base, l);
                System.out.println(ff);
                BufferedImage bi = ImageIO.read(ff);
                if (frames == null)
                    frames = new Anim(bi.getWidth(), bi.getHeight());
                int[] rows = BigIOFunctions.genRows(frames.height, 0, frames.width);
                int[] image = new int[frames.width * frames.height];
                bi.getRGB(0, 0, frames.width, frames.height, image, 0, frames.width);
                frames.frames.add(new Anim.Frame(l, image, rows, new RatioTimed(1000, fps)));
            }
            br.close();
            System.out.println("Writing...");
            BigIOFunctions.writeAPNG(frames, new File(args[4]), compressor);
        } else if (args[0].equals("apngopt")) {
            if (args.length != 3) {
                System.out.println("apngopt INAPNG OUTAPNG");
                System.exit(1);
            }
            System.out.println("Loading APNG...");
            Anim frames = APNGReader.readAPNG(new File(args[1]));
            System.out.println("Writing...");
            BigIOFunctions.writeAPNG(frames, new File(args[2]), compressor);
        } else if (args[0].equals("ffmpeg")) {
            if (args.length < 3) {
                System.out.println("ffmpeg JSON PNG FFMPEGARGS...");
                System.exit(1);
            }
            File tmpF = new File("__ISLE_TMP_WillBeDeleted__.apng");
            if (tmpF.exists()) {
                System.out.println("__ISLE_TMP_WillBeDeleted__.apng already exists.");
                System.exit(1);
            }
            System.out.println("Loading spritesheet...");
            Anim frames = BigIOFunctions.readLSSpritesheet(new File(args[1]), new File(args[2]));
            System.out.println("Writing...");
            BigIOFunctions.writeAPNG(frames, tmpF, compressor);
            System.out.println("Running ffmpeg...");
            LinkedList<String> cmd = new LinkedList<>();
            cmd.add("ffmpeg");
            cmd.add("-i");
            cmd.add(tmpF.toString());
            for (int i = 3; i < args.length; i++)
                cmd.add(args[i]);
            ProcessBuilder pb = new ProcessBuilder();
            pb.command(cmd);
            pb.start().waitFor();
            System.out.println("Done");
            tmpF.delete();
        } else if (args[0].equals("guessfps")) {
            if (args.length != 2) {
                System.out.println("guessfps JSON");
                System.exit(1);
            }
            Anim frames = BigIOFunctions.readLSSpritesheet(new File(args[1]), null);
            int fps = frames.guessFPS(65535);
            System.out.println(fps);
        } else {
            showHelp();
            System.exit(1);
        }
    }
    private static void completeSplit(Anim frames, String argA, String argB, String argC, boolean unix) throws Exception {
        SplitAFLResult frameFiles = BigIOFunctions.splitAFL(frames, Integer.parseInt(argA), new File(argB), unix);
        PrintStream fos = new PrintStream(new File(argC), "UTF-8");
        for (File f : frameFiles.files)
            fos.println(f.toString());
        fos.close();
    }
    private static void completeGS(Anim frames, String[] args, int argPtr, boolean unix) throws Exception {
        int fps = Integer.parseInt(args[argPtr++]);
        File f = new File("__ISLE_TMP_WillBeDeleted__");
        if (fps == -1) {
            fps = frames.guessFPS(1000);
            System.out.println("Detected FPS: " + fps);
        }
        if (fps == -1) {
            System.out.println("Failed to find a valid FPS");
            System.exit(1);
        }
        SplitAFLResult frameFiles = BigIOFunctions.splitAFL(frames, fps, f, unix);
        // prepare gifski command
        LinkedList<String> cmd = new LinkedList<>();
        cmd.add("gifski");
        while (argPtr < args.length)
            cmd.add(args[argPtr++]);
        cmd.add("-r");
        cmd.add(Integer.toString(fps));
        // gifski platform-dependent issues
        if (unix) {
            cmd.add("--no-sort");
            for (File ff : frameFiles.files)
                cmd.add(ff.toString());
        } else {
            cmd.add(frameFiles.wildcardGifski);
        }
        // announce
        System.out.println("Running gifski:");
        for (String s : cmd)
            System.out.print(" " + s);
        System.out.println();
        // run gifski
        ProcessBuilder pb = new ProcessBuilder();
        pb.command(cmd);
        pb.start().waitFor();
        System.out.println("Done");
        // clean
        frameFiles.delete();
    }
    public static void showHelp() {
        System.out.println("java -jar isle-tooling.jar CMD ARGS...");
        System.out.println("Commands:");
        System.out.println("splitafl[u] JSON PNG FPS OUTDIR OUTLIST");
        System.out.println(" splits JSON/PNG into frames");
        System.out.println(" adding 'u' enables unix mode (blame gifski)");
        System.out.println("splitapng[u] APNG FPS OUTDIR OUTLIST");
        System.out.println(" splits APNG into frames");
        System.out.println(" adding 'u' enables unix mode (blame gifski)");
        System.out.println("aflgs[u] JSON PNG FPS GIFSKIARGS...");
        System.out.println(" splitafl to a temporary directory, then automatically runs gifski ; you're expected to pass -o, --fixed etc.");
        System.out.println(" if FPS is -1, the FPS is guessed (THIS CAN FAIL)");
        System.out.println(" adding 'u' enables unix mode (blame gifski)");
        System.out.println("apnggs[u] APNG FPS GIFSKIARGS...");
        System.out.println(" see aflgs");
        System.out.println("apng JSON PNG OUTAPNG");
        System.out.println(" APNGifies the spritesheet");
        System.out.println("lst2apng LST FPS OUTAPNG");
        System.out.println(" APNGifies from split frame list");
        System.out.println("apngopt INAPNG OUTAPNG");
        System.out.println(" Optimizes an APNG");
        System.out.println(" Pass environment variable ISLE_TOOLING_ZOPFLI to use whatever you pass as zopfli (otherwise, Java's DEFLATE is used)");
        System.out.println(" Space-separated additional options go in ISLE_TOOLING_ZOPFLIOPTS.");
        System.out.println(" Notably, these settings work on all APNG-outputting modes.");
        System.out.println("ffmpeg JSON PNG FFMPEGARGS...");
        System.out.println(" APNGifies the spritesheet and then passes to ffmpeg");
        System.out.println(" input args are setup for you, output args aren't");
        System.out.println(" example: ffmpeg bleh.json bleh.png out.mp4");
        System.out.println(" this is good for using ffmpeg to optimize an APNG");
        System.out.println("guessfps JSON");
        System.out.println(" run FPS guesser and output to stdout, outputs -1 on failure");
    }
}
