/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

/**
 * Base class for stuff that's timed by ratios.
 */
public class RatioTimed {
    public final int delayNum, delayDiv;
    public RatioTimed(int dn, int dd) {
        delayNum = dn;
        delayDiv = dd;
    }
    public RatioTimed(RatioTimed src) {
        delayNum = src.delayNum;
        delayDiv = src.delayDiv;
    }
}
