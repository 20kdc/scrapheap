/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */
package isle;

import java.io.DataOutputStream;

import isle.APNG.Frame;

/**
 * Core APNG writing logic
 */
public class APNGWriter {
    public final DataOutputStream dos;
    public int fctlSeq;
    public APNGWriter(DataOutputStream dos, int width, int height, int frameCount) throws Exception {
        this.dos = dos;
        dos.write(APNG.MAGIC);
        try (APNGChunkWriter chk = new APNGChunkWriter(dos, "IHDR")) {
            chk.contents.writeInt(width);
            chk.contents.writeInt(height);
            chk.contents.write(8);
            chk.contents.write(6);
            chk.contents.write(0);
            chk.contents.write(0);
            chk.contents.write(0);
        }
        try (APNGChunkWriter chk = new APNGChunkWriter(dos, "acTL")) {
            chk.contents.writeInt(frameCount);
            chk.contents.writeInt(0);
        }
    }

    public void writeFrame(Frame diff, byte[] compressedPixels) throws Exception {
        try (APNGChunkWriter chk = new APNGChunkWriter(dos, "fcTL")) {
            chk.contents.writeInt(fctlSeq++);
            chk.contents.writeInt(diff.width);
            chk.contents.writeInt(diff.height);
            chk.contents.writeInt(diff.x);
            chk.contents.writeInt(diff.y);
            chk.contents.writeShort(diff.delayNum);
            chk.contents.writeShort(diff.delayDiv);
            chk.contents.write(diff.disposeOp);
            chk.contents.write(diff.blendOp);
        }
        try (APNGChunkWriter chk = new APNGChunkWriter(dos, (fctlSeq == 1) ? "IDAT" : "fdAT")) {
            if (fctlSeq != 1)
                chk.contents.writeInt(fctlSeq++);
            chk.contents.write(compressedPixels);
        }
    }

    public void end() throws Exception {
        try (APNGChunkWriter chk = new APNGChunkWriter(dos, "IEND")) {
            // :3
        }
    }
}
