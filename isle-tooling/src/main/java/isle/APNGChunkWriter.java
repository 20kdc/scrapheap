/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.nio.charset.StandardCharsets;

/**
 * Tool for writing APNG chunks.
 */
public class APNGChunkWriter implements AutoCloseable {
    private final DataOutputStream parent;
    private final ByteArrayOutputStream buffer;
    public final DataOutputStream contents;

    private static final int[] crc32tab = new int[256];
    static {
        for (int i = 0; i < crc32tab.length; i++) {
            int crc = i;
            for (int j = 0; j < 8; j++) {
                if ((crc & 1) != 0) {
                    crc = (crc >>> 1) ^ 0xEDB88320;
                } else {
                    crc = crc >>> 1;
                }
            }
            crc32tab[i] = crc;
        }
    }

    public APNGChunkWriter(DataOutputStream p, String ct) {
        parent = p;
        byte[] chunkType = ct.getBytes(StandardCharsets.UTF_8);
        if (chunkType.length != 4)
            throw new RuntimeException("bad chunk type");
        contents = new DataOutputStream(buffer = new ByteArrayOutputStream());
        // So it gets included in CRC. The 4 bytes are removed from the length manually later.
        buffer.write(chunkType[0]);
        buffer.write(chunkType[1]);
        buffer.write(chunkType[2]);
        buffer.write(chunkType[3]);
    }

    public static void writeChunk(DataOutputStream dos, String string, byte[] plte) throws Exception {
        try (APNGChunkWriter acw = new APNGChunkWriter(dos, string)) {
            acw.contents.write(plte);
        }
    }

    @Override
    public void close() throws Exception {
        contents.flush();
        byte[] bufDat = buffer.toByteArray();
        parent.writeInt(bufDat.length - 4);
        parent.write(bufDat);
        parent.writeInt(crc32(bufDat));
    }
    public int crc32(byte[] data) {
        int v = -1;
        for (byte b : data) {
            int lb = (b & 0xFF) ^ (v & 0xFF);
            v = crc32tab[lb] ^ (v >>> 8);
        }
        return v ^ -1;
    }
}