/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.imageio.ImageIO;

/**
 * APNG reading code.
 * The basic principle of this thing comes from my 20kdc_godot_addons aimgio stuff.
 */
public class APNGReader {
    public static Anim readAPNG(File file) throws Exception {
        DataInputStream fileDIS = new DataInputStream(new FileInputStream(file));
        fileDIS.readLong();
        FileKnowledge kn = new FileKnowledge();
        Anim anim = null;
        BufferedImage fb1 = null;
        BufferedImage fb2 = null;
        int[] fb3 = null;
        BFrame currentBFrame = null;
        while (fileDIS.available() > 0) {
            ChunkInput chunk = new ChunkInput(fileDIS);
            if (chunk.type.equals("IHDR")) {
                // save the IHDR for later transformation
                // but *also* now we know the width/height!
                kn.ihdr = chunk.data;
                ByteBuffer bb = chunk.openByteBuffer();
                anim = new Anim(bb.getInt(0), bb.getInt(4));
                fb1 = new BufferedImage(anim.width, anim.height, BufferedImage.TYPE_INT_ARGB);
                fb2 = new BufferedImage(anim.width, anim.height, BufferedImage.TYPE_INT_ARGB);
                fb3 = new int[anim.width * anim.height];
            } else if (chunk.type.equals("PLTE")) {
                kn.plte = chunk.data;
            } else if (chunk.type.equals("TRNS")) {
                kn.trns = chunk.data;
            } else if (chunk.type.equals("fcTL")) {
                if (currentBFrame != null) {
                    APNG.Frame tmp = currentBFrame.reconstruct(kn);
                    currentBFrame = null;
                    addFrameToAnim(anim, fb1, fb2, fb3, tmp);
                }
                DataInputStream dis = chunk.openStream();
                dis.readInt();
                int fW = dis.readInt();
                int fH = dis.readInt();
                int fX = dis.readInt();
                int fY = dis.readInt();
                int fDN = dis.readUnsignedShort();
                int fDD = dis.readUnsignedShort();
                int fDO = dis.readUnsignedByte();
                int fBO = dis.readUnsignedByte();
                currentBFrame = new BFrame(fX, fY, fW, fH, fDN, fDD, fDO, fBO);
                System.out.println(currentBFrame);
            } else if (chunk.type.equals("IDAT")) {
                if (currentBFrame != null)
                    currentBFrame.dataColl.write(chunk.data);
            } else if (chunk.type.equals("fdAT")) {
                if (currentBFrame != null)
                    currentBFrame.dataColl.write(chunk.data, 4, chunk.data.length - 4);
            }
        }
        if (currentBFrame != null) {
            APNG.Frame tmp = currentBFrame.reconstruct(kn);
            currentBFrame = null;
            addFrameToAnim(anim, fb1, fb2, fb3, tmp);
        }
        return anim;
    }

    private static void addFrameToAnim(Anim anim, BufferedImage fb1, BufferedImage fb2, int[] fb3, APNG.Frame frame) {
        // switch FBs if dispose op previous requires it
        BufferedImage editFB = fb1;
        if (frame.disposeOp == APNG.DISPOSE_PREVIOUS) {
            // dispose op previous requires we revert, which means we need to use FB2
            // transfer via FB3
            fb1.getRaster().getDataElements(0, 0, anim.width, anim.height, fb3);
            fb2.getRaster().setDataElements(0, 0, anim.width, anim.height, fb3);
            editFB = fb2;
            // something important to note here: PREVIOUS only mentions reverting the frame's region of the output buffer
            // however, that is also the only area that *can* be affected by the frame, so the implied full revert here is fine
            // this is NOT the case for BACKGROUND
        }
        // actually do the body of the work
        applyBlendOp(editFB, frame);
        // pull data out to a frame
        Anim.Frame animFrame = Anim.Frame.newLinear("#" + anim.frames.size(), anim.width, anim.height, frame);
        editFB.getRaster().getDataElements(0, 0, anim.width, anim.height, animFrame.pixels);
        anim.frames.add(animFrame);
        // finish
        if (frame.disposeOp == APNG.DISPOSE_BACKGROUND) {
            // dispose op BACKGROUND now wants us to finish by destroying the area the frame covers of FB1
            Arrays.fill(fb3, 0);
            fb1.getRaster().setDataElements(frame.x, frame.y, frame.width, frame.height, fb3);
        }
    }

    private static void applyBlendOp(BufferedImage fb, APNG.Frame src) {
        if (src.blendOp == APNG.BLEND_SOURCE) {
            // source
            fb.getRaster().setDataElements(src.x, src.y, src.width, src.height, src.pixels);
        } else {
            // over
            fb.getGraphics().drawImage(src.asBufferedImage(), src.x, src.y, null);
        }
    }

    public static class ChunkInput {
        public final String type;
        public final byte[] data;

        public ChunkInput(DataInputStream dis) throws Exception {
            int len = dis.readInt();
            char a = (char) dis.readUnsignedByte();
            char b = (char) dis.readUnsignedByte();
            char c = (char) dis.readUnsignedByte();
            char d = (char) dis.readUnsignedByte();
            type = new String(new char[] {a, b, c, d});
            data = new byte[len];
            dis.readFully(data);
            dis.readInt(); // crc
        }

        public DataInputStream openStream() {
            return new DataInputStream(new ByteArrayInputStream(data));
        }

        public ByteBuffer openByteBuffer() {
            return ByteBuffer.wrap(data);
        }
    }

    /**
     * globals for BFrame reconstruct
     */
    public static class FileKnowledge {
        byte[] ihdr = null;
        byte[] plte = null;
        byte[] trns = null;
    }

    /**
     * Collates data for a frame until a new frame somehow interrupts it.
     */
    public static class BFrame extends RatioTimed {
        public int disposeOp, blendOp, x, y, w, h;
        public ByteArrayOutputStream dataColl = new ByteArrayOutputStream();

        public BFrame(int x, int y, int w, int h, int dn, int dd, int disposeOp, int blendOp) {
            super(dn, dd);
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.disposeOp = disposeOp;
            this.blendOp = blendOp;
        }

        @Override
        public String toString() {
            return "BFrame " + x + "," + y + " " + w + "x" + h + " " + delayNum + "/" + delayDiv + " D" + disposeOp + " B" + blendOp;
        }

        /**
         * Reconstruct into a PNG
         */
        public byte[] intermediary(FileKnowledge kn) throws Exception {
            // make patched IHDR
            byte[] patchedIHDRA = new byte[kn.ihdr.length];
            System.arraycopy(kn.ihdr, 0, patchedIHDRA, 0, patchedIHDRA.length);
            ByteBuffer patchedIHDR = ByteBuffer.wrap(patchedIHDRA);
            patchedIHDR.putInt(0, w);
            patchedIHDR.putInt(4, h);
            // begin
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(APNG.MAGIC);
            DataOutputStream dos = new DataOutputStream(baos);
            APNGChunkWriter.writeChunk(dos, "IHDR", patchedIHDRA);
            if (kn.plte != null)
                APNGChunkWriter.writeChunk(dos, "PLTE", kn.plte);
            if (kn.trns != null)
                APNGChunkWriter.writeChunk(dos, "tRNS", kn.plte);
            APNGChunkWriter.writeChunk(dos, "IDAT", dataColl.toByteArray());
            APNGChunkWriter.writeChunk(dos, "IEND", new byte[0]);
            return baos.toByteArray();
        }

        /**
         * Actually turn this into an APNGFrame
         * note that this is not in itself enough for a full Anim simulator but that's APNGFrame's problem now
         */
        public APNG.Frame reconstruct(FileKnowledge kn) throws Exception {
            BufferedImage bi = ImageIO.read(new ByteArrayInputStream(intermediary(kn)));
            BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            bi2.getGraphics().drawImage(bi, 0, 0, null);
            int[] pixels = new int[w * h];
            bi2.getRaster().getDataElements(0, 0, w, h, pixels);
            return new APNG.Frame(pixels, x, y, w, h, delayNum, delayDiv, disposeOp, blendOp);
        }
    }
}
