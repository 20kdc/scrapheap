/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.util.LinkedList;

/**
 * Isle animation class
 */
public class Anim {
    public final int width, height;
    public final LinkedList<Anim.Frame> frames = new LinkedList<>();

    public Anim(int w, int h) {
        width = w;
        height = h;
    }

    public int guessFPS(int limit) {
        for (int i = 1; i <= limit; i++)
            if (isFPSValid(i))
                return i;
        // :(
        return -1;
    }

    public boolean isFPSValid(int proposedDiv) {
        if (proposedDiv < 1)
            return false;
        for (Anim.Frame f : frames) {
            // "absolute time" divisor to compare bases
            // absoluteTimeDiv = f.delayDiv * proposedDiv
            // raise to absolute time
            int raisedToAbsTime = f.delayNum * proposedDiv;
            // if we can't lower this into proposedDiv without loss, we can't have it 
            if ((raisedToAbsTime % f.delayDiv) != 0)
                return false;
        }
        return true;
    }

    public static class Frame extends RatioTimed {
        public final String name;
        /**
         * The pixel buffer.
         */
        public final int[] pixels;
        /**
         * The base locations of each row.
         */
        public final int[] rows;

        public Frame(String n, int[] px, int[] r, RatioTimed src) {
            super(src);
            name = n;
            pixels = px;
            rows = r;
        }

        public static Frame newLinear(String name, int width, int height, RatioTimed src) {
            int[] frameData = new int[width * height];
            int[] rowPositions = new int[height];
            for (int i = 0; i < rowPositions.length; i++)
                rowPositions[i] = i * width;
            return new Anim.Frame(name, frameData, rowPositions, src);
        }
    }
}