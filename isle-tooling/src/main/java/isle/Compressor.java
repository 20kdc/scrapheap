/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/**
 * Represents a compressor. I think.
 */
public abstract class Compressor {
    public static final Compressor JAVA_DEFLATE = new JavaDeflate();

    /**
     * Detects the system compressor.
     */
    public static Compressor detect() {
        String zopfli = System.getenv("ISLE_TOOLING_ZOPFLI");
        if (zopfli != null) {
            String opts = System.getenv("ISLE_TOOLING_ZOPFLIOPTS");
            String[] xargs = new String[0];
            if (opts != null) {
                xargs = opts.split(" ");
            }
            return new ZopfliDeflate(zopfli, xargs);
        } else {
            return JAVA_DEFLATE;
        }
    }

    /**
     * Compresses some data.
     * Notably, the data is wrapped in a zlib header/footer.
     */
    public abstract byte[] compress(byte[] data);

    public final static class JavaDeflate extends Compressor {
        @Override
        public byte[] compress(byte[] data) {
            try {
                ByteArrayOutputStream res = new ByteArrayOutputStream();
                DeflaterOutputStream dfos = new DeflaterOutputStream(res, new Deflater(Deflater.BEST_COMPRESSION, false));
                dfos.write(data);
                dfos.close();
                return res.toByteArray();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public final static class ZopfliDeflate extends Compressor {
        public final String program;
        public final String[] args;

        public ZopfliDeflate(String p, String[] a) {
            program = p;
            args = a;
        }

        @Override
        public byte[] compress(byte[] data) {
            try {
                File tmpA = File.createTempFile("isle-zopfli-input", null);
                tmpA.deleteOnExit();
                Files.write(tmpA.toPath(), data);
                File tmpB = File.createTempFile("isle-zopfli-output", null);
                tmpB.deleteOnExit();
                ProcessBuilder pb = new ProcessBuilder();
                LinkedList<String> pargs = new LinkedList<>();
                pargs.add(program);
                for (String arg : args)
                    pargs.add(arg);
                pargs.add("--zlib");
                pargs.add("-c");
                pargs.add(tmpA.toString());
                pb.command(pargs);
                pb.inheritIO();
                pb.redirectOutput(tmpB);
                if (pb.start().waitFor() != 0)
                    throw new RuntimeException("wtf?");
                byte[] res = Files.readAllBytes(tmpB.toPath());
                try {
                    tmpA.delete();
                    tmpB.delete();
                } catch (Exception ex) {
                    // nope, don't care
                }
                return res;
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
