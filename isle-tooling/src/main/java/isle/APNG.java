/*
 * isle-tooling - Solve weird problems that shouldn't exist
 * Written starting in 2023 by contributors (see CREDITS.txt)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * A copy of the Unlicense should have been supplied as COPYING.txt in this repository. Alternatively, you can find it at <https://unlicense.org/>.
 */

package isle;

import java.awt.image.BufferedImage;

/**
 * Information for APNG.
 * The colour format is normalized to ARGB8888.
 */
public class APNG {
    public static final byte[] MAGIC = new byte[] {(byte) 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};

    public static final int DISPOSE_NONE = 0;
    public static final int DISPOSE_BACKGROUND = 1;
    public static final int DISPOSE_PREVIOUS = 2;
    public static final int BLEND_SOURCE = 0;
    public static final int BLEND_OVER = 1;

    public static class Frame extends RatioTimed {
        public final int[] pixels;
        public final int x, y, width, height;
        public final int blendOp;
        // Not final because this affects the next frame, so it gets adjusted after the fact
        public int disposeOp;

        public Frame(int[] data, int x, int y, int w, int h, int delayNum, int delayDiv, int disposeOp, int blendOp) {
            super(delayNum, delayDiv);
            this.pixels = data;
            this.x = x;
            this.y = y;
            this.width = w;
            this.height = h;
            this.disposeOp = disposeOp;
            this.blendOp = blendOp;
        }

        /**
         * Contents as a BufferedImage for compositing.
         */
        public BufferedImage asBufferedImage() {
            BufferedImage tmp = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            tmp.getRaster().setDataElements(0, 0, width, height, pixels);
            return tmp;
        }

        /**
         * Compressed pixels!
         */
        public byte[] asCompressedPixels(Compressor compressor) {
            int dPIdx = 0;
            int w = width;
            int h = height;
            int oIdx = 0;
            byte[] frameBuf = new byte[((width * 4) + 1) * height];
            for (int i = 0; i < h; i++) {
                // init to filter type 0
                frameBuf[oIdx++] = 0;
                for (int j = 0; j < w; j++) {
                    int col = pixels[dPIdx++];
                    frameBuf[oIdx] = (byte) (col >> 16);
                    frameBuf[oIdx + 1] = (byte) (col >> 8);
                    frameBuf[oIdx + 2] = (byte) (col);
                    frameBuf[oIdx + 3] = (byte) (col >> 24);
                    oIdx += 4;
                }
            }
            // OLD ALG.
            //  filtered: 1814369
            //  unfiltered: 1762173
            // NEW ALG.
            //  unfiltered: 1599128
            // target: 1595839
            // done!
            return compressor.compress(frameBuf);
        }

        /**
         * Cost estimator
         */
        public int cost() {
            return asCompressedPixels(Compressor.JAVA_DEFLATE).length;
        }
    }
}
