# isle-tooling

Not sure where to put this.

This program does a few different kinds of conversion.

Mainly, it can read one of:

* LibreSprite JSON/PNG
* APNG

And output to:

* APNG
* directory and list of frames (for use with ImageOptim's https://gif.ski/ )
* GIF (via running gifski)

## Why not use ffmpeg?

ffmpeg works best as a converter, not an authoring tool, and there's no good format to feed ffmpeg the data to make an APNG except APNG.

Last frame duration is something ffmpeg seems to have trouble with.

## Why is there an APNG to GIF conversion flow? Surely you'd just directly convert from your source to GIF?

User request.

There's also the possibility of using this as an "APNG optimizer" to some extent, but the more niche features (loop count, default image) aren't handled.

## The program loses my fancy colourspace metadata chunk that replaces RGB with BGR by swapping the colour primaries!

What a shame.

To be more serious, the concept of a portable image with a colourspace that isn't sRGB is a contradiction in terms.

If it needs to be viewed in your special colourspace to be strictly correct, then it's not portable, is it?

And if you want it to be mapped; well, map it, then. To sRGB. Like everybody else.

This was barely acceptable in the case of linear light, because at least that has a usecase and still maps to the sRGB primaries and so forth.

The same can be said for premultiplied alpha. Any user's hardware *can* handle premultiplied alpha, and it has a usecase, so it stands.

## The program has no option for loop counts!

Does anyone actually seriously use loop counts, or were they included in APNG for GIF compatibility?

## The program ignores the default image!

Serious question: Does anyone actually use the default image?

## The APNGs don't work on Discord!

Fun fact: Discord detects uploaded PNGs and "adjusts" them.

