#!/usr/bin/env python3

import linku

print("digraph lukin_kin {")

for k in linku.WORDS.keys():
	v = linku.WORDS[k]
	print(k + ";")
	for k2 in v["see_also"]:
		print(k + " -> " + k2 + ";")

print("}")
