# Tools relating to Toki Pona

This is a place for tools relating to Toki Pona!

## Stuff in this repository (that I did create)

* `b` -- build script
* `linku.py` -- for reading the ilo Linku dataset for use in other scripts
* `lukin_kin.dot.py` -- for making `lukin_kin.dot` (see-also graph)
* `anki_tsv_gen.py` -- Converts ilo Linku to TSV for import into Anki. If you want to learn Toki Pona and you need to get the words memorized, you can use this!

## Stuff useful for learning (that I didn't create)

* jan Sa's version of blinry's cheat sheet (CC0) <https://github.com/janSa-tp/tpcheatsheet>
* <https://linku.la/>
