# ilo pi toki pona

ni li ma tan ilo pi toki pona!

## ijo pi ma ni (mi mama e ni)

* `b` -- ilo pi pali lipu
* `linku.py` -- lukin tawa ilo Linku tan kepeken tan ilo ante
* `lukin_kin.dot.py` -- sitelen e lipu `lukin_kin.dot` (kulupu linja anu linluwi pi lukin kin pi nimi)
* `anki_tsv_gen.py` -- ante e ilo Linku tawa lipu TSV. sina ken pana e ni tawa ilo Anki. sina kama sona e toki pona la sina ken kepeken ni.

## ijo pi jan kama sona pi toki pona (mi mama ala e ni)

* jan Sa en blinry li mama e lipu lili tan kama sona pi toki pona (CC0) <https://github.com/janSa-tp/tpcheatsheet>
* <https://linku.la/>
