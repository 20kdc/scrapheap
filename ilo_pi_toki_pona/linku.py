#!/usr/bin/env python3

import json

jb = json.load(open("linku_data_goes_here/words.json", "r"))

BOOKS = []
USAGES = []
WORDS = {}

for k in jb.keys():
	if k == "$schema":
		continue
	WORDS[k] = jb[k]
	if not jb[k]["book"] in BOOKS:
		BOOKS.append(jb[k]["book"])
	if not jb[k]["usage_category"] in USAGES:
		USAGES.append(jb[k]["usage_category"])

# -- Opinionated part --
# This isn't ilo Linku data, but it's tightly bound to it.
# You can think of it kind of as... not a derivative work,
# but a set of statements about the dataset (as of whenever I last updated this)

# Must contain all words which can't be reasonably mapped from their definitions.
# Must only contain those words.
# Be careful with these.
# It usually requires mucking around in Anki to filter them "right".
# ("Right" here means "Forwards but not backwards".)
AMBIGUOUS = {
	"ju": "reserved word, definition insufficient",
	"lu": "reserved word, definition insufficient",
	"nu": "reserved word, definition insufficient",
	"u": "reserved word, definition insufficient",
}
# Must contain all typo words.
# Must only contain these words.
TYPOS = {
	"suke": True,
	"pakola": True,
	"toma": True,
}
# Levels of usage in order.
for v in [
	"core",
	"common",
	"uncommon",
	"obscure",
	"sandbox"
]:
	# Reorders known usages to be in-order, at the end.
	# If a usage doesn't appear, it's unknown.
	if v in USAGES:
		USAGES.remove(v)
		USAGES.append(v)
# Books in order.
for v in [
	"pu",
	"ku suli",
	"ku lili",
	"none"
]:
	if v in BOOKS:
		BOOKS.remove(v)
		BOOKS.append(v)

