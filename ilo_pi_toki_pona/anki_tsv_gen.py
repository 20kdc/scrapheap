#!/usr/bin/env python3

import argparse
import linku
import sys

def ante_kipisi_pi_TSV(sitelen):
	return sitelen.replace("\t", " ").replace("\n", " ")

def ante_kulupu_nimi(sitelen):
	return sitelen.replace(" ", "_")

sona_kulupu = []
for lipu in linku.BOOKS:
	sona_kulupu.append("tp:book:" + ante_kulupu_nimi(lipu))
for kepeken in linku.USAGES:
	sona_kulupu.append("tp:usage:" + ante_kulupu_nimi(kepeken))
sona_kulupu.append("tp:nimi_pi_sona_pona_ala")
sona_kulupu.append("tp:nimi_nasa")
sona_kulupu.append("tp:sitelen_emosi")
sona_kulupu.append("tp:ucsur")
sona_kulupu.append("tp:sitelen_pona")

parser = argparse.ArgumentParser(prog="anki_tsv_gen.py", description="lipu Linku data to TSV for Anki import.\nFilters 'typo words' by default but otherwise offers filtering options or leaves it up to the user to filter by tags.\nTranslates UCSUR to the equivalent Unicode codepoints, which allows using it for sitelen pona support using nasin nanpa (https://github.com/ETBCOR/nasin-nanpa).\nThis tends to be somewhat more reliable; lipu Linku's 'sitelen pona' field appears to assume sitelen seli kiwen is the canonical definition of what's in sitelen pona, so isn't strictly compatible with i.e. linja pona.")
parser.add_argument("-l", "--language", "--toki", help="Language for definitions and commentary.", default="eng")
parser.add_argument("-r", "--require", "--wile", help="Filter to only these tags. No filter if none are specified. Tags: " + (",".join(sona_kulupu)), action="append", default=[])
parser.add_argument("-d", "--delete", "--weka", help="Remove words with these tags. See --wile.", action="append", default=[])
parser.add_argument("-o", "--out", "--tawa", help="Output filename. If none, standard output is used.")
parser.add_argument("-f", "--filter-ambiguous-definitions", "--weka-e-nimi-pi-sona-pona-ala", help="Removes words with ambiguous definitions that make reversing from the definition impossible. The full list with reasonings can be found in the source code (in linku.py). Tag 'tp:nimi_pi_sona_pona_ala' is always added in case you need to do this from within Anki.", action="store_true")
parser.add_argument("--no-filter-typos", "--weka-ala-e-nimi-nasa", help="Disables filtering 'typo words'. The full list can be found in the source code (in linku.py). Tag 'tp:nimi_nasa' is always added in case you need to do this from within Anki.", action="store_true")
sona_lawa = parser.parse_args()

lipu = sys.stdout
if not sona_lawa.out is None:
	lipu = open(sona_lawa.out, "w", encoding="utf8")

def ante_r_ucsur(r_ucsur):
	if r_ucsur == "":
		return ""
	assert r_ucsur[0:2] == "U+"
	return chr(int(r_ucsur[2:], 16))

def sitelen_TSV(sinpin, monsi, kulupu_mute, sitelen_tan_nimi, r_emosi, r_ucsur, r_sitelen_pona, jan_pi_mama_nimi, lipu_pi_nimi_ni, kepeken_pi_nimi_ni):
	r_ucsur = ante_r_ucsur(r_ucsur)
	sitelen = [
		ante_kipisi_pi_TSV(sinpin),
		ante_kipisi_pi_TSV(monsi),
		ante_kipisi_pi_TSV(sitelen_tan_nimi),
		ante_kipisi_pi_TSV(r_emosi),
		ante_kipisi_pi_TSV(r_ucsur),
		ante_kipisi_pi_TSV(r_sitelen_pona),
		ante_kipisi_pi_TSV(jan_pi_mama_nimi),
		ante_kipisi_pi_TSV(lipu_pi_nimi_ni),
		ante_kipisi_pi_TSV(kepeken_pi_nimi_ni),
		ante_kipisi_pi_TSV(" ".join(kulupu_mute))
	]
	sitelen = "\t".join(sitelen)
	lipu.write(sitelen + "\n")

for nimi in linku.WORDS:
	lipu_nimi = linku.WORDS[nimi]
	# weka
	lipu_pi_nimi_ni = lipu_nimi["book"]
	kepeken_pi_nimi_ni = lipu_nimi["usage_category"]
	if sona_lawa.filter_ambiguous_definitions:
		if nimi in linku.AMBIGUOUS:
			continue
	if not sona_lawa.no_filter_typos:
		if nimi in linku.TYPOS:
			continue
	# toki ante
	# lipu pi lipu Linku la ni li wile ala.
	# lipu Linku la lipu weka pi toki ante li kepeken toki Inli.
	# taso, jan li ken ante e lipu.
	if sona_lawa.language in lipu_nimi["translations"]:
		toki_ni = lipu_nimi["translations"][sona_lawa.language]
	else:
		toki_ni = lipu_nimi["translations"]["en"]
	# sitelen
	nasin_sitelen = lipu_nimi.get("representations", {})
	r_emosi = nasin_sitelen.get("sitelen_emosi", "")
	r_ucsur = nasin_sitelen.get("ucsur", "")
	r_sitelen_pona = nasin_sitelen.get("ligatures", "")
	jan_pi_mama_nimi = (", ").join(lipu_nimi["creator"])
	if len(r_sitelen_pona) == 0:
		r_sitelen_pona = ""
	else:
		r_sitelen_pona = r_sitelen_pona[0]
	kulupu_mute = ["tp:book:" + ante_kulupu_nimi(lipu_pi_nimi_ni), "tp:usage:" + ante_kulupu_nimi(kepeken_pi_nimi_ni)]
	if nimi in linku.AMBIGUOUS:
		kulupu_mute.append("tp:nimi_pi_sona_pona_ala")
	if nimi in linku.TYPOS:
		kulupu_mute.append("tp:nimi_nasa")
	if r_emosi != "":
		kulupu_mute.append("tp:sitelen_emosi")
	if r_ucsur != "":
		kulupu_mute.append("tp:ucsur")
	if r_sitelen_pona != "":
		kulupu_mute.append("tp:sitelen_pona")
	# 'kulupu_mute' li pini.
	# weka ala weka e nimi?
	weka_ni = False
	for wile_kulupu in sona_lawa.require:
		if not wile_kulupu in kulupu_mute:
			weka_ni = True
			break
	for weka_kulupu in sona_lawa.delete:
		if weka_kulupu in kulupu_mute:
			weka_ni = True
			break
	if not weka_ni:
		sitelen_TSV(nimi, toki_ni["definition"], kulupu_mute, toki_ni["commentary"], r_emosi, r_ucsur, r_sitelen_pona, jan_pi_mama_nimi, lipu_pi_nimi_ni, kepeken_pi_nimi_ni)

if not sona_lawa.out is None:
	lipu.close()

