use std::{
    io::{stdin, stdout, Read},
    process::exit,
    sync::atomic::AtomicU64,
};

use klc_core::{DiagAutoReportable, DiagLoggerImpl, RcCellExtensions, StdioDiag};
use klc_meltedmd::{mmd_parse_and_melt, twily, MMDContent, MMDDoc, MMDPara, MMDRun};
use klc_xmldom::{
    elm, frg, XMLAttrList, XMLAttrListOps, XMLElement, XMLNode, XMLNodeList, XMLNodeOps,
    XMLPushable, XMLPushableEx,
};

#[allow(unused_parens)]
fn transform_run(run: &MMDRun) -> XMLNode {
    let mut core = match &run.0 {
        MMDContent::Text(text) => XMLNode::new_text(text),
        MMDContent::Sigil(text, attrs) => {
            if text.eq("br") {
                elm!(<br>).node()
            } else if text.eq("img") {
                let xal = XMLAttrList::new(attrs.clone());
                let alt = xal.attr_get_or("alt", "");
                let src = xal.attr_get_or("src", "");
                elm!(<img "alt"=alt "src"=src>).node()
            } else if text.eq("how-big-is-pva") {
                XMLNode::new_text(format!(
                    "{} bytes",
                    std::fs::read("public/pva/libpva.min.js").unwrap().len()
                ))
            } else {
                panic!("NYI Sigil: {}", text);
            }
        }
    };
    match &run.1.link {
        klc_meltedmd::MMDLink::None => {}
        klc_meltedmd::MMDLink::Url(url) => {
            core = elm!(<a "href"=url> core).node();
        }
        klc_meltedmd::MMDLink::Anchor(url) => {
            core = elm!(<a "href"=(format!("#{}", url))> core).node();
        }
        klc_meltedmd::MMDLink::Footnote(_url) => {
            // This would be nice, but not for the MVP for the site
            panic!("NYI");
        }
    }
    for tag in &run.1.tags {
        if tag.eq("builtin:link-url") {
            continue;
        }
        let ntag = XMLElement::new(tag);
        ntag.push_node(core);
        core = ntag.node();
    }
    core
}
fn transform_runs(target: &dyn XMLPushable, runs: &[MMDRun]) {
    for v in runs {
        target.push_node(transform_run(v));
    }
}
#[allow(unused_parens)]
fn transform_para(target: &XMLNodeList, para: &MMDPara) {
    match para {
        MMDPara::General(runs) => {
            let para = elm!(<p>);
            transform_runs(&para, &runs);
            target.push(para);
            target.push_text_str("\n");
        }
        MMDPara::Heading(heading) => {
            let para = XMLElement::new(format!("h{}", heading.depth));
            transform_runs(&para, &heading.text);
            target.push(para);
            target.push_text_str("\n");
        }
        MMDPara::List(start, content) => {
            let para = if let Some(start) = start {
                elm!(<ol "start"=(format!("{}", start))>)
            } else {
                elm!(<ul>)
            };
            for v in content {
                let choice = elm!(<li>);
                transform_doc(&choice.content, v);
                para.push(choice);
            }
            target.push(para);
            target.push_text_str("\n");
        }
        MMDPara::Code(text, lang, _) => {
            let israw = lang.clone().is_some_and(|v| v.eq("raw-html"));
            if israw {
                target.push(XMLNode::new_raw(text));
            } else {
                target.push(elm!(<pre>
                    elm!(<code>
                        XMLNode::new_text(text)
                    )
                ));
            }
        }
        MMDPara::Block(kind, _attrs, contents) => {
            if kind.eq("warning") {
                let warning = elm!(<div "class"="div-warning">);
                transform_doc(&warning.content, contents);
                target.push(warning);
            } else if kind.starts_with("tangent-") {
                let block = elm!(<div "class"="div-tangent">);
                // Reader helper
                block.push(elm!(<p> XMLNode::new_text(format!("{} {}", &kind[8..], "{"))));
                transform_doc(&block.content, contents);
                block.push(elm!(<p> XMLNode::new_text("}")));
                target.push(block);
            } else if kind.starts_with("cw-") {
                let warning = format!("CW: {}", &kind[3..]);
                let block = elm!(<details>);
                block.push(elm!(<summary> XMLNode::new_text(warning)));
                transform_doc(&block.content, contents);
                target.push(block);
            } else {
                panic!("Block kind NYI: {}", kind);
            }
        }
        MMDPara::Cue(kind, _attrs) => {
            if kind.eq("signoff-2") {
                target.push(elm!(<div "class"="div-eol">
                    elm!(<i>
                        XMLNode::new_text("~ K")
                    )
                ));
            } else if kind.starts_with("pva-example-") {
                let example = &kind[12..];
                let src = format!("iframe.html#{}.pva", example);
                let bytes = std::fs::read(format!("public/pva/{}.pva", example))
                    .unwrap()
                    .len();
                let w = "256";
                let mut h = "256";
                if example.eq("city") {
                    h = "192";
                }
                target.push(elm!(<center>
                    elm!(<iframe "style"="border: 0;" "src"=src "width"=w "height"=h>),
                    elm!(<br>),
                    XMLNode::new_text(format!("{} bytes", bytes))
                ))
            } else {
                panic!("Cue kind NYI: {}", kind);
            }
        }
        MMDPara::Anchor(_) => {
            panic!("NYI");
        }
        MMDPara::FootnoteDefinition(_) => {
            panic!("NYI");
        }
    }
}
#[allow(unused_parens)]
fn transform_doc(target: &XMLNodeList, doc: &MMDDoc) {
    for v2 in doc.i().iter() {
        transform_para(target, v2);
    }
}

#[allow(unused_parens)]
fn main() {
    let unique = AtomicU64::new(0);
    let diag = StdioDiag::new();
    let mut args = std::env::args();
    let title = std::env::var("SHCMS_PAGE_TITLE").unwrap_or("Untitled".to_string());
    let backlink = std::env::var("SHCMS_PAGE_BACKLINK").unwrap_or("".to_string());
    let url_prefix = std::env::var("SHCMS_URL_PREFIX").unwrap_or("".to_string());
    args.next().expect("pages-md-compiler MODE");
    let mode = args.next().expect("pages-md-compiler MODE");
    if mode.eq("blog") {
        // ok
    } else if mode.eq("content") {
        // ok
    } else {
        panic!("Unknown mode: {}", mode);
    }
    assert!(args.next().is_none());
    let mut text = String::new();
    stdin().read_to_string(&mut text).report_to(&diag);
    let doc = mmd_parse_and_melt(
        &text,
        klc_meltedmd::MMDMeltOptions {
            base_depth: 0,
            unique_id_source: &unique,
            diag: &diag,
        },
    );
    twily(&doc);
    let maybe_backlink: Option<XMLElement> = if backlink.eq("") {
        None
    } else {
        Some(
            elm!(<a "href"=(format!("{}/{}", url_prefix, backlink))> XMLNode::new_text("Back".to_string())),
        )
    };
    // continue...
    let head = elm!(<head>
        XMLNode::new_text("\n".to_string()),
        elm!(<title> XMLNode::new_text(title.clone())),
        XMLNode::new_text("\n".to_string()),
        elm!(<link "rel"="stylesheet" "href"=(format!("{}/css.css", url_prefix))>),
        XMLNode::new_text("\n".to_string())
    );
    if mode.eq("blog") {
        head.push(elm!(<link "href"=(format!("{}/rss.rss", url_prefix)) "rel"="alternate" "type"="application/rss+xml">));
    }
    let output = frg!();
    transform_doc(&output, &doc);
    let finale = frg!(
        XMLNode::new_doctype("html"),
        elm!(<html>
            head,
            elm!(<body>
                elm!(<div "id"="header">
                    maybe_backlink,
                    elm!(<h1> XMLNode::new_text(title.clone()))
                ),
                XMLNode::new_text("\n".to_string()),
                output
            )
        )
    );
    finale
        .write_to(&mut stdout(), klc_xmldom::XMLWriteMode::HTML)
        .report_to(&diag);

    // done
    if diag.has_any_error_occurred() {
        exit(1);
    }
}
