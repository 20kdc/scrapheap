# Personal notes on the 1D Discrete Cosine Transform.

The Discrete Cosine Transform is often used in image and signal processing applications, but is often described in mathematician's language, which isn't very clearly translatable by non-mathematically-trained programmers.

This section of the scrapheap contains a matched DCT-II/DCT-III implementation pair written in C++ that inputs and outputs EXR files using TinyEXR.

These implementations use a 1D DCT, even though they handle 2D images. This is because I found attempting to implement a 2D DCT resulted in weird axis interactions. There's probably some misreading going on but the problem is that, again, _the information isn't very clearly translatable by non-mathematically-trained programmers._

I decided that, given this, a not-too-crazy idea for custom uses is to construct a fake "2D DCT" by performing a 1D DCT on each individual row, and then on each individual column of the output. This sacrifices performance when calculating individual values in favour of making overall performance less exponentially taxing and keeping it conceptually simple and easy to understand. Plus, it seems to produce 2D DCT-*like* spectra, at least; it can even recover the outlines of the Wikipedia "the sphinx" image from its DCT spectrum.

The optimizations that can be made to the implementations are pretty obvious:

* For a fixed size, and for a fixed sampling, there's a fixed set of involved cosines. It's possible to precalculate these in various ways.
* Most useful DCT operations can be made more consistent and potentially faster with a fixed-point implementation, though this increases the error.
