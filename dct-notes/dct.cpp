#include <math.h>
#define TINYEXR_IMPLEMENTATION
#include <tinyexr.h>

// Samples the DCT-II.
static float sampleDCT(float * input, int width, int stride, int k) {
	float result = 0;
	float piw = M_PI / width;
	for (int n = 0; n < width; n++) {
		float cosx = cos(piw * (n + 0.5f) * k);
		result += input[n * stride] * cosx;
	}
	if (k == 0)
		result *= 1.0f / sqrt(2.0f);
	result *= sqrt(2.0f / width);
	return result;
}

// Samples the DCT-III.
static float sampleIDCT(float * input, int width, int stride, float k) {
	float result = input[0] / sqrt(2);
	float piw = M_PI / width;
	for (int n = 1; n < width; n++) {
		float cosx = cos(piw * (k + 0.5f) * n);
		result += input[n * stride] * cosx;
	}
	result *= sqrt(2.0f / width);
	return result;
}

typedef struct {
	float r, g, b, a;
} rgba_t;

static float coreOp(float * rgba, int width, int stride, int k) {
#ifdef ENCODER
	return sampleDCT(rgba, width, stride, k);
#else
	return sampleIDCT(rgba, width, stride, k);
#endif
}

static void doOp1D(float * rgba, rgba_t * two, int width, int height) {
	for (int ox = 0; ox < width; ox++) {
		for (int oy = 0; oy < height; oy++) {
			float val = coreOp(rgba, width * height, 4, ox + (oy * width));
			rgba_t res = {val, val, val, 1};
			two[ox + (oy * width)] = res;
		}
	}
}

static void doOpC2D(float * rgba, rgba_t * two, float * tmp, int width, int height) {
#ifdef ENCODER
	// convert scanlines
	for (int oy = 0; oy < height; oy++)
		for (int ox = 0; ox < width; ox++)
			tmp[ox + (oy * width)] = coreOp(rgba + (oy * width * 4), width, 4, ox);
	// convert columns
	for (int ox = 0; ox < width; ox++) {
		for (int oy = 0; oy < height; oy++) {
			float val = coreOp(tmp + ox, height, width, oy);
			rgba_t res = {val, val, val, 1};
			two[ox + (oy * width)] = res;
		}
	}
#else
	// convert columns
	for (int ox = 0; ox < width; ox++) {
		for (int oy = 0; oy < height; oy++) {
			float val = coreOp(rgba + (ox * 4), height, width * 4, oy);
			tmp[ox + (oy * width)] = val;
		}
	}
	// convert scanlines
	for (int oy = 0; oy < height; oy++) {
		for (int ox = 0; ox < width; ox++) {
			float val = coreOp(tmp + (oy * width), width, 1, ox);
			rgba_t res = {val, val, val, 1};
			two[ox + (oy * width)] = res;
		}
	}
#endif
}

int main(int argc, char ** argv) {
	float * rgba;
	int width, height;
	const char * err;
	if (argc != 3) {
		puts("dct IN OUT");
		return 1;
	}
	if (LoadEXR(&rgba, &width, &height, argv[1], &err)) {
		puts(err);
		return 1;
	}
	float * gry = (float *) malloc(sizeof(float) * width * height);
	rgba_t * two = (rgba_t *) malloc(sizeof(rgba_t) * width * height);
	// do the sampling (and also store as RGBA for compat.) {
#ifndef C2D
	doOp1D(rgba, two, width, height);
#else
	doOpC2D(rgba, two, gry, width, height);
#endif
	// }
	SaveEXR((float *) two, width, height, 4, 0, argv[2], &err);
	return 0;
}
