#include <pulse/context.h>
#include <pulse/introspect.h>
#include <pulse/mainloop.h>
#include <pulse/pulseaudio.h>
#include <stdio.h>
#include <unistd.h>

static int read_led() {
	FILE * f = fopen(getenv("P2T_LED"), "rb");
	if (f) {
		int res = fgetc(f) != '0';
		fclose(f);
		return res;
	} else {
		printf("ERR\n");
		return 0;
	}
}

int main(int argc, char ** argv) {
	if (argc != 2 || !getenv("P2T_LED")) {
		fprintf(stderr, "pulse_p2t SOURCE\n");
		fprintf(stderr, "P2T_LED= something like '/sys/class/leds/input3::numlock/brightness'\n");
		return 1;
	}
	pa_mainloop * mainloop = pa_mainloop_new();
	pa_context * ctx = pa_context_new(pa_mainloop_get_api(mainloop), "kdcp2t");
	pa_context_connect(ctx, NULL, PA_CONTEXT_NOAUTOSPAWN, NULL);
	while (1) {
		pa_mainloop_iterate(mainloop, 0, NULL);
		int toggle = read_led();
		pa_context_set_source_mute_by_name(ctx, argv[1], toggle ? 0 : 1, NULL, NULL);
		printf("\r%i", (int) toggle);
		fflush(stdout);
		usleep(10000UL);
	}
	return 0;
}
