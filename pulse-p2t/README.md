# Pulseaudio/Numlock Push To Talk

This program takes a parameter, a PulseAudio source name (i.e. audio input device). You can get this with, say, `pactl get-default-source`.

It also takes an environment variable, `P2T_LED`, the name of an LED file, such as `/sys/class/leds/input3::numlock/brightness`.

It repeatedly reads from the LED file and updates the mute status of the source.

This provides a *universal* push-to-talk feature which works independently of window server (because Wayland's security focus is basically _How To Sandbag Linux As A Desktop Platform Used By Actual People For Dummies;_ wlroots helped initially but now it seems like GNOME is pulling ahead a little with per-window recording(???) and honestly with the state of OBS Studio on Linux being massively worsened by using Wayland _even given XWayland because of the massive reduction in FPS..._)

Because it targets the audio source itself, it should work with any application and doesn't require rewiring like a JACK or Pipewire-based solution would. Due to the various compatibility layers, using PulseAudio here allows it to also work with Pipewire.

This can be a downside in some situations, but when combined with an application which doesn't support usable PTT (like [Vesktop](https://github.com/Vencord/Vesktop/)) it's usable and simple.
