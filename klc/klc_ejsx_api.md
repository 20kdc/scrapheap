# `klc_ejsx` JavaScript API

`klc_ejsx` is a HTML/XML templating library based on `klc_xmldom` and `mini-v8`.

It provides three things:

1. Its own somewhat semi-broken implementation of CommmonJS / `require()` (requiring that modules be loaded in advance from Rust code).
2. Its own "static JSX" implementation under several module names.
3. It can automatically invoke [Bun](https://bun.sh/) to bundle/compile JSX/TSX/etc. into the necessary CommonJS modules.

This is an API reference of what is provided outside of the APIs provided by `mini-v8`.

## Globals

The prelude is mostly occupied with 'interface code' for simplifying JS/Rust interaction, but also provides these globals:

* `global`: Set to `globalThis`.
* `require(what)`: Function which retrieves a module from `require.cache` or throws an error.
* `require.cache`: Maps module IDs to their `module` objects.
* `console`: Browser console imitation
* `console.log(text)`: Log (information) to buffer
* `console.warn(text)`: Log (warning) to buffer
* `console.error(text)`: Log (error) to buffer. _Due to how KLC diagnostics work, this may cause an error status, so be careful with this!_
* `console.group(group)`: Begin console log group.
* `console.groupEnd()`: End console log group.

## JSX Runtime

This is provided as:

* `require("klc_ejsx/jsx-runtime")`
* `require("klc_ejsx/jsx-dev-runtime")`
* `require("react/jsx-runtime")`
* `require("react/jsx-dev-runtime")`

The API design is borrowed from React/Preact, but the implementation is entirely custom owing to the static-rendering-only usecase and embedding in Rust.

It has 12 exports in total:

* `Fragment`: JSX Fragment implementation
* `jsxDEV`: JSX element implementation
* `render(v[, container[, context]])`: Renders Virtual DOM to `klc_xmldom` JSON. `container` defaults to `[]`, and is always returned. `context` defaults to `{}`.
* `createContext(initialValue[, valueFromJSON])`: Creates context. See <https://preactjs.com/guide/v10/context#createcontext> for how this works.
    * From the above:
        * `Context.Provider` is a component that renders the enclosing components with the context set to the `value` prop.
        * `Context.Consumer` is a component that calls a function, expected to be the only child, with the value of the context.
    * Custom extensions:
        * `valueFromJSON` is a callback, stored as `Context.valueFromJSON`, useful to deserialize the context's value.
        * `Context.id` is a number which can be looked up in `contextDeserializationTable`.
        * `Context.toJSON` returns said ID.
        * `Context.runWith(value, fn)` runs `fn` with this Context set to `value`.
* `contextDeserializationTable`: Maps context IDs to the corresponding context objects.
* `useContext(Context)`: Returns the value in context during rendering. See <https://preactjs.com/guide/v10/hooks#usecontext> for how this works.
* `contextToJSON()`: Returns a (presumably) JSONable value that contains all context.
* `runWithJSONContext(json, fn)`: The inverse of `contextToJSON`, this unfreezes JSON context. Passing `{}` as `json` is fine.
* `toChildArray(children[, container])`: Utility function. See <https://preactjs.com/guide/v10/api-reference/#tochildarray>.
* `Doctype`: Custom element along the lines of `<Doctype text="html"/>`
* `Raw`: Custom element along the lines of `<Raw text="raw XML can go here that avoids escaping and parsing. this is an escape hatch and should be avoided"/>`
* `ProcessingInstruction`: Custom element along the lines of `<ProcessingInstruction context="php" text="print phpversion();"/>`

Explicitly missing versus i.e. Preact:

* `h`/`createElement` are not implemented as they are unused in the dev JSX config.
* `cloneElement` is not implemented -- it hasn't come up as being necessary, and it requires overcomplicating my Virtual DOM type structure.
* Class components are not implemented because all components are stateless in templating.
* `createRef` is not implemented because it kind of doesn't work here. There's no browser engine we're interacting with and we don't have class components.
* Almost all of the hooks package, except for `useContext`, is out of scope. Everything is stateless and this all runs ahead of time.
