use std::{cell::Cell, process::exit};

use clap::{Arg, ArgAction};
use klc_core::*;
use klc_meltedmd::*;

fn main() {
    let cmd = clap::Command::new("klc_dump")
        .arg(
            Arg::new("inmd")
                .long("in-md")
                .num_args(1)
                .required(true)
                .value_name("file")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("t")
                .long("twily")
                .num_args(0)
                .action(ArgAction::SetTrue),
        );
    let matches = cmd.get_matches();
    let inmd = matches.get_one::<String>("inmd").unwrap();
    let str = &std::fs::read_to_string(inmd).unwrap();
    let diag = StdioDiag::new();
    let unique_id_source = Cell::new(0);
    let target = mmd_parse_and_melt(
        str,
        MMDMeltOptions {
            base_depth: 0,
            unique_id_source: &unique_id_source,
            diag: &diag,
        },
    );
    if matches.get_flag("t") {
        twily(&target);
    }
    println!("{:#?}", target);
    if diag.has_any_error_occurred() {
        diag.warn(String::from("Errors have occurred."));
        exit(1);
    }
}
