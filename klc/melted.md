# MeltedMD

MeltedMD is an intermediate representation and set of inline formatting directives meant to be built upon an existing Markdown parser.

**MeltedMD is not supposed to contain 100% of your document-specific content.**

It is designed for *90%* of your document, and then you write the remaining 10%:

* As target-format-specific processors.
* As a script or plugin implementing what your document needs in such a processor.

This can be summed up as: 'Rather than try to cover everything, make failing to cover everything okay.' [^MMD1]

As a result of all of this, MeltedMD is by no means a perfect format. _It probably isn't even ever going to stabilize._ But for my personal targets it works.

[^MMD1]: MeltedMD was created after my previous attempt involved Python, MD-to-HTML-to-ODT, BeautifulSoup, UNO, generating two copies of each section for left versus right starting page, and PDF merging. It was also awfully slow.

## Structure & Semantics

A MeltedMD document is made up of _paragraphs._

Paragraphs can be one of various kinds:

* General: Formatted text content.
* Heading: Heading (H1/2/...). [^MMD2]
* Anchor: Converted from definitions: `[myanchor]: nowhere://`. The contents are discarded.
* Footnote Definition: As in Markdown: `[^example]: (...)`: Defines the contents of a footnote. [^MMD3]
* List: Unordered lists are like this list, while ordered lists are 1./2./3.-style lists (with start indices).
* Code: A code block. Contents are pretty much just pulled as-is from mdast, including lang/meta, since this is needed for highlighting.
* Block: Converted from lone XML start/end tags: `<example>` and `</example>`.
* Cue: Converted from lone XML self-closing tags beginning with `cue-`: `<cue-example/>`.

[^MMD2]: It is worth explicitly noting that the parser allows offsetting the depths of headings. Headings are expected to be given special treatment by processors; i.e. adding to TOCs. The expected workflow is that the 'digital TOC' is comprehensive, and 'in-document TOCs' will include all headings from one set depth limit to another set depth limit in a given region.
[^MMD3]: This data is copied during parsing to the footnote itself, so the renderer can render the footnote contents at either or both sites.

## Runs, Tags, and Sigils.

Text content is made up of *runs.* Each run is a list of 'tags', along with an optional link, and some content.

The tags provide the formatting for the text. Markdown's formatting directives are turned into built-in tags, and HTML start/end tags can be used.

The link can be to an anchor, a footnote, or a hyperlink. Footnote references automatically receive the content of the footnote.

The content of a run can be text, or a sigil (the contents of a self-closing tag).

The built-in tags (those MeltedMD inherently generates or expects the user to use) are:

* `b`: Generated for `**bold**` text.
* `i`: Generated for `_italic_` text.
* `s`: Generated for `~~strikethrough~~` text.
* `code`: Generated for code.
* `builtin:link-url`: Generated for links to a URL.
* `builtin:link-anchor`: Generated for links to an anchor.
* `builtin:footnoteref`: Generated for footnote references.
* `twily-off`: This disables transforming quotes and dashes.
* `clear`: This is a special tag which clears all the tags outside of it for text within it.

There are two built-in sigils:

* `br`: Generated for line breaks.
* `img`: Generated for images. Attributes `src` and `alt` act as expected.

While `u` is not in this list, it is present in HTML, referring to <u>underlined</u> text.

## Blocks, Cues, and Modes.

Blocks, Cues, and Tags are _user-defined_ signals. To be clear, it is totally against the point of MeltedMD to hardcode these _unless_ your program is a one-shot formatter where editing the code for each document is expected. Any _general-purpose_ MeltedMD formatter needs to have an extension mechanism for these.

Cues allow for simply inserting special formatting whenever MeltedMD is insufficient. You can think of them as 'formatting setpieces' -- title page logos, special page breaks, or things that are so utterly output-specific you cannot possibly represent it in the Markdown.

Blocks are much the same, except _blocks can contain content,_ and the content is rendered according to the block's corresponding mode (as in modal).

Modes aren't actually part of the MeltedMD AST, but they define how you are expected to resolve the implementations.

In short: Inheritance. Blocks are expected to occupy a position in a 'mode tree', in which all defined things exist, and definitions are inherited from parent modes. Blocks, cues, and tags are resolved this way.

Importantly, _only blocks_ change the mode, not tags. (And cues can't, because they don't contain other content.) If it is necessary for tags to be capable of interfering with each other, this can be implemented as part of the mode's logic.

_Absolutely all of these definitions are out-of-band, by design._ Being defined elsewhere means that the processor can give them as much power as possible, in a way that is tailored to the output format.

MeltedMD itself only generates one cue, `builtin-hr` (in response to a thematic break: `***`).

## A Quick Note On Parsing

Blocks, cues, tags, and sigils are tokenized as per XML.

Keep in mind though that MeltedMD considers code such as `<b></b>` to be _tags_ or _blocks_ while it considers self-closing tags like `<b/>` to be _sigils_ or _cues._

Blocks, cues, and sigils may have attributes. Tags read but discard attributes, due to the 'binary flag' nature of the tag system.

A key note about parsing is the distinction between _paragraph-level markup_ and _inline markup._

Cues and block start/end directives are only parsed at paragraph-level. Tags are only parsed inline.

Sigils may be parsed at either level; the `cue-` prefix is used on all cues because cues and sigils both use self-closing tags, so they'd conflict.

Theoretically, blocks and tags are similarly ambiguous, but tags are much less likely to be used in all-markup cases.

_A key rule for formatters: If you are tripping over markup being misinterpreted, fold the offending section into a cue or block._

A sigil at paragraph-level has a wrapping paragraph automatically added.

## Twily

'Twily' is a post-processing step. It's similar to the SmartyPants tool, but it's executed at the MeltedMD level rather than as a text processing step.

The idea here is that Twily will never misinterpret a formatting directive as part of the text to be adjusted.

But, the only way to fix text that shouldn't be converted is to either use the span tag `<twily-off>` or for the text to be inside an inline code block.

Twily switches single and double-quotes to the correct types based on neighbouring whitespace. Neighbouring whitespace is checked between runs, as long as no run is of zero length, which should not occur.

Twily then performs the following substitutions:

* ` --` or `-- ` becomes an 'en dash' ( -- ) with the space preserved.
* `...` becomes ellipsis ( ... ).

I am aware that this is fairly limited. However, I think it serves its purpose: it converts 'programmer's typography' to document typography. Keeping it as a post-processing step after parsing also allows it to be disabled.
