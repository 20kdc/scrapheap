use std::{
    fmt::Display,
    io::{Seek, Write},
};

use klc_xmldom::{elm, frg, XMLNode, XMLNodeList, XMLNodeOps, XMLWriteMode};
use zip::{write::FileOptions, ZipWriter};

#[derive(Debug)]
pub enum EPubError {
    ZipError(zip::result::ZipError),
    IOError(std::io::Error),
}

impl From<zip::result::ZipError> for EPubError {
    fn from(value: zip::result::ZipError) -> Self {
        Self::ZipError(value)
    }
}

impl From<std::io::Error> for EPubError {
    fn from(value: std::io::Error) -> Self {
        Self::IOError(value)
    }
}

impl Display for EPubError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ZipError(i) => i.fmt(f),
            Self::IOError(i) => i.fmt(f),
        }
    }
}

impl std::error::Error for EPubError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
    #[allow(deprecated)]
    fn description(&self) -> &str {
        match self {
            Self::ZipError(i) => i.description(),
            Self::IOError(i) => i.description(),
        }
    }
    fn cause(&self) -> Option<&dyn std::error::Error> {
        match self {
            Self::ZipError(i) => Some(i),
            Self::IOError(i) => Some(i),
        }
    }
}

/// Data of a file entering the EPub version of the output.
#[derive(Clone)]
pub struct EPubAttachment {
    /// The ID, used to refer to the file in the spine/etc.
    pub id: String,
    /// The 'href' (filename).
    pub href: String,
    /// The MIME type.
    pub media_type: String,
    /// The actual contents.
    pub contents: Vec<u8>,
}

/// EPub navpoint.
#[derive(Clone)]
pub struct EPubNavPoint {
    /// Nav label text.
    pub text: String,
    /// Content src, i.e. `example.xhtml#position`
    pub src: String,
    /// Interior navpoints.
    pub children: Vec<EPubNavPoint>,
}

/// EPub preparation area. Takes care of the 'outer shell'.
///
/// Particular guarantees this ensures automatically:
///
/// * `META-INF/container.xml` exists and links to `metadata.opf`
/// * All assets relevant to OPF are in the manifest, including the NCX `toc.ncx`.
/// * The NCX file exists and is linked to the spine
///
/// Particular guarantees this does _not_ ensure automatically:
///
/// * Anything _whatsoever_ to do with the XHTML content
/// * That the spine contents are correct
/// * Sync between HTML and logical TOC
/// * The existence of proper metadata, i.e. `dc:language`, `dc:title`, `meta name="cover"`
/// * Nesting limits for specific platforms
/// * The presence of a cover image
#[derive(Clone)]
pub struct EPub {
    /// OPF metadata keys can go into this node list.
    /// It is recommended that the user is allowed to append arbitrary XML here.
    pub opf_metadata: XMLNodeList,
    /// OPF spine entries can go into this node list.
    pub opf_spine: XMLNodeList,
    /// Attached files.
    /// It is recommended that the user is allowed to append arbitrary files here.
    pub files: Vec<EPubAttachment>,
    /// "toc.ncx" file contents.
    pub nav_map: Vec<EPubNavPoint>,
}

impl Default for EPub {
    fn default() -> Self {
        Self {
            opf_metadata: XMLNodeList::new(),
            opf_spine: XMLNodeList::new(),
            files: vec![],
            nav_map: vec![],
        }
    }
}

impl EPub {
    /// Writes the EPub data to an EPub file.
    pub fn write_to<W: Write + Seek>(&self, writer_base: W) -> Result<(), EPubError> {
        let mut writer = ZipWriter::new(writer_base);
        // OCF metadata
        writer.start_file("META-INF/container.xml", FileOptions::default())?;
        writer.write_all(&frg!(
            XMLNode::new_xml_utf8(),
            elm!(<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
                elm!(<rootfiles>
                    elm!(<rootfile "full-path"="metadata.opf" "media-type"="application/oebps-package+xml">)
                )
            )
        ).to_bytes(XMLWriteMode::XML))?;
        // OPF metadata
        writer.start_file("metadata.opf", FileOptions::default())?;
        writer.write_all(&self.opf_gen_manifest().to_bytes(XMLWriteMode::XML))?;
        // TOC
        writer.start_file("toc.ncx", FileOptions::default())?;
        writer.write_all(&self.ncx_gen().to_bytes(XMLWriteMode::XML))?;
        // Files
        for v in &self.files {
            writer.start_file(&v.href, FileOptions::default())?;
            writer.write_all(&v.contents)?;
        }
        Ok(())
    }

    /// Generates an OPF manifest.
    /// Auto-adds a "toc" file called "toc.ncx".
    #[allow(unused_parens)]
    fn opf_gen_manifest(&self) -> XMLNodeList {
        frg!(
            XMLNode::new_xml_utf8(),
            elm!(<package "xmlns"="http://www.idpf.org/2007/opf" "unique-identifier"="uuid_id" "version"="2.0">
                elm!(<metadata "xmlns:dc"="http://purl.org/dc/elements/1.1/" "xmlns:opf"="http://www.idpf.org/2007/opf">
                    self.opf_metadata.clone()
                ),
                elm!(<manifest>
                    elm!(<item "id"="toc" "media-type"="application/x-dtbncx+xml" "href"="toc.ncx">),
                    self.files.iter().map(|attachment| elm!(<item
                        "id"=(&attachment.id)
                        "media-type"=(&attachment.media_type)
                        "href"=(&attachment.href)
                    >))
                ),
                elm!(<spine toc="toc">
                    self.opf_spine.clone()
                )
            )
        )
    }

    /// Generates the NCX.
    #[allow(unused_parens)]
    fn ncx_gen(&self) -> XMLNodeList {
        let mut play_order: usize = 1;
        frg!(
            XMLNode::new_xml_utf8(),
            elm!(<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
                elm!(<navMap>
                    self.nav_map.iter().map(|v| EPub::ncx_navpoint_conv(v, &mut play_order))
                )
            )
        )
    }

    /// Generates an NCX navpoint.
    #[allow(unused_parens)]
    fn ncx_navpoint_conv(v: &EPubNavPoint, play_order: &mut usize) -> XMLNode {
        let np_counter = *play_order;
        *play_order += 1;
        elm!(<navPoint id=(format!("num_{}", np_counter)) playOrder=(format!("{}", np_counter))>
            elm!(<navLabel>
                elm!(<text>
                    XMLNode::new_text(&v.text)
                )
            ),
            elm!(<content src=(&v.src)>),
            v.children.iter().map(|v| EPub::ncx_navpoint_conv(v, play_order))
        )
        .node()
    }
}
