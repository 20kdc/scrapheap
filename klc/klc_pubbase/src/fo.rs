use klc_xmldom::*;

/// FO tree outline.
pub type FOTreeOutline = Vec<FOTreeOutlineNode>;

/// FO tree outline node.
pub struct FOTreeOutlineNode {
    pub text: String,
    /// Internal destination. This is, for example, the ID of a fo:block (i.e. setting to `"a"` implies `<fo:block id="a"/>`).
    pub internal_destination: String,
    pub children: FOTreeOutline,
}

/// Converts an FO outline tree from the structure to the actual nodes.
pub fn fotoc_outline_to_xml(doc: &FOTreeOutline) -> XMLNode {
    elm!(<"fo:bookmark-tree">
        doc.iter().map(fotoc_bookmark)
    )
    .node()
}

#[allow(unused_parens)]
fn fotoc_bookmark(node: &FOTreeOutlineNode) -> XMLNode {
    elm!(<"fo:bookmark" "internal-destination"=(&node.internal_destination)>
        elm!(<"fo:bookmark-title">
            XMLNode::new_text(&node.text)
        ),
        (node.children.iter().map(fotoc_bookmark))
    )
    .node()
}
