import examplemodule from "./test_mod.jsx";
import { createContext, useContext, Doctype } from "klc_ejsx/jsx-runtime";

console.log("Testing console.log");

let TestContext = createContext("no value");

export const hi = "BYE";

function ContextTestValue() {
    return <TestContext.Consumer>
        {(value) => value}
    </TestContext.Consumer>;
}

function ContextTestHook() {
    return useContext(TestContext);
}

global.myTemplate = function () {
    return <>
        <Doctype root="html"/>
        <html>{"\n"}
            <head>
                <title>Templating Test</title>
            </head>
            <body>{"\n"}
                <a href="example.com">Example Single</a>{"\n"}
                <>
                    {"A"}
                    {"B"}
                    <ContextTestValue/>
                    <TestContext.Provider value="Stored">
                        <ContextTestHook/>
                    </TestContext.Provider>
                    <ContextTestValue/>
                </>{"\n"}
                <></>
                <fo:test>
                    namespacing test
                </fo:test>{"\n"}
            </body>
        </html>{"\n"}
    </>;
};
