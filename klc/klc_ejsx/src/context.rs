use std::cell::RefCell;

use json::JsonValue;
use klc_core::{DepTracker, DiagLogger};
use klc_xmldom::XMLTarget;
use mini_v8::{MiniV8, Script, ScriptOrigin, Value};

use crate::compile_esm_to_cjs;

/// Template library/context.
/// Beware: Direct access to this should be a last resort.
/// (This is owing to what happened last time I had to change JS engines...)
pub struct EJSXContext {
    pub runtime: MiniV8,
    pub loaded_cjs_modules: RefCell<Vec<String>>,
}

/// 'Sensibleish' script converter.
fn newscript(filename: &str, code: &str) -> Script {
    Script {
        source: code.to_string(),
        origin: Some(ScriptOrigin {
            name: filename.to_string(),
            ..Default::default()
        }),
        ..Default::default()
    }
}

/// Handles errors by reporting them to a DiagLogger.
fn handle_error<T>(
    m: &MiniV8,
    r: mini_v8::Result<T>,
    locator: impl FnOnce() -> String,
    diag: &DiagLogger,
) -> Option<T> {
    match r {
        Err(e) => {
            let estr = e.to_string();
            let mut total = format!("{}: {}", locator(), estr);
            let eval = e.to_value(m);
            if let Ok(vstr) = eval.coerce_string(m) {
                total += " / ";
                total += &vstr.to_string();
            }
            if let Some(eobj) = eval.as_object() {
                let stack: mini_v8::Result<String> = eobj.get("stack");
                if let Ok(stack) = stack {
                    total += " @ ";
                    total += &stack;
                }
            }
            diag.error_text(total);
            None
        }
        Ok(v) => Some(v),
    }
}

impl EJSXContext {
    /// Creates an embedded JSX context.
    /// Template libraries can then be loaded with [EJSXContext::install_esm_file].
    pub fn new(diag: &DiagLogger) -> EJSXContext {
        // alright, we have code. let's setup runtime
        let rt = MiniV8::new();
        let _ignored: Option<()> = handle_error(
            &rt,
            rt.eval(newscript("klc_ejsx/prelude.js", include_str!("prelude.js"))),
            || "klc_ejsx prelude".to_string(),
            diag,
        );
        let finale = EJSXContext {
            runtime: rt,
            loaded_cjs_modules: RefCell::new(vec![]),
        };
        finale.install_cjs_text("klc_ejsx/jsx-runtime", include_str!("jsx-runtime.js"), diag);
        finale.alias_module("klc_ejsx/jsx-runtime", "klc_ejsx/jsx-dev-runtime", diag);
        // compat. with default Bun settings
        finale.alias_module("klc_ejsx/jsx-runtime", "react/jsx-dev-runtime", diag);
        // should really check API compatibility, Bun documentation seems to imply args are different.
        // ...really different.
        // as it is we'll let the import succeed, since the exports don't overlap
        finale.alias_module("klc_ejsx/jsx-runtime", "react/jsx-runtime", diag);
        finale
    }

    /// Sets a global in the EJSX context.
    pub fn set_global_str(&self, name: &str, value: &str, diag: &DiagLogger) {
        handle_error(
            &self.runtime,
            self.runtime.global().set(name, value),
            || format!("set_global_str {}", name),
            diag,
        );
    }

    /// Sets a global in the EJSX context.
    pub fn set_global(&self, name: &str, value: impl Into<json::JsonValue>, diag: &DiagLogger) {
        self.set_global_str("_ejsx_globalset_key", name, diag);
        self.set_global_str("_ejsx_globalset_value", &json::stringify(value), diag);
        self.eval_void(
            name,
            "globalThis[_ejsx_globalset_key] = JSON.parse(_ejsx_globalset_value);",
            diag,
        );
    }

    /// Flushes the diagnostic queue.
    pub fn flush_diag(&self, diag: &DiagLogger) {
        // Diag flush
        let json: Option<String> = handle_error(
            &self.runtime,
            self.runtime
                .eval(newscript("flush_diag", "JSON.stringify(_ejsx_logFlush())")),
            || "log-to-json".to_string(),
            diag,
        );
        if let Some(logdata) = json {
            let jsonres = json::parse(&logdata);
            match jsonres {
                Err(e) => {
                    diag.error_text(format!(
                        "Result extraction parsing error in flush_diag: {} JSON '{}'",
                        e, logdata
                    ));
                }
                Ok(v) => {
                    let mut depth: i32 = 0;
                    for member in v.members() {
                        let entrytype = member["type"].as_str().unwrap();
                        if entrytype.eq("enter") {
                            diag.enter(member["text"].as_str().unwrap());
                            depth += 1;
                        } else if entrytype.eq("info") {
                            diag.info(member["text"].as_str().unwrap().to_string());
                        } else if entrytype.eq("warn") {
                            diag.warn(member["text"].as_str().unwrap().to_string());
                        } else if entrytype.eq("error") {
                            diag.error_text(member["text"].as_str().unwrap().to_string());
                        } else if entrytype.eq("leave") {
                            if depth > 0 {
                                diag.leave();
                                depth -= 1;
                            }
                        }
                    }
                    while depth > 0 {
                        diag.leave();
                        depth -= 1;
                    }
                }
            }
        }
    }

    /// Evals an expression in the EJSX context.
    pub fn eval(&self, filename: &str, code: &str, diag: &DiagLogger) -> Option<json::JsonValue> {
        let jsony: Value = handle_error(
            &self.runtime,
            self.runtime.eval(newscript(filename, code)),
            || filename.to_string(),
            diag,
        )?;
        handle_error(
            &self.runtime,
            self.runtime.global().set("_ejsx_eval_extract", jsony),
            || "eval extraction".to_string(),
            diag,
        );
        let json: String = handle_error(
            &self.runtime,
            self.runtime
                .eval(newscript("from_json", "JSON.stringify(_ejsx_eval_extract)")),
            || filename.to_string() + ":to-json",
            diag,
        )?;
        self.flush_diag(diag);
        let jsonres = json::parse(&json);
        match jsonres {
            Err(e) => {
                diag.error_text(format!(
                    "Result extraction parsing error @ {}: {} JSON '{}'",
                    filename, e, json
                ));
                None
            }
            Ok(v) => Some(v),
        }
    }

    /// Evals an expression in the EJSX context with no result.
    pub fn eval_void(&self, filename: &str, code: &str, diag: &DiagLogger) {
        let _ignored: Option<()> = handle_error(
            &self.runtime,
            self.runtime.eval(newscript(filename, code)),
            || filename.to_string(),
            diag,
        );
        self.flush_diag(diag);
    }

    fn call_setup(&self, func: &str, args: &[json::JsonValue], diag: &DiagLogger) {
        let mut argsarray = json::Array::new();
        for v in args {
            argsarray.push(v.clone());
        }
        self.set_global_str("_ejsx_thunk_fn", func, diag);
        self.set_global("_ejsx_thunk_args", JsonValue::Array(argsarray), diag);
    }

    /// Calls a function in the EJSX context.
    /// Returns None on error.
    pub fn call(
        &self,
        filename: &str,
        func: &str,
        args: &[json::JsonValue],
        diag: &DiagLogger,
    ) -> Option<json::JsonValue> {
        self.call_setup(func, args, diag);
        self.eval(filename, "_ejsx_thunk()", diag)
    }

    /// Calls a function in the EJSX context with no result.
    pub fn call_void(
        &self,
        filename: &str,
        func: &str,
        args: &[json::JsonValue],
        diag: &DiagLogger,
    ) {
        self.call_setup(func, args, diag);
        self.eval_void(filename, "_ejsx_thunk()", diag);
    }

    /// Calls a function expecting an XML fragment return value, which is then appended to the target.
    pub fn call_jsx(
        &self,
        filename: &str,
        target: &XMLTarget,
        func: &str,
        args: &[json::JsonValue],
        diag: &DiagLogger,
    ) {
        self.call_setup(func, args, diag);
        let res = self.eval(filename, "_ejsx_thunk_jsx()", diag);
        if let Some(jsx) = res {
            if let Err(_) = klc_xmldom::json::decode_list(target, &jsx) {
                diag.error_text(format!("Unable to decode JSON into node list: {:?}", jsx));
            }
        }
    }

    /// Loads a CommonJS module into the EJSX context.
    pub fn install_cjs_text(&self, id: &str, code: &str, diag: &DiagLogger) {
        self.call_void(
            id,
            "_ejsx_install",
            &[
                JsonValue::String(id.to_string()),
                JsonValue::String(code.to_string()),
            ],
            diag,
        );
        self.loaded_cjs_modules.borrow_mut().push(id.to_string());
    }

    /// Compiles and loads a module into the EJSX context.
    pub fn install_esm_file(&self, id: &str, path: &str, deps: &mut DepTracker, diag: &DiagLogger) {
        let compiled = {
            let tmp = self.loaded_cjs_modules.borrow();
            compile_esm_to_cjs(path, &tmp, deps)
        };
        match compiled {
            Err(e) => diag.error(e),
            Ok(compiled) => self.install_cjs_text(id, &compiled, diag),
        }
    }

    /// Aliases one module (already loaded) to another.
    pub fn alias_module(&self, src: &str, dst: &str, diag: &DiagLogger) {
        self.call_void(
            "klc_ejsx/module-alias",
            "_ejsx_alias",
            &[
                JsonValue::String(src.to_string()),
                JsonValue::String(dst.to_string()),
            ],
            diag,
        );
        self.loaded_cjs_modules.borrow_mut().push(dst.to_string());
    }
}
