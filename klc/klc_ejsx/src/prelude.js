// Prelude.
// The basic idea here is that this provides:
// 1. A "thunk" to simplify the Rust code for now
// 2. A CommonJS-ish module framework to install extern modules.

// -- Thunk --

globalThis._ejsx_thunk_fn = "";
globalThis._ejsx_thunk_args = [];
function _ejsx_thunk() {
    const fn = globalThis[_ejsx_thunk_fn];
    if (fn === void 0) {
        throw new Error("No such function '" + _ejsx_thunk_fn + "'");
    }
    return fn.apply(null, _ejsx_thunk_args);
}

// -- CommonJS 1.1 implementation --

globalThis.global = globalThis; // Node compatibility

function require(what) {
    const mod = require.cache[what];
    if (mod === void 0) {
        throw new Error("Module " + what + " require()d, but not present.");
    }
    return mod.exports;
}

// This isn't really the same thing as in Node.js, but don't think about that, hmm?
// It's spiritually similar.
require.cache = {};

function _ejsx_install(id, code) {
    let cleanedId = "_ejsx_module_";
    for (let i = 0; i < id.length; i++) {
        let cc = id.charCodeAt(i);
        if (
            ((cc >= 97) && (cc <= 122)) ||
            ((cc >= 65) && (cc <= 90)) ||
            ((cc >= 48) && (cc <= 57))
        ) {
            cleanedId += id[i];
        } else {
            cleanedId += "_";
        }
    }
    // to preserve the ID, some Shenanigans must be done
    // ultimately, this has to be done to improve debugging
    let fn = new Function("module", "exports", "function " + cleanedId + "() {" + code + "\n} " + cleanedId + "();");
    let module = {
        id: id,
        exports: {}
    };
    // Fun fact: Bun's compilation output violates the CommonJS 1.1 specification.
    // This is because it performs "module.exports = ...;", which is not a valid way of exporting symbols.
    // (Object.assign(exports, ...) seems more correct.)
    // In fact, it likely potentially breaks some of the guarantees in regards to dependency cycles.
    // ...we don't need to really care.
    require.cache[id] = module;
    fn(module, module.exports);
}

function _ejsx_alias(src, dst) {
    const mod = require.cache[src];
    if (mod === void 0) {
        throw new Error("Module " + src + " is not present, but we want to alias it as " + dst);
    }
    require.cache[dst] = mod;
}

// -- Cheese --

function _ejsx_thunk_jsx() {
    return require.cache["klc_ejsx/jsx-runtime"].exports.render(_ejsx_thunk());
}

// -- Logging --

(() => {
    let exports = {};
    let logBuffer = [];

    exports.log = (text) => {
        logBuffer.push({"type": "info", "text": String(text)});
    };

    exports.warn = (text) => {
        logBuffer.push({"type": "warn", "text": String(text)});
    };

    exports.error = (text) => {
        logBuffer.push({"type": "error", "text": String(text)});
    };

    exports.group = (text) => {
        logBuffer.push({"type": "enter", "text": String(text)});
    };

    exports.groupEnd = () => {
        logBuffer.push({"type": "leave"});
    };

    global.console = exports;

    global._ejsx_logFlush = () => {
        let lb = logBuffer;
        logBuffer = [];
        return lb;
    };
})();
