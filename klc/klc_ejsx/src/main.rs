use std::io::stdout;

use klc_core::StdioDiag;
use klc_xmldom::{XMLNodeList, XMLNodeOps};

fn main() {
    let diag = StdioDiag::new();
    let ctx = klc_ejsx::EJSXContext::new(&diag);
    let mut deps = klc_core::DepTracker::default();
    ctx.install_esm_file("test_main", "test_main.jsx", &mut deps, &diag);
    let list = XMLNodeList::new();
    ctx.call_jsx(
        "calljsx",
        &klc_xmldom::XMLTarget::End(list.clone()),
        "myTemplate",
        &[],
        &diag,
    );
    list.write_to(&mut stdout(), klc_xmldom::XMLWriteMode::XML)
        .unwrap();
}
