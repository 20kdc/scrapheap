export type VNode = any;
export type Component<P> = (props: P) => VNode;
export type NodeType<P> = Component<P> | string;
export type XMLProps = {[key: string]: string | undefined};

export const Fragment: Component<{}>;
export function jsxDEV<P>(cls: NodeType<P>, props: P): VNode;
export function render(v: VNode, container?: VNode[]): VNode[];

export interface Context<V> {
    runWith<R>(value: V, fn: () => R): R
    Provider: Component<{value: V, children?: any}>,
    Consumer: Component<{children?: (value: V) => VNode}>,
}

export const contextDeserializationTable: {[key: number]: Context<any>};

export function createContext<V>(initialValue: V, fromJSON?: (json: any) => V): Context<V>;
export function useContext<V>(context: Context<V>): V;
export function contextToJSON(): any;
export function runWithJSONContext<R>(json: any, fn: () => R): R;

export function toChildArray(src: any): VNode[];

export const Doctype: Component<any>;
export const Raw: Component<any>;
export const ProcessingInstruction: Component<any>;
