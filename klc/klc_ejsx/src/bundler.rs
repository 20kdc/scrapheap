use klc_core::DepTracker;
use simple_error::{simple_error, SimpleError, SimpleResult};
use std::{path::PathBuf, process::Command};

/// Compiles ESM code into a CommonJS 1.1-ish module.
/// See: https://wiki.commonjs.org/wiki/Modules/1.1
/// Providing a CommonJS 1.1 environment is the responsibility of the EJSX prelude.
/// (Bun seems to have a ?bug? where it overwrites "exports" in the module object rather than doing exports properly, so this isn't actually compliant.)
pub fn compile_esm_to_cjs(
    path: &str,
    externs: &[String],
    deps: &mut DepTracker,
) -> SimpleResult<String> {
    // ...This isn't complete.
    // A theoretical fix for this would be to actually use ES modules properly, but that's a lot of work.
    deps.add_input(&PathBuf::from(path));
    // ...Continue...
    let mut args = vec![
        "build".to_string(),
        path.to_string(),
        "--format".to_string(),
        "cjs".to_string(),
    ];
    for v in externs {
        args.push("-e".to_string());
        args.push(v.to_string());
    }
    let output = Command::new("bun").args(&args).output();
    let output = output.map_err(SimpleError::from)?;
    if !output.status.success() {
        Err(simple_error!(
            "{}: {}",
            path,
            String::from_utf8_lossy(&output.stderr)
        ))
    } else {
        Ok(String::from_utf8_lossy(&output.stdout).to_string())
    }
}
