// A static implementation of some of Preact's APIs resulting in a JSON XML AST.
// That JSON AST can then be parsed into klc_xmldom form. See that Rust module for details on the format.
// The biggest takeaway is that in order to ensure text nodes work more or less as they should, strings become text nodes.
// Unrendered nodes are represented as function (container, context).
// Everything else is a pretty typical type-indicated JSON tree.

// All components are stateless, functional, and receive (props).

// A "virtual DOM" node can be one of:
//  * Array: List of virtual DOM nodes to render.
//  * Function: Unrendered context: A function (container). Must put rendered content into the container.
//  * Object, String: Rendered content in klc_xmldom form (Notably, this is used as-is; children are not run through rendering, etc. Basically this is the 'rendered content' boundary.)

// The important properties of this are:
// 1. Content that has already been rendered can be rendered again harmlessly.
//    Components can render their content themselves (this is used to implement context) but don't have to.
// 2. Fragment is trivial.
// 3. useContext is still implemented.
// 4. A third of this file is comments.

// -- Core --

/**
 * Simple implementation of Fragment: returns children as-is.
 */
exports.Fragment = (props) => props.children;

// Some particular APIs of note:
// * toChildArray exists, roughly following Preact's description.
// * createContext exists.

let jsx = function (cls, props) {
    if (typeof(cls) === "string") {
        let attributes = Object.assign({}, props);
        delete attributes.children;
        return (container) => container.push({
            "type": "element",
            "tagName": cls,
            "attributes": attributes,
            "children": render(props.children, [])
        });
    } else if (typeof(cls) === "function") {
        return (container) => render(cls(props), container);
    } else {
        throw new Error("JSX class " + cls + " isn't a class at all");
    }
};

exports.jsxDEV = jsx;

/**
 * render renders Virtual DOM into a klc_xmldom node array (the container node), which is returned (and provided if not specified).
 * Replacement nodes don't exist.
 */
function render(v, container) {
    container ||= [];
    // alright, on with it
    if (v instanceof Array) {
        // flatten arrays
        for (let elm of v) {
            render(elm, container);
        }
    } else if (typeof(v) === "function") {
        // renderer function
        v(container);
    } else if ((typeof(v) === "string") || (v instanceof Object)) {
        container.push(v);
    }
    // otherwise, null? undefined?
    return container;
}
exports.render = render;

// -- Context --

let jsContextId = 0;
const contextDeserializationTable = {};
exports.contextDeserializationTable = contextDeserializationTable;

/**
 * Creates a context Provider/Consumer pair.
 * Notably, the resulting object serializes as its ID.
 */
exports.createContext = (initialValue, valueFromJSON) => {
    const contextId = jsContextId++;
    let key = {
        _value: initialValue,
        runWith: (value, fn) => {
            let saved = key._value;
            try {
                key._value = value;
                return fn();
            } finally {
                key._value = saved;
            }
        },
        Provider: (props) => {
            let saved = key._value;
            try {
                key._value = props.value;
                return render(props.children, []);
            } finally {
                key._value = saved;
            }
        },
        Consumer: (props) => props.children(key._value),
        toJSON: () => contextId,
        valueFromJSON: (valueFromJSON !== void 0) ? valueFromJSON : (v) => v
    };
    contextDeserializationTable[contextId] = key;
    return key;
};

/**
 * Retrieves a context value.
 */
exports.useContext = (key) => key._value;

/**
 * Context to JSON
 */
exports.contextToJSON = () => {
    let result = {};
    for (let k in contextDeserializationTable) {
        let v = contextDeserializationTable[k];
        result[k] = v._value;
    }
    return result;
};

/**
 * JSON to Context
 */
exports.runWithJSONContext = (json, fn) => {
    let backward = [];
    try {
        for (let k in json) {
            let ctx = contextDeserializationTable[k];
            if (ctx && ctx.valueFromJSON) {
                let saved = ctx._value;
                backward.push(() => ctx._value = saved);
                ctx._value = ctx.valueFromJSON(json[k]);
            }
        }
        return fn();
    } finally {
        for (let v of backward) {
            v();
        }
    }
};

// -- Utilities --

/**
 * Converts a props.children value into a flattened element array.
 */
exports.toChildArray = (v, out) => {
    out ||= [];
    if (v instanceof Array) {
        // flatten arrays
        for (let elm of v) {
            toChildArray(elm, out);
        }
    } else if ((typeof(v) === "string") || (v instanceof Object)) {
        out.push(v);
    }
    // null? undefined?
    return out;
};

// -- Extended Node Types --

exports.Doctype = (props) => ({
    "type": "doctype",
    "text": String(props.root)
});

exports.Raw = (props) => ({
    "type": "raw",
    "text": String(props.text)
});

exports.ProcessingInstruction = (props) => ({
    "type": "pi",
    "context": String(props.context),
    "text": String(props.text)
});
