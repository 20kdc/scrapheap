use crate::*;

#[test]
fn syntactic_fun() {
    let mut tmp: Vec<u8> = Vec::new();
    let elm = elm!(
        <test test="bee">
        elm!(<Test2>)
    );
    _ = elm.write_to(&mut tmp, XMLWriteMode::XML);
    assert_eq!(
        String::from_utf8(tmp.clone()).unwrap(),
        "<test test=\"bee\"><Test2/></test>"
    );
    tmp.clear();
    _ = elm!(<"testtag" "testattr"="testvalue">).write_to(&mut tmp, XMLWriteMode::XML);
    assert_eq!(
        String::from_utf8(tmp.clone()).unwrap(),
        "<testtag testattr=\"testvalue\"/>"
    );
    tmp.clear();
    _ = frg!(elm!(<A>), elm!(<B>)).write_to(&mut tmp, XMLWriteMode::XML);
    assert_eq!(String::from_utf8(tmp.clone()).unwrap(), "<A/><B/>");
}
