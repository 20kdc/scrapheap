//! Alright, let's exchange between XML and JSON.
//! Format:
//! A node list is an array of nodes, or a node, or null. When possible it should always be written as a flat array of nodes.
//! A node is either an object with a "type" field, or a string (text node).

use json::JsonValue;

use crate::{XMLAttr, XMLElement, XMLGenericListOps, XMLNode, XMLNodeList, XMLPushable, XMLTarget};

/// Decodes a node list.
pub fn decode_list(target: &XMLTarget, value: &JsonValue) -> Result<(), ()> {
    match value {
        JsonValue::Array(contents) => {
            for v in contents {
                decode_list(target, v)?;
            }
            Ok(())
        }
        JsonValue::Null => Ok(()),
        JsonValue::Object(_) => decode_node(target, value),
        JsonValue::String(_) => decode_node(target, value),
        JsonValue::Short(_) => decode_node(target, value),
        _ => Err(()),
    }
}

/// Decodes a node.
pub fn decode_node(target: &XMLTarget, value: &JsonValue) -> Result<(), ()> {
    if let Some(val) = value.as_str() {
        target.push_text_str(val);
        return Ok(());
    } else if let Some(tid) = value["type"].as_str() {
        if tid.eq("element") {
            if let Some(tag) = value["tagName"].as_str() {
                let elm = XMLElement::new(tag);
                decode_attrs(&mut elm.attrs.borrow_mut(), &value["attributes"]);
                decode_list(&XMLTarget::from(elm.clone()), &value["children"])?;
                target.push_node(elm.node());
                return Ok(());
            }
        } else if tid.eq("text") {
            if let Some(val) = value["text"].as_str() {
                target.push_text_str(val);
                return Ok(());
            }
        } else if tid.eq("comment") {
            if let Some(val) = value["text"].as_str() {
                target.push_node(XMLNode::new_comment(val));
                return Ok(());
            }
        } else if tid.eq("raw") {
            if let Some(val) = value["text"].as_str() {
                target.push_node(XMLNode::new_raw(val));
                return Ok(());
            }
        } else if tid.eq("doctype") {
            if let Some(val) = value["text"].as_str() {
                target.push_node(XMLNode::new_doctype(val));
                return Ok(());
            }
        } else if tid.eq("pi") {
            if let Some(val1) = value["context"].as_str() {
                if let Some(val2) = value["text"].as_str() {
                    target.push_node(XMLNode::new_pi(val1, val2));
                    return Ok(());
                }
            }
        }
    }
    Err(())
}

/// Decodes an attribute list. Note that this clears and readds the attributes.
pub fn decode_attrs(target: &mut Vec<XMLAttr>, value: &JsonValue) {
    target.clear();
    for ent in value.entries() {
        if let Some(kval) = ent.1.as_str() {
            target.push((ent.0.to_string(), kval.to_string()));
        }
    }
}

/// Encodes a list of nodes.
pub fn encode_list(node: &XMLNodeList) -> JsonValue {
    let mut array = vec![];
    node.foreach_v(|child| {
        encode_node(&mut array, child);
    });
    JsonValue::Array(array)
}

/// Encodes a node into a list.
/// Note that some nodes are deliberately skipped (insertion points).
pub fn encode_node(list: &mut Vec<JsonValue>, node: &XMLNode) {
    match node {
        XMLNode::InsertionPoint(_) => {}
        XMLNode::Element(elm) => {
            let mut obj = JsonValue::new_object();
            obj["type"] = "element".into();
            obj["tagName"] = elm.name.borrow().clone().into();
            obj["attributes"] = encode_attrs(&elm.attrs.borrow());
            obj["children"] = encode_list(&elm.content);
            list.push(obj);
        }
        XMLNode::Text(text) => list.push(text.borrow().clone().into()),
        XMLNode::Comment(text) => {
            let mut obj = JsonValue::new_object();
            obj["type"] = "comment".into();
            obj["text"] = text.borrow().clone().into();
            list.push(obj);
        }
        XMLNode::Raw(text) => {
            let mut obj = JsonValue::new_object();
            obj["type"] = "raw".into();
            obj["text"] = text.borrow().clone().into();
            list.push(obj);
        }
        XMLNode::Doctype(text) => {
            let mut obj = JsonValue::new_object();
            obj["type"] = "doctype".into();
            obj["text"] = text.borrow().clone().into();
            list.push(obj);
        }
        XMLNode::ProcessingInstruction(a, b) => {
            let mut obj = JsonValue::new_object();
            obj["type"] = "pi".into();
            obj["context"] = a.borrow().clone().into();
            obj["text"] = b.borrow().clone().into();
            list.push(obj);
        }
    }
}

/// Encodes an attribute list.
pub fn encode_attrs(attrs: &[XMLAttr]) -> JsonValue {
    let mut attributes = JsonValue::new_object();
    for attr in attrs.iter() {
        attributes[attr.0.clone()] = attr.1.clone().into();
    }
    attributes
}
