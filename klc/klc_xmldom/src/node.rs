use std::{
    fmt::Display,
    io::{self, Write},
    ops::{Deref, DerefMut},
};

use rccell::RcCell;

use crate::*;

/// Node in the element tree. This is either metadata or an element.
///
/// It's worth noting: in this library the node data is as 'close to the surface' as possible.
/// This allows for cleaner syntax, particularly around matching.
///
/// Finally, be aware that the [RcCell] and [Rc] fields of nodes are meant to exist as a group.
/// They should _not_ be "partially" shared between XMLNode instances.
///
/// With this in mind, `ptr_eq` only checks the first field.
#[derive(Clone, Debug)]
pub enum XMLNode {
    /// Represents an insertion point. This is 'sticky' and updates itself to wherever it's pushed.
    InsertionPoint(XMLInsertionPoint),
    /// Element. Can be forcibly deoptimized to start/end tags.
    /// Note that switching between [XMLNode] and [XMLElement] is lossless.
    Element(XMLElement),
    /// Text. Escaped during serialization.
    Text(RcCell<String>),
    /// Processing Instruction. Not escaped.
    ProcessingInstruction(RcCell<String>, RcCell<String>),
    /// Comment. Not escaped, so don't write sequences that end the tag.
    Comment(RcCell<String>),
    /// Arbitrary XML string.
    Raw(RcCell<String>),
    /// `<!DOCTYPE (text)>`.
    /// This is like processing instructions: unescaped raw content.
    /// The parser discards the external identifier for now.
    Doctype(RcCell<String>),
}

/// Mutable list with an RcCell layer over it.
/// This is useful to refer to "something to which nodes can be appended" while doing walk-and-modify.
/// It also works as the "root type".
/// See [XMLGenericListOps] and [XMLNodeListOps].
/// Beware that the contents of the vec shouldn't be edited too heavily.
/// This is because insertion points rely on side effects to track their current intended destination.
#[derive(Clone, Debug)]
pub struct XMLNodeList(RcCell<Vec<XMLNode>>);

impl XMLNodeList {
    /// New XMLNodeList. Starts empty.
    pub fn new() -> XMLNodeList {
        Self(RcCell::new(Vec::new()))
    }

    /// Because consistency is important, messing with the innards is explicit.
    pub fn from_guts(guts: RcCell<Vec<XMLNode>>) -> XMLNodeList {
        Self(guts)
    }

    /// New XMLNodeList from vec.
    pub fn from_vec(v: Vec<XMLNode>) -> Self {
        Self(RcCell::new(v))
    }
}

impl FromIterator<XMLNode> for XMLNodeList {
    fn from_iter<T: IntoIterator<Item = XMLNode>>(iter: T) -> Self {
        Self(RcCell::new(iter.into_iter().collect()))
    }
}

impl Default for XMLNodeList {
    fn default() -> Self {
        Self::new()
    }
}

impl Deref for XMLNodeList {
    type Target = RcCell<Vec<XMLNode>>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for XMLNodeList {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Display for XMLNodeList {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for v in self.0.borrow().iter() {
            v.fmt(f)?;
        }
        Ok(())
    }
}

impl XMLNode {
    /// Creates a text node.
    pub fn new_text(text: impl Into<String>) -> XMLNode {
        XMLNode::Text(RcCell::new(text.into()))
    }

    /// Creates a comment node.
    pub fn new_comment(text: impl Into<String>) -> XMLNode {
        XMLNode::Comment(RcCell::new(text.into()))
    }

    /// Creates a processing instruction node.
    pub fn new_pi(text: impl Into<String>, data: impl Into<String>) -> XMLNode {
        XMLNode::ProcessingInstruction(RcCell::new(text.into()), RcCell::new(data.into()))
    }

    /// Creates a raw node.
    pub fn new_raw(text: impl Into<String>) -> XMLNode {
        XMLNode::Raw(RcCell::new(text.into()))
    }

    /// Creates a doctype node.
    pub fn new_doctype(text: impl Into<String>) -> XMLNode {
        XMLNode::Doctype(RcCell::new(text.into()))
    }

    /// Creates an XML UTF-8 processing instruction:
    /// `<?xml version="1.0" encoding="UTF-8"?>`
    pub fn new_xml_utf8() -> XMLNode {
        XMLNode::ProcessingInstruction(
            RcCell::new(String::from("xml")),
            RcCell::new(String::from("version=\"1.0\" encoding=\"UTF-8\"")),
        )
    }

    /// Returns the element name, if this is an element.
    pub fn element_name(&self) -> Option<String> {
        self.as_element().map(|v| v.name.borrow().clone())
    }

    /// Attempts to convert to element.
    pub fn as_element(&self) -> Option<&XMLElement> {
        if let XMLNode::Element(elm) = self {
            Some(elm)
        } else {
            None
        }
    }

    /// Assumes this is an element.
    pub fn unwrap_element(&self) -> &XMLElement {
        self.as_element().unwrap()
    }

    /// Deeply clones this node.
    /// Notably, this makes insertion points rather useless.
    pub fn deep_clone(&self) -> XMLNode {
        match self {
            Self::InsertionPoint(_) => Self::InsertionPoint(XMLInsertionPoint::new()),
            Self::Element(elm) => Self::Element(elm.deep_clone()),
            Self::Text(text) => Self::Text(RcCell::new(text.borrow().clone())),
            Self::ProcessingInstruction(target, text) => Self::ProcessingInstruction(
                RcCell::new(target.borrow().clone()),
                RcCell::new(text.borrow().clone()),
            ),
            Self::Comment(text) => Self::Comment(RcCell::new(text.borrow().clone())),
            Self::Raw(text) => Self::Raw(RcCell::new(text.borrow().clone())),
            Self::Doctype(text) => Self::Doctype(RcCell::new(text.borrow().clone())),
        }
    }

    /// Deeply clones this node without children.
    pub fn deep_clone_no_children(&self) -> XMLNode {
        match self {
            Self::Element(elm) => Self::Element(elm.deep_clone_no_children()),
            _ => self.deep_clone(),
        }
    }

    /// Checks pointer equality between nodes.
    pub fn ptr_eq(a: &XMLNode, b: &XMLNode) -> bool {
        match a {
            Self::InsertionPoint(ip) => {
                if let Self::InsertionPoint(ip2) = b {
                    XMLInsertionPoint::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
            Self::Element(ip) => {
                if let Self::Element(ip2) = b {
                    XMLElement::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
            Self::Text(ip) => {
                if let Self::Text(ip2) = b {
                    RcCell::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
            Self::ProcessingInstruction(a1, _) => {
                if let Self::ProcessingInstruction(b1, _) = b {
                    RcCell::ptr_eq(a1, b1)
                } else {
                    false
                }
            }
            Self::Comment(ip) => {
                if let Self::Comment(ip2) = b {
                    RcCell::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
            Self::Raw(ip) => {
                if let Self::Raw(ip2) = b {
                    RcCell::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
            Self::Doctype(ip) => {
                if let Self::Doctype(ip2) = b {
                    RcCell::ptr_eq(ip, ip2)
                } else {
                    false
                }
            }
        }
    }
}

impl From<XMLInsertionPoint> for XMLNode {
    fn from(value: XMLInsertionPoint) -> Self {
        XMLNode::InsertionPoint(value)
    }
}

impl From<XMLElement> for XMLNode {
    fn from(value: XMLElement) -> Self {
        XMLNode::Element(value)
    }
}

impl Display for XMLNode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InsertionPoint(identity) => {
                f.write_fmt(format_args!("<?IP {:?}?>", identity.parent.as_ptr()))
            }
            Self::Element(elm) => elm.fmt(f),
            Self::Text(text) => f.write_str(&text.borrow()),
            Self::ProcessingInstruction(context, text) => {
                f.write_fmt(format_args!("<?{} {}?>", &context.borrow(), &text.borrow()))
            }
            Self::Comment(text) => f.write_fmt(format_args!("<!--{}--!>", &text.borrow())),
            Self::Raw(text) => f.write_str(&text.borrow()),
            Self::Doctype(text) => f.write_fmt(format_args!("<!DOCTYPE {}>", &text.borrow())),
        }
    }
}

/// Writing mode
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum XMLWriteMode {
    XML,
    /// HTML. All void elements are forcibly optimized, everything else is forcibly deoptimized.
    HTML,
}

/// Common operations between [XMLNode], [XMLElement], and [XMLNodeList]
pub trait XMLNodeOps {
    /// Gets all text in this node.
    fn text(&self) -> String {
        let mut res = String::from("");
        self.text_into(&mut res);
        res
    }

    /// Writes all text in this node into a result string.
    fn text_into(&self, result: &mut String);

    /// Writes this node to a writer.
    fn write_to<W: Write>(&self, writer: &mut W, mode: XMLWriteMode) -> io::Result<()>;

    /// Writes this node out as bytes.
    fn to_bytes(&self, mode: XMLWriteMode) -> Vec<u8> {
        let mut file = Vec::new();
        self.write_to(&mut file, mode)
            .expect("vec can't fail right");
        file
    }

    /// Replaces a processing instruction with an insertion point.
    fn replace_pi_with_insertion_point(&self, context: &str, content: &str) -> Option<XMLTarget>;
}

impl IntoIterator for XMLNode {
    type Item = XMLNode;
    type IntoIter = std::iter::Once<XMLNode>;
    fn into_iter(self) -> Self::IntoIter {
        std::iter::once(self)
    }
}

impl XMLNodeOps for XMLNode {
    fn text_into(&self, result: &mut String) {
        match self {
            Self::InsertionPoint(_) => {}
            Self::Element(elm) => elm.text_into(result),
            Self::Text(v) => result.push_str(&v.borrow()),
            Self::ProcessingInstruction(_, _) => {}
            Self::Comment(_) => {}
            Self::Raw(_) => {}
            Self::Doctype(_) => {}
        }
    }

    fn write_to<W: Write>(&self, writer: &mut W, mode: XMLWriteMode) -> io::Result<()> {
        match self {
            Self::InsertionPoint(_) => Ok(()),
            Self::Element(body) => body.write_to(writer, mode),
            Self::Text(text) => {
                for c in text.borrow().bytes() {
                    if c == b'&' {
                        writer.write_all(b"&amp;")
                    } else if c == b'<' {
                        writer.write_all(b"&lt;")
                    } else if c == b'>' {
                        writer.write_all(b"&gt;")
                    } else {
                        writer.write_all(&[c])
                    }?
                }
                Ok(())
            }
            Self::ProcessingInstruction(target, content) => {
                writer.write_fmt(format_args!("<?{} {}?>", target.borrow(), content.borrow()))
            }
            Self::Comment(text) => writer.write_fmt(format_args!("<!--{}-->", text.borrow())),
            Self::Raw(text) => writer.write_all(text.borrow().as_bytes()),
            Self::Doctype(text) => writer.write_fmt(format_args!("<!DOCTYPE {}>", text.borrow())),
        }
    }

    fn replace_pi_with_insertion_point(&self, context: &str, content: &str) -> Option<XMLTarget> {
        match self {
            Self::Element(elm) => elm.replace_pi_with_insertion_point(context, content),
            _ => None,
        }
    }
}

impl XMLNodeOps for XMLNodeList {
    fn text_into(&self, result: &mut String) {
        for v in self.borrow().iter() {
            v.text_into(result);
        }
    }

    fn write_to<W: Write>(&self, writer: &mut W, mode: XMLWriteMode) -> io::Result<()> {
        for child in self.borrow().iter() {
            child.write_to(writer, mode)?;
        }
        Ok(())
    }

    fn replace_pi_with_insertion_point(&self, context: &str, content: &str) -> Option<XMLTarget> {
        for v in self.0.borrow_mut().iter_mut() {
            let is_target = if let XMLNode::ProcessingInstruction(c, t) = &v {
                // eprintln!("test {} {} vs {} {}", c.borrow(), t.borrow(), context, content);
                c.borrow().eq(context) && t.borrow().eq(content)
            } else {
                false
            };
            if is_target {
                let identity = XMLInsertionPoint::new();
                *v = XMLNode::InsertionPoint(identity.clone());
                // bind with wrong index, that's fine, it'll adjust
                identity.bind(&self, 0);
                return Some(XMLTarget::IP(identity));
            } else {
                if let Some(result) = v.replace_pi_with_insertion_point(context, content) {
                    return Some(result);
                }
            }
        }
        None
    }
}

const VOID_ELEMENTS: &[&str] = &[
    "area", "base", "br", "col", "embed", "hr", "img", "input", "link", "meta", "param", "source",
    "track", "wbr",
];

impl XMLNodeOps for XMLElement {
    fn text_into(&self, result: &mut String) {
        self.content.text_into(result);
    }

    fn write_to<W: Write>(&self, writer: &mut W, mode: XMLWriteMode) -> io::Result<()> {
        writer.write_all(b"<")?;
        writer.write_all(self.name.borrow().as_bytes())?;
        for attr in self.attrs.borrow().iter() {
            writer.write_all(b" ")?;
            writer.write_all(attr.0.as_bytes())?;
            writer.write_all(b"=\"")?;
            for c in attr.1.bytes() {
                if c == b'&' {
                    writer.write_all(b"&amp;")
                } else if c == b'"' {
                    writer.write_all(b"&quot;")
                } else {
                    writer.write_all(&[c])
                }?
            }
            writer.write_all(b"\"")?;
        }
        let self_closing_tag = if mode == XMLWriteMode::XML {
            !self.deoptimized.get() && self.content.is_empty()
        } else {
            // in HTML, self closing tags are impossible except for void elements, which are ALWAYS self-closing
            let mut res = false;
            for v in VOID_ELEMENTS {
                if self.name.borrow().eq_ignore_ascii_case(v) {
                    res = true;
                    break;
                }
            }
            res
        };
        if self_closing_tag {
            writer.write_all(b"/>")
        } else {
            writer.write_all(b">")?;
            self.content.write_to(writer, mode)?;
            writer.write_all(b"</")?;
            writer.write_all(self.name.borrow().as_bytes())?;
            writer.write_all(b">")
        }
    }

    fn replace_pi_with_insertion_point(&self, context: &str, content: &str) -> Option<XMLTarget> {
        self.content
            .replace_pi_with_insertion_point(context, content)
    }
}

impl IntoIterator for XMLElement {
    type Item = XMLNode;
    type IntoIter = std::iter::Once<XMLNode>;
    fn into_iter(self) -> Self::IntoIter {
        std::iter::once(self.node())
    }
}

impl Display for XMLElement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "<{} {:?}>",
            &self.name.borrow(),
            self.content.as_ptr()
        ))?;
        self.content.fmt(f)?;
        f.write_fmt(format_args!("</{}>", &self.name.borrow()))
    }
}

/// Operators on XMLNodeList and XMLElement
pub trait XMLNodeListOps: Sized + XMLGenericListOps<XMLNode> + XMLPushable {
    /// Index of insertion point
    /// Notably, this also will automatically bind the point to this list if found.
    fn index_of_ip(&self, target: &XMLInsertionPoint) -> Option<usize> {
        // Check cache
        let list = self.get_children_xml_generic_list().borrow();
        let cache = target.cache.get();
        if cache < list.len() {
            if let XMLNode::InsertionPoint(p) = &list[cache] {
                if XMLInsertionPoint::ptr_eq(p, target) {
                    *target.parent.borrow_mut() = self.get_children_xml_generic_list().downgrade();
                    return Some(cache);
                }
            }
        }
        // Linear search
        for (k, v) in list.iter().enumerate() {
            if let XMLNode::InsertionPoint(p) = &v {
                if XMLInsertionPoint::ptr_eq(p, target) {
                    // Update cache
                    target.cache.set(k);
                    *target.parent.borrow_mut() = self.get_children_xml_generic_list().downgrade();
                    return Some(k);
                }
            }
        }
        None
    }

    /// Create insertion point at index
    fn insert_ip(&self, idx: usize) -> XMLTarget {
        let ip = XMLInsertionPoint::new();
        // insertion stickiness will take over and bind it for us
        self.insert(idx, XMLNode::InsertionPoint(ip.clone()));
        XMLTarget::IP(ip)
    }

    /// Insert before the given insertion point (asserted to be found)
    fn insert_before_ip(&self, target: &XMLInsertionPoint, nt: XMLNode) {
        let idx = self.index_of_ip(target).unwrap();
        self.insert(idx, nt);
        // Update cache
        // This is what actually turns the cache into an optimization, as this optimizes the case of repeated inserts onto the same insertion point.
        target.cache.set(idx + 1);
    }

    /// Insert after the given insertion point (asserted to be found)
    fn insert_after_ip(&self, target: &XMLInsertionPoint, nt: XMLNode) {
        let idx = self.index_of_ip(target).unwrap();
        self.insert(idx + 1, nt);
    }
}

impl XMLPushable for XMLNodeList {
    fn push_node(&self, nt: XMLNode) {
        self.append(nt);
    }

    fn push_ip(&self) -> XMLTarget {
        let ip = XMLInsertionPoint::new();
        self.push_node(XMLNode::InsertionPoint(ip.clone()));
        XMLTarget::IP(ip)
    }

    fn push_text_str(&self, text: &str) {
        let mut lst = self.get_children_xml_generic_list().borrow_mut();
        if let Some(XMLNode::Text(t)) = lst.last_mut() {
            t.replace_with(|v| format!("{}{}", v, text));
            return;
        }
        lst.push(XMLNode::new_text(text));
    }

    fn push_text_string(&self, text: String) {
        let mut lst = self.get_children_xml_generic_list().borrow_mut();
        if let Some(XMLNode::Text(t)) = lst.last_mut() {
            t.replace_with(|v| format!("{}{}", v, text));
            return;
        }
        lst.push(XMLNode::new_text(text));
    }
}

impl XMLGenericListOps<XMLNode> for XMLNodeList {
    fn get_children_xml_generic_list(&self) -> &RcCell<Vec<XMLNode>> {
        &self.0
    }

    fn on_entry_xml_generic_list(&self, elm: &XMLNode, index: usize) {
        if let XMLNode::InsertionPoint(node) = elm {
            node.bind(self.get_children_xml_generic_list(), index);
        }
    }
}

impl XMLNodeListOps for XMLNodeList {}

impl IntoIterator for XMLNodeList {
    type Item = XMLNode;
    type IntoIter = std::vec::IntoIter<XMLNode>;
    fn into_iter(self) -> Self::IntoIter {
        self.borrow().clone().into_iter()
    }
}
