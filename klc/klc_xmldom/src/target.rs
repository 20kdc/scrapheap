//! Abstraction for places you can push XML elements.

use std::fmt::Display;
use std::rc::Rc;

use crate::*;

/// Common functions between [XMLTarget] and [XMLElement].
///
/// This trait is deliberately object-safe (otherwise, you'd just use [XMLTarget] instead).
pub trait XMLPushable {
    /// Pushes a node
    fn push_node(&self, value: XMLNode);

    /// Pushes insertion point
    fn push_ip(&self) -> XMLTarget;

    /// Pushes text. This is a genericless version to ensure XMLPushable can be a trait-object.
    fn push_text_str(&self, value: &str);

    /// Pushes text. This is a genericless version to ensure XMLPushable can be a trait-object.
    fn push_text_string(&self, value: String);
}

// There used to be a thing called XMLInsertable. It's been replaced with IntoIterator -> Into<XMLNode>.
// This is important because it lets vecs & iterators join in the fun, which is good for elm! UX.
// The catch is that making everything work properly kinda messed up XMLNodeList...

/// XMLPushable functions which are generics and thus not object-safe.
/// This trait is used to attach them to all the relevant types.
pub trait XMLPushableEx: XMLPushable + Sized {
    /// Pushes an insertable
    fn push<I: Into<XMLNode>>(&self, src: impl IntoIterator<Item = I>) {
        for v in src.into_iter() {
            self.push_node(v.into())
        }
    }

    /// Add... ('builder-style')
    fn with<I: Into<XMLNode>>(self, src: impl IntoIterator<Item = I>) -> Self {
        for v in src.into_iter() {
            self.push_node(v.into())
        }
        self
    }
    /// Add text ('builder-style')
    fn with_text(self, v: impl Into<String>) -> Self {
        self.push_text(v);
        self
    }

    /// Pushes text node (with reduction in obvious cases)
    fn push_text(&self, text: impl Into<String>) {
        self.push_text_string(text.into())
    }
}

impl<T> XMLPushableEx for T where T: XMLPushable {}

/// A place XML nodes can be "written" to.
#[derive(Clone)]
pub enum XMLTarget {
    /// Nowhere - simply drop whatever's pushed.
    Nowhere,
    /// End of a node list.
    End(XMLNodeList),
    /// At an insertion point.
    IP(XMLInsertionPoint),
    /// Into a dynamic XMLPushable.
    Dynamic(Rc<dyn XMLPushable>),
}

impl Display for XMLTarget {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Nowhere => f.write_fmt(format_args!("XMLTarget::Nowhere")),
            Self::End(_) => f.write_fmt(format_args!("XMLTarget::End")),
            Self::IP(ipi) => f.write_fmt(format_args!("XMLTarget::IP({:?})", ipi.parent.as_ptr())),
            Self::Dynamic(_) => f.write_fmt(format_args!("XMLTarget::Dynamic")),
        }
    }
}

impl XMLPushable for XMLTarget {
    fn push_node(&self, value: XMLNode) {
        match self {
            Self::Nowhere => {}
            Self::End(nl) => nl.push_node(value),
            Self::IP(ip) => ip.push_node(value),
            Self::Dynamic(rc) => rc.push_node(value),
        }
    }

    fn push_ip(&self) -> XMLTarget {
        match self {
            Self::Nowhere => Self::Nowhere,
            Self::End(nl) => nl.push_ip(),
            Self::IP(ip) => ip.push_ip(),
            Self::Dynamic(rc) => rc.push_ip(),
        }
    }

    fn push_text_str(&self, text: &str) {
        match self {
            Self::Nowhere => {}
            Self::End(nl) => nl.push_text_str(text),
            Self::IP(ip) => ip.push_text_str(text),
            Self::Dynamic(dn) => dn.push_text_str(text),
        }
    }

    fn push_text_string(&self, text: String) {
        match self {
            Self::Nowhere => {}
            Self::End(nl) => nl.push_text_string(text),
            Self::IP(ip) => ip.push_text_string(text),
            Self::Dynamic(dn) => dn.push_text_string(text),
        }
    }
}

impl From<XMLNodeList> for XMLTarget {
    fn from(value: XMLNodeList) -> Self {
        XMLTarget::End(value)
    }
}

impl From<XMLElement> for XMLTarget {
    fn from(value: XMLElement) -> Self {
        XMLTarget::End(value.content)
    }
}

impl From<XMLInsertionPoint> for XMLTarget {
    fn from(value: XMLInsertionPoint) -> Self {
        XMLTarget::IP(value)
    }
}
