use std::ops::Deref;

use rccell::RcCell;

use crate::*;

/// XML attribute.
pub type XMLAttr = (String, String);

/// Similar to XMLNodeList but for attributes.
/// See [XMLGenericListOps] and [XMLAttrListOps].
pub type XMLAttrList = RcCell<Vec<XMLAttr>>;

/// Operators on [XMLAttrList] and [XMLElement].
/// Note this does not use the generic list trait.
/// This is because [XMLElement] shouldn't proxy the attributes as "children".
pub trait XMLAttrListOps: Sized {
    /// Gets the XMLGenericList that XMLGenericListOps applies to.
    fn get_children_xml_attr_list(&self) -> &RcCell<Vec<XMLAttr>>;

    /// Add attribute ('builder-style')
    fn with_attr(self, key: impl Into<String>, value: impl Into<String>) -> Self {
        self.attr_set(key, value);
        self
    }

    /// Gets an attribute.
    fn attr_get(&self, key: impl Deref<Target = str>) -> Option<String> {
        let al = self.get_children_xml_attr_list().borrow();
        for v in al.iter() {
            if v.0.eq(&*key) {
                return Some(v.1.clone());
            }
        }
        None
    }

    /// Gets an attribute or a default value
    fn attr_get_or(&self, key: impl Deref<Target = str>, val: impl Into<String>) -> String {
        let al = self.get_children_xml_attr_list().borrow();
        for v in al.iter() {
            if v.0.eq(&*key) {
                return v.1.clone();
            }
        }
        val.into()
    }

    /// Gets a boolean attribute.
    fn attr_get_flag(&self, key: impl Deref<Target = str>, def: bool) -> bool {
        if let Some(val) = self.attr_get(key) {
            if val.eq("0") {
                // i.e. `<video autoplay="">`
                false
            } else if val.eq("1") || val.eq("") {
                // i.e. `<video autoplay>`, `<video autoplay="1">`...
                true
            } else {
                def
            }
        } else {
            def
        }
    }

    /// Sets an attribute. This assumes there is at most one instance of the attribute, and will override it if found (otherwise appending).
    fn attr_set(&self, key: impl Into<String>, value: impl Into<String>) {
        self.attr_set3(key, value, true);
    }

    /// Sets an attribute.
    /// If not set to overwrite, and another instance of the attribute is found, will do nothing.
    /// If set to overwrite, assumes there is at most one instance, and will alter it.
    /// In either case, if no instance is found, will append.
    fn attr_set3(&self, key: impl Into<String>, value: impl Into<String>, overwrite: bool) {
        let key = key.into();
        let mut al = self.get_children_xml_attr_list().borrow_mut();
        for v in al.iter_mut() {
            if v.0.eq(&key) {
                if overwrite {
                    v.1 = value.into();
                }
                return;
            }
        }
        al.push((key, value.into()));
    }
}

impl XMLGenericListOps<XMLAttr> for XMLAttrList {
    fn get_children_xml_generic_list(&self) -> &XMLAttrList {
        self
    }
    fn on_entry_xml_generic_list(&self, _elm: &XMLAttr, _index: usize) {
        // nothing to do here
    }
}

impl XMLAttrListOps for XMLAttrList {
    fn get_children_xml_attr_list(&self) -> &RcCell<Vec<XMLAttr>> {
        self
    }
}
