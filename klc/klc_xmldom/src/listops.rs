use rccell::RcCell;

/// Generic operations on lists
pub trait XMLGenericListOps<E: Clone> {
    /// Gets the XMLGenericList that XMLGenericListOps applies to.
    /// Be careful with this, directly writing will skip on_entry_xml_generic_list
    fn get_children_xml_generic_list(&self) -> &RcCell<Vec<E>>;

    /// Handles any special logic on entry (insertion point stickiness).
    fn on_entry_xml_generic_list(&self, elm: &E, index: usize);

    /// Gets first element.
    fn first(&self) -> Option<E> {
        self.get_children_xml_generic_list()
            .borrow()
            .first()
            .cloned()
    }

    /// Gets last element.
    fn last(&self) -> Option<E> {
        self.get_children_xml_generic_list()
            .borrow()
            .last()
            .cloned()
    }

    /// Indexes (with panic on out of bounds).
    fn index(&self, idx: usize) -> E {
        self.get_children_xml_generic_list().borrow()[idx].clone()
    }

    /// Gets length
    fn len(&self) -> usize {
        self.get_children_xml_generic_list().borrow().len()
    }

    /// Gets if empty
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Insert at index
    fn insert(&self, idx: usize, nt: E) {
        self.on_entry_xml_generic_list(&nt, idx);
        self.get_children_xml_generic_list()
            .borrow_mut()
            .insert(idx, nt);
    }

    /// Remove
    fn remove(&self, idx: usize) -> E {
        self.get_children_xml_generic_list()
            .borrow_mut()
            .remove(idx)
    }

    /// Append node
    fn append(&self, nt: E) {
        let mut list = self.get_children_xml_generic_list().borrow_mut();
        self.on_entry_xml_generic_list(&nt, list.len());
        list.push(nt);
    }

    /// Vector of children
    fn collect(&self) -> Vec<E> {
        self.get_children_xml_generic_list().borrow().clone()
    }

    /// Index of node meeting predicate
    fn index_of<F: FnMut(&E) -> bool>(&self, mut target: F) -> Option<usize> {
        for (i, n) in self
            .get_children_xml_generic_list()
            .borrow()
            .iter()
            .enumerate()
        {
            if target(n) {
                return Some(i);
            }
        }
        None
    }

    /// Iterate over children
    fn foreach_v<F: FnMut(&E)>(&self, f: F) {
        self.get_children_xml_generic_list()
            .borrow()
            .iter()
            .for_each(f)
    }

    /// Iterate over children with indices
    fn foreach_kv<F: FnMut((usize, &E))>(&self, f: F) {
        self.get_children_xml_generic_list()
            .borrow()
            .iter()
            .enumerate()
            .for_each(f)
    }

    /// Iterate over children (error handling)
    fn foreach_v_r<F: FnMut(&E) -> Result<(), X>, X>(&self, mut f: F) -> Result<(), X> {
        for v in self.get_children_xml_generic_list().borrow().iter() {
            f(v)?
        }
        Ok(())
    }

    /// Iterate over children with indices (error handling)
    fn foreach_kv_r<F: FnMut((usize, &E)) -> Result<(), X>, X>(&self, mut f: F) -> Result<(), X> {
        for v in self
            .get_children_xml_generic_list()
            .borrow()
            .iter()
            .enumerate()
        {
            f(v)?
        }
        Ok(())
    }
}
