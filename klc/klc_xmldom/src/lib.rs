//! **I am aware `xot` exists. Furthermore, it is probably better. This library is kept in case I am unable to convert my code to `xot` due to various reasons (the main risk is namespace issues).**
//!
//! RcCell-based "DOM-ish" library for handling XML trees.
//!
//! This library aims to keep code clean and simple, without unnecessary "fluff" when outputting nodes or navigating the tree.
//!
//! For example, in `markup5ever_rcdom`, you might `node.children.borrow().iter().for_each(|v| {})`.
//!
//! This library:
//!
//! ```
//! # use klc_xmldom::*;
//! # let node = XMLElement::new("example").node();
//! node.unwrap_element().foreach_v(|v| {});
//! ```
//!
//! Notably, [XMLElement] and [XMLNode] can be interchanged relatively frictionlessly, and [XMLElement] shares the important [XMLNode] functions via the trait [XMLNodeOps], avoiding the need to assert a node's 'element-ness' without requiring carrying both handles.
//!
//! * `rcdom`: Disambiguating element types requires matching `node.data`, which is distinct from the node itself.
//! This isn't so bad, but it still can be a bit awkward.
//!   * This library: [XMLNode] is the enum.
//!     If you have already confirmed the node is an element, there's a dedicated type for that: [XMLElement], which can be converted back to [XMLNode] at any time using [XMLElement::node].
//!
//! * `rcdom`: Makes heavy use of `tendril`, making interactions with the Rust standard library awkward.
//!   * This library: Sticks to [String] and [&str]. Operations typically use [Into]\<[String]\> if they expect to require a [String] and rely on conversion via [&str] to make up the difference.
//! * `rcdom`: Insertion requires an existing node (which must be something concretely written into the file) as reference, or index tracking.
//!   * This library: [XMLTarget]s may be created. Corresponding nodes are created, but they do not affect the output in any way. [XMLTarget] is a unified type that can be used for both appending to an element and appending onto an insertion point.
//! * `rcdom`: Can have scary side effects for isolated trees due to the [Drop] implementation on Node. Expects a "true" DOM setup with a root node and a dedicated manager instance, and some functionality is not available without access to the RcDom instance.
//!   * This library: No central objects. Freely manipulate nodes. All the operations that work on the contents of an element are part of [XMLNodeListOps] - all the operations that work on the attributes of an element are part of [XMLAttrListOps]. It is possible to maintain and use an attribute list outside of an element; it is _expected_ to use an [XMLNodeList] as your root. Code does not need to worry about the "Document node special case" as there is no such thing.
//!
//! The cost of this API is that there is likely a lot of refcount-twiddling, and some `rcdom` features aren't supported, namely:
//!
//! * Tracking parents would mean introducing the "outer struct" layer similar to `rcdom`. This makes code more awkward.
//!   * The main argument for tracking parents has been mostly supplanted by insertion points, with the exception being node deletion. The expected workflow for this library is a "modifying deep clone", which simply does not copy nodes that would need to be erased later.
//!
//! It does not handle namespace resolution, as complicated situations can make getting the "right" XML output difficult (this was a problem that the author found with markup5ever's XML handling).
//!
//! The general assumption with this library is that you are handling 'relatively trusted' input (with fixed namespace prefixes, etc.) and need to create relatively "precise" output for applications which can't be trusted to do things correctly.
//!
//! In practice, you should be thinking along the lines of templating engines and other things that transform XML, but in a way that might be awkward to arrange outside of a general-purpose programming language such as Rust.
//!
//! `xmlparser` is used for parsing; if parsing isn't desired, then the dependency can be removed entirely.

mod listops;
pub use listops::*;

mod attr;
pub use attr::*;

mod node;
pub use node::*;

mod elem;
pub use elem::*;

mod ip;
pub use ip::*;

mod target;
pub use target::*;

#[cfg(feature = "json")]
pub mod json;

#[cfg(feature = "parser")]
pub mod parser;

// -- macros --

/// Fragment!
#[macro_export]
macro_rules! frg {
    ($($content:expr),*) => {
        {
            #[allow(unused_imports)]
            use $crate::{XMLAttrListOps, XMLPushableEx};
            $crate::XMLNodeList::new()
            $(
                .with($content)
            )*
        }
    };
}

/// Element!
#[macro_export]
macro_rules! elm {
    (< $tag:ident $($attr:ident = $value:tt )* > $($content:expr),*) => {
        {
            #[allow(unused_imports)]
            use $crate::{XMLAttrListOps, XMLPushableEx};
            $crate::XMLElement::new(stringify!($tag))
            $(
                .with_attr(stringify!($attr), $value)
            )*
            $(
                .with($content)
            )*
        }
    };
    (< $tag:literal $($attr:ident = $value:tt )* > $($content:expr),*) => {
        {
            #[allow(unused_imports)]
            use $crate::{XMLAttrListOps, XMLPushableEx};
            $crate::XMLElement::new($tag)
            $(
                .with_attr(stringify!($attr), $value)
            )*
            $(
                .with($content)
            )*
        }
    };
    (< $tag:ident $($attr:literal = $value:tt )* > $($content:expr),*) => {
        {
            #[allow(unused_imports)]
            use $crate::{XMLAttrListOps, XMLPushableEx};
            $crate::XMLElement::new(stringify!($tag))
            $(
                .with_attr($attr, $value)
            )*
            $(
                .with($content)
            )*
        }
    };
    (< $tag:literal $($attr:literal = $value:tt )* > $($content:expr),*) => {
        {
            #[allow(unused_imports)]
            use $crate::{XMLAttrListOps, XMLPushableEx};
            $crate::XMLElement::new($tag)
            $(
                .with_attr($attr, $value)
            )*
            $(
                .with($content)
            )*
        }
    };
}

// tests

#[cfg(test)]
mod tests;
