//! Uses [xmlparser] to make DOM trees.
//! Handles all the difficult stuff that library doesn't tell you about.
//!
//! ```
//! use klc_xmldom::*;
//! let res = parser::parse_xml("<example moon='&#127764;'>Thar&apos; be sharks in them hooves!</example>", true).unwrap();
//! assert_eq!(res.first().unwrap().unwrap_element().attr_get("moon").unwrap(), "🌔");
//! ```

use crate::*;
use std::error::Error;
use std::fmt::{Display, Formatter};
use xmlparser::{ElementEnd, StrSpan, TextPos, Token};

struct XMLParsingElementState {
    textpos: TextPos,
    id: String,
    attrs: Vec<XMLAttr>,
}

/// Mid-level XML parser state.
/// This 'mid-level parser' essentially exists to clean up usability issues with the xmlparser crate.
pub struct XMLMidLevelParser {
    element_state: Option<XMLParsingElementState>,
    pub parse_errors: Vec<ParseError>,
}

impl Default for XMLMidLevelParser {
    fn default() -> Self {
        XMLMidLevelParser {
            element_state: None,
            parse_errors: vec![],
        }
    }
}

/// Mid-level XML parser token.
#[derive(Clone, Debug)]
pub enum XMLMidLevelToken {
    ProcessingInstruction(String, String),
    Comment(String),
    Doctype(String),
    OpenTag(String, Vec<(String, String)>),
    CloseTag(String),
    EmptyTag(String, Vec<(String, String)>),
    Text(String),
}

impl XMLMidLevelParser {
    fn process_entities(&mut self, text: &str) -> String {
        let mut stream = xmlparser::Stream::from(text);
        let mut total = String::from("");
        // alright, here's where the stupid stuff happens
        while !stream.at_end() {
            if let Some(v) = stream.try_consume_reference() {
                match v {
                    xmlparser::Reference::Entity(_) => {
                        // oh.
                    }
                    xmlparser::Reference::Char(chr) => {
                        total.push(chr);
                    }
                }
            } else {
                total.push_str(&stream.consume_bytes(|_, c| c != b'&'));
            }
        }
        total
    }
    pub fn process_token(&mut self, pos: TextPos, token: Token) -> Option<XMLMidLevelToken> {
        if let Some(mut es) = self.element_state.take() {
            match token {
                Token::Attribute {
                    prefix,
                    local,
                    value,
                    ..
                } => {
                    es.attrs.push((
                        qualname_to_str(&prefix, &local),
                        self.process_entities(&value),
                    ));
                    self.element_state = Some(es);
                    None
                }
                Token::ElementEnd { end, .. } => match end {
                    ElementEnd::Open => {
                        self.element_state = None;
                        Some(XMLMidLevelToken::OpenTag(es.id, es.attrs))
                    }
                    ElementEnd::Empty => {
                        self.element_state = None;
                        Some(XMLMidLevelToken::EmptyTag(es.id, es.attrs))
                    }
                    _ => {
                        self.parse_errors.push(ParseError::new(
                            pos,
                            ParseErrorDetail::UnexpectedTokenTypeRegularInElement,
                        ));
                        self.element_state = None;
                        None
                    }
                },
                _ => {
                    self.parse_errors.push(ParseError::new(
                        pos,
                        ParseErrorDetail::UnexpectedTokenTypeRegularInElement,
                    ));
                    self.element_state = None;
                    None
                }
            }
        } else {
            match token {
                Token::Declaration { .. } => None,
                Token::ProcessingInstruction {
                    target, content, ..
                } => Some(XMLMidLevelToken::ProcessingInstruction(
                    target.to_string(),
                    content.unwrap_or("".into()).to_string(),
                )),
                Token::Comment { text, .. } => Some(XMLMidLevelToken::Comment(text.to_string())),
                Token::DtdStart { .. } => None,
                Token::EmptyDtd { name, .. } => Some(XMLMidLevelToken::Doctype(name.to_string())),
                Token::EntityDeclaration { .. } => None,
                Token::DtdEnd { .. } => None,
                Token::ElementStart { prefix, local, .. } => {
                    self.element_state = Some(XMLParsingElementState {
                        textpos: pos,
                        id: qualname_to_str(&prefix, &local),
                        attrs: vec![],
                    });
                    None
                }
                Token::Attribute { .. } => {
                    self.parse_errors.push(ParseError::new(
                        pos,
                        ParseErrorDetail::UnexpectedTokenTypeElementInRegular,
                    ));
                    None
                }
                Token::ElementEnd { end, .. } => match end {
                    ElementEnd::Close(local, prefix) => {
                        let tsn = qualname_to_str(&local, &prefix);
                        Some(XMLMidLevelToken::CloseTag(tsn))
                    }
                    _ => {
                        self.parse_errors.push(ParseError::new(
                            pos,
                            ParseErrorDetail::UnexpectedTokenTypeElementInRegular,
                        ));
                        None
                    }
                },
                Token::Text { text } => {
                    let str = self.process_entities(&text);
                    Some(XMLMidLevelToken::Text(str))
                }
                Token::Cdata { text, .. } => Some(XMLMidLevelToken::Text(text.to_string())),
            }
        }
    }
    pub fn end(&mut self) {
        if let Some(es) = &mut self.element_state {
            self.parse_errors.push(ParseError::new(
                es.textpos,
                ParseErrorDetail::EOFInAttributes,
            ));
        }
    }
}

/// XML-parsing token sink.
struct XMLParsingTokenSink {
    parents: Vec<XMLNodeList>,
    tag_stack_info: Vec<(TextPos, String)>,
    mid_level_parser: XMLMidLevelParser,
}

fn qualname_to_str(prefix: &StrSpan, local: &StrSpan) -> String {
    if prefix.is_empty() {
        local.to_string()
    } else {
        format!("{}:{}", prefix, local)
    }
}

impl XMLParsingTokenSink {
    fn get_current_parent(&self) -> XMLNodeList {
        self.parents.last().unwrap().clone()
    }
    fn append(&self, node: XMLNode) {
        self.get_current_parent().push_node(node)
    }
    fn append_text(&self, text: &str) {
        self.get_current_parent().push_text(text);
    }
    fn process_token(&mut self, pos: TextPos, token: Token) {
        match self.mid_level_parser.process_token(pos, token) {
            None => {
                // *shrug*
            }
            Some(mlt) => match mlt {
                XMLMidLevelToken::ProcessingInstruction(target, content) => {
                    self.append(XMLNode::new_pi(target, content));
                }
                XMLMidLevelToken::Comment(comment) => {
                    self.append(XMLNode::new_comment(comment));
                }
                XMLMidLevelToken::Doctype(doctype) => {
                    self.append(XMLNode::new_doctype(doctype));
                }
                XMLMidLevelToken::Text(text) => {
                    self.append_text(&text);
                }
                XMLMidLevelToken::OpenTag(text, attributes) => {
                    let elm = XMLElement::new(&text);
                    self.append(elm.node());
                    for attr in &attributes {
                        elm.attr_set(&attr.0, &attr.1);
                    }
                    elm.deoptimized.set(true);
                    self.parents.push(elm.content.clone());
                    self.tag_stack_info.push((pos, text));
                }
                XMLMidLevelToken::CloseTag(tsn) => {
                    if self.parents.len() > 1 {
                        let verify = self.tag_stack_info.pop().unwrap();
                        if !verify.1.eq(&tsn) {
                            self.mid_level_parser.parse_errors.push(ParseError::new(
                                pos,
                                ParseErrorDetail::StartEndMismatch(verify.1.clone(), tsn),
                            ));
                        }
                        self.parents.pop();
                    } else {
                        self.mid_level_parser
                            .parse_errors
                            .push(ParseError::new(pos, ParseErrorDetail::Underflow(tsn)));
                    }
                }
                XMLMidLevelToken::EmptyTag(text, attributes) => {
                    let elm = XMLElement::new(text);
                    self.append(elm.node());
                    for attr in attributes {
                        elm.attr_set(attr.0, attr.1);
                    }
                }
            },
        }
    }
    fn end(&mut self) {
        self.mid_level_parser.end();
        loop {
            let name = self.tag_stack_info.pop();
            if let Some(name) = name {
                self.mid_level_parser.parse_errors.push(ParseError::new(
                    name.0,
                    ParseErrorDetail::UnclosedTag(name.1),
                ));
            } else {
                break;
            }
        }
    }
}

/// XML parsing error detail.
/// Note: The library considers start/end tags that do not exactly match to be an error.
#[derive(Clone, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum ParseErrorDetail {
    /// Error from tokenizer.
    Tokenizer(String),
    /// Mismatch between start and end tags.
    StartEndMismatch(String, String),
    /// Close tag with empty stack.
    Underflow(String),
    /// Unclosed tag at exit
    UnclosedTag(String),
    /// Unexpected token type
    UnexpectedTokenTypeRegularInElement,
    /// Unexpected token type
    UnexpectedTokenTypeElementInRegular,
    /// Unknown entity reference.
    UnknownEntityReference(String),
    /// EOF in tag attributes.
    EOFInAttributes,
}

impl Display for ParseErrorDetail {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::EOFInAttributes => f.write_str("EOF in attributes"),
            Self::Tokenizer(detail) => f.write_str(detail),
            Self::StartEndMismatch(start, end) => {
                f.write_str("start `")?;
                f.write_str(start)?;
                f.write_str("` != end `")?;
                f.write_str(end)?;
                f.write_str("`")
            }
            Self::Underflow(tag) => {
                f.write_str("cannot close `")?;
                f.write_str(tag)?;
                f.write_str("`, no opening tag")
            }
            Self::UnclosedTag(tag) => {
                f.write_str("unclosed tag `")?;
                f.write_str(tag)?;
                f.write_str("`")
            }
            Self::UnexpectedTokenTypeRegularInElement => {
                f.write_str("unexpected token type: out-of-element token in element")
            }
            Self::UnexpectedTokenTypeElementInRegular => {
                f.write_str("unexpected token type: in-element token escaped")
            }
            Self::UnknownEntityReference(r) => {
                f.write_str("unknown entity reference: &")?;
                f.write_str(r)?;
                f.write_str(";")
            }
        }
    }
}

/// XML parsing error.
/// Note: The library considers start/end tags that do not exactly match to be an error.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ParseError {
    pub pos: TextPos,
    pub detail: ParseErrorDetail,
    preformatted: String,
}

impl ParseError {
    /// Creates a parse error from details, etc.
    pub fn new(pos: TextPos, detail: ParseErrorDetail) -> ParseError {
        let preformatted = format!("{}: {}", pos, detail);
        ParseError {
            pos,
            detail,
            preformatted,
        }
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.preformatted)
    }
}

impl Error for ParseError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
    fn description(&self) -> &str {
        &self.preformatted
    }
    fn cause(&self) -> Option<&dyn std::error::Error> {
        None
    }
}

pub type ParseResult = Result<XMLNodeList, Vec<ParseError>>;

/// Parses text.
pub fn parse_xml(text: &str, fragment: bool) -> ParseResult {
    let vec = XMLNodeList::default();
    let mut tokenizer = if fragment {
        xmlparser::Tokenizer::from_fragment(text, 0..text.len())
    } else {
        text.into()
    };
    let mut ts = XMLParsingTokenSink {
        parents: vec![vec.clone()],
        tag_stack_info: vec![],
        mid_level_parser: XMLMidLevelParser::default(),
    };
    loop {
        let pos = tokenizer.stream().gen_text_pos();
        let v = tokenizer.next();
        if let Some(v) = v {
            match v {
                Ok(tkn) => ts.process_token(pos, tkn),
                Err(err) => {
                    ts.mid_level_parser.parse_errors.push(ParseError::new(
                        pos,
                        ParseErrorDetail::Tokenizer(err.to_string()),
                    ));
                }
            }
        } else {
            break;
        }
    }
    ts.end();
    if ts.mid_level_parser.parse_errors.is_empty() {
        Ok(vec)
    } else {
        Err(ts.mid_level_parser.parse_errors)
    }
}

pub type ParseTokensResult = Result<Vec<XMLMidLevelToken>, Vec<ParseError>>;

/// Parses text into mid-level tokens.
/// This can be useful for embedded XHTML (in Markdown processing i.e. MeltedMD)
pub fn parse_xml_tokens(text: &str, fragment: bool) -> ParseTokensResult {
    let mut vec = Vec::default();
    let mut tokenizer = if fragment {
        xmlparser::Tokenizer::from_fragment(text, 0..text.len())
    } else {
        text.into()
    };
    let mut ts = XMLMidLevelParser::default();
    loop {
        let pos = tokenizer.stream().gen_text_pos();
        let v = tokenizer.next();
        if let Some(v) = v {
            match v {
                Ok(tkn) => {
                    if let Some(v) = ts.process_token(pos, tkn) {
                        vec.push(v);
                    }
                }
                Err(err) => {
                    ts.parse_errors.push(ParseError::new(
                        pos,
                        ParseErrorDetail::Tokenizer(err.to_string()),
                    ));
                }
            }
        } else {
            break;
        }
    }
    ts.end();
    if ts.parse_errors.is_empty() {
        Ok(vec)
    } else {
        Err(ts.parse_errors)
    }
}
