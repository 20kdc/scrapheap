use crate::{
    target::*, XMLAttr, XMLAttrList, XMLAttrListOps, XMLGenericListOps, XMLNode, XMLNodeList,
    XMLNodeListOps,
};
use rccell::RcCell;
use std::cell::Cell;
use std::rc::Rc;

/// This is an element, i.e. an open/close tag pair or an isolated tag.
#[derive(Clone, Debug)]
pub struct XMLElement {
    /// Node name.
    pub name: RcCell<String>,
    /// Attributes.
    pub attrs: XMLAttrList,
    /// Elements/text inside.
    /// This is separate because there's a utility in allowing independent references to content.
    pub content: XMLNodeList,
    /// If true, deliberately not written via the "isolated tag" shorthand.
    /// This is ignored when writing as HTML.
    pub deoptimized: Rc<Cell<bool>>,
}

impl XMLElement {
    /// New element (using String). See "with_*" functions
    pub fn new(name: impl Into<String>) -> XMLElement {
        XMLElement {
            name: RcCell::new(name.into()),
            attrs: XMLAttrList::new(Vec::new()),
            content: XMLNodeList::new(),
            deoptimized: Rc::new(Cell::new(false)),
        }
    }

    /// Gets name.
    pub fn name(&self) -> String {
        self.name.borrow().clone()
    }

    /// Sets name
    pub fn set_name(&self, value: impl Into<String>) {
        *self.name.borrow_mut() = value.into();
    }

    /// Clone and convert to [XMLNode]
    pub fn node(&self) -> XMLNode {
        XMLNode::Element(self.clone())
    }

    /// Name is equal to...
    pub fn is(&self, other: &str) -> bool {
        self.name.borrow().eq(other)
    }

    /// Deeply clones this element.
    pub fn deep_clone(&self) -> Self {
        let content = XMLNodeList::new();
        for v in self.content.borrow().iter() {
            content.push_node(v.deep_clone());
        }
        XMLElement {
            name: RcCell::new(self.name.borrow().clone()),
            attrs: RcCell::new(self.attrs.borrow().clone()),
            content,
            deoptimized: Rc::new(Cell::new(self.deoptimized.get())),
        }
    }

    /// Deeply clones this node without children.
    pub fn deep_clone_no_children(&self) -> Self {
        XMLElement {
            name: RcCell::new(self.name.borrow().clone()),
            attrs: RcCell::new(self.attrs.borrow().clone()),
            content: XMLNodeList::default(),
            deoptimized: Rc::new(Cell::new(self.deoptimized.get())),
        }
    }

    /// Checks pointer equality between elements.
    pub fn ptr_eq(a: &XMLElement, b: &XMLElement) -> bool {
        RcCell::ptr_eq(&a.name, &b.name)
    }
}

impl XMLPushable for XMLElement {
    fn push_node(&self, nt: XMLNode) {
        self.content.push_node(nt);
    }

    fn push_ip(&self) -> XMLTarget {
        self.content.push_ip()
    }

    fn push_text_str(&self, value: &str) {
        self.content.push_text_str(value)
    }

    fn push_text_string(&self, value: String) {
        self.content.push_text_string(value)
    }
}

impl XMLGenericListOps<XMLNode> for XMLElement {
    fn get_children_xml_generic_list(&self) -> &RcCell<Vec<XMLNode>> {
        &self.content.get_children_xml_generic_list()
    }
    fn on_entry_xml_generic_list(&self, elm: &XMLNode, index: usize) {
        self.content.on_entry_xml_generic_list(elm, index)
    }
}

impl XMLNodeListOps for XMLElement {}

impl XMLAttrListOps for XMLElement {
    fn get_children_xml_attr_list(&self) -> &RcCell<Vec<XMLAttr>> {
        &self.attrs
    }
}
