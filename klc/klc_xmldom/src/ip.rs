use std::cell::Cell;

use rccell::{RcCell, WeakCell};

use crate::{XMLNode, XMLNodeList, XMLNodeListOps, XMLPushable, XMLTarget};

/// Insertion point.
/// BE WARNED: This 'attaches' to the last XMLDOM place it's pushed.
/// Implicit to all this is the gotcha if lists are messed with "raw".
#[derive(Clone, Debug)]
pub struct XMLInsertionPoint {
    /// Also acts as identity due to the outer RcCell.
    pub parent: RcCell<WeakCell<Vec<XMLNode>>>,
    /// This is just a cache and can be freely lost or abused.
    pub cache: Cell<usize>,
}

impl XMLInsertionPoint {
    /// New insertion point.
    pub fn new() -> XMLInsertionPoint {
        XMLInsertionPoint {
            parent: RcCell::new(WeakCell::new()),
            cache: Cell::new(0),
        }
    }

    /// Identity equivalence.
    pub fn ptr_eq(a: &XMLInsertionPoint, b: &XMLInsertionPoint) -> bool {
        RcCell::ptr_eq(&a.parent, &b.parent)
    }

    /// Binds this insertion point to a list.
    pub fn bind(&self, to: &RcCell<Vec<XMLNode>>, index: usize) {
        *self.parent.borrow_mut() = to.downgrade();
        self.cache.set(index);
    }

    /// Verifies this insertion point is bound.
    pub fn is_bound(&self) -> bool {
        self.parent.borrow().upgrade().is_some()
    }
}

impl XMLPushable for XMLInsertionPoint {
    fn push_node(&self, value: XMLNode) {
        let upgraded = self.parent.borrow().upgrade();
        if let Some(target) = upgraded {
            let xnl = XMLNodeList::from_guts(target);
            xnl.insert_before_ip(self, value);
        }
    }

    fn push_ip(&self) -> XMLTarget {
        let upgraded = self.parent.borrow().upgrade();
        if let Some(target) = upgraded {
            let xnl = XMLNodeList::from_guts(target);
            let new_ip = XMLInsertionPoint::new();
            xnl.insert_before_ip(self, XMLNode::InsertionPoint(new_ip.clone()));
            XMLTarget::IP(new_ip)
        } else {
            XMLTarget::Nowhere
        }
    }

    fn push_text_str(&self, text: &str) {
        self.push_node(XMLNode::new_text(text));
    }

    fn push_text_string(&self, text: String) {
        self.push_node(XMLNode::new_text(text));
    }
}
