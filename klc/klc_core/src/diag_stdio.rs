use std::cell::{Cell, RefCell};

use crate::DiagLoggerImpl;

pub struct StdioDiag {
    pub scope_stack: RefCell<Vec<String>>,
    pub errors: Cell<bool>,
}

impl Default for StdioDiag {
    fn default() -> Self {
        Self::new()
    }
}

impl StdioDiag {
    pub fn new() -> StdioDiag {
        StdioDiag {
            scope_stack: RefCell::new(Vec::new()),
            errors: Cell::new(false),
        }
    }
    pub fn get_prefix(&self) -> String {
        let bref = self.scope_stack.borrow();
        match bref.last() {
            None => String::from(""),
            Some(x) => x.clone(),
        }
    }
}

impl DiagLoggerImpl for StdioDiag {
    fn enter(&self, text: &str) {
        let new_scope = match self.scope_stack.borrow().last() {
            None => String::from(text) + ": ",
            Some(x) => x.clone() + text + ": ",
        };
        self.scope_stack.borrow_mut().push(new_scope);
    }

    fn leave(&self) {
        self.scope_stack.borrow_mut().pop();
    }

    fn info(&self, text: String) {
        eprintln!("INFO: {}{}", self.get_prefix(), text);
    }

    fn warn(&self, text: String) {
        eprintln!("WARN: {}{}", self.get_prefix(), text);
    }

    fn error_text(&self, text: String) {
        eprintln!("ERR: {}{}", self.get_prefix(), text);
        self.errors.set(true);
    }

    fn has_any_error_occurred(&self) -> bool {
        self.errors.get()
    }
}
