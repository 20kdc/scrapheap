use rccell::RcCell;

/// DeepClone
pub trait DeepClone {
    /// Deep-clones this data.
    /// RcCell has its contents deep-cloned. If there are multiple references to the same RcCell, each reference becomes its own copy.
    fn deep_clone(&self) -> Self;
}

/// Implement DeepClone using Clone
pub trait CloneIsDeepClone: Clone {}

impl<T: CloneIsDeepClone> DeepClone for T {
    fn deep_clone(&self) -> Self {
        self.clone()
    }
}

/// Implement DeepClone on Strings
impl CloneIsDeepClone for String {}

/// Implement DeepClone on Vecs
impl<T: DeepClone> DeepClone for Vec<T> {
    fn deep_clone(&self) -> Self {
        self.iter().map(|e| e.deep_clone()).collect()
    }
}

/// Implement DeepClone on 2-tuples
impl<A: DeepClone, B: DeepClone> DeepClone for (A, B) {
    fn deep_clone(&self) -> Self {
        (self.0.deep_clone(), self.1.deep_clone())
    }
}

/// Implement DeepClone on RcCell
impl<T: DeepClone> DeepClone for RcCell<T> {
    fn deep_clone(&self) -> Self {
        RcCell::new(self.borrow().deep_clone())
    }
}

/// Extensions to RcCell for added convenience
pub trait RcCellExtensions<Data> {
    fn i(&self) -> core::cell::Ref<'_, Data>;
    fn m(&self) -> core::cell::RefMut<'_, Data>;
}

impl<Data> RcCellExtensions<Data> for RcCell<Data> {
    #[inline]
    fn i(&self) -> core::cell::Ref<'_, Data> {
        self.borrow()
    }
    #[inline]
    fn m(&self) -> core::cell::RefMut<'_, Data> {
        self.borrow_mut()
    }
}
