use std::{collections::HashSet, error::Error, fs::File, io::Read, path::Path};

use simple_error::*;

/// Diagnostic logger.
pub trait DiagLoggerImpl {
    /// Enter scope.
    fn enter(&self, text: &str);
    /// Leave scope (manual).
    fn leave(&self);

    /// log info, use with format!
    fn info(&self, text: String);
    /// log warning, use with format!
    fn warn(&self, text: String);
    /// log error (as text)
    fn error_text(&self, text: String);
    /// log error (as not text)
    /// this is assumed to terminate the error's life
    fn error(&self, err: SimpleError) {
        self.error_text(err.to_string());
    }
    /// log error (as not text)
    /// this is assumed to terminate the error's life
    fn error_opt(&self, err: Option<SimpleError>) {
        if let Some(err) = err {
            self.error(err);
        }
    }
    /// returns true if any error has been logged thus far
    fn has_any_error_occurred(&self) -> bool;
}
pub type DiagLogger = dyn DiagLoggerImpl;

/// Extension trait to make working with errors easier.
pub trait DiagAutoReportable<T> {
    /// Automatically reports any error to DiagLogger.
    fn report_to(self, diag: &DiagLogger) -> Option<T>;
}

impl<T, E: Error> DiagAutoReportable<T> for Result<T, E> {
    fn report_to(self, diag: &DiagLogger) -> Option<T> {
        match self {
            Self::Ok(v) => Some(v),
            Self::Err(e) => {
                diag.error(SimpleError::from(e));
                None
            }
        }
    }
}

/// Scope open/close handler
pub struct DiagScope<'a> {
    pub scope: &'a DiagLogger,
}

impl<'a> DiagScope<'a> {
    pub fn new(log: &'a DiagLogger, text: &str) -> DiagScope<'a> {
        log.enter(text);
        DiagScope { scope: log }
    }
}

impl<'a> Drop for DiagScope<'a> {
    fn drop(&mut self) {
        self.scope.leave()
    }
}

pub struct DepTracker {
    pub inputs: HashSet<String>,
    pub outputs: HashSet<String>,
}

impl Default for DepTracker {
    fn default() -> Self {
        DepTracker {
            inputs: HashSet::new(),
            outputs: HashSet::new(),
        }
    }
}

impl DepTracker {
    /// Adds an input to the dependency tracker.
    pub fn add_input(&mut self, input: &Path) {
        self.inputs.insert(
            std::fs::canonicalize(input)
                .unwrap_or_else(|_| panic!("Expected {}", input.to_string_lossy()))
                .to_string_lossy()
                .to_string(),
        );
    }
    /// Adds an output to the dependency tracker.
    pub fn add_output(&mut self, output: &Path) {
        self.outputs.insert(
            std::fs::canonicalize(output)
                .unwrap_or_else(|_| panic!("Expected {}", output.to_string_lossy()))
                .to_string_lossy()
                .to_string(),
        );
    }
    /// Open an existing file and log that as a dependency.
    pub fn open(&mut self, input: &Path) -> SimpleResult<File> {
        self.add_input(input);
        Ok(try_with!(
            File::open(input),
            "unable to open {}",
            input.to_string_lossy()
        ))
    }
    /// Create a new file and log that.
    pub fn create(&mut self, output: &Path) -> SimpleResult<File> {
        let file = File::create(output);
        // canonicalization will fail otherwise
        if file.is_ok() {
            self.add_output(output);
        }
        Ok(try_with!(
            file,
            "unable to create {}",
            output.to_string_lossy()
        ))
    }
    /// String from inputs.
    pub fn read_to_string(&mut self, path: &Path) -> SimpleResult<String> {
        self.add_input(path);
        Ok(try_with!(
            std::fs::read_to_string(path),
            "unable to read {}",
            path.to_string_lossy()
        ))
    }
    /// Bytes from inputs.
    pub fn read_to_end(&mut self, path: &Path) -> SimpleResult<Vec<u8>> {
        let mut contents = Vec::new();
        try_with!(
            try_with!(self.open(path), "open").read_to_end(&mut contents),
            "read"
        );
        Ok(contents)
    }
    /// List dependencies.
    pub fn deps(&self) -> String {
        let mut str = String::from("");
        for kv in self.outputs.iter().enumerate() {
            if kv.0 != 0 {
                str += " ";
            }
            str += kv.1;
        }
        str += ":";
        for v in self.inputs.iter() {
            str += " ";
            str += v;
        }
        str
    }
}
