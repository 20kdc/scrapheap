// Twily: Custom "Smart quotes"(ish) etc. implementation
// Implemented as an in-place modification on the MeltedMD AST.
// Notably, tag "twily-off" disables Twily, along with "builtin:code".

use crate::*;
use klc_core::*;

/// Core quoting rules.
fn twily_quoter(before_ws: bool, chr: char, after_ws: bool) -> char {
    if chr == '\'' && before_ws && !after_ws {
        '\u{2018}' // LQ
    } else if chr == '\'' && !before_ws {
        // lack of !after_ws condition so it works for apostrophe
        return '\u{2019}'; // RQ
    } else if chr == '"' && before_ws && !after_ws {
        return '\u{201C}'; // LQ
    } else if chr == '"' && !before_ws {
        return '\u{201D}'; // RQ
    } else {
        return chr;
    }
}

/// Core Twily algorithm.
/// Basically, less "aware" replacements.
/// It should be fine though.
pub fn twily_core(mut before: char, text: &str, afterx: char) -> String {
    let mut cv: Vec<char> = text.chars().collect();
    let mut i = 0;
    while i < cv.len() {
        let chr = cv[i];
        let after = if i == (cv.len() - 1) {
            afterx
        } else {
            cv[i + 1]
        };
        let before_ws = before.is_whitespace();
        let after_ws = after.is_whitespace();
        // figure out what to do
        cv[i] = twily_quoter(before_ws, chr, after_ws);
        // forward
        i += 1;
        before = chr;
    }
    // quote handling complete
    let str: String = cv.iter().collect();
    // dashes should need a space on one side to make them "complete"
    // this leads to consistent treatment should the algorithm ever be replaced
    str.replace("-- ", "— ")
        .replace(" --", " —")
        .replace("...", "…")
}

fn run_text_or_empty(text: Option<&MMDRun>) -> &str {
    if let Some(x) = text {
        if let MMDContent::Text(text) = &x.0 {
            text
        } else {
            ""
        }
    } else {
        ""
    }
}

fn first_char(text: &str) -> char {
    if let Some(cil) = text.chars().next() {
        cil
    } else {
        ' '
    }
}

fn last_char(text: &str) -> char {
    if let Some(cil) = text.chars().last() {
        cil
    } else {
        ' '
    }
}

/// Runs Twily on a run
pub fn twily_run(before_char: char, run: &mut MMDRun, after_char: char) {
    if run.1.tags.contains(&String::from(MMD_TAG_TWILYOFF))
        || run.1.tags.contains(&String::from(MMD_TAG_CODE))
    {
        return;
    }
    if let MMDContent::Text(text) = &mut run.0 {
        *text = twily_core(before_char, text, after_char);
    }
}

/// Runs Twily on a series of runs
pub fn twily_runs(runs: &mut Vec<MMDRun>) {
    for i in 0..runs.len() {
        let prev = if i == 0 { None } else { Some(&runs[i - 1]) };
        let before_char = last_char(run_text_or_empty(prev));
        let next = if i == runs.len() - 1 {
            None
        } else {
            Some(&runs[i + 1])
        };
        let after_char = first_char(run_text_or_empty(next));
        let run = &mut runs[i];
        twily_run(before_char, run, after_char);
    }
}

/// Runs Twily on a paragraph or similar
pub fn twily_para(para: &mut MMDPara) {
    match para {
        MMDPara::General(p) => twily_runs(p),
        MMDPara::Heading(p) => twily_runs(&mut p.text),
        MMDPara::Anchor(_) => {}
        MMDPara::FootnoteDefinition(p) => twily(&p.doc),
        MMDPara::List(_, c) => {
            for v in c {
                twily(v)
            }
        }
        MMDPara::Code(_, _, _) => {}
        MMDPara::Block(_, _, c) => twily(c),
        MMDPara::Cue(_, _) => {}
    }
}

/// Runs Twily on a whole document
pub fn twily(doc: &MMDDoc) {
    for v in doc.m().iter_mut() {
        twily_para(v);
    }
}
