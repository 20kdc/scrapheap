use klc_core::*;
use rccell::RcCell;

use crate::{MMDDoc, MMDHeading, MMDPara};

/// Outline (TOC data).
pub type MMDOutline = Vec<(Vec<String>, MMDHeading)>;

/// Extracts a TOC outline from the given document given a function to determine if a node should be hidden.
/// Note that hiding a node does not necessarily hide the child nodes. This, among other things, may lead to sharp 'jumps' in depth.
pub fn mmd_extract_outline(
    doc: &MMDDoc,
    hide: &mut impl FnMut(&[String], &MMDHeading) -> bool,
) -> MMDOutline {
    let mut outline = MMDOutline::new();
    mmd_extract_outline_into(&mut outline, doc, &vec![], hide);
    outline
}

fn mmd_extract_outline_into(
    outline: &mut MMDOutline,
    doc: &MMDDoc,
    context_path: &Vec<String>,
    hide: &mut impl FnMut(&[String], &MMDHeading) -> bool,
) {
    for v in doc.i().iter() {
        match &v {
            MMDPara::Heading(heading) => {
                if !hide(context_path, heading) {
                    outline.push((context_path.clone(), heading.clone()));
                }
            }
            MMDPara::General(_) => {}
            MMDPara::Anchor(_) => {}
            MMDPara::FootnoteDefinition(footnote) => {
                mmd_extract_outline_into(outline, &footnote.doc, context_path, hide)
            }
            MMDPara::List(_, contents) => {
                for v in contents {
                    mmd_extract_outline_into(outline, v, context_path, hide)
                }
            }
            MMDPara::Code(_, _, _) => {}
            MMDPara::Block(id, _, contents) => {
                let mut context_path = context_path.clone();
                context_path.push(id.clone());
                mmd_extract_outline_into(outline, contents, &context_path, hide)
            }
            MMDPara::Cue(_, _) => {}
        }
    }
}

pub type MMDTreeOutline = RcCell<Vec<MMDTreeOutlineNode>>;
#[derive(Clone)]
pub struct MMDTreeOutlineNode {
    pub heading: MMDHeading,
    pub children: MMDTreeOutline,
}

/// Converts the 'flat' outline to a 'tree' outline.
/// Attempts to account for weirdness like inconsistent depth (i.e. due to omitted nodes) to make a reasonably well-formed tree.
/// These issues can be reported, but also important is that they can be deliberately _not_ reported (i.e. if the omissions are intentional).
pub fn mmd_tree_outline(
    outline: &MMDOutline,
    report_inconsistencies: Option<&DiagLogger>,
) -> MMDTreeOutline {
    let root: MMDTreeOutline = MMDTreeOutline::default();
    let mut stack: Vec<(u8, MMDTreeOutline)> = Vec::new();
    for line in outline {
        let node = MMDTreeOutlineNode {
            heading: line.1.clone(),
            children: MMDTreeOutline::default(),
        };
        while stack.last().is_some_and(|v| v.0 >= line.1.depth) {
            stack.pop();
        }
        let mut parent = root.clone();
        let mut parent_depth: u8 = 0;
        if let Some(ab) = stack.last() {
            parent = ab.1.clone();
            parent_depth = ab.0;
        }
        if let Some(diag) = report_inconsistencies {
            if node.heading.depth != parent_depth + 1 {
                diag.warn(format!(
                    "TOC not well-formed (item '{}' had depth {} expected {})",
                    &line.1.outline_text,
                    line.1.depth,
                    parent_depth + 1
                ));
            }
        }
        parent.m().push(node.clone());
        stack.push((node.heading.depth, node.children));
    }
    root
}
