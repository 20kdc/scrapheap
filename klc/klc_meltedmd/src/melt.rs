use std::{
    cell::Cell,
    collections::{HashMap, VecDeque},
    sync::atomic::{AtomicU64, Ordering},
};

use klc_core::*;
use klc_xmldom::parser::XMLMidLevelToken;

use crate::*;

/// Unique ID source. Object-safe.
pub trait MMDUniqueIDSource {
    fn new_id(&self) -> u64;
}
impl MMDUniqueIDSource for AtomicU64 {
    fn new_id(&self) -> u64 {
        self.fetch_add(1, Ordering::Relaxed)
    }
}
impl MMDUniqueIDSource for Cell<u64> {
    fn new_id(&self) -> u64 {
        self.replace(self.get() + 1)
    }
}

/// Parse options.
pub fn mmd_parseoptions() -> markdown::ParseOptions {
    markdown::ParseOptions {
        constructs: MMD_CONSTRUCTS,
        gfm_strikethrough_single_tilde: false,
        math_text_single_dollar: false,
        ..markdown::ParseOptions::default()
    }
}

/// Global options for when melting.
#[derive(Clone, Copy)]
pub struct MMDMeltOptions<'unique, 'diag> {
    /// Base depth to add to header depths.
    pub base_depth: u8,
    /// Source of unique IDs for headers/etc.
    pub unique_id_source: &'unique dyn MMDUniqueIDSource,
    /// Diagnostic logger.
    pub diag: &'diag DiagLogger,
}

/// Convenience function to parse & melt MDAst into MeltedMD.
/// Saves on setting a markdown crate version in downstream crates...
pub fn mmd_parse_and_melt(source: impl AsRef<str>, opts: MMDMeltOptions) -> MMDDoc {
    let res = markdown::to_mdast(source.as_ref(), &mmd_parseoptions());
    match res {
        Err(err) => {
            opts.diag.error_text(err.to_string());
            MMDDoc::new(Vec::new())
        }
        Ok(ok) => mmd_melt(&ok, opts),
    }
}

fn mmd_convxml(text: &str, opts: MMDMeltOptions) -> Vec<XMLMidLevelToken> {
    let res = klc_xmldom::parser::parse_xml_tokens(text, true);
    match res {
        Err(errs) => {
            let scope = DiagScope::new(opts.diag, &format!("in XML fragment '{}'", text));
            for err in errs {
                opts.diag.error_text(err.to_string());
            }
            drop(scope);
            vec![]
        }
        Ok(tkns) => tkns,
    }
}

/// "Melts" MDAst into MeltedMD
pub fn mmd_melt(source: &markdown::mdast::Node, opts: MMDMeltOptions) -> MMDDoc {
    let doc = MMDDoc::default();
    let mut stack = vec![doc.clone()];
    let mut queue: VecDeque<markdown::mdast::Node> = VecDeque::new();
    let mut footnote_buffer: HashMap<String, MMDFootnote> = HashMap::new();
    fn target(stack: &Vec<MMDDoc>) -> &MMDDoc {
        stack.last().expect("block stack underflow")
    }
    fn write(stack: &Vec<MMDDoc>, obj: MMDPara) {
        target(stack).m().push(obj)
    }
    queue.push_front(source.clone());
    loop {
        match queue.pop_front() {
            Some(node) => match node {
                markdown::mdast::Node::Root(root) => {
                    // simply prepend to the queue as-is
                    for v in root.children.iter().rev() {
                        queue.push_front(v.clone());
                    }
                }
                markdown::mdast::Node::Html(html) => {
                    let res = mmd_convxml(&html.value, opts);
                    for tkn in res {
                        match tkn {
                            XMLMidLevelToken::Comment(_) => {}
                            XMLMidLevelToken::OpenTag(str, attrs) => {
                                // push block to stack
                                let doc = MMDDoc::default();
                                write(&stack, MMDPara::Block(str, attrs, doc.clone()));
                                stack.push(doc);
                            }
                            XMLMidLevelToken::EmptyTag(str, attrs) => {
                                if str.starts_with("cue-") {
                                    write(&stack, MMDPara::Cue(String::from(&str[4..]), attrs));
                                } else {
                                    write(
                                        &stack,
                                        MMDPara::General(vec![(
                                            MMDContent::Sigil(str, attrs),
                                            MMDRunOpts {
                                                tags: vec![],
                                                link: MMDLink::None,
                                            },
                                        )]),
                                    );
                                }
                            }
                            XMLMidLevelToken::CloseTag(_) => {
                                // pop block from stack
                                stack.pop();
                            }
                            _ => {
                                opts.diag.warn(format!("unknown para HTML {:?}", tkn));
                            }
                        }
                    }
                }
                _ => mmd_melt_inner(&mut target(&stack).m(), node, &mut footnote_buffer, opts),
            },
            None => break,
        }
    }
    doc
}

/// "Melts" MDAst into MeltedMD
fn mmd_melt_inner(
    target: &mut Vec<MMDPara>,
    source: markdown::mdast::Node,
    footnote_buffer: &mut HashMap<String, MMDFootnote>,
    opts: MMDMeltOptions,
) {
    match source {
        markdown::mdast::Node::Paragraph(contents) => {
            let runs = mmd_melt_paragraph(&contents.children, footnote_buffer, opts);
            target.push(MMDPara::General(runs));
        }
        markdown::mdast::Node::Heading(contents) => {
            let runs = mmd_melt_paragraph(&contents.children, footnote_buffer, opts);
            let text = mmd_textify_runs(&runs);
            target.push(MMDPara::Heading(MMDHeading {
                text: runs,
                depth: contents.depth.saturating_add(opts.base_depth),
                outline_text: text,
                unique: opts.unique_id_source.new_id(),
            }));
        }
        markdown::mdast::Node::List(list) => {
            let mut list_content = Vec::new();
            for v in list.children {
                let doc = MMDDoc::default();
                mmd_melt_inner(&mut doc.m(), v, footnote_buffer, opts);
                list_content.push(doc);
            }
            target.push(match list.start {
                Some(start) => MMDPara::List(Some(start), list_content),
                None => MMDPara::List(None, list_content),
            });
        }
        markdown::mdast::Node::ListItem(li) => {
            for v in li.children {
                mmd_melt_inner(target, v, footnote_buffer, opts);
            }
        }
        markdown::mdast::Node::Definition(def) => {
            target.push(MMDPara::Anchor(def.identifier.clone()));
        }
        markdown::mdast::Node::FootnoteDefinition(def) => {
            let list_content = MMDDoc::default();
            for v in def.children {
                mmd_melt_inner(&mut list_content.m(), v, footnote_buffer, opts);
            }
            let footnote = footnote_buffer.remove(&def.identifier);
            if let Some(footnote) = footnote {
                (*footnote.doc.m()) = list_content.i().clone();
                target.push(MMDPara::FootnoteDefinition(footnote.clone()));
            } else {
                opts.diag
                    .error_text(format!("Missing footnote: {}", def.identifier));
            }
        }
        markdown::mdast::Node::Code(def) => {
            target.push(MMDPara::Code(def.value, def.lang, def.meta));
        }
        markdown::mdast::Node::ThematicBreak(_) => {
            target.push(MMDPara::Cue(String::from(MMD_CUE_THEMATICBREAK), vec![]));
        }
        _ => {
            // !?!?!?
            opts.diag
                .warn(format!("Unable to melt (para level) {:?}", source));
        }
    }
}

fn mmd_melt_paragraph(
    source: &Vec<markdown::mdast::Node>,
    footnote_buffer: &mut HashMap<String, MMDFootnote>,
    melt_opts: MMDMeltOptions,
) -> Vec<MMDRun> {
    let mut runs = Vec::new();
    mmd_melt_paragraph_nodes(
        &mut runs,
        source,
        &MMDRunOpts::new(),
        footnote_buffer,
        melt_opts,
    );
    runs
}

fn mmd_melt_paragraph_nodes(
    target: &mut Vec<MMDRun>,
    source: &Vec<markdown::mdast::Node>,
    opts: &MMDRunOpts,
    footnote_buffer: &mut HashMap<String, MMDFootnote>,
    melt_opts: MMDMeltOptions,
) {
    let mut stack: Vec<MMDRunOpts> = Vec::new();
    // to support HTML tags, this has to hold the state and do toggles
    let mut mod_opts = opts.clone();
    for v in source {
        if let markdown::mdast::Node::Html(html) = v {
            let res = mmd_convxml(&html.value, melt_opts);
            for tkn in res {
                match tkn {
                    XMLMidLevelToken::Comment(_) => {}
                    XMLMidLevelToken::OpenTag(tag, _) => {
                        stack.push(mod_opts.clone());
                        if tag.eq("clear") {
                            mod_opts.tags.clear();
                            mod_opts.link = MMDLink::None;
                        } else {
                            mod_opts.toggle_tag(&tag);
                        }
                    }
                    XMLMidLevelToken::EmptyTag(tag, attrs) => {
                        // sigil
                        target.push((MMDContent::Sigil(tag.to_string(), attrs), mod_opts.clone()));
                    }
                    XMLMidLevelToken::CloseTag(_) => {
                        // pop block from stack
                        mod_opts = stack.pop().unwrap();
                    }
                    _ => {
                        melt_opts.diag.warn(format!("unknown para HTML {:?}", tkn));
                    }
                }
            }
        } else {
            mmd_melt_paragraph_node_not_html(target, v, &mod_opts, footnote_buffer, melt_opts);
        }
    }
}

fn mmd_melt_paragraph_node_not_html(
    target: &mut Vec<MMDRun>,
    source: &markdown::mdast::Node,
    opts: &MMDRunOpts,
    footnote_buffer: &mut HashMap<String, MMDFootnote>,
    melt_opts: MMDMeltOptions,
) {
    match source {
        markdown::mdast::Node::Text(text) => {
            target.push((MMDContent::Text(text.value.clone()), opts.clone()));
        }
        markdown::mdast::Node::Emphasis(em) => {
            let mut new_opts = opts.clone();
            new_opts.toggle_tag(MMD_TAG_ITALIC);
            mmd_melt_paragraph_nodes(target, &em.children, &new_opts, footnote_buffer, melt_opts);
        }
        markdown::mdast::Node::Strong(em) => {
            let mut new_opts = opts.clone();
            new_opts.toggle_tag(MMD_TAG_BOLD);
            mmd_melt_paragraph_nodes(target, &em.children, &new_opts, footnote_buffer, melt_opts);
        }
        markdown::mdast::Node::Delete(em) => {
            let mut new_opts = opts.clone();
            new_opts.toggle_tag(MMD_TAG_STRIKE);
            mmd_melt_paragraph_nodes(target, &em.children, &new_opts, footnote_buffer, melt_opts);
        }
        markdown::mdast::Node::InlineCode(em) => {
            let mut new_opts = opts.clone();
            new_opts.toggle_tag(MMD_TAG_CODE);
            target.push((MMDContent::Text(em.value.clone()), new_opts));
        }
        markdown::mdast::Node::Link(link) => {
            let mut new_opts = opts.clone();
            if link.url.starts_with('#') {
                new_opts.link = MMDLink::Anchor(String::from(&link.url[1..]));
                new_opts.ensure_tag(MMD_TAG_LINK_ANCHOR);
            } else {
                new_opts.link = MMDLink::Url(link.url.clone());
                new_opts.ensure_tag(MMD_TAG_LINK_URL);
            }
            mmd_melt_paragraph_nodes(
                target,
                &link.children,
                &new_opts,
                footnote_buffer,
                melt_opts,
            );
        }
        markdown::mdast::Node::Code(code) => {
            let mut new_opts = opts.clone();
            new_opts.ensure_tag(MMD_TAG_CODE);
            target.push((MMDContent::Text(code.value.clone()), new_opts));
        }
        markdown::mdast::Node::FootnoteReference(code) => {
            let mut new_opts = opts.clone();
            new_opts.ensure_tag(MMD_TAG_FOOTNOTEREF);
            let footnote_doc = MMDDoc::default();
            let footnote = MMDFootnote {
                unique: melt_opts.unique_id_source.new_id(),
                label: code.label.clone().unwrap_or(code.identifier.clone()),
                doc: footnote_doc.clone(),
            };
            footnote_buffer.insert(code.identifier.clone(), footnote.clone());
            new_opts.link = MMDLink::Footnote(footnote);
            target.push((
                MMDContent::Text(match &code.label {
                    Some(t) => t.clone(),
                    None => String::from(""),
                }),
                new_opts,
            ));
        }
        markdown::mdast::Node::Break(_) => target.push((
            MMDContent::Sigil(MMD_SIGIL_LINEBREAK.to_string(), vec![]),
            opts.clone(),
        )),
        markdown::mdast::Node::Image(img) => target.push((
            MMDContent::Sigil(
                MMD_SIGIL_IMG.to_string(),
                vec![
                    (MMD_SIGIL_ATTR_SRC.to_string(), img.url.clone()),
                    (MMD_SIGIL_ATTR_ALT.to_string(), img.alt.clone()),
                ],
            ),
            opts.clone(),
        )),
        _ => {
            // !?!?!?
            melt_opts
                .diag
                .warn(format!("Unable to melt (span level) {:?}", source));
        }
    }
}
