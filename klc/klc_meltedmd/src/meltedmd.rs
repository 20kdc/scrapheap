use klc_core::*;
use rccell::RcCell;

pub const MMD_CONSTRUCTS: markdown::Constructs = markdown::Constructs {
    attention: true,
    autolink: true,
    block_quote: false,
    character_escape: true,
    character_reference: true,
    code_indented: false,
    code_fenced: true,
    code_text: true,
    // used for anchors
    definition: true,
    frontmatter: false,
    gfm_autolink_literal: false,
    gfm_footnote_definition: true,
    gfm_label_start_footnote: true,
    gfm_strikethrough: true,
    gfm_table: true,
    gfm_task_list_item: false,
    hard_break_escape: true,
    hard_break_trailing: false,
    heading_atx: true,
    heading_setext: true,
    // converted to Context changes
    html_flow: true,
    // implemented as toggling span tags. these stack. within "<clear>", all tags and links are cleared.
    html_text: true,
    label_start_image: true,
    label_start_link: true,
    label_end: true,
    list_item: true,
    math_flow: false,
    math_text: false,
    mdx_esm: false,
    mdx_expression_flow: false,
    mdx_expression_text: false,
    mdx_jsx_flow: false,
    mdx_jsx_text: false,
    thematic_break: true,
};

// Tags are applied arbitrarily to spans.
pub const MMD_TAG_BOLD: &str = "b";
pub const MMD_TAG_ITALIC: &str = "i";
pub const MMD_TAG_STRIKE: &str = "s";
pub const MMD_TAG_CODE: &str = "code";
pub const MMD_TAG_LINK_URL: &str = "builtin:link-url";
pub const MMD_TAG_LINK_ANCHOR: &str = "builtin:link-anchor";
pub const MMD_TAG_FOOTNOTEREF: &str = "builtin:footnoteref";
pub const MMD_TAG_TWILYOFF: &str = "twily-off";

pub const MMD_CUE_THEMATICBREAK: &str = "builtin-hr";

pub const MMD_SIGIL_LINEBREAK: &str = "br";
pub const MMD_SIGIL_IMG: &str = "img";
pub const MMD_SIGIL_ATTR_SRC: &str = "src";
pub const MMD_SIGIL_ATTR_ALT: &str = "alt";

/// MeltedMD content is only text - for now
#[derive(Clone, Debug)]
pub enum MMDContent {
    Text(String),
    Sigil(String, Vec<(String, String)>),
}
impl CloneIsDeepClone for MMDContent {}

#[derive(Clone, Debug)]
pub struct MMDFootnote {
    pub unique: u64,
    pub label: String,
    pub doc: MMDDoc,
}
impl DeepClone for MMDFootnote {
    fn deep_clone(&self) -> Self {
        MMDFootnote {
            doc: self.doc.deep_clone(),
            ..self.clone()
        }
    }
}

#[derive(Clone, Debug)]
pub enum MMDLink {
    /// No link at all.
    None,
    /// Ordinary URL.
    Url(String),
    /// Anchor (converted from #blah)
    Anchor(String),
    /// Footnote reference
    /// The melter retroactively patches the contents of FootnoteDefinitions into the last use of the footnote.
    Footnote(MMDFootnote),
}
impl DeepClone for MMDLink {
    fn deep_clone(&self) -> Self {
        if let MMDLink::Footnote(f) = self {
            MMDLink::Footnote(f.deep_clone())
        } else {
            self.clone()
        }
    }
}

/// Inheritable properties of a Run
#[derive(Clone, Debug)]
pub struct MMDRunOpts {
    pub tags: Vec<String>,
    pub link: MMDLink,
}
impl DeepClone for MMDRunOpts {
    fn deep_clone(&self) -> Self {
        MMDRunOpts {
            tags: self.tags.clone(),
            link: self.link.deep_clone(),
        }
    }
}

impl MMDRunOpts {
    pub fn new() -> Self {
        Self {
            tags: Vec::new(),
            link: MMDLink::None,
        }
    }
    pub fn toggle_tag(&mut self, tag: &str) {
        let res = self.tags.iter().position(|p| p.eq(tag));
        match res {
            Some(idx) => {
                self.tags.remove(idx);
            }
            None => self.tags.push(String::from(tag)),
        }
    }
    pub fn ensure_tag(&mut self, tag: &str) {
        let res = self.tags.iter().position(|p| p.eq(tag));
        match res {
            Some(_idx) => {}
            None => self.tags.push(String::from(tag)),
        }
    }
}

/// MeltedMD text is made up of runs, each run having a set of tags and a content.
pub type MMDRun = (MMDContent, MMDRunOpts);

/// Heading. As a struct (for TOC extraction).
#[derive(Clone, Debug)]
pub struct MMDHeading {
    pub text: Vec<MMDRun>,
    pub depth: u8,
    pub outline_text: String,
    /// Unique ID for the heading.
    /// This is pretty much guaranteed to be unique *across the application's run*.
    /// (Unless your application runs for a very, very long time, on a very, very fast machine, anyway.)
    /// (Suffice to say you're not supposed to use this program that way.)
    pub unique: u64,
}
impl DeepClone for MMDHeading {
    fn deep_clone(&self) -> Self {
        MMDHeading {
            text: self.text.deep_clone(),
            ..self.clone()
        }
    }
}

/// Paragraphs are the main object of MeltedMD.
/// A MeltedMD document is a series of paragraphs.
#[derive(Clone, Debug)]
pub enum MMDPara {
    /// Any normal paragraph
    General(Vec<MMDRun>),
    /// Heading. These become part of the TOC.
    Heading(MMDHeading),
    /// Anchor (for interior linking: `#blah`)
    /// Uses definition syntax: `[myanchor]: (...)`
    Anchor(String),
    /// Footnote
    /// `[^footnoteexample]: text`
    /// id, label, contents
    FootnoteDefinition(MMDFootnote),
    /// List. If ordered, ordered_start will be Some. If unordered, None.
    /// ordered_start, contents
    List(Option<u32>, Vec<MMDDoc>),
    /// text, lang, meta
    Code(String, Option<String>, Option<String>),
    /// Block
    /// context, attrs, contents
    Block(String, Vec<(String, String)>, MMDDoc),
    /// A Cue is: `<cue-blah/>`
    Cue(String, Vec<(String, String)>),
}
impl DeepClone for MMDPara {
    fn deep_clone(&self) -> Self {
        match self {
            Self::General(p) => Self::General(p.deep_clone()),
            Self::Heading(h) => Self::Heading(h.deep_clone()),
            Self::FootnoteDefinition(f) => Self::FootnoteDefinition(f.deep_clone()),
            Self::List(i, v) => Self::List(*i, v.deep_clone()),
            Self::Block(s, a, d) => Self::Block(s.clone(), a.clone(), d.deep_clone()),
            _ => self.clone(),
        }
    }
}

/// A document is made up of paragraphs.
pub type MMDDoc = RcCell<Vec<MMDPara>>;

pub fn mmd_textify_runs(runs: &Vec<MMDRun>) -> String {
    let mut str = String::from("");
    for v in runs {
        if v.1.tags.contains(&String::from(MMD_TAG_TWILYOFF)) {
            continue;
        }
        match &v.0 {
            MMDContent::Text(text) => {
                str += text;
            }
            _ => {}
        }
    }
    str
}
