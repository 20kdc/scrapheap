#!/usr/bin/env python3

import os
import bpy
import bmesh
import mathutils
import re

bl_info = {
	"name": "Asset Reactor Toolkit",
	"version": (0, 1, 0),
	"blender": (3, 6, 0),
	"category": "Import-Export",
	"location": "Top bar, Scene properties",
	"description": "Simple 'universal Save & Export button'."
}

import bpy
import os

BRAND = bl_info["name"]
BRAND_SHORT = "ART"
LIGHTMAP_UV_CHANNEL_NAME = "UV1"

# Public API {

# -- Resolvers --

def resolve_object(id):
	"""
	If the given value is None, returns it.
	If the given value is an Object, returns it.
	Otherwise, looks the value up in bpy.data.objects.
	This is used for all pigline API functions as it helps keep things convenient.
	"""
	if id is None:
		return id
	if isinstance(id, bpy.types.Object):
		return id
	return bpy.data.objects[id]

def resolve_collection(id):
	"""
	If the given value is None, returns it.
	If the given value is a Collection, returns it.
	Otherwise, looks the value up in bpy.data.collections.
	This is used for all pigline API functions as it helps keep things convenient.
	"""
	if id is None:
		return id
	if isinstance(id, bpy.types.Collection):
		return id
	return bpy.data.collections[id]

def resolve_regex(id):
	"""
	If the given value is None, returns it.
	If the given value is a re.Pattern, returns it.
	Otherwise, compiles it.
	This is used for all pigline API functions as it helps keep things convenient.
	"""
	if id is None:
		return id
	if isinstance(id, re.Pattern):
		return id
	return re.compile(id)

# -- State Manipulation --

def guard():
	"""
	Bails if we are not reasonably likely to be working on a scratch file.
	"""
	assert bpy.context.scene.pigline_original_file != ""

def select(active = None, *elements):
	"""
	Sets the selection to exactly these objects. (See resolve_object.)
	The first object becomes the active object.
	"""
	bpy.ops.object.select_all(action = "DESELECT")
	for v in elements:
		resolve_object(v).select_set(True)
	active = resolve_object(active)
	if not (active is None):
		active.select_set(True)
	bpy.context.view_layer.objects.active = resolve_object(active)

def get_selection():
	"""
	Returns an array 'selection' which can be passed as 'select(*selection)' to restore the selection state.
	"""
	return [bpy.context.object] + list(bpy.context.selected_objects)

# -- Data Manipulation --

def join(active, *elements, merge = None):
	"""
	Performs a join into the first object from the other objects provided. (See resolve_object.)
	This is an *extremely* common 'basic task' for Pigline as it's critical to draw call reduction.
	If 'merge' is provided, then vertex merging will be performed.
	"""
	active = resolve_object(active)
	select(active, *elements)
	bpy.ops.object.join()
	if not (merge is None):
		if active.active_shape_key_index > 0:
			active.active_shape_key_index = 0
		bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)
		bpy.ops.object.editmode_toggle()
		bm = bmesh.from_edit_mesh(active.data)
		bmesh.ops.remove_doubles(bm, verts = bm.verts, dist = merge)
		bmesh.update_edit_mesh(active.data, loop_triangles = True, destructive = True)
		bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)

def apply_modifiers(target, do_single_user = True):
	"""
	Selects an object, single_user's it (by default) and applies all modifiers.
	REQUIRES that you have ApplyModifiersWithShapeKeys installed!
	Has a check in case of no modifiers (the operator will otherwise usually fail).
	single_user is used to prevent a footgun with linked Meshes.
	"""
	target = resolve_object(target)
	if do_single_user:
		single_user(target)
	else:
		select(target)
	if len(target.modifiers) > 0:
		bpy.ops.amwsk.apply_all_modifiers()

def recursively_instance(elements):
	"""
	Functional form of bpy.ops.object.duplicates_make_real().
	Recursive, too.
	"""
	# expand for instancing
	instancing_unchecked = elements
	output = []
	while True:
		instance_these = []
		for v in instancing_unchecked:
			obj = resolve_object(v)
			if obj.instance_type != "NONE":
				instance_these.append(obj)
		if len(instance_these) == 0:
			break
		instancing_unchecked = []
		select(*instance_these)
		bpy.ops.object.duplicates_make_real()
		for v in bpy.context.selected_objects:
			output.append(v)
			instancing_unchecked.append(v)
	return output

def apply_join(active, *elements, merge = None, do_single_user = True):
	"""
	Similar to join, but runs apply_modifiers (with do_single_user either specified or True) on each element first.
	Does NOT run apply_modifiers on the active target; this is intentional as it allows you to apply a more 'global' modifier stack.
	In addition, performs bpy.ops.object.duplicates_make_real where relevant.
	"""
	elements = list(elements) + recursively_instance(elements)
	for v in elements:
		apply_modifiers(v, do_single_user = single_user)
	join(active, *elements, merge = merge)

def apply_join_collection(target, collection):
	"""
	Performs apply_join on the contents of a collection.
	"""
	collection = resolve_collection(collection)
	apply_join(target, *collection.all_objects)

def single_user(target):
	"""
	Shorthand to make the object's data single-user.
	Like a lot of these operations, leaves the target selected, assumed to be in the current scene.
	"""
	select(target)
	bpy.ops.object.make_single_user(obdata = True)

def object_del(target):
	"""
	Shorthand to delete an object.
	"""
	target = resolve_object(target)
	bpy.data.objects.remove(target)

def object_move_to_collection(target, collection):
	"""
	Shorthand to move an object to solely be in the given collection.
	"""
	target = resolve_object(target)
	for v in list(target.users_collection):
		v.objects.unlink(target)
	collection = resolve_collection(collection)
	collection.objects.link(target)

def collection_objects(collection):
	"""
	Gets all objects in the given collection (see resolve_collection)
	"""
	return list(resolve_collection(collection).all_objects)

def run_join_tree(collection):
	"""
	Executes a join tree, which is assumed to be in the current scene.
	A join tree is a collection with no objects and a set of child collections.
	Each child collection becomes one mesh object inside the join tree.
	"""
	collection = resolve_collection(collection)
	for subcollection in collection.children:
		target = bpy.data.objects.new(subcollection.name, bpy.data.meshes.new(subcollection.name))
		collection.objects.link(target)
		apply_join_collection(target, subcollection)

# -- Shape Keys --

def shapekey_ensure_basis(target):
	"""
	Ensures the basis shape key exists on the target. Leaves the target selected.
	"""
	target = resolve_object(target)
	select(target)
	if target.data.shape_keys is None:
		# need to do something
		target.shape_key_add(name = "Basis")

def shapekey_create_mix(target, name):
	"""
	Creates a shapekey from the current mix (see shapekey_set_values).
	Leaves the target selected and the new key active.
	"""
	target = resolve_object(target)
	shapekey_ensure_basis(target)
	bpy.ops.object.shape_key_add(from_mix = True)
	new_key = target.data.shape_keys.key_blocks[target.active_shape_key_index]
	new_key.name = name

def shapekey_create_transform(target, name, matrix):
	"""
	Creates a shapekey from the given matrix.
	Leaves the target selected and the new key active.
	"""
	target = resolve_object(target)
	shapekey_ensure_basis(target)
	bpy.ops.object.shape_key_add(from_mix = False)
	new_key = target.data.shape_keys.key_blocks[target.active_shape_key_index]
	new_key.name = name
	bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)
	bpy.ops.object.editmode_toggle()
	bm = bmesh.from_edit_mesh(target.data)
	bm.transform(mathutils.Matrix.Diagonal((0.0, 0.0, 0.0, 1.0)))
	bmesh.update_edit_mesh(target.data, loop_triangles = False, destructive = False)
	bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)

def shapekey_create_hide(target, name):
	"""
	Creates a 'hide' shapekey that scales the mesh's vertices towards the object's origin.
	Leaves the target selected and the new key active.
	"""
	shapekey_create_transform(target, name, mathutils.Matrix.Diagonal((0.0, 0.0, 0.0, 1.0)))

def shapekey_select(target, name):
	"""
	Selects an object and a shape key inside it.
	"""
	target = resolve_object(target)
	select(target)
	target.active_shape_key_index = target.data.shape_keys.key_blocks.find(name)

def shapekey_del(target, name):
	"""
	Selects an object and deletes a shapekey.
	"""
	shapekey_select(target, name)
	bpy.ops.object.shape_key_remove()

def shapekey_set_values(target, values = {}):
	"""
	Resets all shape key values.
	This can be useful when setting up mixes.
	Does not change the selection/active object.
	"""
	target = resolve_object(target)
	if target.data.shape_keys is None:
		return
	for block in target.data.shape_keys.key_blocks:
		v = 0.0
		if block.name in values:
			v = values[block.name]
		block.value = v

def shapekey_rename(target, f, t):
	"""
	Renames a shapekey.
	Does not change the selection/active object.
	"""
	target = resolve_object(target)
	target.data.shape_keys.key_blocks[f].name = t

def shapekey_rename_batch(target, regex, replacement):
	"""
	Batch-renames shapekeys in the target object with the given regex.
	Does not change the selection/active object.
	"""
	regex = resolve_regex(regex)
	target = resolve_object(target)
	if target.data.shape_keys is None:
		return
	do_rename = []
	for key in target.data.shape_keys.key_blocks.keys():
		key_new = regex.sub(replacement, key)
		if key_new != key:
			do_rename.append([key, key_new])
	for ent in do_rename:
		shapekey_rename(target, ent[0], ent[1])

# -- Material patching --

def material_ensure_bake_image_node(material, image):
	"""
	Ensures an image bake target node exists in the material pointing at the given image.
	"""
	image_node = material.node_tree.nodes.new(type = "ShaderNodeTexImage")
	image_node.image = image
	material.node_tree.nodes.active = image_node

# }

# Operators {

class _PiglineOpExport(bpy.types.Operator):
	bl_idname = "pigline.export"
	bl_label = BRAND_SHORT + ": Save/Export"
	bl_description = "Saves & exports using the current export script."

	def execute(self, context):
		if context.scene.pigline_script_id is None:
			raise Exception("Pigline: Script not selected in Scene settings.")
		# if this errors, we DON'T want to continue. period
		bpy.ops.wm.save_mainfile()
		original_filepath = bpy.data.filepath
		do_reload = context.scene.pigline_reload
		context.scene.pigline_original_file = original_filepath
		adj_path = bpy.data.filepath + ".pigline_delme"
		# have to save again to make sure that we don't damage the user's data
		# I'd really like to figure out how to do this in a way which doesn't thrash the disk needlessly...
		bpy.ops.wm.save_as_mainfile(filepath = adj_path, relative_remap = False, check_existing = False)
		try:
			os.unlink(bpy.data.filepath)
		except:
			pass
		# final check
		assert context.scene.pigline_original_file != bpy.data.filepath
		# alright, we're safe. do what we've gotta do
		bpy.ops.pigline.cleanup()
		with context.temp_override(edit_text = context.scene.pigline_script_id):
			bpy.ops.text.run_script()
		# try copying original filename to *context* scene (scene switching could have happened)
		try:
			bpy.context.window.scene.pigline_original_file = original_filepath
		except:
			pass
		# now revert if asked
		if context.scene.pigline_reload:
			bpy.ops.wm.open_mainfile(filepath = original_filepath)
		return {"FINISHED"}

class _PiglineOpCleanup(bpy.types.Operator):
	bl_idname = "pigline.cleanup"
	bl_label = BRAND_SHORT + ": Cleanup"
	bl_description = "Cleans up the initial state of the editor. This helps to ensure consistency in Pigline scripts."

	def execute(self, context):
		if not (context.active_object is None):
			bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)
		bpy.ops.object.select_all(action = "DESELECT")
		context.scene.tool_settings.transform_pivot_point = "MEDIAN_POINT"
		return {"FINISHED"}

class _PiglineOpReload(bpy.types.Operator):
	bl_idname = "pigline.reload"
	bl_label = BRAND_SHORT + ": Reload"
	bl_description = "Reloads original file."

	def execute(self, context):
		bpy.ops.wm.open_mainfile(filepath = context.scene.pigline_original_file)
		return {"FINISHED"}

class _PiglineOpShapeKeyMakeTop(bpy.types.Operator):
	bl_idname = "pigline.shape_key_make_basis"
	bl_label = BRAND_SHORT + ": Make Shape Key Basis"
	bl_description = "Copies the current shape key to the basis & does necessary fixup."

	@classmethod
	def poll(self, context):
		return (not (context.object is None)) and context.object.mode == "OBJECT" and (not (context.object.data.shape_keys is None)) and context.object.active_shape_key_index >= 0

	def execute(self, context):
		obj = context.object
		new_basis_index = obj.active_shape_key_index
		old_basis = obj.data.shape_keys.key_blocks[0]
		new_basis = obj.data.shape_keys.key_blocks[new_basis_index]
		# so originally, this was done the *right* way (resorting & relative_key editing). but avytest broke ;.;
		# then I tried to fix it... that ALSO didn't work.
		# there seems to be some kinda quantum entanglement going on between shape keys.
		# this entanglement breaks the join() operation.
		# oddly, this only happened with avytest, which makes reproducing it REALLY awkward.
		# it's so bad it can actually merge shape keys between mesh objects(!)
		# so to make it work:
		# 1. We overwrite the basis with the new basis. Notably, we can still recover the original basis...
		# 2. We subtract the new basis from itself * 2. The first is to undo itself, and the second is to undo the transformation on the original basis.
		# 3. We've got what we wanted; old_basis is now exactly equal to new_basis and new_basis has been inverted.
		obj.active_shape_key_index = 0
		bpy.ops.object.editmode_toggle()
		bpy.ops.mesh.select_mode(type = "VERT")
		bpy.ops.mesh.select_all(action = "SELECT")
		bpy.ops.mesh.blend_from_shape(shape = new_basis.name, blend = 1.0, add = False)
		bpy.ops.object.editmode_toggle()
		obj.active_shape_key_index = new_basis_index
		bpy.ops.object.editmode_toggle()
		bpy.ops.mesh.select_mode(type = "VERT")
		bpy.ops.mesh.select_all(action = "SELECT")
		bpy.ops.mesh.blend_from_shape(shape = new_basis.name, blend = -2.0, add = True)
		bpy.ops.object.editmode_toggle()
		return {"FINISHED"}

class _PiglineOpCreateLightmapImage(bpy.types.Operator):
	bl_idname = "pigline.create_lightmap_image"
	bl_label = BRAND_SHORT + ": Create Lightmap Image"
	bl_description = "Associates a newly made lightmap image with a set of objects at Object.pigline_lightmap."
	bl_options = {"REGISTER", "UNDO"}

	size: bpy.props.IntProperty(name = "Size", default = 1024, description = "Texture size")
	prefix: bpy.props.StringProperty(name = "Prefix", default = "", description = "Prefix for IDs")

	def execute(self, context):
		new_image = bpy.data.images.new(self.prefix + "PiglineLightmap", self.size, self.size, float_buffer = True)
		for obj in list(context.selected_objects):
			obj.pigline_lightmap = new_image
		return {"FINISHED"}

class _PiglineOpUICreateLightmapImage(bpy.types.Panel):
	bl_idname = "pigline_PT_create_lightmap_image"
	bl_label = "Create Lightmap Image"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Tool"

	def draw(self, context):
		self.layout.operator("pigline.create_lightmap_image")

class _PiglineOpAddLightmapUVs(bpy.types.Operator):
	bl_idname = "pigline.add_lightmap_uvs"
	bl_label = BRAND_SHORT + ": Add Lightmap UVs"
	bl_description = "Creates or re-unwraps a UV layer specifically called " + LIGHTMAP_UV_CHANNEL_NAME + ", using Smart UV Project. This layer is selected for editing & baking. Can apply to multiple objects."
	bl_options = {"REGISTER", "UNDO"}

	# Common Lightmap UV properties
	delete_uvs: bpy.props.BoolProperty(name = "Delete UVs", default = False, description = "Delete UV maps")
	island_margin: bpy.props.FloatProperty(name = "UV Margin", default = 0.01, description = "As in Smart UV Project")
	area_weight: bpy.props.FloatProperty(name = "Area Weight", default = 0.0, description = "As in Smart UV Project")

	def execute(self, context):
		# setup all objects
		for v in list(context.selected_objects):
			if self.delete_uvs:
				while len(v.data.uv_layers) > 0:
					v.data.uv_layers.remove(obj.data.uv_layers[0])
			# ensure the layer exists and is active
			uv_layer = None
			uv_layer_index = v.data.uv_layers.find(LIGHTMAP_UV_CHANNEL_NAME)
			if uv_layer_index == -1:
				uv_layer = v.data.uv_layers.new(name = LIGHTMAP_UV_CHANNEL_NAME)
			else:
				uv_layer = v.data.uv_layers[LIGHTMAP_UV_CHANNEL_NAME]
			v.data.uv_layers.active = uv_layer
			# lightmap UV layers exist to be rendered to
			uv_layer.active_render = True
		# project into all objects at once (creating a shared UV set)
		bpy.ops.object.mode_set(mode = "EDIT", toggle = False)
		bpy.ops.mesh.select_mode(type = "FACE")
		bpy.ops.mesh.select_all(action = "SELECT")
		bpy.ops.uv.smart_project(island_margin = self.island_margin, area_weight = self.area_weight, margin_method = "FRACTION", correct_aspect = False, scale_to_bounds = False)
		bpy.ops.object.mode_set(mode = "OBJECT", toggle = False)
		return {"FINISHED"}

class _PiglineOpUIAddLightmapUVs(bpy.types.Panel):
	bl_idname = "pigline_PT_add_lightmap_uvs"
	bl_label = "Add Lightmap UVs"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Tool"

	def draw(self, context):
		self.layout.operator("pigline.add_lightmap_uvs")

class _PiglineOpLightmapPatchMaterials(bpy.types.Operator):
	bl_idname = "pigline.bake_patch_materials"
	bl_label = BRAND_SHORT + ": Patch Materials For Baking"
	bl_description = "Ensures your materials point at pigline_lightmap images. Frankly, this is an awful workaround for bad Blender UX that makes a mess of your materials and should only really be used from Pigline scripts unless you're poking it to debug it."
	bl_options = {"REGISTER", "UNDO"}

	def execute(self, context):
		bpy.ops.object.make_local(type = "SELECT_OBDATA_MATERIAL")
		bpy.ops.object.make_single_user(obdata = True, material = True)
		for obj in list(context.selected_objects):
			for slot in obj.material_slots:
				slot.material = self._do_patch(obj, slot.material)
			for slot in range(len(obj.data.materials)):
				obj.data.materials[slot] = self._do_patch(obj, obj.data.materials[slot])
		return {"FINISHED"}

	def _do_patch(self, obj, material):
		if material is None:
			# need to create a dummy material or else baking will consider the object invalid
			material = bpy.data.materials.new("PiglineBakeFixDummy")
			material.use_nodes = True
		if not ("PiglineBakeTargetImageNode" in material.node_tree.nodes):
			target = material.node_tree.nodes.new("ShaderNodeTexImage")
			target.name = "PiglineBakeTargetImageNode"
		target = material.node_tree.nodes["PiglineBakeTargetImageNode"]
		target.image = obj.pigline_lightmap
		material.node_tree.nodes.active = target
		return material

class _PiglineOpSetupLightmapMaterial(bpy.types.Operator):
	bl_idname = "pigline.setup_lightmap_material"
	bl_label = BRAND_SHORT + ": Make Lightmap S2A/Test Material"
	bl_description = "Creates a simple emissive material with accompanying image node (from active object's pigline_lightmap). By default, unwraps the mesh (this can be disabled)."
	bl_options = {"REGISTER", "UNDO"}

	size: bpy.props.IntProperty(name = "Size", default = 1024, description = "Texture size")
	nearest: bpy.props.BoolProperty(name = "Nearest", default = False, description = "Nearest-neighbour in preview")

	prefix: bpy.props.StringProperty(name = "Prefix", default = "", description = "Prefix for IDs")

	def execute(self, context):
		"""
		This operator is expected to be used one of two ways:
		1. Selected-to-Active baking: A proxy object is made and this operator is used to prep it for Selected-to-Active baking.
		   It's kind of like moulding; we discard the mould to get the finished product.
		2. Direct baking: The baking is done beforehand.
		"""
		new_material = bpy.data.materials.new(self.prefix + "PiglineLightmapMaterial")
		new_material.use_nodes = True
		new_material.use_backface_culling = True
		new_material.node_tree.nodes.clear()
		uvmap_node = new_material.node_tree.nodes.new(type = "ShaderNodeUVMap")
		image_node = new_material.node_tree.nodes.new(type = "ShaderNodeTexImage")
		emission_node = new_material.node_tree.nodes.new(type = "ShaderNodeEmission")
		output_node = new_material.node_tree.nodes.new(type = "ShaderNodeOutputMaterial")
		new_material.node_tree.links.new(uvmap_node.outputs["UV"], image_node.inputs["Vector"])
		new_material.node_tree.links.new(image_node.outputs["Color"], emission_node.inputs["Color"])
		new_material.node_tree.links.new(emission_node.outputs["Emission"], output_node.inputs["Surface"])
		new_material.node_tree.nodes.active = image_node
		image_node.name = "PiglineLMImageNode"
		image_node.image = context.object.pigline_lightmap
		if self.nearest:
			image_node.interpolation = "Closest"
		for obj in list(context.selected_objects):
			for slot in obj.material_slots:
				slot.material = new_material
			for slot in range(len(obj.data.materials)):
				obj.data.materials[slot] = new_material
		# now that we've either made UV1 *or* it's expected to have been made during add_lightmap_uvs elsewhere (i.e. before the preview proxy was created)
		uvmap_node.uv_map = LIGHTMAP_UV_CHANNEL_NAME
		return {"FINISHED"}

class _PiglineOpUISetupLightmapMaterial(bpy.types.Panel):
	bl_idname = "pigline_PT_setup_lightmap_material"
	bl_label = "Make Lightmap S2A/Test Material"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Tool"

	def draw(self, context):
		self.layout.operator("pigline.setup_lightmap_material")

# }

def _pigline_upper_bar(self, context):
	if context.region.alignment == "RIGHT":
		if context.scene.pigline_original_file != "":
			self.layout.operator("pigline.reload")
		else:
			self.layout.operator("pigline.export")
		self.layout.prop(context.scene, "pigline_script_id", text = "")
		self.layout.prop(context.scene, "pigline_reload")

class _SCENE_PT_ScenePanelPigline(bpy.types.Panel):
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_context = "scene"
	bl_label = BRAND
	bl_options = {"DEFAULT_CLOSED"}

	def draw(self, context):
		self.layout.prop(context.scene, "pigline_original_file")
		if context.scene.pigline_original_file != "":
			self.layout.operator("pigline.reload")

class _OBJECT_PT_ObjectPanelPigline(bpy.types.Panel):
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_context = "object"
	bl_label = BRAND
	bl_options = {"DEFAULT_CLOSED"}

	def draw(self, context):
		self.layout.prop(context.object, "pigline_lightmap")
		self.layout.operator("pigline.create_lightmap_image")
		self.layout.operator("pigline.add_lightmap_uvs")
		self.layout.operator("pigline.setup_lightmap_material")

# Register/Unregister
def register():
	"""
	Internal (BPY Interface)
	"""
	bpy.types.Scene.pigline_original_file = bpy.props.StringProperty(name = "Original File", default = "", subtype = "FILE_PATH", description = "Original (pre-export) file location for reload.")
	bpy.types.Scene.pigline_script_id = bpy.props.PointerProperty(name = "Export Script", type = bpy.types.Text, description = "Save & Export script")
	bpy.types.Scene.pigline_reload = bpy.props.BoolProperty(name = "Reload", default = True, description = "Automatically reloads once done.")

	bpy.types.Object.pigline_lightmap = bpy.props.PointerProperty(name = "Lightmap", type = bpy.types.Image, description = "Associated lightmap. " + BRAND_SHORT + " operators use this to setup materials.")

	bpy.types.TOPBAR_HT_upper_bar.append(_pigline_upper_bar)
	bpy.utils.register_class(_PiglineOpExport)
	bpy.utils.register_class(_PiglineOpCleanup)
	bpy.utils.register_class(_PiglineOpReload)
	bpy.utils.register_class(_PiglineOpShapeKeyMakeTop)
	bpy.utils.register_class(_PiglineOpCreateLightmapImage)
	bpy.utils.register_class(_PiglineOpUICreateLightmapImage)
	bpy.utils.register_class(_PiglineOpAddLightmapUVs)
	bpy.utils.register_class(_PiglineOpUIAddLightmapUVs)
	bpy.utils.register_class(_PiglineOpLightmapPatchMaterials)
	bpy.utils.register_class(_PiglineOpSetupLightmapMaterial)
	bpy.utils.register_class(_PiglineOpUISetupLightmapMaterial)
	bpy.utils.register_class(_SCENE_PT_ScenePanelPigline)
	bpy.utils.register_class(_OBJECT_PT_ObjectPanelPigline)

def unregister():
	"""
	Internal (BPY Interface)
	"""
	bpy.utils.unregister_class(_PiglineOpExport)
	bpy.utils.unregister_class(_PiglineOpCleanup)
	bpy.utils.unregister_class(_PiglineOpReload)
	bpy.utils.unregister_class(_PiglineOpShapeKeyMakeTop)
	bpy.utils.unregister_class(_PiglineOpCreateLightmapImage)
	bpy.utils.unregister_class(_PiglineOpUICreateLightmapImage)
	bpy.utils.unregister_class(_PiglineOpAddLightmapUVs)
	bpy.utils.unregister_class(_PiglineOpUIAddLightmapUVs)
	bpy.utils.unregister_class(_PiglineOpLightmapPatchMaterials)
	bpy.utils.unregister_class(_PiglineOpSetupLightmapMaterial)
	bpy.utils.unregister_class(_PiglineOpUISetupLightmapMaterial)
	bpy.utils.unregister_class(_SCENE_PT_ScenePanelPigline)
	bpy.utils.unregister_class(_OBJECT_PT_ObjectPanelPigline)

if __name__ == "__main__":
	register()

