# The Asset Reactor Toolkit

The Asset Reactor Toolkit's job is to provide a simple feature that Blender is missing in game export usecases: _Save, Optimize, Export, Revert._

Many 'new' model formats fail at trivial optimizations that have been solved since the days of `.smd` files. The formats are too complex and attempt to preserve details that *shouldn't* be preserved. The results cost performance that lower-end devices cannot spare.

In addition, some environments (such as VRChat) create particularly awkward requirements (i.e. blendshape toggles) that just make things unnecessarily difficult.

Therefore, it'd be really useful for non-destructive workflows if it was possible to automate the destructive tasks to occur solely at export time.

Well, that's possible. Blender is scriptable. But the Blender scripting system has a few UX issues that I want to get rid of, and as far as I'm aware, there's no convenient universal standard for The Button, so I'm writing my own.

**Basically, the Asset Reactor Toolkit's job is to add a convenient, easy-to-access 'save and export' button which runs a script and tries to ensure it won't accidentally overwrite something important. It also adds some supporting script APIs to cover some common tasks.**

## Example Script

```python
import bpy
import pigline
pigline.guard()

# https://github.com/SAM-tak/ApplyModifiersWithShapeKeys
#  provides bpy.ops.amwsk.apply_all_modifiers()

# Let's assume we have objects BodyL and BodyR.
# We'd like these two objects to share data during editing.
# But on export, we need to cook the whole thing into a single object.
# Plus, sometimes we need left/right specific stuff in shape keys.
pigline.single_user("BodyR")

# The testcase doesn't actually have modifiers on the body right now.
# Really, you'd have at least a fixup modifier
#  for the mirrored linked duplicate.
# pigline.select("BodyL")
# bpy.ops.amwsk.apply_all_modifiers()
# pigline.select("BodyR")
# bpy.ops.amwsk.apply_all_modifiers()

# Now, we want to make these into left/right shape keys.
# You can think of this as similar to vertex group mirroring.
pigline.shapekey_rename_batch("BodyR", "\\.L$", ".R")

# Join the two halves & rename.
pigline.join("BodyL", "BodyR", merge = 0.01)
bpy.data.objects["BodyL"].name = "Body"

# Create unified keys.
# shapekey_set_values resets unspecified values to 0.
pigline.shapekey_set_values("Body", {"Growl.L": 1.0, "Growl.R": 1.0})
pigline.shapekey_create_mix("Body", "Growl")

# Let's say that we don't use Growl.L alone, so remove it for optimization reasons
pigline.shapekey_del("Body", "Growl.L")

# Done.
pigline.shapekey_set_values("Body")

# Merge in an "Alert" emote.
# This workflow is great for VRC toggles.
pigline.select("Alert")
bpy.ops.amwsk.apply_all_modifiers()
pigline.shapekey_create_hide("Alert", "alert_hide")

pigline.join("Body", "Alert")
bpy.ops.export_scene.fbx(filepath = bpy.path.abspath("//untitled.fbx"))
```

A more advanced example, covering world creation, is in `blender-asset-catalog/catalog/core.blend`.

## Why not implement a proper addon to do whatever baking/export task you need?

The problem is, in essence: It is _impossible_ to anticipate all user requirements, and trying to do so anyway leads to users bouncing off of your project due to an 'overcomplicated' interface. Trying to do this in _Blender_ of all applications is just asking for trouble.

## What's up with the lightmapping operators?

There are four lightmapping operators:

1. `pigline.create_lightmap_image`: Creates a square float-buffer image of the given size. Sets a property (`pigline_lightmap`) on all objects to hold this image.
2. `pigline.add_lightmap_uvs`: Adds a UV layer called `UV1` and sets it for rendering. Can be set to delete other layers (i.e. for emissive-only Selected to Active).
3. `pigline.bake_patch_materials`: Single-users all materials, then ensures that `PiglineDummyImageNode` exists in the material and is active. Sets the image node's target to the object's lightmap.
4. `pigline.setup_lightmap_material`: Sets up an emissive material with the lightmap image in all material slots of all selected objects. Leaves the image node selected.
