class_name B2SpkicoButton
extends Button

func setup(writer: String):
	text = B2DBOps.speaker_icon(writer)
	add_color_override("font_color", B2DBOps.speaker_colour(writer))
	add_font_override("font", HiDPIManager.font_ico)
