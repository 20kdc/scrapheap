extends Button

export var start_closed: bool
export var prefix: String = ""
onready var _linked: Control = get_parent().get_node(name + "_tray")

func _ready():
	if start_closed:
		_linked.visible = false
	_update_text()

func _pressed():
	_linked.visible = not _linked.visible
	_update_text()

func _update_text():
	if _linked.visible:
		text = prefix + "<"
	else:
		text = prefix + ">"
