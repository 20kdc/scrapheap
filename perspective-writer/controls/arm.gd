extends CheckBox

onready var _linked: BaseButton = get_parent().get_node(name + "_btn")

func _ready():
	_pressed()

func _pressed():
	_linked.set_disabled(not pressed)
