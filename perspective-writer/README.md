# Perspective Writer

Perspective Writer is a writing tool. It allows defining "speakers" with different icons and colours, which can be switched between.

It is mainly intended for use on tablets.

It's been pulled out of a personal repository for public use, so it might be a mess.

Perspective Writer is licensed under the Unlicense. (see `COPYING`, or <https://unlicense.org/>)
