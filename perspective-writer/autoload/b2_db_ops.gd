extends Node

func migrate():
	var db_version = B2DB.data_get("db_version", 0)
	print("db_version: " + str(db_version))
	if db_version == 0:
		print("Migration: 0 -> 2 (Init)")
		B2DB.data_set("speaker_list", [
			"red",
			"orange",
			"yellow",
			"green",
			"cyan",
			"blue",
			"lavender",
			"magenta",
			"white",
			# out-of-band / messages
			"announcer"
		])

		B2DB.data_set("speaker.icon.red", "1")
		B2DB.data_set("speaker.icon.orange", "2")
		B2DB.data_set("speaker.icon.yellow", "3")
		B2DB.data_set("speaker.icon.green", "4")
		B2DB.data_set("speaker.icon.cyan", "5")
		B2DB.data_set("speaker.icon.blue", "6")
		B2DB.data_set("speaker.icon.lavender", "7")
		B2DB.data_set("speaker.icon.magenta", "8")
		B2DB.data_set("speaker.icon.white", "9")
		B2DB.data_set("speaker.icon.announcer", "*")

		B2DB.data_set("speaker.colour.red", Color(1, 0.501961, 0.501961).to_html())
		B2DB.data_set("speaker.colour.orange", Color(1, 0.756863, 0.501961).to_html())
		B2DB.data_set("speaker.colour.yellow", Color(1, 0.984314, 0.501961).to_html())
		B2DB.data_set("speaker.colour.green", Color(0.501961, 1, 0.509804).to_html())
		B2DB.data_set("speaker.colour.cyan", Color(0.501961, 1, 1).to_html())
		B2DB.data_set("speaker.colour.blue", Color(0.501961, 0.6, 1).to_html())
		B2DB.data_set("speaker.colour.lavender", Color(0.768627, 0.501961, 1).to_html())
		B2DB.data_set("speaker.colour.magenta", Color(0.984314, 0.501961, 1).to_html())
		B2DB.data_set("speaker.colour.white", Color.white.to_html())
		B2DB.data_set("speaker.colour.announcer", Color.white.to_html())

		# announcer special props
		B2DB.data_set("speaker.log_prefix.announcer", "* ")
		B2DB.data_set("speaker.log_suffix.announcer", "")
		B2DB.data_set("speaker.font.announcer", "mono")
		B2DB.data_set("speaker.align.announcer", RichTextLabel.ALIGN_CENTER)

		# superscript abuse
		B2DB.data_set("speaker.prefixes.red", "¹")
		B2DB.data_set("speaker.prefixes.orange", "²")
		B2DB.data_set("speaker.prefixes.yellow", "³")
		B2DB.data_set("speaker.prefixes.green", "⁴\n€")
		B2DB.data_set("speaker.prefixes.cyan", "⁵\n½")
		B2DB.data_set("speaker.prefixes.blue", "⁶\n¾")
		B2DB.data_set("speaker.prefixes.lavender", "⁷\n{")
		B2DB.data_set("speaker.prefixes.magenta", "⁸\n[")
		B2DB.data_set("speaker.prefixes.white", "⁹\n]")

		db_version = 2
	if db_version == 1:
		print("Migration: 1 -> 2 (...)")
		var dictionary = JSON.parse(B2DB.get_json()).result
		var dictionary_new := {}
		for k in dictionary.keys():
			var replk = k
			if k != "db_version":
				replk = "speaker" + k.substr(9)
			dictionary_new[replk] = dictionary[k]
		B2DB.install_json(JSON.print(dictionary_new))
		db_version = 2
	B2DB.data_set("db_version", db_version)

func speaker_list() -> PoolStringArray:
	return PoolStringArray(B2DB.data_get("speaker_list", []))

func speaker_colour(id: String) -> Color:
	return Color(B2DB.data_get("speaker.colour." + id, "ffffffff"))

# we need this feature for colourblind usage
# but also we don't have a config interface yet
# let's just do what we can
func speaker_icon(id: String) -> String:
	return B2DB.data_get("speaker.icon." + id, "?")

func speaker_example_text(id: String) -> String:
	return B2DB.data_get("speaker.example." + id, "tenpo pini wan la ale li pona")

# Bonfire 1 used prefixes, now a UI is used
# BUT prefix/suffix logic is here
func speaker_prefixes(id: String) -> String:
	return B2DB.data_get("speaker.prefixes." + id, "")
func speaker_suffixes(id: String) -> String:
	return B2DB.data_get("speaker.suffixes." + id, "")

func speaker_log_prefix(id: String) -> String:
	return B2DB.data_get("speaker.log_prefix." + id, id + ": ")

func speaker_log_suffix(id: String) -> String:
	return B2DB.data_get("speaker.log_suffix." + id, "")

func speaker_font_hint(id: String) -> String:
	return B2DB.data_get("speaker.font." + id, "serif")

func speaker_align(id: String) -> int:
	return B2DB.data_get("speaker.align." + id, RichTextLabel.ALIGN_LEFT)

func speaker_has_switch(id: String) -> bool:
	return B2DB.data_get("speaker.switch_button." + id, true)

func speaker_save(id: String, colour: Color, icon: String, example_text: String, prefixes: String, suffixes: String, align: int, font: String, lp: String, ls: String, switch_button: bool):
	var members := speaker_list()
	if not members.has(id):
		members.push_back(id)
		B2DB.data_set("speaker_list", Array(members))
	B2DB.data_set("speaker.colour." + id, colour.to_html())
	B2DB.data_set("speaker.icon." + id, icon)
	B2DB.data_set("speaker.example." + id, example_text)
	B2DB.data_set("speaker.prefixes." + id, prefixes)
	B2DB.data_set("speaker.suffixes." + id, suffixes)
	B2DB.data_set("speaker.align." + id, align)
	B2DB.data_set("speaker.font." + id, font)
	B2DB.data_set("speaker.log_prefix." + id, lp)
	B2DB.data_set("speaker.log_suffix." + id, ls)
	B2DB.data_set("speaker.switch_button." + id, switch_button)

func speaker_del(id: String):
	# :(
	var members := speaker_list()
	var idx := members.find(id)
	if idx != -1:
		members.remove(idx)
		B2DB.data_set("speaker_list", Array(members))
	B2DB.data_erase("speaker.colour." + id)
	B2DB.data_erase("speaker.icon." + id)
	B2DB.data_erase("speaker.example." + id)
	B2DB.data_erase("speaker.prefixes." + id)
	B2DB.data_erase("speaker.align." + id)
	B2DB.data_erase("speaker.font." + id)
	B2DB.data_erase("speaker.log_prefix." + id)
	B2DB.data_erase("speaker.log_suffix." + id)

# --- log ---
# the log is represented in a rather dumb way
# this allows for arbitrary operations but also is kind of bad
# translation to/from DLCBonfireEntry happens here

func log_list() -> PoolStringArray:
	return PoolStringArray(B2DB.data_get("log_list", ["default"]))

func log_current_get() -> String:
	return B2DB.data_get("log_current", "default")

func log_current_set(id: String):
	B2DB.data_set("log_current", id)

func log_get(id: String) -> Array:
	var transformed = []
	for v in B2DB.data_get("log." + id, []):
		var ent := DLCBonfireEntry.new()
		ent.writer = v[0]
		ent.text = v[1]
		transformed.push_back(ent)
	return transformed

func log_set(id: String, data: Array):
	var members := log_list()
	if not members.has(id):
		members.invert()
		members.push_back(id)
		members.invert()
		B2DB.data_set("log_list", Array(members))
	# --
	var transformed = []
	for v in data:
		transformed.push_back([v.writer, v.text])
	B2DB.data_set("log." + id, transformed)

func log_del(id: String):
	var members := log_list()
	var idx := members.find(id)
	if idx != -1:
		members.remove(idx)
		B2DB.data_set("log_list", Array(members))
	B2DB.data_erase("log." + id)
