extends Node

var file_prefix = "./"

func attempt_init() -> bool:
	if OS.get_name() == "Android":
		file_prefix = "/sdcard/bonfire2/"
		# prepare prefix
		var dir = Directory.new()
		dir.make_dir_recursive(file_prefix)
	elif OS.get_name() == "HTML5":
		file_prefix = "user://"
	elif not OS.has_feature("editor"):
		file_prefix = OS.get_executable_path().get_base_dir() + "/"
	print("file prefix: " + file_prefix)
	# check the target is writable
	var tf := File.new()
	if tf.open(file_prefix + ".file-access-test", File.WRITE) != OK:
		return false
	tf.close()
	return true
