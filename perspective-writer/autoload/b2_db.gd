extends Node

## If compaction looks like a good idea
var ought_to_compact = false

## Config
var _ready := false
var _dirty := {}

var _base_path := ""
var _base_file := File.new()

## Real data
var _real_data := {}

signal changed()

## Run once we know we can access data to setup the DB.
func open() -> bool:
	_base_path = B2Storage.file_prefix + "bonfire2.jsonl"
	if _base_file.open(_base_path, File.READ_WRITE) != OK:
		print("failed to access " + _base_path + " RW")
		if _base_file.open(_base_path, File.WRITE_READ) != OK:
			# oh NO
			push_error("FAILED TO OPEN: " + _base_path)
			return false
	var action_count = 0
	# will NOT seek us to EOF
	for line in _base_file.get_as_text().split("\n"):
		var jpr := JSON.parse(line)
		if jpr.error == OK:
			if jpr.result is Dictionary:
				_apply_action(jpr.result)
				action_count += 1
	ought_to_compact = action_count >= 2
	_base_file.seek_end()
	# grrr testing
	#print("B2DB ready, " + JSON.print(_real_data))
	_ready = true
	emit_signal("changed")
	return true

func get_json() -> String:
	return JSON.print(_real_data, "\t")

func install_json(json: String) -> bool:
	var res := JSON.parse(json)
	if res.error != OK:
		return false
	if not (res.result is Dictionary):
		return false
	for k in _real_data.keys():
		_dirty[k] = null
	_real_data = res.result
	for k in _real_data.keys():
		_dirty[k] = _real_data[k]
	commit()
	return true

func compact() -> bool:
	if not _ready:
		return false
	print("Doing DB compact!")
	_base_file.close()
	if _base_file.open(_base_path + ".compacting", File.WRITE_READ) != OK:
		push_error("OH NO...")
		_ready = false
		return false
	_base_file.store_string(JSON.print(_real_data))
	_base_file.flush()
	_base_file.close()
	var dir := Directory.new()
	if dir.rename(_base_path + ".compacting", _base_path) != OK:
		# uhhhhhh ok well that sucks
		print("Failed to do DB compact rename! this is bad but survivable")
		return true
	return _do_reopen_base_file()

func _do_reopen_base_file():
	if _base_file.open(_base_path, File.READ_WRITE) != OK:
		_ready = false
		push_error("OH NO...")
		return false
	_base_file.seek_end()
	ought_to_compact = false
	return true

func _apply_action(action: Dictionary):
	for k in action.keys():
		var v = action[k]
		if v == null:
			_real_data.erase(k)
		else:
			_real_data[k] = v

func commit():
	if _dirty.size() > 0:
		_base_file.store_string("\n" + JSON.print(_dirty))
		_base_file.flush()
		_dirty.clear()
		ought_to_compact = true
		# web requires this because flush doesn't REALLY work
		_base_file.close()
		print("- supposedly committed -")
		_do_reopen_base_file()
		emit_signal("changed")

func _process(_delta):
	commit()

func data_set(key: String, value):
	if not _ready:
		return
	if _real_data.has(key):
		if _real_data[key] == value:
			return
	_real_data[key] = value
	_dirty[key] = value

func data_get(key: String, default):
	var val = _real_data.get(key, default)
	if typeof(val) != typeof(default):
		# just in case JSON acts up
		if typeof(val) == TYPE_REAL and typeof(default) == TYPE_INT:
			return int(val)
		push_warning("key " + key + " did not match expected type...")
		return default
	return val

func data_erase(key: String):
	if not _ready:
		return
	_real_data.erase(key)
	_dirty[key] = null
