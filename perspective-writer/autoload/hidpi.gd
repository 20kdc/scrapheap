class_name DLCHiDPIManager
extends Node

var font_mono: B2FontFamily
var font_sans: B2FontFamily
var font_serif: B2FontFamily
var font_ico: Font

var scale = 1

func adjust_cm(sm: StyleBoxFlat, v: float):
	sm.content_margin_left = v * scale
	sm.content_margin_right = v * scale
	sm.content_margin_top = v * scale
	sm.content_margin_bottom = v * scale

func make_sb(col: Color) -> StyleBoxFlat:
	var panel_stylebox = StyleBoxFlat.new()
	panel_stylebox.bg_color = col
	panel_stylebox.set_border_width_all(4 * scale)
	panel_stylebox.set_corner_radius_all(2 * scale)
	adjust_cm(panel_stylebox, 8)
	panel_stylebox.border_color = col.darkened(0.4)
	return panel_stylebox

func get_gr(f: float):
	f *= 0.15
	return Color(f, f, f)

func get_font(id: String) -> B2FontFamily:
	if id == "sans":
		return font_sans
	if id == "serif":
		return font_serif
	return font_mono

func _init():
	print("HiDPI manager installing...")
	scale = max(1, floor(OS.get_screen_dpi() / 96))
	var theme: Theme = preload("res://autoload/theme.tres")

	var panel_stylebox = make_sb(get_gr(0.333))
	var button_stylebox = make_sb(get_gr(0.7))
	var button_h_stylebox = make_sb(get_gr(1.0))
	var button_d_stylebox = make_sb(get_gr(0.333))
	var button_disabled_stylebox = make_sb(get_gr(0.5))

	theme.set_stylebox("panel", "Panel", panel_stylebox)
	theme.set_stylebox("panel", "PanelContainer", panel_stylebox)
	theme.set_stylebox("panel", "TabContainer", panel_stylebox)

	theme.set_color("font_color", "TooltipLabel", Color.white)

	theme.set_stylebox("normal", "Button", button_stylebox)
	theme.set_stylebox("hover", "Button", button_h_stylebox)
	theme.set_stylebox("focus", "Button", StyleBoxEmpty.new())
	theme.set_stylebox("pressed", "Button", button_d_stylebox)
	theme.set_stylebox("disabled", "Button", button_disabled_stylebox)
	theme.set_color("font_color_disabled", "Button", get_gr(3))

	var tab_bg_stylebox = make_sb(get_gr(0.333))
	var tab_fg_stylebox = make_sb(get_gr(1.0))

	theme.set_stylebox("tab_bg", "TabContainer", tab_bg_stylebox)
	theme.set_stylebox("tab_fg", "TabContainer", tab_fg_stylebox)

	var il_bg_stylebox = make_sb(get_gr(0.5))
	var il_selected_stylebox = make_sb(get_gr(1.0))

	theme.set_stylebox("selected_focus", "ItemList", il_selected_stylebox)
	theme.set_stylebox("selected", "ItemList", il_selected_stylebox)
	theme.set_stylebox("bg", "ItemList", il_bg_stylebox)
	theme.set_stylebox("bg_focus", "ItemList", il_bg_stylebox)

	theme.set_stylebox("selected_focus", "Tree", il_selected_stylebox)
	theme.set_stylebox("selected", "Tree", il_selected_stylebox)
	theme.set_stylebox("bg", "Tree", il_bg_stylebox)
	# bg_focus breaks the tree? but not itemlist. hm.

	var sb_stylebox = make_sb(Color(0.4, 0.4, 0.6))
	var sbh_stylebox = make_sb(Color(0.4, 0.6, 0.8))
	var sbs_stylebox = make_sb(Color(0.1, 0.15, 0.2))
	adjust_cm(sb_stylebox, 16)
	adjust_cm(sbh_stylebox, 16)
	adjust_cm(sbs_stylebox, 16)

	theme.set_stylebox("grabber", "HScrollBar", sb_stylebox)
	theme.set_stylebox("grabber_pressed", "HScrollBar", sbh_stylebox)
	theme.set_stylebox("grabber_highlight", "HScrollBar", sb_stylebox)
	theme.set_stylebox("scroll", "HScrollBar", sbs_stylebox)
	theme.set_stylebox("scroll_focus", "HScrollBar", sbs_stylebox)

	theme.set_stylebox("grabber", "VScrollBar", sb_stylebox)
	theme.set_stylebox("grabber_pressed", "VScrollBar", sbh_stylebox)
	theme.set_stylebox("grabber_highlight", "VScrollBar", sb_stylebox)
	theme.set_stylebox("scroll", "VScrollBar", sbs_stylebox)
	theme.set_stylebox("scroll_focus", "VScrollBar", sbs_stylebox)

	theme.set_constant("autohide", "VSplitContainer", 0)
	theme.set_constant("autohide", "HSplitContainer", 0)
	theme.set_constant("separation", "VSplitContainer", int(scale * 36))
	theme.set_constant("separation", "HSplitContainer", int(scale * 36))

	font_mono = preload("b2ff_mono.tres")
	font_mono.set_size(scale * 16)

	font_sans = preload("b2ff_sans.tres")
	font_sans.set_size(scale * 16)

	font_serif = preload("b2ff_serif.tres")
	font_serif.set_size(scale * 16)

	font_ico = preload("font_ico.tres")
	font_ico.size = scale * 16

	theme.default_font = font_mono.regular

	theme.set_font("normal_font", "RichTextLabel", font_sans.regular)
	theme.set_font("bold_font", "RichTextLabel", font_sans.bold)
	theme.set_font("italics_font", "RichTextLabel", font_sans.italic)
	theme.set_font("bold_italics_font", "RichTextLabel", font_sans.bold_italic)
