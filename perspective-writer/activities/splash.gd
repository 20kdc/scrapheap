extends Node

var has_yanked = false
var retry_timer = 1.0

func _ready():
	OS.request_permissions()
	$TabContainer.current_tab = 3

func _process(delta):
	if not has_yanked:
		retry_timer += delta
		if retry_timer >= 1.0:
			retry_timer = 0.0
			if B2Storage.attempt_init():
				has_yanked = true
				if not B2DB.open():
					$TabContainer/Status.text = "Failed to open DB"
					return
				B2DBOps.migrate()
				B2DB.commit()
				if B2DB.ought_to_compact:
					if not B2DB.compact():
						$TabContainer/Status.text = "Failed to compact DB"
						return
				Yank.start(preload("bonfire/bonfire.tscn"), {})
