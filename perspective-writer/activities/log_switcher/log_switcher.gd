extends PanelContainer

onready var log_title = find_node("log_title")
onready var log_content = find_node("log_content")
onready var logs = find_node("logs")

func _ready():
	B2DB.connect("changed", self, "_refresh")
	_refresh()

func _refresh():
	var current_log := B2DBOps.log_current_get()
	log_title.text = current_log
	logs.clear()
	var select_me := -1
	var idx := 0
	for v in B2DBOps.log_list():
		logs.add_item(v)
		if current_log == v:
			select_me = idx
		idx += 1
	if select_me != -1:
		logs.select(select_me)
	logs.ensure_current_is_visible()
	var text := ""
	for v in B2DBOps.log_get(log_title.text):
		text += v.render_log()
	log_content.text = text

func _on_enter_pressed():
	Yank.start(load("res://activities/bonfire/bonfire.tscn"), {})

func _on_del_btn_pressed():
	B2DBOps.log_del(B2DBOps.log_current_get())

func _on_new_log_pressed():
	B2DBOps.log_current_set(Time.get_datetime_string_from_system())

func _on_logs_item_selected(index):
	B2DBOps.log_current_set(logs.get_item_text(index))

func _on_copy_pressed():
	OS.clipboard = log_content.text
