extends PanelContainer

func _on_back_pressed():
	Yank.start(load("res://activities/bonfire/bonfire.tscn"), {})

func _on_read_pressed():
	$"%json".text = B2DB.get_json()

func _on_overwrite_btn_pressed():
	B2DB.install_json($"%json".text)
	$"%json".text = B2DB.get_json()
