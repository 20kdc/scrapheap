extends PanelContainer

onready var speakers: ItemList = find_node("speakers")
onready var individual_member: B2SpeakerEditor = find_node("individual_member")
onready var new_name = find_node("new_name")

func _ready():
	B2DB.connect("changed", self, "_db_change")
	_db_change()

func _db_change():
	speakers.clear()
	for v in B2DBOps.speaker_list():
		speakers.add_item(v)

func _on_back_pressed():
	Yank.start(load("res://activities/bonfire/bonfire.tscn"), {})

func _on_speakers_item_selected(index):
	individual_member.setup(speakers.get_item_text(index))

func _on_new_pressed():
	if new_name.text == "":
		return
	if B2DBOps.speaker_list().has(new_name.text):
		individual_member.setup(new_name.text)
		return
	individual_member.setup_no_read(new_name.text)
	individual_member.save()
