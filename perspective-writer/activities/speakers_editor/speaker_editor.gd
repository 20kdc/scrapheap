class_name B2SpeakerEditor
extends PanelContainer

var selected_speaker := "announcer"

onready var id_text = find_node("id_text")
onready var preview_chat = find_node("preview_chat")
onready var preview_member = find_node("preview_member")
onready var icon_colour = find_node("icon_colour")
onready var icon_text = find_node("icon_text")
onready var example_text = find_node("example_text")
onready var text_font = find_node("text_font")
onready var text_align = find_node("text_align")
onready var log_preview_mirrorthis = find_node("log_preview_mirrorthis")
onready var prefixes = find_node("prefixes")
onready var suffixes = find_node("suffixes")
onready var log_prefix = find_node("log_prefix")
onready var log_suffix = find_node("log_suffix")
onready var switch_button = find_node("switch_button")

var example_bonfire_entry := DLCBonfireEntry.new()

func _ready():
	_read_in()

func setup(target: String):
	selected_speaker = target
	_read_in()

func setup_no_read(target: String):
	selected_speaker = target
	_refresh_previews()

func _read_in():
	icon_colour.color = B2DBOps.speaker_colour(selected_speaker)
	icon_text.text = B2DBOps.speaker_icon(selected_speaker)
	example_text.text = B2DBOps.speaker_example_text(selected_speaker)
	prefixes.text = B2DBOps.speaker_prefixes(selected_speaker)
	suffixes.text = B2DBOps.speaker_suffixes(selected_speaker)
	text_align.selected = B2DBOps.speaker_align(selected_speaker)
	var font = B2DBOps.speaker_font_hint(selected_speaker)
	if font == "sans":
		text_font.selected = 2
	elif font == "serif":
		text_font.selected = 1
	else:
		text_font.selected = 0
	log_prefix.text = B2DBOps.speaker_log_prefix(selected_speaker)
	log_suffix.text = B2DBOps.speaker_log_suffix(selected_speaker)
	switch_button.pressed = B2DBOps.speaker_has_switch(selected_speaker)
	_refresh_previews()

func _on_Button2_pressed():
	save()

func save():
	B2DBOps.speaker_save(
		selected_speaker,
		icon_colour.color,
		icon_text.text,
		example_text.text,
		prefixes.text,
		suffixes.text,
		text_align.selected,
		_get_font_setting(),
		log_prefix.text,
		log_suffix.text,
		switch_button.pressed
	)

func _get_font_setting() -> String:
	if text_font.selected == 2:
		return "sans"
	elif text_font.selected == 1:
		return "serif"
	return "mono"

func _refresh_previews():
	id_text.text = selected_speaker
	# Icon Preview
	preview_member.text = icon_text.text
	preview_member.add_font_override("font", HiDPIManager.font_ico)
	preview_member.add_color_override("font_color", icon_colour.color)
	# Log Preview
	log_preview_mirrorthis.text = example_text.text
	# Chat Preview
	example_bonfire_entry.writer = "example_entry" # to catch bugs
	example_bonfire_entry.text = example_text.text
	example_bonfire_entry.writer_override = true
	example_bonfire_entry.writer_override_align = text_align.selected
	example_bonfire_entry.writer_override_colour = icon_colour.color
	example_bonfire_entry.writer_override_font_hint = _get_font_setting()
	example_bonfire_entry.writer_override_icon = icon_text.text
	preview_chat.clear()
	example_bonfire_entry.render_rt(preview_chat, 0, false)

# -- preview refreshers --

func _on_example_text_text_changed(_a):
	_refresh_previews()

func _on_icon_colour_color_changed(_a):
	_refresh_previews()

func _on_text_align_item_selected(_a):
	_refresh_previews()

func _on_icon_text_text_changed(_a):
	_refresh_previews()

func _on_text_font_item_selected(_a):
	_refresh_previews()

func _on_del_btn_pressed():
	B2DBOps.speaker_del(selected_speaker)
