extends PanelContainer

onready var le = find_node("message_editor")
onready var sc = find_node("messages_sc")
onready var sm = find_node("speakers_list")
onready var csb: B2SpkicoButton = find_node("current_speaker")
onready var save_button = find_node("save")

var selected_speaker: String = "announcer"
var selected_log: String = "default"

var entries: Array

var editing: DLCBonfireEntry

func _ready():
	selected_log = B2DBOps.log_current_get()
	save_button.text = "Log: " + selected_log
	# on a DB change we need to rebuild the members and entries tables
	B2DB.connect("changed", self, "_rebuildspeakers")
	B2DB.connect("changed", self, "_rebuildents")
	entries = B2DBOps.log_get(selected_log)
	_rebuildspeakers()
	_rebuildents()

func _on_clear_pressed():
	entries = []
	editing = null
	_rebuildents_write()

func _on_LineEdit_text_entered(new_text: String):
	# doing this has a tendency to dismiss the virtual keyboard
	call_deferred("_rsvb")
	le.text = ""
	# strip pass 1
	new_text = new_text.strip_edges()

	# new prefix code
	var recognized_fix := ""
	var recognized_fix_suffix := false
	var recognized_fix_owner = null

	for possible_member in B2DBOps.speaker_list():
		for prefix in B2DBOps.speaker_prefixes(possible_member).split("\n"):
			if len(prefix) <= len(recognized_fix):
				continue
			if new_text.begins_with(prefix):
				recognized_fix = prefix
				recognized_fix_suffix = false
				recognized_fix_owner = possible_member
		for suffix in B2DBOps.speaker_suffixes(possible_member).split("\n"):
			if len(suffix) <= len(recognized_fix):
				continue
			if new_text.ends_with(suffix):
				recognized_fix = suffix
				recognized_fix_suffix = true
				recognized_fix_owner = possible_member

	# if we recognize a prefix then switch them in, monologues etc.
	if recognized_fix_owner != null:
		if recognized_fix_suffix:
			new_text = new_text.substr(0, len(new_text) - len(recognized_fix))
		else:
			new_text = new_text.substr(len(recognized_fix))
		_set_speaker(recognized_fix_owner)

	# strip pass 2
	new_text = new_text.strip_edges()
	# continue
	var ent: DLCBonfireEntry
	if editing == null:
		if new_text == "":
			return
		ent = DLCBonfireEntry.new()
		entries.push_back(ent)
	else:
		ent = editing
		editing = null
		if new_text == "":
			entries.erase(ent)
			_rebuildents_write()
			return
	ent.writer = selected_speaker
	ent.text = new_text
	_rebuildents_write()

func _rsvb():
	OS.show_virtual_keyboard()

func _on_save_pressed():
	Yank.start(preload("res://activities/log_switcher/log_switcher.tscn"), {})

func _rebuildspeakers():
	for v in sm.get_children():
		v.queue_free()
	for v in B2DBOps.speaker_list():
		if not B2DBOps.speaker_has_switch(v):
			continue
		var btn := B2SpkicoButton.new()
		btn.setup(v)
		btn.connect("pressed", self, "_set_speaker_and_focus_textedit", [v])
		sm.add_child(btn)
	csb.setup(selected_speaker)

func _set_speaker(v):
	selected_speaker = v
	csb.setup(selected_speaker)

func _set_speaker_and_focus_textedit(v):
	_set_speaker(v)
	le.grab_focus()

func _rebuildents():
	sc.clear()
	var idx = 0
	for v in entries:
		var vl: DLCBonfireEntry = v
		vl.render_rt(sc, idx, editing == vl)
		idx += 1

func _rebuildents_write():
	B2DBOps.log_set(selected_log, entries)
	_rebuildents()

func _on_messages_sc_meta_clicked(meta):
	if typeof(meta) == TYPE_STRING:
		var parts = meta.split(":")
		if parts[0] == "edit":
			editing = entries[int(parts[1])]
			le.text = editing.text
			_rebuildents()
			_set_speaker_and_focus_textedit(editing.writer)

func _on_speakers_pressed():
	Yank.start(preload("res://activities/speakers_editor/speakers_editor.tscn"), {})

func _on_db_pressed():
	Yank.start(preload("res://activities/credits/credits.tscn"), {})

func _on_datamgr_pressed():
	Yank.start(preload("res://activities/data_manager/data_manager.tscn"), {})
