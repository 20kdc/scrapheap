extends RichTextLabel

func _ready():
	connect("meta_clicked", self, "_meta_clicked")

func _meta_clicked(text):
	OS.shell_open(text)

