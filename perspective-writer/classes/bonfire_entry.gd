class_name DLCBonfireEntry
extends Reference

var writer := ""
var text := "" setget _set_text
var _segments := []

# writer override for example text renderer
var writer_override := false
var writer_override_align := 0
var writer_override_colour := Color.white
var writer_override_icon := "?"
var writer_override_font_hint := "mono"

func render_rt(rtl: RichTextLabel, idx: int, editing: bool):
	var fh := "mono"
	if writer_override:
		rtl.push_align(writer_override_align)
		rtl.push_color(writer_override_colour)
		rtl.push_font(HiDPIManager.font_ico)
		rtl.add_text(writer_override_icon + " ")
		fh = writer_override_font_hint
	else:
		rtl.push_align(B2DBOps.speaker_align(writer))
		rtl.push_color(B2DBOps.speaker_colour(writer))
		rtl.push_font(HiDPIManager.font_ico)
		rtl.add_text(B2DBOps.speaker_icon(writer) + " ")
		fh = B2DBOps.speaker_font_hint(writer)
	# --
	var ff := HiDPIManager.get_font(fh)
	for v in _segments:
		if v.style == 1:
			rtl.push_font(ff.bold)
		elif v.style == 2:
			rtl.push_font(ff.italic)
		elif v.style == 3:
			rtl.push_font(ff.bold_italic)
		else:
			rtl.push_font(ff.regular)
		rtl.add_text(v.text)
		rtl.pop()
	if editing:
		rtl.append_bbcode(" >🖊<\n")
	else:
		rtl.append_bbcode(" [url=edit:" + str(idx) + "]🖊[/url]\n")
	rtl.pop()
	rtl.pop()
	rtl.pop()

func _set_text(v: String):
	if v == text:
		return
	text = v
	_segments = []
	var style := 0
	while len(v) > 0:
		if v.begins_with("\\__"):
			_add_segment("__", style)
			v = v.substr(3)
		elif v.begins_with("\\_"):
			_add_segment("_", style)
			v = v.substr(2)
		elif v.begins_with("\\**"):
			_add_segment("**", style)
			v = v.substr(3)
		elif v.begins_with("\\*"):
			_add_segment("*", style)
			v = v.substr(2)
		elif v.begins_with("__") or v.begins_with("**"):
			style ^= 1
			v = v.substr(2)
		elif v.begins_with("_") or v.begins_with("*"):
			style ^= 2
			v = v.substr(1)
		else:
			_add_segment(v.substr(0, 1), style)
			v = v.substr(1)

func _add_segment(t: String, s: int):
	if _segments.size() == 0:
		_segments.push_back(Segment.new(t, s))
		return
	var seg: Segment = _segments[len(_segments) - 1]
	if seg.style == s:
		seg.text += t
	else:
		_segments.push_back(Segment.new(t, s))

func render_log() -> String:
	return B2DBOps.speaker_log_prefix(writer) + text + B2DBOps.speaker_log_suffix(writer) + "\n"

class Segment:
	extends Reference
	var text = ""
	var style = 0
	func _init(t: String, s: int):
		text = t
		style = s
