class_name B2FontFamily
extends Resource

export var regular: Font
export var bold: Font
export var italic: Font
export var bold_italic: Font

func set_size(size):
	regular.size = size
	bold.size = size
	italic.size = size
	bold_italic.size = size
