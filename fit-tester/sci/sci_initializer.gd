extends Node

var impl: SCIImplBase
var ui_pad: Node
var locator: Transform3D = Transform3D.IDENTITY

func _ready() -> void:
	print("SCI: Boot")
	var openxr = XRServer.find_interface("OpenXR")
	if openxr.is_initialized():
		print("SCI: OpenXR init success")
		# xr init
		get_viewport().use_xr = true
		DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_DISABLED)
		# *ideally*, it would be nice to not just assume XR works correctly here
		# load SCI
		impl = preload("openxr_touch.tscn").instantiate()
	else:
		# Godot kinda just freezes if it can't init OpenXR???
		# :(
		print("SCI: OpenXR init fail")
		impl = preload("desktop.tscn").instantiate()
	add_child(impl)
	ui_pad = impl.ui_pad
