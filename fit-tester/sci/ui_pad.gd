# manages the SCI Virtual Cursor
class_name SCIUIPad
extends SubViewport

var input_thumbstick: Vector2 = Vector2.ZERO
var cursor_pos: Vector2 = Vector2.ZERO
var doubleclick_chk = -1.0

@export var cursor_view: Node3D

func _ready() -> void:
	cursor_pos = Vector2(size.x, size.y) / 2

func _process(delta: float) -> void:
	doubleclick_chk -= delta
	# print("cursor_pos", cursor_pos)
	cursor_pos += input_thumbstick * 300 * delta * Vector2(1, -1)
	cursor_pos = cursor_pos.max(Vector2.ZERO)
	cursor_pos = cursor_pos.min(Vector2(size.x, size.y))
	var rel_pos = cursor_pos - (Vector2(size.x, size.y) / 2)
	cursor_view.transform.origin = Vector3(rel_pos.x, -rel_pos.y, 0)
	var motion = InputEventMouseMotion.new()
	_do_mouse_ev(motion)

func _map_id(name: String) -> int:
	if name == "trigger_click":
		return MOUSE_BUTTON_LEFT
	if name == "grip_click":
		return MOUSE_BUTTON_RIGHT
	return MOUSE_BUTTON_NONE

func _do_mouse_button(id: int, pressed: bool):
	var ev = InputEventMouseButton.new()
	ev.button_index = id
	ev.pressed = pressed
	ev.double_click = doubleclick_chk > 0.0
	if pressed:
		doubleclick_chk = 0.3
	_do_mouse_ev(ev)

func _do_mouse_ev(ev: InputEventMouse):
	ev.position = cursor_pos
	ev.global_position = cursor_pos
	push_input(ev, true)

func _on_left_controller_button_pressed(name: String) -> void:
	var id = _map_id(name)
	if id == MOUSE_BUTTON_NONE:
		return
	_do_mouse_button(id, true)

func _on_left_controller_button_released(name: String) -> void:
	var id = _map_id(name)
	if id == MOUSE_BUTTON_NONE:
		return
	_do_mouse_button(id, false)

func _on_left_controller_input_vector_2_changed(name: String, value: Vector2) -> void:
	# print(name, value)
	if name == "primary":
		input_thumbstick = value
