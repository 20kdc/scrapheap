extends XRController3D

func _process(delta: float) -> void:
	var pos = $InstallPoint.position
	pos.y += get_vector2("primary").y * delta * 0.1
	$InstallPoint.position = pos
	var gtf: Transform3D = $InstallPoint.global_transform
	# filter basis unless a specific input is given
	if not get_input("ax_button"):
		gtf.basis = Basis.from_euler(Vector3(0, atan2(gtf.basis.x.x, gtf.basis.x.z), 0), EULER_ORDER_YXZ)
	# continue
	var locator := SCI.locator
	if get_input("trigger_click"):
		locator.origin = gtf.origin
	if get_input("grip_click"):
		locator.basis = gtf.basis
	SCI.locator = locator
