extends Node3D

var widget: MeshInstance3D
var ui: Node

func _ready() -> void:
	widget = $Widget
	ui = $UI
	var files = $UI/SC/Files
	OS.request_permissions()
	var file_prefix = "/storage/emulated/0/"
	if DirAccess.dir_exists_absolute("/media/ramdisk/fit-tester/"):
		file_prefix = "/media/ramdisk/fit-tester/"
	for v in DirAccess.get_files_at(file_prefix):
		var fb = Button.new()
		fb.text = v
		var fv = file_prefix + v
		fb.pressed.connect(func ():
			_select_file(fv))
		files.add_child(fb)
	remove_child(ui)
	SCI.ui_pad.add_child(ui)

func _select_file(file: String) -> void:
	print("UI: Load: " + file)
	var img = Image.load_from_file(file)
	if img != null:
		# image! load as cardboard cutout (Yuki)
		var quad = QuadMesh.new()
		var magic = StandardMaterial3D.new()
		magic.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
		magic.albedo_texture = ImageTexture.create_from_image(img)
		magic.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
		quad.material = magic
		var sz = img.get_size()
		var smx = max(float(sz.x), float(sz.y))
		quad.size = Vector2(sz) / smx
		quad.center_offset = Vector3(0, sz.y / (smx * 2), 0)
		widget.mesh = quad
	else:
		print("UI: Unable to load as image...")
		# try STL
		var data := FileAccess.open(file, FileAccess.READ)
		data.big_endian = false
		data.seek(80)
		var tris := data.get_32()
		if tris > 0x20000:
			# nope
			print("too many triangles")
			return
		var conversion_basis = Basis(
			# STL for 3DP uses mm as the base unit
			# and it uses Z as vertical
			Vector3(0.001, 0, 0),
			Vector3(0, 0, 0.001),
			Vector3(0, 0.001, 0)
		)
		var tri_data := []
		for i in range(tris):
			# ignore
			data.get_float()
			data.get_float()
			data.get_float()
			var v1x := data.get_float()
			var v1y := data.get_float()
			var v1z := data.get_float()
			var v2x := data.get_float()
			var v2y := data.get_float()
			var v2z := data.get_float()
			var v3x := data.get_float()
			var v3y := data.get_float()
			var v3z := data.get_float()
			data.get_16()
			var v1 = Vector3(v1x, v1y, v1z) * conversion_basis
			var v2 = Vector3(v2x, v2y, v2z) * conversion_basis
			var v3 = Vector3(v3x, v3y, v3z) * conversion_basis
			tri_data.push_back([
				(v3 - v1).cross(v2 - v1).normalized(),
				v1,
				v2,
				v3
			])
		var mesh = ImmediateMesh.new()
		mesh.surface_begin(Mesh.PRIMITIVE_TRIANGLES, preload("pla_material.tres"))
		for tri in tri_data:
			var vN: Vector3 = tri[0]
			var v1: Vector3 = tri[1]
			var v2: Vector3 = tri[2]
			var v3: Vector3 = tri[3]
			mesh.surface_set_normal(vN)
			mesh.surface_add_vertex(v1)
			mesh.surface_add_vertex(v2)
			mesh.surface_add_vertex(v3)
		mesh.surface_end()
		widget.mesh = mesh

func _on_file_dialog_files_selected(paths: PackedStringArray) -> void:
	print("UI: what ", paths)

func _process(_delta: float) -> void:
	var tf := SCI.locator
	var scn: float = ui.get_node("HSlider").value / 100.0
	tf.basis = tf.basis.scaled(Vector3.ONE * scn)
	widget.global_transform = tf

func _on_reset_pressed() -> void:
	ui.get_node("HSlider").value = 100
