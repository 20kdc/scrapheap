#include "pcxp.h"

int PCXP_MAIN(int argc, char ** argv) {
	FILE * test;
	void * lib;
	printf("Test\xC3\xA4\n");
	test = pcxp_fopen("Test\xC3\xA4", "wb");
	if (test) {
		fputs("Hello!", test);
		fclose(test);
	}
	lib = pcxp_dlopen(PCXP_LIBC_NAME);
	if (lib) {
		printf("found libc. printf=%p\n", pcxp_dlsym(lib, "printf"));
		pcxp_dlclose(lib);
	}
	return 0;
}

PCXP_WMAIN
