# 'Portable C Extension Package'

This has been unceremoniously dumped here under the expectation that I'll return to it at a later date.

That doesn't actually mean I will return to it, though.

The goal is a single-header public-domain C89 library which implements two key things:

1. The proper platform abstraction layer C never got
2. Utilities and very small but common data structures

It is intended for code intended to be compatible with very old compilers up to modern compilers.

Core principles are:

1. When reasonably possible, try to be a zero-cost-abstraction. More importantly, if there's any way, never wrap a type; alias if you must, but never wrap.
2. Keep support as broad as possible.
3. Don't overlap existing names. It's awkward but it'll avoid problems.
4. The C standard level expected is `-std=c89 -Wpedantic` & VC60, which apparently means no `stdint.h`.
5. The theory here is that code should run on as many platforms as possible with as few ifdef blocks as possible, without needing to include something annoying build/distribution-wise like a DLL.
6. No mandatory `IMPLEMENTATION` define. All functions should be `static inline`. This might be changed to a mixed model where the user can opt-in to deinlining some functions to reduce code duplication.
7. GUI isn't in-scope, so the platform abstraction should be dealing with the console, threading, networking, _maybe_ dlopen, and filesystem access.
8. Never reinvent an stdlib function solely for consistency, only for real issues or lack of availability.

Obviously, in its current state, it's hyper-unstable.
