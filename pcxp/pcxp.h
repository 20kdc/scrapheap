/*
 * PCXP: Portable C Expansion Package
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <https://unlicense.org/>
 */

/* -- prologue -- */
#ifndef _PCXP_HEADER_H
#define _PCXP_HEADER_H
/* -- includes -- */
#include <stdio.h>
#ifdef _WIN32
/* Provides its own set of stdint-esque types but with weird names */
#include <windows.h>
#else
/* Not available on VC60 */
#include <stdint.h>
/* Needed for mutexes etc. */
#include <pthread.h>
/* dlopen/dlclose etc. */
#include <dlfcn.h>
#endif
/* -- C++ guard -- */
#ifdef __cplusplus
extern "C" {
#endif
/* -- inline flag -- */
#if defined(_MSC_VER)
#define PCXP_INLINE _inline
#elif defined(__GNUC__)
#define PCXP_INLINE __inline__
#else
#define PCXP_INLINE
#endif
/* -- Project defines -- */
/* Controls the declaration flags of lightweight (should-be-inline) functions. */
#ifndef PCXP_FC_LIGHT
#define PCXP_FC_LIGHT static PCXP_INLINE
#endif
/* Controls the declaration flags of heavyweight (less inlinable) functions. */
#ifndef PCXP_FC_HEAVY
#define PCXP_FC_HEAVY static
#endif
/* #define PCXP_FC_HEAVY_OMIT : Omits heavyweight function bodies. */
/* -- types -- */
#ifdef _WIN32
typedef int pcxp_int32_t;
typedef DWORD pcxp_uint32_t;
typedef LONGLONG pcxp_int64_t;
typedef ULONGLONG pcxp_uint64_t;
#else
typedef int32_t pcxp_int32_t;
typedef uint32_t pcxp_uint32_t;
typedef int64_t pcxp_int64_t;
typedef uint64_t pcxp_uint64_t;
#endif
/* -- string conversion -- */
#ifdef _WIN32
/* duplicates a UTF-8 string as wchar_t */
PCXP_FC_HEAVY wchar_t * pcxp_utf8_wide(const char * string)
#ifdef PCXP_FC_HEAVY_OMIT
;
#else
{
	int out_len;
	wchar_t * out;
	if (!string)
		return 0;
	if (!string[0]) {
		out = (wchar_t *) malloc(2);
		if (out)
			out[0] = 0;
		return out;
	}
	out_len = MultiByteToWideChar(CP_UTF8, 0, string, -1, 0, 0);
	if (!out_len)
		return 0;
	out = (wchar_t *) malloc((out_len + 1) * 2);
	if (!out)
		return 0;
	if (!MultiByteToWideChar(CP_UTF8, 0, string, -1, out, out_len)) {
		free(out);
		return 0;
	}
	out[out_len] = 0;
	return out;
}
#endif
/* duplicates a wchar_t string as UTF-8 */
PCXP_FC_HEAVY char * pcxp_wide_utf8(const wchar_t * string)
#ifdef PCXP_FC_HEAVY_OMIT
;
#else
{
	int out_len;
	char * out;
	if (!string)
		return 0;
	if (!string[0]) {
		out = (char *) malloc(1);
		if (out)
			out[0] = 0;
		return out;
	}
	out_len = WideCharToMultiByte(CP_UTF8, 0, string, -1, 0, 0, 0, 0);
	if (!out_len)
		return 0;
	out = (char *) malloc(out_len + 1);
	if (!out)
		return 0;
	if (!WideCharToMultiByte(CP_UTF8, 0, string, -1, out, out_len, 0, 0)) {
		free(out);
		return 0;
	}
	out[out_len] = 0;
	return out;
}
#endif
#endif
/* -- wmain -- */
#ifdef _WIN32
#define PCXP_MAIN pcxp_main
#define PCXP_WMAIN int wmain(int argc, wchar_t * argv[]) { \
	int i; \
	for (i = 0; i < argc; i++) \
		argv[i] = (wchar_t *) pcxp_wide_utf8(argv[i]); \
	return pcxp_main(argc, (char **) (void *) argv); \
}
#else
#define PCXP_MAIN main
#define PCXP_WMAIN
#endif
/* -- file handling -- */
#ifdef _WIN32
/* See fopen, but uses UTF-8. */
PCXP_FC_LIGHT FILE * pcxp_fopen(const char * filename, const char * mode) {
	wchar_t * c_filename = pcxp_utf8_wide(filename);
	wchar_t * c_mode = pcxp_utf8_wide(mode);
	FILE * file = 0;
	if (c_filename && c_mode)
		file = _wfopen(c_filename, c_mode);
	free(c_filename);
	free(c_mode);
	return file;
}
#else
/* all sane implementations of all sane platforms standardized on UTF-8 decades ago */
#define pcxp_fopen fopen
#endif
/* -- mutexes -- */
#ifdef _WIN32
/* Mutex. Do not move or copy. */
typedef CRITICAL_SECTION pcxp_mutex_t;
/* Initializes a mutex. */
PCXP_FC_LIGHT void pcxp_mutex_init(pcxp_mutex_t * mutex) {
	InitializeCriticalSection(mutex);
}
#define pcxp_mutex_destroy DeleteCriticalSection
#define pcxp_mutex_lock EnterCriticalSection
#define pcxp_mutex_unlock LeaveCriticalSection
#else
/* Mutex. Do not move or copy. */
typedef pthread_mutex_t pcxp_mutex_t;
/* Initializes a mutex. */
PCXP_FC_LIGHT void pcxp_init_mutex(pcxp_mutex_t * mutex) {
	pthread_mutex_init(mutex, NULL);
}
#define pcxp_mutex_destroy pthread_mutex_destroy
#define pcxp_mutex_lock pthread_mutex_lock
#define pcxp_mutex_unlock pthread_mutex_unlock
#endif
/* -- threads -- */
#ifdef _WIN32
#define pcxp_thread_self GetCurrentThread
#else
#define pcxp_thread_self pthread_self
#endif
/* -- networking -- */
/* -- dlopen -- */
#ifdef _WIN32
PCXP_FC_LIGHT void * pcxp_dlopen(const char * filename) {
	return LoadLibraryA(filename);
}
#define pcxp_dlclose FreeLibrary
#define pcxp_dlsym GetProcAddress
#define PCXP_LIBC_NAME "msvcrt"
#define PCXP_DLL_EXPORT __declspec(dllexport)
#define PCXP_DLL_IMPORT __declspec(dllimport)
#else
PCXP_FC_LIGHT void * pcxp_dlopen(const char * filename) {
	return dlopen(filename, RTLD_LAZY);
}
#define pcxp_dlclose dlclose
#define pcxp_dlsym dlsym
#define PCXP_LIBC_NAME "libc.so.6"
#define PCXP_DLL_EXPORT __attribute__((visibility("default")))
#define PCXP_DLL_IMPORT
#endif
/* -- utilities -- */
/* -- epilogue -- */
#ifdef __cplusplus
}
#endif
#endif
