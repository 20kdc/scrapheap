#include "boilerplate.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan_core.h>

SDL_Window * gSDLWindow;
boilerplate_t gVK;
VkSurfaceKHR gVKSurface;
VkSwapchainKHR gVKSwapchain;

VkExtent2D gExtent = {800, 600};

int main() {
	gSDLWindow = SDL_CreateWindow("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, gExtent.width, gExtent.height, SDL_WINDOW_VULKAN);
	unsigned int counter = 64;
	const char * instanceExtensions[64];
	SDL_Vulkan_GetInstanceExtensions(gSDLWindow, &counter, instanceExtensions);
	gVK = boilerplate_doVkInit(counter, instanceExtensions);
	SDL_Vulkan_CreateSurface(gSDLWindow, gVK.instance, &gVKSurface);
	assert(gVKSurface);
	// alright, we've gotten this far
	VkSwapchainCreateInfoKHR scci = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = gVKSurface,
		.minImageCount = 2,
		.imageFormat = VK_FORMAT_R8G8B8A8_SRGB,
		.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
		.imageExtent = gExtent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE
	};
	vkCreateSwapchainKHR(gVK.device, &scci, NULL, &gVKSwapchain);
	assert(gVKSwapchain);
	uint32_t swapchainImageCount = 2;
	VkImage swapchainImages[2];
	VkImageView swapchainImageViews[2];
	vkGetSwapchainImagesKHR(gVK.device, gVKSwapchain, &swapchainImageCount, swapchainImages);
	for (int i = 0; i < 2; i++) {
		VkImageViewCreateInfo ivci = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = swapchainImages[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = VK_FORMAT_R8G8B8A8_SRGB,
			.subresourceRange = {
				.layerCount = 1,
				.levelCount = 1,
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT
			}
		};
		vkCreateImageView(gVK.device, &ivci, NULL, swapchainImageViews + i);
	}
	while (1) {
		// event handling
		SDL_Event ev;
		while (SDL_PollEvent(&ev))
			if (ev.type == SDL_QUIT)
				return 0;
		// acquisition
		uint32_t imgIdx;
		vkAcquireNextImageKHR(gVK.device, gVKSwapchain, UINT64_MAX, 0, 0, &imgIdx);
		// command buffer
		VkCommandBuffer frameCB;
		boilerplate_createAndBeginCommandBuffer(gVK, VK_COMMAND_BUFFER_LEVEL_PRIMARY, &frameCB);
		assert(frameCB);
		VkClearColorValue ccv = {
			.float32 = {1, 0, 1, 1}
		};
		VkImageSubresourceRange isr = {
			.layerCount = 1,
			.levelCount = 1,
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT
		};
		vkCmdClearColorImage(frameCB, swapchainImages[imgIdx], VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR, &ccv, 1, &isr);
		VkRenderingAttachmentInfo rai = {
			.sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
			.imageView = swapchainImageViews[imgIdx],
			.imageLayout = VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR
		};
		VkRenderingInfo vri = {
			.sType = VK_STRUCTURE_TYPE_RENDERING_INFO_KHR,
			.renderArea = {
				.extent = gExtent,
				.offset = {0, 0}
			},
			.layerCount = 1,
			.colorAttachmentCount = 1,
			.pColorAttachments = &rai
		};
		vkCmdBeginRendering(frameCB, &vri);
		vkCmdEndRendering(frameCB);
		vkEndCommandBuffer(frameCB);
		// submit & delete temp CB
		boilerplate_submitConfirm(gVK, frameCB);
		vkFreeCommandBuffers(gVK.device, gVK.commandPool, 1, &frameCB);
		// present
		VkPresentInfoKHR presentInfo = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pImageIndices = &imgIdx,
			.swapchainCount = 1,
			.pSwapchains = &gVKSwapchain
		};
		vkQueuePresentKHR(gVK.queue, &presentInfo);
	}
	return 0;
}
