#pragma once

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>
#include <assert.h>

typedef struct {
	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	VkDevice device;
	VkQueue queue;
	VkCommandPool commandPool;
} boilerplate_t;

inline static boilerplate_t boilerplate_doVkInit(uint32_t instanceExtensionCount, const char ** instanceExtensions) {
	boilerplate_t gVK = {};
	VkApplicationInfo appInfo = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.apiVersion = VK_API_VERSION_1_3
	};
	VkInstanceCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.flags = VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR,
		.pApplicationInfo = &appInfo,
		.enabledExtensionCount = instanceExtensionCount,
		.ppEnabledExtensionNames = instanceExtensions
	};
	vkCreateInstance(&info, NULL, &gVK.instance);
	assert(gVK.instance);
	// vulkan instance and surface created! yay
	uint32_t devCount = 1;
	vkEnumeratePhysicalDevices(gVK.instance, &devCount, &gVK.physicalDevice);
	assert(gVK.physicalDevice);
	VkDeviceQueueCreateInfo dqci = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueCount = 1
	};
	const char * extNames[] = {
		"VK_KHR_swapchain"
	};
	VkDeviceCreateInfo dci = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &dqci,
		.enabledExtensionCount = 1,
		.ppEnabledExtensionNames = extNames
	};
	vkCreateDevice(gVK.physicalDevice, &dci, NULL, &gVK.device);
	assert(gVK.device);
	vkGetDeviceQueue(gVK.device, 0, 0, &gVK.queue);
	assert(gVK.queue);
	VkCommandPoolCreateInfo cpci = {.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
	vkCreateCommandPool(gVK.device, &cpci, NULL, &gVK.commandPool);
	return gVK;
}

inline static VkResult boilerplate_createAndBeginCommandBuffer(boilerplate_t gVK, VkCommandBufferLevel level, VkCommandBuffer * vcb) {
	VkCommandBufferAllocateInfo cba = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = gVK.commandPool,
		.level = level,
		.commandBufferCount = 1
	};
	VkResult res = vkAllocateCommandBuffers(gVK.device, &cba, vcb);
	if (res != VK_SUCCESS)
		return res;
	VkCommandBufferBeginInfo cbbi = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT
	};
	return vkBeginCommandBuffer(*vcb, &cbbi);
}

inline static void boilerplate_submit(boilerplate_t gVK, VkCommandBuffer vcb) {
	VkSubmitInfo vsi = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.commandBufferCount = 1,
		.pCommandBuffers = &vcb
	};
	vkQueueSubmit(gVK.queue, 1, &vsi, 0);
}

inline static void boilerplate_submitConfirm(boilerplate_t gVK, VkCommandBuffer vcb) {
	VkFenceCreateInfo fci = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
	};
	VkFence fence;
	vkCreateFence(gVK.device, &fci, NULL, &fence);
	VkSubmitInfo vsi = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.commandBufferCount = 1,
		.pCommandBuffers = &vcb
	};
	vkQueueSubmit(gVK.queue, 1, &vsi, fence);
	vkWaitForFences(gVK.device, 1, &fence, 1, UINT64_MAX);
	vkDestroyFence(gVK.device, fence, NULL);
}
