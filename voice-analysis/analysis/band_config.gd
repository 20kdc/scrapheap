class_name TVTBandConfig
extends Reference

var band_hz := PoolRealArray()
var band_low_hz := PoolRealArray()
var band_high_hz := PoolRealArray()
var band_shade := PoolRealArray()

func _init():
	# C-2 aka MIDI 36
	# C-5 is MIDI 72
	for i in range(TVTNoteScale.C2, TVTNoteScale.C2 + 63):
		var this_band_hz := _calc_band_centre_hz(i)
		# print("X: " + TVTNoteScale.get_note_name(i) + " Y: " + str(this_band_hz))
		var this_band_low_hz := _calc_band_centre_hz(i - 0.5)
		var this_band_high_hz := _calc_band_centre_hz(i + 0.5)
		band_hz.push_back(this_band_hz)
		band_low_hz.push_back(this_band_low_hz)
		band_high_hz.push_back(this_band_high_hz)
		# borrowing an approximation here
		var shade := 0.0
		if i >= TVTNoteScale.F2 and i <= TVTNoteScale.E4:
			if i < TVTNoteScale.E3:
				shade = 0.25
			elif i <= TVTNoteScale.F3:
				shade = 0.75
			else:
				shade = 0.5
		band_shade.push_back(shade)

func _calc_band_centre_hz(note: float) -> float:
	# constant guessed by matching G#9
	var band_zero_hz = 65.4064
	return band_zero_hz * pow(2, ((note - TVTNoteScale.C2) / 12.0))

func load_from_fft(fft: PoolVector2Array, mix_rate: float) -> PoolRealArray:
	var res = PoolRealArray()
	res.resize(band_hz.size())
	var band_idx := 0
	while band_idx < len(res):
		var low = GDScriptFFTBuffer.hz_to_fft_band(band_low_hz[band_idx], len(fft), mix_rate) - 0.5
		var high = GDScriptFFTBuffer.hz_to_fft_band(band_high_hz[band_idx], len(fft), mix_rate) + 0.5
		# ok, so the idea here is, unlike Godot's spectrum analyzer,
		# we won't privilege overlapping frequencies too much, but also not too little
		var fft_idx := int(max(floor(low), 0))
		var last_to_check := int(min(ceil(high), len(fft) - 1))
		var total = 0.0
		while fft_idx <= last_to_check:
			# represent importance as the "area" of the 1D line (low, high) clipped to this FFT band
			var importance = clamp(high, fft_idx, fft_idx + 1) - clamp(low, fft_idx, fft_idx + 1)
			total = max(total, fft[fft_idx].length() * importance)
			fft_idx += 1
		res[band_idx] = total
		band_idx += 1
	return res

func hz_to_index(hz: float) -> int:
	for i in range(len(band_hz)):
		if band_low_hz[i] <= hz and band_high_hz[i] >= hz:
			return i
	var min_dist := INF
	var min_index := 0
	for i in range(len(band_hz)):
		var dist := abs(band_hz[i] - hz)
		if dist < min_dist:
			min_dist = dist
			min_index = i
	return min_index
