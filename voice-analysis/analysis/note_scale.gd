class_name TVTNoteScale

const C0 = 12

const C1 = 24

const C2 = 36
const F2 = 41

const C3 = 48
const E3 = 52
const F3 = 53

const C4 = 60
const E4 = 64

const SUBNOTES = [
	"C-", "C#", "D-", "D#",
	"E-", "F-", "F#", "G-",
	"G#", "A-", "A#", "B-",
]

static func get_note_hz(note: int) -> float:
	return 440 * pow(2, ((note - 69) / 12.0))

static func get_note_name(note: int) -> String:
	var subnote = note % 12
	var octave = (note / 12) - 1
	return SUBNOTES[subnote] + str(octave)

static func hz_to_note(hz: float) -> int:
	var likely_note = 0
	var likely_hz_dist = INF
	for i in range(128):
		var target = get_note_hz(i)
		var dist = abs(target - hz)
		if dist < likely_hz_dist:
			likely_hz_dist = dist
			likely_note = i
	return likely_note
