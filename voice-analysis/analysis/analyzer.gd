class_name TVTAnalyzer
extends Reference

# Value must exceed this to detect something.
var noise_floor := 0.125
# Value must be lower than this to stop detecting the first peak.
var stop_threshold := 0.125

func analyze(from: TVTFrequencyFrame) -> TVTAnalysis:
	# So we need to find the bottom "base" peak.
	# This is not just the highest value, mind.
	# Otherwise the system has a tendency to lock onto the wrong peaks.
	var peak_a_index := -1
	var peak_a_value := noise_floor
	for i in range(len(from.values)):
		var v := from.values[i]
		if v > peak_a_value:
			peak_a_index = i
			peak_a_value = v
		if peak_a_index != -1 and v < stop_threshold:
			break
	# Centring peak found.
	# Collate other peaks.
	var peak_b_index := -1
	var peak_b_value := 0.0
	var peak_c_index := -1
	var peak_c_value := 0.0
	var peak_d_index := -1
	var peak_d_value := 0.0
	if peak_a_index != -1:
		peak_a_value = 0.0
		peak_b_index = peak_a_index + 12
		peak_c_index = peak_a_index + 18
		peak_d_index = peak_a_index + 24
		for i in range(len(from.values)):
			var v := from.values[i]
			if i >= peak_a_index - 1 and i <= peak_a_index + 1:
				peak_a_value += v
			if i >= peak_b_index - 1 and i <= peak_b_index + 1:
				peak_b_value += v
			if i >= peak_c_index - 1 and i <= peak_c_index + 1:
				peak_c_value += v
			if i >= peak_d_index - 1 and i <= peak_d_index + 1:
				peak_d_value += v
	# Note down stuff
	var me = TVTAnalysis.new()
	me.from = from
	me.peak_a_index = peak_a_index
	me.peak_b_index = peak_b_index
	me.peak_c_index = peak_c_index
	me.peak_d_index = peak_d_index
	me.peak_b_relative_strength = 0.0
	me.peak_c_relative_strength = 0.0
	me.peak_d_relative_strength = 0.0
	if peak_a_value > 0.0:
		me.peak_b_relative_strength = peak_b_value / peak_a_value
		me.peak_c_relative_strength = peak_c_value / peak_a_value
		me.peak_d_relative_strength = peak_d_value / peak_a_value
	return me
