class_name TVTAnalysis
extends Reference

# Associated TVTFrequencyFrame
var from: TVTFrequencyFrame
# Index of peak A (-1 if not found)
var peak_a_index: int
# Index of peak B (-1 if not found)
var peak_b_index: int
# Index of peak C (-1 if not found)
var peak_c_index: int
# Index of peak D (-1 if not found)
var peak_d_index: int
# Relative (1 = 100%) strength of peak B relative to peak A.
# 0 if anything weird happens.
var peak_b_relative_strength: float
# Same for peak C.
# Peak C disappears for [i] and [u] vowels.
# Peak C strength is measured relative to A.
var peak_c_relative_strength: float
# Same for peak D.
# Peak D strength is measured relative to A.
var peak_d_relative_strength: float
