# FFT code ported from https://github.com/wareya/fft/blob/master/fft.hpp
# ...and then optimized because GDScript
# License: {
# Public-domain single-header library
# implementing radix-2 decimation-in-time FFT (i.e. FFT for powers of 2)
#
# This software is dual-licensed to the public domain and under the following
# license: you are granted a perpetual, irrevocable license to copy, modify,
# publish, and distribute this file as you see fit.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
# OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
# THIS SOFTWARE.
# }
# In addition to the above license, this derived version may,
# if legally permissible, be considered to be under the Unlicense:
# {
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>
# }

class_name GDScriptFFTBuffer
extends Reference

# precomputation stuff (a reminder that this is GDScript)
var size: int
# so, funny thing: for the power-of-two progression,
# the table of size X can be placed at offset X - 1
var sines: PoolRealArray
var cosines: PoolRealArray
# "pyramidal remap" offsets (takes over "gap" responsibilities from the C++).
# Values are input offsets for the given output offsets.
var remap: PoolIntArray

# main data buffers
# these are (real, imaginary) vectors
# this works with a lot of stuff that gets done with these,
# so it's more efficient
var input: PoolVector2Array
var output: PoolVector2Array

const SIGN_FORWARDS = -1
const SIGN_BACKWARDS = 1

func _init(fft_size: int, other_self = null):
	size = fft_size
	input.resize(size)
	output.resize(size)
	if other_self != null:
		assert(other_self.size >= size)
		# Borrow tables from the other instance
		cosines = other_self.cosines
		sines = other_self.sines
		remap = other_self.remap
	else:
		# precompute tables - informal proof by example of the layout:
		# 1: 0 (0)
		# 2: 1 (1, 2)
		# 4: 3 (3, 4, 5, 6)
		# 8: 7 (7, 8, 9, 10, 11, 12, 13, 14)
		# 16: 15
		# hypothetically we "don't need to" generate so many tables
		# we also don't need more than half the table
		# BUT this reduces the ops in the inner loop!!!!
		var table_size = 1
		while true:
			# print(table_size, " @ ", len(cosines))
			for i in range(table_size):
				cosines.push_back(cos((2.0 * PI * i) / table_size))
				sines.push_back(sin((2.0 * PI * i) / table_size))
			_gen_remap(0, table_size, 1)
			if table_size == size:
				break
			elif table_size > size:
				push_error("FFT size " + str(size) + " invalid")
				break
			table_size *= 2

func _gen_remap(base: int, table_size: int, gap: int):
	if table_size == 1:
		remap.push_back(base)
	else:
		_gen_remap(base, table_size >> 1, gap << 1)
		_gen_remap(base + gap, table_size >> 1, gap << 1)

# Main "twiddler" loop.
# This is the reference implementation the fastpaths skip steps of as appropriate.
func _fft_core_twiddle(output_ofs: int, stage_size: int, sgn: int):
	# assert(stage_size > 1, "nonsensical stage_size")
	var s2 := stage_size >> 1
	var output_ofs_b := output_ofs + s2
	# inline the first table entry - see _fft_core_2 for explanation
	var a := output[output_ofs]
	var b := output[output_ofs_b]
	output[output_ofs] = a + b
	output[output_ofs_b] = a - b
	# done, now for the meat of it...
	# note the first table entry is skipped
	var i := 1
	var table_idx = stage_size
	while i < s2:
		# advance offsets (accounting for what was already done)
		output_ofs += 1
		output_ofs_b += 1

		a = output[output_ofs]
		b = output[output_ofs_b]

		var twiddle_real := cosines[table_idx]
		var twiddle_imag := sines[table_idx] * sgn

		var bias := Vector2(b.x * twiddle_real - b.y * twiddle_imag, b.y * twiddle_real + b.x * twiddle_imag)

		output[output_ofs] = a + bias
		output[output_ofs_b] = a - bias

		i += 1
		table_idx += 1

func _fft_core_2(ofs: int, table_size: int):
	# Fastpath.
	# Merges several calculations together and inlines.
	# Firstly, we skip doing two copies from input->output.
	# The values that would have been copied:
	var a := input[remap[ofs + table_size - 1]]
	var b := input[remap[ofs + table_size]]

	# Only doing one twiddle (s2 == 1)
	# twiddle_real = cos((2*pi*0)/2) = 1.0
	# twiddle_imag = sin((2*pi*0)/2) = 0.0
	# this removes twiddle_imag terms, thus bias == b
	output[ofs] = a + b
	output[ofs + 1] = a - b

# 21:26 WATCH ANNOYING

# IMPORTANT: Unlike the upstream codebase, remapping is table-based.
# As such, there's a difference between table size and stage size.
# Input and output offsets are assumed to be anchored at 0 for reasons.
func fft_core(ofs: int, table_size: int, stage_size: int, sgn: int):
	# Start with the "small enough not to try and unroll" paths.
	if stage_size == 8:
		# Need this here as part of removing the recursion.
		_fft_core_2(ofs, table_size)
		_fft_core_2(ofs + 2, table_size)
		_fft_core_2(ofs + 4, table_size)
		_fft_core_2(ofs + 6, table_size)
		_fft_core_twiddle(ofs, 4, sgn)
		_fft_core_twiddle(ofs + 4, 4, sgn)
		_fft_core_twiddle(ofs, 8, sgn)
	elif stage_size == 4:
		# Need this here as part of removing the recursion.
		_fft_core_2(ofs, table_size)
		_fft_core_2(ofs + 2, table_size)
		_fft_core_twiddle(ofs, 4, sgn)
	elif stage_size == 2:
		# We have this, might as well use it
		_fft_core_2(ofs, table_size)
	elif stage_size == 1:
		output[ofs] = input[remap[ofs + table_size - 1]]
	else:
		# Main "recurser".
		# We need to twiddle upwards through the levels.
		var ofs_lim := ofs + stage_size
		# Step 1: Base 2-twiddle pass.
		# Notably this can be "vectorized and inlined".
		var ofs_tmp := ofs
		var rm_tmp := ofs + table_size - 1
		while ofs_tmp < ofs_lim:
			var x0 := input[remap[rm_tmp]]
			var x1 := input[remap[rm_tmp + 1]]
			var x2 := input[remap[rm_tmp + 2]]
			var x3 := input[remap[rm_tmp + 3]]
			var x4 := input[remap[rm_tmp + 4]]
			var x5 := input[remap[rm_tmp + 5]]
			var x6 := input[remap[rm_tmp + 6]]
			var x7 := input[remap[rm_tmp + 7]]
			# inlined 2-twiddles for the whole set of 8
			var y0 := x0 + x1
			var y1 := x0 - x1
			var y2 := x2 + x3
			var y3 := x2 - x3
			var y4 := x4 + x5
			var y5 := x4 - x5
			var y6 := x6 + x7
			var y7 := x6 - x7
			# inlined 4-twiddles
			# (2*math.pi*0)/4
			# var b02 := y2
			# var b46 := y6
			# (2*math.pi*1)/4
			# an interesting victim of maths, this one:
			# it's a 90-degree angle, so it, too, optimizes out
			# but, it crosses over
			var b13 := Vector2(-y3.y * sgn, y3.x * sgn)
			var b57 := Vector2(-y7.y * sgn, y7.x * sgn)
			# inlined 4-twiddles
			output[ofs_tmp] = y0 + y2
			output[ofs_tmp + 1] = y1 + b13
			output[ofs_tmp + 2] = y0 - y2
			output[ofs_tmp + 3] = y1 - b13
			# -
			output[ofs_tmp + 4] = y4 + y6
			output[ofs_tmp + 5] = y5 + b57
			output[ofs_tmp + 6] = y4 - y6
			output[ofs_tmp + 7] = y5 - b57
			ofs_tmp += 8
			rm_tmp += 8

		# Step 2: Twiddle upwards.
		var substage_size := 8
		while substage_size <= stage_size:
			ofs_tmp = ofs
			while ofs_tmp < ofs_lim:
				_fft_core_twiddle(ofs_tmp, substage_size, sgn)
				ofs_tmp += substage_size
			substage_size = substage_size << 1

func normalize_fft():
	var i := 0
	while i < size:
		output[i] /= size
		i += 1

func half_normalize_fft():
	var i := 0
	var st := sqrt(size)
	while i < size:
		output[i] /= st
		i += 1

# These methods all return self and have dummy args for use with Thread.

func fft_unnormalized(_dummy = null):
	fft_core(0, size, size, SIGN_FORWARDS)
	return self

func ifft_unnormalized(_dummy = null):
	fft_core(0, size, size, SIGN_BACKWARDS)
	return self

func fft(_dummy = null):
	fft_unnormalized()
	half_normalize_fft()
	return self

func ifft(_dummy = null):
	ifft_unnormalized()
	half_normalize_fft()
	return self

func fft_fully_normalized(_dummy = null):
	fft_unnormalized()
	normalize_fft()
	return self

func ifft_fully_normalized(_dummy = null):
	ifft_unnormalized()
	normalize_fft()
	return self

# Converts a hz value to the corresponding FFT band. As a float to allow measuring "coverage".
static func hz_to_fft_band(hz: float, xsize: int, mix_rate: float) -> float:
	return hz * xsize / mix_rate
