extends PanelContainer

var analyzer := TVTAnalyzer.new()

signal did_analysis(analysis)

func analyze(ff: TVTFrequencyFrame, _original: PoolRealArray, _mul: float):
	var res := analyzer.analyze(ff)
	emit_signal("did_analysis", res)
