class_name TVTNoiseModule
extends PanelContainer

onready var noise_profile: PoolRealArray
onready var collator: PoolRealArray
onready var main_vbox = $VBoxContainer/ScrollContainer/VBoxContainer
onready var main_vbox_marker = main_vbox.get_node("marker")

var mul = 1.0
var collator_softness = 1

signal collated_adjusted_frame(ff, original, mul)

func receive_frequency_frame(ff: TVTFrequencyFrame):
	# ensure internal buffers are correct
	var bands = len(ff.values)
	if len(noise_profile) != bands:
		noise_profile.resize(bands)
		collator.resize(bands)
		noise_profile.fill(0.0)
		collator.fill(0.0)
	# remove noise
	var values_collated_noiseless = PoolRealArray()
	values_collated_noiseless.resize(bands)
	for i in range(bands):
		collator[i] = (ff.values[i] + (collator[i] * collator_softness)) / (collator_softness + 1)
		values_collated_noiseless[i] = (collator[i] - noise_profile[i]) * mul
	# create ff with no noise
	var no_noise = TVTFrequencyFrame.new()
	no_noise.band_config = ff.band_config
	no_noise.values = values_collated_noiseless
	# export data from module to indicators
	emit_signal("collated_adjusted_frame", no_noise, ff.values, mul)

func _on_HSlider_value_changed(value):
	mul = value

func _on_b_clear_pressed():
	noise_profile.fill(0.0)
	collator.fill(0.0)

func _on_b_profile_pressed():
	for i in range(len(collator)):
		noise_profile[i] = collator[i]

func _on_csoftness_value_changed(value):
	collator_softness = value
