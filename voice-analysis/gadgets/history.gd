class_name TVTHistoryViewport
extends Control

var _analyses = []
var _cell_size := Vector2.ONE
var _image := Image.new()
var _texture := ImageTexture.new()

export var history = 1

signal clicked_analysis(analysis)

func _draw():
	if len(_analyses) == 0:
		return
	var first_analysis: TVTAnalysis = _analyses[0]
	var img_w := len(_analyses) + 1
	var img_h := len(first_analysis.from.values)
	var data := PoolByteArray()
	data.resize(img_w * img_h)
	for i in range(img_w - 1):
		var a: TVTAnalysis = _analyses[i]
		if len(a.from.values) != img_h:
			continue
		var data_idx := i + (img_w * (img_h - 1))
		for j in range(img_h):
			data[data_idx] = int(min(max(a.from.values[j] * 255, 0), 255))
			data_idx -= img_w
	for j in range(img_h):
		data[((img_h - (j + 1)) * img_w) + img_w - 1] = int(first_analysis.from.band_config.band_shade[j] * 255)
	_image.create_from_data(img_w, img_h, false, Image.FORMAT_L8, data)
	var cell_width = floor(rect_size.x / img_w)
	var cell_height = floor(rect_size.y / img_h)
	_cell_size = Vector2(cell_width, cell_height)
	_texture.create_from_image(_image, 0)
	draw_texture_rect(_texture, Rect2(0, 0, cell_width * img_w, cell_height * img_h), false)
	for i in range(img_w - 1):
		var x = cell_width * i
		var bar1_height = img_h - (1 + _analyses[i].peak_a_index)
		var bar2_height = img_h - (1 + _analyses[i].peak_b_index)
		var bar3_height = img_h - (1 + _analyses[i].peak_c_index)
		var bar4_height = img_h - (1 + _analyses[i].peak_d_index)
		draw_rect(Rect2(x, (cell_height * bar1_height) + (cell_height / 4), cell_width, cell_height / 2), Color(1, 0, 0, 0.5))
		draw_rect(Rect2(x, (cell_height * bar2_height) + (cell_height / 4), cell_width, cell_height / 2), Color(0, 1, 0, 0.5))
		draw_rect(Rect2(x, (cell_height * bar3_height) + (cell_height / 4), cell_width, cell_height / 2), Color(0, 0, 1, 0.5))
		draw_rect(Rect2(x, (cell_height * bar4_height) + (cell_height / 4), cell_width, cell_height / 2), Color(1, 0, 1, 0.5))

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			var idx = floor(event.position.x / _cell_size.x)
			if idx >= 0 and idx < len(_analyses):
				emit_signal("clicked_analysis", _analyses[idx])
		accept_event()

func push_analysis(analysis: TVTAnalysis):
	_analyses.push_back(analysis)
	while len(_analyses) > history:
		_analyses.remove(0)
	update()
