class_name TVTAudioModule
extends Control

var volume_config = 100

# cannot be any less precise than 4096, or it fails to even detect E-3 properly.
var fft_parent := GDScriptFFTBuffer.new(4096)
var band_config := TVTBandConfig.new()

var left_channel := true

var recording_maker_buffer := StreamPeerBuffer.new()
var recording_active := false

onready var esp: AudioEffectCapture = AudioServer.get_bus_effect(0, 0)

var should_be_analyzing := true
signal should_be_analyzing_set(value)

var analysis_queue = []

signal frequency_frame(fr)

func _ready():
	_update_volume()
	_on_set_recording()

func _on_mute_output_toggled(button_pressed):
	AudioServer.set_bus_mute(0, not button_pressed)

func _on_microphone_button_toggled(button_pressed):
	if button_pressed:
		$microphone.play()
	else:
		$microphone.stop()
	_reconsider_should_be_analyzing()

func _on_volume_value_changed(value):
	volume_config = value
	_update_volume()

func _update_volume():
	$microphone.volume_db = linear2db(volume_config / 100.0)
	$VBoxContainer/volume_is_what.text = "Volume: " + str(volume_config) + "%"

func _reconsider_should_be_analyzing():
	var res = $microphone.playing or $recording.playing
	if res != should_be_analyzing:
		should_be_analyzing = res
		print("should be analyzing: ", res)
		emit_signal("should_be_analyzing_set", res)

func _process(_delta):
	if not should_be_analyzing:
		esp.clear_buffer()
		return
	# - actual FFT bit -
	var fft_size_i := int(fft_parent.size)
	var fft_size_f := float(fft_size_i)
	while esp.can_get_buffer(fft_size_i):
		var frame: PoolVector2Array = esp.get_buffer(fft_size_i)
		if recording_active:
			if left_channel:
				var i := 0
				while i < fft_size_i:
					recording_maker_buffer.put_16(int(min(max(frame[i].x * 32767, -32768), 32767)))
					i += 1
			else:
				var i := 0
				while i < fft_size_i:
					recording_maker_buffer.put_16(int(min(max(frame[i].x * 32767, -32768), 32767)))
					i += 1
		var fft_instance := GDScriptFFTBuffer.new(fft_parent.size, fft_parent)
		if left_channel:
			var i := 0
			while i < fft_size_i:
				var frac = i / fft_size_f
				var window = -0.5 * cos(2.0 * PI * frac) + 0.5
				fft_instance.input[i] = Vector2(frame[i].x * window, 0)
				i += 1
		else:
			var i := 0
			while i < fft_size_i:
				var frac = i / fft_size_f
				var window = -0.5 * cos(2.0 * PI * frac) + 0.5
				fft_instance.input[i] = Vector2(frame[i].y * window, 0)
				i += 1
		if OS.can_use_threads():
			var thread := Thread.new()
			thread.start(fft_instance, "fft_fully_normalized")
			analysis_queue.push_back([thread, fft_instance])
		else:
			# not good
			fft_instance.fft_fully_normalized()
			analysis_queue.push_back([null, fft_instance])
	# Send FFTs from threads to analysis
	while len(analysis_queue) > 0:
		var thread: Thread = analysis_queue[0][0]
		if thread != null:
			if thread.is_alive() and OS.can_use_threads():
				# print("not done yet")
				break
			thread.wait_to_finish()
		var fft_instance: GDScriptFFTBuffer = analysis_queue[0][1]
		analysis_queue.pop_front()
		var contents := band_config.load_from_fft(fft_instance.output, AudioServer.get_mix_rate())
		var ff := TVTFrequencyFrame.new()
		ff.band_config = band_config
		ff.values = contents
		emit_signal("frequency_frame", ff)

func _on_right_fft_toggled(button_pressed):
	left_channel = not button_pressed

func _on_play_recording_toggled(button_pressed):
	if button_pressed:
		$recording.play()
	else:
		$recording.stop()
	_reconsider_should_be_analyzing()

func _on_recording_finished():
	$VBoxContainer/HBoxContainer/play_recording.pressed = false
	_reconsider_should_be_analyzing()

func _on_set_recording():
	$VBoxContainer/current_recording.text = "Recorded: " + str($recording.stream.get_length()) + " seconds"

func _on_record_recording_toggled(button_pressed):
	if not button_pressed:
		var a := AudioStreamSample.new()
		a.data = recording_maker_buffer.data_array
		a.format = AudioStreamSample.FORMAT_16_BITS
		a.mix_rate = int(AudioServer.get_mix_rate())
		$recording.stream = a
		_on_set_recording()
	recording_maker_buffer.data_array = PoolByteArray()
	recording_maker_buffer.big_endian = false
	recording_active = button_pressed
