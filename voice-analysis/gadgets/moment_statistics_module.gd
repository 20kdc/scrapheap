extends PanelContainer

var last_live_analysis: TVTAnalysis
var live_enabled := true

onready var op1: ProgressBar = $VBoxContainer/GridContainer/op1
onready var op2: ProgressBar = $VBoxContainer/GridContainer/op2
onready var op3: ProgressBar = $VBoxContainer/GridContainer/op3

func receive_live_analysis(analysis: TVTAnalysis):
	last_live_analysis = analysis
	if live_enabled:
		show_analysis(analysis)

func selected_live_analysis():
	if last_live_analysis != null:
		show_analysis(last_live_analysis)
	live_enabled = true
	$VBoxContainer/Label.text = "Live Statistics"

func selected_history_analysis(analysis: TVTAnalysis):
	show_analysis(analysis)
	live_enabled = false
	$VBoxContainer/Label.text = "Moment Statistics"

func show_analysis(analysis: TVTAnalysis):
	var hz = analysis.from.band_config.band_hz[analysis.peak_a_index]
	var note = TVTNoteScale.hz_to_note(hz)
	$VBoxContainer/peak_note.text = "Peak Note: " + TVTNoteScale.get_note_name(note)
	op1.value = analysis.peak_b_relative_strength
	op2.value = analysis.peak_c_relative_strength
	op3.value = analysis.peak_d_relative_strength
