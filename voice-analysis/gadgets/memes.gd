extends Label

const MEMES = [
	"20kdc's Voice Analyzer: Out Of The Closet Edition",
	"Introducing, the Egg Egg.",
	"Nomfing ears since... 2022? 2021? 2020? Aaaaaaaa",
	"kijetesantakalu li pona tawa tonsi (to the tune of musi Supakalipatalisinkepijalitonson)",
	"musi Manka la mu'ing contest",
	"Repeat after me: \"The ELS unit has been decommissioned, but it served well.\"",
	"Unsurprisingly, it was not necessary to wait for Godot, but for hardware support",
	"Repeat after me: \"I will not have obscene hardware requirements\"",
	"At kulupu 20kdc, we carve a better future. Literally. The noise you're hearing is the TBM.",
	"Do not pet the cat",
	"Do not pet the dog",
	"Do not the yupekosi",
	"10 years since the beginning of time",
	"I will not lawa the house, I will pali or wile it"
]

func _ready():
	randomize()
	text = "'" + MEMES[randi() % len(MEMES)] + "'"
