extends PanelContainer

signal selected_live_analysis()
signal selected_history_analysis(analysis)

func receive_analysis(analysis: TVTAnalysis):
	$HSplitContainer/history.push_analysis(analysis)
	$HSplitContainer/live.push_analysis(analysis)

func _on_live_clicked_analysis(_analysis: TVTAnalysis):
	emit_signal("selected_live_analysis")

func _on_history_clicked_analysis(analysis: TVTAnalysis):
	emit_signal("selected_history_analysis", analysis)
