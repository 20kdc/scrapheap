## Single animation, intended for conversion into GIF/APNG.
tool
class_name LACPanel
extends LACAnimationSingle

## FPS of the animation when exported as GIF or APNG.
export var animation_fps = 20.0

func _preload():
	var _meh = preload("anim_single.gd")

func _get_configuration_warning():
	var base = ._get_configuration_warning()
	if base != "":
		return base
	if next_scene == null and not get_animation("RESET").loop:
		return "The RESET animation should loop."
	if autoplay != "RESET":
		return "The RESET animation should be set to autoplay."
	return ""
