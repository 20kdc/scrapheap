## Contains root animation parameters.
## To be clear: This extends AnimationPlayer because it has to for downstream nodes to be created correctly.
## HOWEVER, it does not require you to do anything with the AnimationPlayer!!!
## With that in turn said, the LACAnimation is a good place for a splash screen fadeout/etc.
tool
class_name LACAnimation
extends AnimationPlayer

# 650x450 is the standard
export var size = Vector2(650, 450) setget _set_size
export var transparency_enabled = false setget _set_transparency_enabled
export var boot_splash_show_image = true setget _set_boot_splash_show_image
export var boot_splash_path = "" setget _set_boot_splash_path
export var boot_splash_fullsize = true setget _set_boot_splash_fullsize
export var boot_splash_use_filter = true setget _set_boot_splash_use_filter
export var boot_splash_bg_color = Color(0.14, 0.14, 0.14) setget _set_boot_splash_bg_color

func _ready():
	LACAnimSettingsBroker.update_project_settings()

func _set_size(v):
	size = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_transparency_enabled(v):
	transparency_enabled = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_boot_splash_show_image(v):
	boot_splash_show_image = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_boot_splash_path(v):
	boot_splash_path = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_boot_splash_fullsize(v):
	boot_splash_fullsize = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_boot_splash_use_filter(v):
	boot_splash_use_filter = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()

func _set_boot_splash_bg_color(v):
	boot_splash_bg_color = v
	if is_inside_tree():
		LACAnimSettingsBroker.update_project_settings()
