## Single animation
tool
class_name LACAnimationSingle
extends LACAnimation

## Next scene, if any.
export var next_scene: PackedScene

func _ready():
	._ready()
	connect("animation_started", self, "_animation_started")
	connect("animation_finished", self, "_animation_finished")

func _preload():
	var _meh = preload("anim.gd")

func _animation_started(name):
	if LACAnimSettingsBroker.rec_mode:
		# absolutely DO NOT loop, or we'll never finish the export!
		get_animation(name).loop = false

func _animation_finished(_blah):
	if next_scene != null:
		get_tree().change_scene_to(next_scene)
	elif LACAnimSettingsBroker.rec_mode:
		LACAnimSettingsBroker.rec_completed = true

func _get_configuration_warning():
	var animations = get_animation_list()
	if len(animations) != 1:
		return "There should be only a single animation, called RESET."
	if animations[0] != "RESET":
		return "The animation should be renamed to RESET."
	if not reset_on_save:
		return "Reset On Save should be set."
	var info = get_animation("RESET")
	if next_scene != null and info.loop:
		return "RESET loops, but there is a next scene."
	return ""
