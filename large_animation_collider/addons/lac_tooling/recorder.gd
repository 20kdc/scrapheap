extends Node

var frame = 0
var png_names = PoolStringArray()
var has_initiated_complete_process = false

const FILEBASE = "__Animator_TMP_WillBeDeleted"

func _ready():
	var dir = Directory.new()
	if dir.dir_exists(FILEBASE):
		print("Recording directory " + FILEBASE + " already exists, DELETING CONTENTS")
		var dir_inner = Directory.new()
		if dir_inner.open(FILEBASE) == OK:
			dir_inner.list_dir_begin(true, false)
			var delme = PoolStringArray()
			while true:
				var f = dir_inner.get_next()
				if f == "":
					break
				delme.push_back(f)
			for v in delme:
				dir_inner.remove(v)
	else:
		dir.make_dir(FILEBASE)
	File.new().open(FILEBASE + "/.gdignore", File.WRITE)
	var tmp = File.new()
	tmp.open(FILEBASE + "/.gitignore", File.WRITE)
	tmp.store_line("*")
	tmp.close()

func _do_exec(filename, args: PoolStringArray):
	# we want this to go to console
	var output = []
	print("LACRecorder: Running ", filename)
	OS.execute(filename, args, true, output, true)
	for v in output:
		print(v)

func _process(_delta):
	if LACAnimSettingsBroker.rec_completed:
		if not has_initiated_complete_process:
			has_initiated_complete_process = true
			call_deferred("_actually_complete")
	# first frame doesn't render properly
	if frame > 0:
		var tex = get_viewport().get_texture()
		var img = tex.get_data()
		img.flip_y()
		# really small images cause problems, crop them
		img.crop(LACAnimSettingsBroker.rec_w, LACAnimSettingsBroker.rec_h)
		var png_name = FILEBASE + "/" + str(frame).pad_zeros(8) + ".png"
		png_names.push_back(png_name)
		img.save_png(png_name)
	frame += 1

# to try and ensure seamless looping...
func _actually_complete():
	# alright, what now?
	var fps = LACAnimSettingsBroker.rec_fps
	var ffe = "ffmpeg"
	if ProjectSettings.has_setting("large_animation_collider/ffmpeg_path_beware_this_is_exported"):
		ffe = ProjectSettings.get_setting("large_animation_collider/ffmpeg_path_beware_this_is_exported")
	var ske = "gifski"
	if ProjectSettings.has_setting("large_animation_collider/gifski_path_beware_this_is_exported"):
		ske = ProjectSettings.get_setting("large_animation_collider/gifski_path_beware_this_is_exported")
	_do_exec(ffe, PoolStringArray(["-framerate", str(fps), "-i", FILEBASE + "/%08d.png", "-plays", "0", FILEBASE + "/output.apng", "-nostats", "-hide_banner"]))
	# "feature" (of being stupid and broken and needing wildcards)
	var pool = PoolStringArray()
	if OS.has_feature("Windows"):
		pool.push_back(FILEBASE + "/*.png")
	else:
		pool.push_back("--no-sort")
		pool.append_array(png_names)
	pool.push_back("-W")
	pool.push_back("4096")
	pool.push_back("-H")
	pool.push_back("4096")
	pool.push_back("-r")
	pool.push_back(str(fps))
	pool.push_back("-o")
	pool.push_back(FILEBASE + "/output.gif")
	_do_exec(ske, pool)
	get_tree().quit(0)
