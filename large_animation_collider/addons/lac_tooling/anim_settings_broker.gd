## Animations report to this to tell the editor plugin to fixup stuff.
## This is mainly so window size matches what the animation is doing.
## This is also used for the tooling UI to access the editor plugin.
tool
extends Node

# only valid in-editor, of course
var editor_plugin: Node = null
var rec_mode: bool = false
var rec_fps: float = 1
var rec_completed: bool = false
var rec_w: int = 1
var rec_h: int = 1

func _ready():
	if not Engine.editor_hint:
		var ca = OS.get_cmdline_args()
		var lr = ca.find("--lac-record")
		if lr != -1:
			rec_mode = true
			rec_fps = float(ca[lr + 1])
			rec_w = int(ca[lr + 2])
			rec_h = int(ca[lr + 3])
			print("LACAnimSettingsBroker believes it needs to write out a recording at " + str(rec_fps) + " FPS, " + str(rec_w) + "x" + str(rec_h))
			var recorder = load("res://addons/lac_tooling/recorder.gd").new()
			add_child(recorder)

func update_project_settings():
	if Engine.editor_hint:
		if editor_plugin != null:
			editor_plugin.plugin_update_project_settings()
	elif is_inside_tree():
		if get_tree().current_scene is LACAnimation:
			var anim: LACAnimation = get_tree().current_scene
			get_viewport().transparent_bg = anim.transparency_enabled
			if not rec_mode:
				var expected_size = anim.size
				OS.window_size = expected_size
