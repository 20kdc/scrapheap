class_name LACEditorPlugin
tool
extends EditorPlugin

var is_visible: bool
var menu1: Control
var menu2: Control

func _enter_tree():
	var did_important = false
	if not ProjectSettings.has_setting("large_animation_collider/ffmpeg_path_beware_this_is_exported"):
		ProjectSettings.set_setting("large_animation_collider/ffmpeg_path_beware_this_is_exported", "ffmpeg")
		did_important = true
	if not ProjectSettings.has_setting("large_animation_collider/gifski_path_beware_this_is_exported"):
		ProjectSettings.set_setting("large_animation_collider/gifski_path_beware_this_is_exported", "gifski")
		did_important = true
	ProjectSettings.set_initial_value("large_animation_collider/ffmpeg_path_beware_this_is_exported", "ffmpeg")
	ProjectSettings.set_initial_value("large_animation_collider/gifski_path_beware_this_is_exported", "gifski")
	ProjectSettings.add_property_info({
		"name": "large_animation_collider/ffmpeg_path_beware_this_is_exported",
		"type": TYPE_STRING
	})
	ProjectSettings.add_property_info({
		"name": "large_animation_collider/gifski_path_beware_this_is_exported",
		"type": TYPE_STRING
	})
	if did_important:
		ProjectSettings.save()
	add_autoload_singleton("LACAnimSettingsBroker", "res://addons/lac_tooling/anim_settings_broker.gd")
	LACAnimSettingsBroker.editor_plugin = self

func _exit_tree():
	make_visible(false)
	if menu1 != null:
		menu1.free()
		menu1 = null
	if menu2 != null:
		menu2.free()
		menu2 = null

func handles(object):
	return object is LACAnimation

func make_visible(visible):
	if menu1 == null:
		menu1 = preload("editor/dock.tscn").instance()
	if menu2 == null:
		menu2 = preload("editor/dock.tscn").instance()
	if visible and not is_visible:
		add_control_to_container(EditorPlugin.CONTAINER_CANVAS_EDITOR_MENU, menu1)
		add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, menu2)
	if is_visible and not visible:
		remove_control_from_container(EditorPlugin.CONTAINER_CANVAS_EDITOR_MENU, menu1)
		remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, menu2)
	is_visible = visible

func plugin_animation_to_gif():
	var esr = get_editor_interface().get_edited_scene_root()
	if not esr is LACPanel:
		print("Can't GIFize! You can only GIFize LACPanel.")
		return
	var backup = ProjectSettings.get("editor/main_run_args")
	ProjectSettings.set("editor/main_run_args", "--fixed-fps " + str(esr.animation_fps) + " --lac-record " + str(esr.animation_fps) + " " + str(esr.size.x) + " " + str(esr.size.y))
	plugin_set_main_scene()
	get_editor_interface().play_current_scene()
	ProjectSettings.set("editor/main_run_args", backup)
	ProjectSettings.save()

func plugin_set_main_scene():
	var esr = get_editor_interface().get_edited_scene_root()
	_int_set_main_scene(esr)

func plugin_update_project_settings():
	var esr = get_editor_interface().get_edited_scene_root()
	_int_update_project_settings(esr)

func _int_set_main_scene(esr: Node):
	ProjectSettings.set("application/run/main_scene", esr.filename)
	_int_update_project_settings(esr)

func _int_update_project_settings(esr: Node):
	if esr is LACAnimation:
		# animation settings
		ProjectSettings.set("display/window/size/width", int(esr.size.x))
		ProjectSettings.set("display/window/size/height", int(esr.size.y))
		ProjectSettings.set("application/boot_splash/show_image", esr.boot_splash_show_image)
		ProjectSettings.set("application/boot_splash/image", esr.boot_splash_path)
		ProjectSettings.set("application/boot_splash/fullsize", esr.boot_splash_fullsize)
		ProjectSettings.set("application/boot_splash/use_filter", esr.boot_splash_use_filter)
		ProjectSettings.set("application/boot_splash/bg_color", esr.boot_splash_bg_color)
		# general settings; we *have* to set these or things don't work properly
		# in particular, you can't get alpha out of an HDR framebuffer
		ProjectSettings.set("rendering/quality/depth/hdr", false)
		ProjectSettings.set("display/window/per_pixel_transparency/allowed", esr.transparency_enabled)
		ProjectSettings.set("display/window/per_pixel_transparency/enabled", esr.transparency_enabled)
		# continue...
		_recursively_find_redrawables(get_tree().root)
		ProjectSettings.save()

func _recursively_find_redrawables(node: Node):
	if node is Control:
		if node.get_class() == "CanvasItemEditorViewport":
			node.update()
			return true
	for v in node.get_children():
		if _recursively_find_redrawables(v):
			return true
	return false
