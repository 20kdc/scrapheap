tool
extends MenuButton

func _ready():
	get_popup().connect("id_pressed", self, "_id_pressed")

func _id_pressed(id):
	if id == 0:
		print("LAC: Set Main Scene")
		LACAnimSettingsBroker.editor_plugin.plugin_set_main_scene()
	elif id == 1:
		print("LAC: Animation To GIF")
		LACAnimSettingsBroker.editor_plugin.plugin_animation_to_gif()
