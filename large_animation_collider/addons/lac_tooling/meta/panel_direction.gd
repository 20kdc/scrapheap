## This is a note to be put on a planning board.
tool
class_name LACMetaPanelDirection
extends Node2D

enum PanelImplementation {
	IMAGE = 0,
	GIF = 1,
	APNG = 2,
	GODOT_ANIM = 3,
	GODOT_GAME = 4,
	VIDEO_WEBM = 5,
	VIDEO_EXTERNAL = 6,
	OTHER = 7
}

export(PanelImplementation) var implementation = 0 setget _set_implementation
export(String, MULTILINE) var scene_direction = "" setget _set_scene_direction
export(String, MULTILINE) var text = "" setget _set_text
export(String, MULTILINE) var links = "" setget _set_links
var _my_visual: Control = null

func _ready():
	connect("renamed", self, "_was_renamed")
	_my_visual = preload("panel_direction_visual.tscn").instance()
	_my_visual.connect("resized", self, "_nudge_visual_offset")
	add_child(_my_visual)
	_update_my_visual()

func _set_scene_direction(v):
	scene_direction = v
	_update_my_visual()

func _set_implementation(v):
	implementation = v
	_update_my_visual()

func _set_text(v):
	text = v
	_update_my_visual()

func _set_links(v):
	links = v
	_update_my_visual()

func _was_renamed():
	_update_my_visual()

func _implementation_to_string() -> String:
	if implementation == PanelImplementation.IMAGE:
		return "Image"
	if implementation == PanelImplementation.GIF:
		return "GIF"
	if implementation == PanelImplementation.APNG:
		return "APNG"
	if implementation == PanelImplementation.GODOT_ANIM:
		return "Godot Animation"
	if implementation == PanelImplementation.GODOT_GAME:
		return "Godot Game"
	if implementation == PanelImplementation.VIDEO_WEBM:
		return "Video (WebM)"
	if implementation == PanelImplementation.VIDEO_EXTERNAL:
		return "Video (External)"
	if implementation == PanelImplementation.OTHER:
		return "Other"
	return "Unknown"

func _update_my_visual():
	if _my_visual != null:
		_my_visual.get_node("v/h/title").text = name
		_my_visual.get_node("v/h/implementation").text = _implementation_to_string()
		_my_visual.get_node("v/scene_direction").text = scene_direction
		_my_visual.get_node("v/text").text = text
		_my_visual.get_node("v/links").text = links
		_my_visual.rect_size = _my_visual.get_minimum_size()

func _nudge_visual_offset():
	if _my_visual != null:
		_my_visual.rect_position = -(_my_visual.rect_size / 2)
