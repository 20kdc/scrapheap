tool
extends Node

func _enter_tree():
	# you WILL get errors here while editing visuals
	if get_parent()._my_visual != self:
		queue_free()
