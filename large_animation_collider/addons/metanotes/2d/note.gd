## This is a note to be put on a planning board.
tool
class_name MetaNote2D, "../icon_note.png"
extends Node2D

export var colour = Color.black setget _set_colour
export(String, MULTILINE) var text = "" setget _set_text
var _my_visual: Control = null

func _ready():
	connect("renamed", self, "_was_renamed")
	_my_visual = preload("note_visual.tscn").instance()
	_my_visual.connect("resized", self, "_nudge_visual_offset")
	add_child(_my_visual)
	_update_my_visual()

func _set_text(v):
	text = v
	_update_my_visual()

func _set_colour(v):
	colour = v
	_update_my_visual()

func _was_renamed():
	_update_my_visual()

func _update_my_visual():
	if _my_visual != null:
		var note_sb = StyleBoxFlat.new()
		note_sb.corner_radius_top_left = 8
		note_sb.corner_radius_bottom_left = 8
		note_sb.corner_radius_top_right = 8
		note_sb.corner_radius_bottom_right = 8
		note_sb.border_width_top = 8
		note_sb.border_width_right = 8
		note_sb.border_width_left = 8
		note_sb.border_width_bottom = 8
		note_sb.border_color = colour
		note_sb.bg_color = Color.black
		note_sb.expand_margin_top = 4
		note_sb.expand_margin_right = 4
		note_sb.expand_margin_left = 4
		note_sb.expand_margin_bottom = 4
		_my_visual.add_stylebox_override("panel", note_sb)
		_my_visual.get_node("v/title").text = name
		_my_visual.get_node("v/detail").text = text
		_my_visual.rect_size = _my_visual.get_minimum_size()

func _nudge_visual_offset():
	if _my_visual != null:
		_my_visual.rect_position = -(_my_visual.rect_size / 2)
