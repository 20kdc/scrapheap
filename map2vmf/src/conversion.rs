use crate::vmf::*;
use shalrath::repr::*;

pub fn wfi(out: &mut Vec<VmfKV>, id_counter: &mut i32) {
	out.push(vmf_kst("id", format!("{}", id_counter)));
	*id_counter += 1;
}

pub fn fvc(v: Point) -> String {
	format!("({} {} {})", v.x, v.y, v.z)
}

pub fn pvc(p: &BrushPlane, vaxis: bool) -> String {
	let scale = if vaxis { p.scale_y } else { p.scale_x };
	let tplane = match p.texture_offset {
		TextureOffset::Valve { u, v } => {
			if vaxis {
				v
			} else {
				u
			}
		}
		TextureOffset::Standard { u, v } => {
			// this isn't accurate just make it work kinda
			if vaxis {
				TexturePlane {
					x: 1.0,
					y: 0.0,
					z: 0.0,
					d: v,
				}
			} else {
				TexturePlane {
					x: 0.0,
					y: 1.0,
					z: 0.0,
					d: u,
				}
			}
		}
	};
	format!(
		"[{} {} {} {}] {}",
		tplane.x, tplane.y, tplane.z, tplane.d, scale
	)
}

pub fn convert_side(plane: &BrushPlane, id_counter: &mut i32, lightmapscale: &str) -> VmfKV {
	let mut obj = vmfobj_new();
	wfi(&mut obj, id_counter);
	obj.push(vmf_kst(
		"plane",
		format!(
			"{} {} {}",
			fvc(plane.plane.v0),
			fvc(plane.plane.v1),
			fvc(plane.plane.v2)
		),
	));
	obj.push(vmf_ksb("material", &plane.texture));
	obj.push(vmf_ksb("uaxis", &pvc(&plane, false)));
	obj.push(vmf_ksb("vaxis", &pvc(&plane, true)));
	obj.push(vmf_kst("rotation", format!("{}", &plane.angle)));
	obj.push(vmf_ksb("lightmapscale", lightmapscale));
	obj.push(vmf_ksb("smoothing_groups", "0"));
	vmf_obj("side", obj)
}

pub fn convert_brush(brush: &Brush, id_counter: &mut i32, lightmapscale: &str) -> VmfKV {
	let mut obj = vmfobj_new();
	wfi(&mut obj, id_counter);
	for plane in &brush.0 {
		obj.push(convert_side(&plane, id_counter, lightmapscale))
	}
	vmf_obj("solid", obj)
}
