pub mod conversion;
pub mod vmf;

use std::{
	env::args,
	fs::File,
	io::{Read, Write},
};

use conversion::{convert_brush, wfi};
use shalrath::repr::*;
use vmf::*;

fn wfb(f: &mut File, s: &str) {
	f.write_all(&s.as_bytes()).unwrap();
}

fn main() {
	let mut input: String = "in.map".to_string();
	let mut output: String = "out.vmf".to_string();
	let mut idx = 0;
	for arg in args().skip(1) {
		if idx == 0 {
			input = arg;
		} else if idx == 1 {
			output = arg;
		} else {
			panic!("must have up to 2 args, input and output");
		}
		idx += 1;
	}
	let mut buf = String::new();
	{
		let mut f = File::open(input).unwrap();
		f.read_to_string(&mut buf).unwrap();
	}
	let map: Map = buf.parse().unwrap();
	let ents = map.0;
	let mut id_counter = 0;
	let mut avmf = vmfobj_new();
	// step 1. prepare versioninfo
	{
		let mut vi = vmfobj_new();
		vi.push(vmf_ksb("mapversion", "0"));
		vi.push(vmf_ksb("formatversion", "100"));
		vi.push(vmf_ksb("prefab", "0"));
		avmf.push(vmf_obj("versioninfo", vi));
	}
	// step 2. assemble stuff
	let mut world = vmfobj_new();
	wfi(&mut world, &mut id_counter);
	let mut world_lightmapscale: String = "16".to_string();
	let mut entities: Vec<VmfObj> = Vec::new();
	{
		// initial header
		for ent in ents {
			// three handlings, work out which it is...
			let mut classname: String = "info_noclassname".to_string();
			let mut lightmapscale: String = "16".to_string();
			// critical handling control props
			for prop in &ent.properties.0 {
				if prop.key == "_m2v_lightmapscale" {
					lightmapscale = prop.value.to_string();
				} else if prop.key == "classname" {
					classname = prop.value.to_string();
				}
			}
			// change handling based on classname
			if classname == "worldspawn" {
				// worldspawn, copy over props & brushes
				for prop in &ent.properties.0 {
					world.push(vmf_ksb(&prop.key, &prop.value));
				}
				for brush in ent.brushes.0 {
					world.push(convert_brush(&brush, &mut id_counter, &lightmapscale));
				}
				world_lightmapscale = lightmapscale.to_string();
			} else if classname == "func_group" || classname == "func_group_custom" {
				// func_group: TB group, just copy brushes to world - custom is for manual adjustment
				for brush in ent.brushes.0 {
					world.push(convert_brush(&brush, &mut id_counter, &world_lightmapscale));
				}
			} else {
				// entity, leave well alone
				let mut vmfent = vmfobj_new();
				wfi(&mut vmfent, &mut id_counter);
				for prop in &ent.properties.0 {
					vmfent.push(vmf_ksb(&prop.key, &prop.value));
				}
				for brush in ent.brushes.0 {
					vmfent.push(convert_brush(&brush, &mut id_counter, &lightmapscale));
				}
				entities.push(vmfent);
			}
		}
	}
	// step 3. add world/entities to file
	avmf.push(vmf_obj("world", world));
	for e in entities {
		avmf.push(vmf_obj("entity", e));
	}
	// step 4. write file
	let mut out = File::create(output).unwrap();
	for v in avmf {
		wfb(&mut out, &format!("{} {}\n", v.0, v.1));
	}
}
