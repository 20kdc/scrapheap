use std::fmt::Display;

/// VMF value
pub enum VmfValue {
	S(String),
	D(VmfObj),
}

pub type VmfKV = (String, VmfValue);
pub type VmfObj = Vec<VmfKV>;

impl Display for VmfValue {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match &self {
			VmfValue::S(str) => {
				f.write_str("\"")?;
				f.write_str(str)?;
				f.write_str("\"")?;
			}
			VmfValue::D(p) => {
				f.write_str("{\n")?;
				for (a, b) in p {
					f.write_fmt(format_args!("{} {}\n", a, b))?;
				}
				f.write_str("}\n")?;
			}
		}
		Ok(())
	}
}

pub fn vmf_ksb(s: &str, v: &str) -> VmfKV {
	(s.to_string(), VmfValue::S(v.to_string()))
}

pub fn vmf_kst(s: &str, v: String) -> VmfKV {
	(s.to_string(), VmfValue::S(v))
}

pub fn vmf_obj(s: &str, v: Vec<VmfKV>) -> VmfKV {
	(s.to_string(), VmfValue::D(v))
}

pub fn vmfobj_new() -> VmfObj {
	Vec::new()
}
