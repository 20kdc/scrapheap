# The Scrapheap

This is a repository used for when something doesn't really need to have a dedicated repository, but is potentially useful for others and thus isn't just in my personal repository.

See COPYING for license details.

## Third-Party Credits

Inclusion in this list does not imply endorsement in any direction.

* voice-analysis FFT code is ported from <https://github.com/wareya/fft/blob/master/fft.hpp>
* Perspective Writer uses [these fonts](https://gitlab.com/20kdc/scrapheap/-/tree/main/perspective-writer/thirdparty/noto?ref_type=heads) under the OFL:
	* https://fonts.google.com/noto/specimen/Noto+Emoji
	* https://fonts.google.com/noto/specimen/Noto+Sans+Mono
	* https://fonts.google.com/noto/specimen/Noto+Sans
	* https://fonts.google.com/noto/specimen/Noto+Serif
* fit-tester uses (but doesn't include in this repository) <https://github.com/godotvr/godot_openxr_vendors>. This is under the MIT license, but fit-tester is unlikely to get binary releases anyway.

## How Do Releases Work?

The scrapheap isn't meant to be a place for a bunch of formal release architecture. It's a place to throw stuff at the wall, and hey, some of it might stick.

Still, apparently someone needs isle-tooling releases, and libpva needs a demo page, which means there's still *some* architecture involved.

There are presently two kinds of releases:

* isle-tooling releases are made by pushing a commit to and adding a tag on the `isle-tooling-release` branch. (The reliability of this mechanism is yet to be tested and it's probably only checking for one or the other. If necessary I'll pick "any merge to isle-tooling-release creates a release".)
* Pages updates are made by hitting a manual button which performs the release.

This probably won't scale to a lot of projects, so it might be an idea to figure out what has to leave scrapheap. Alternatively it might be an idea just to run everything off of the Pages build chain, but the problem there is figuring out how to initiate a sequence of GitLab CI jobs from a single button without making them trigger with every commit. Maybe just tie them all to a branch like with isle-tooling?


