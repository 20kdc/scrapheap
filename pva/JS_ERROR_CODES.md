* BS04: Huffman decode error, overlapping symbols/branches
* BS09: Huffman decode error, overlapping symbols/branches
* BS27: Bitstream ran off of end
* CV48: Canvas 2D context init fail
* GL24: WebGL context init fail
* SL10: WebGL creation threw an exception, trying canvas
* FM32: Unknown CABS chunk type
* FM64: pvaDecArray fed invalid type
* DF18: Reserved Deflate block kind

