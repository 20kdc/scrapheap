/// <reference path="license.ts"/>

/**
 * Reader for bits in the DEFLATE style:
 * The first bit in a byte is 1, the last 128.
 * Fields are stored bottom-bit-first, so it neatly aligns with this order.
 * (Huffman codes are handled bit-by-bit and are special.)
 */
class PVABitReader {
	array: Uint8Array;
	arrayByteIndex: number;
	bitsRemaining: number;
	bitsBuffer: number;

	constructor(arrayBuffer: ArrayBuffer, offset: number) {
		this.array = new Uint8Array(arrayBuffer);
		this.arrayByteIndex = offset;
		this.bitsRemaining = 0;
		this.bitsBuffer = 0;
	}

	/**
	 * Adds the next byte into the bitstream.
	 */
	nextByte(): void {
		if (this.arrayByteIndex >= this.array.length) {
			throw new Error("BS27");
		}
		let v = this.array[this.arrayByteIndex++];
		this.bitsBuffer |= v << this.bitsRemaining;
		this.bitsRemaining += 8;
	}

	/**
	 * Skip to byte alignment.
	 */
	skipToByte(): void {
		this.bitsBuffer = 0;
		this.bitsRemaining = 0;
	}

	/**
	 * Skip to byte alignment, then get X bytes.
	 */
	getAlignedBytes(count: number): Uint8Array {
		this.bitsBuffer = 0;
		this.bitsRemaining = 0;
		let res = this.array.subarray(this.arrayByteIndex, this.arrayByteIndex + count);
		this.arrayByteIndex += count;
		return res;
	}

	/**
	 * Gets a single bit from the bitstream.
	 */
	getBit(): number {
		if (this.bitsRemaining == 0) {
			this.nextByte();
		}
		let bit = this.bitsBuffer & 1;
		this.bitsBuffer >>= 1;
		this.bitsRemaining -= 1;
		return bit;
	}

	/**
	 * Gets the requested number of bits from the bitstream.
	 */
	getBits(count: number): number {
		var v = 0;
		var raise = 0;
		while (count > 8) {
			v |= this.getBits(8) << raise;
			raise += 8;
			count -= 8;
		}
		if (this.bitsRemaining < count) {
			this.nextByte();
		}
		var mask = 0xFF >> (8 - count);
		v |= (this.bitsBuffer & mask) << raise;
		this.bitsBuffer >>= count;
		this.bitsRemaining -= count;
		return v;
	}
}

/**
 * Huffman table navigator.
 */
class PVAHuffmanNode {
	symbol: number | null;
	branch: PVAHuffmanNode[] | null;

	constructor() {
		this.symbol = null;
		this.branch = null;
	}

	/**
	 * Adds a symbol/code pair to this node.
	 * The code is written into the tree.
	 */
	add(symbol: number, code: number, bit: number): void {
		if (bit == 0) {
			if (this.branch != null) {
				throw new Error("BS04");
			}
			this.symbol = symbol;
		} else {
			if (this.symbol != null) {
				throw new Error("BS09");
			}
			if (this.branch == null) {
				this.branch = [new PVAHuffmanNode(), new PVAHuffmanNode()];
			}
			this.branch[(code & bit) ? 1 : 0].add(symbol, code, bit >> 1);
		}
	}

	/**
	 * Reads a symbol from this node.
	 */
	read(src: PVABitReader): number {
		if (this.symbol != null) {
			return this.symbol;
		}
		return this.branch![src.getBit()].read(src);
	}
}

/**
 * Compiles a Huffman table from symbols and code lengths.
 */
function pvaHuffmanCompile(codeLengths: number[]): PVAHuffmanNode {
	/*
	 * Method taken from RFC1951.
	 * Then, got ported from my previous implementations.
	 *
	 * That said, it needs a bit more explanation than given there, so:
	 * The symbols are effectively sorted majorly by code length (longest last),
	 *  and minorly by symbol number (highest last).
	 * This produces a consistent list of symbols
	 *  to code lengths that can be replicated every time.
	 * The way these are allocated relies on the code length ordering.
	 * Starting from the lowest codelength,
	 *  every code length gets a given area of codespace.
	 * The allocation here is essentially numeric, despite this being a tree,
	 *  so rather than reimplement a binary adder, they just use bitshifts and additions.
	 * I don't understand *how* this part works, or how it avoids all sorts of collisions,
	 *  but it does, so I'm just going to shrug.
	 * It probably has something to do with the property that to
	 *  reach any number above or equal to a power of two N,
	 *  you must have the binary bit for a power of two above or equal to N set.
	 * (Would explain the shifting at least.)
	 * How this maps to the actual algorithm here is kind of a mess -
	 *  the RFC1951 algorithm is more of an optimized implementation than a proof of
	 *  how the algorithm successfully allocates tree space.
	 */
	// Pre-Step-1: Calculate maximum number of bits.
	let maxBits = 0;
	for (let i = 0; i < codeLengths.length; i++) {
		if (maxBits < codeLengths[i]) {
			maxBits = codeLengths[i];
		}
	}
	// Step 1
	// This includes a bit length count for 0.
	// That's not necessary, but it's harmless.
	// And it avoids moving mountains to 0-index.
	let bitLengthCount = [0];
	for (let i = 1; i <= maxBits; i++) {
		bitLengthCount.push(0);
	}
	for (let i = 0; i < codeLengths.length; i++) {
		bitLengthCount[codeLengths[i]]++;
	}
	// Step 2
	// Similarly a nextCode for 0 is included.
	let nextCode = [0];
	let code = 0;
	for (let i = 1; i <= maxBits; i++) {
		nextCode.push(code);
		code = (code + bitLengthCount[i]) << 1;
	}
	// Step 3
	let table = new PVAHuffmanNode();
	for (let i = 0; i < codeLengths.length; i++) {
		let cl = codeLengths[i];
		if (cl != 0) {
			table.add(i, nextCode[cl]++, 1 << (cl - 1));
		}
	}
	return table;
}
