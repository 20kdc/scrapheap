/// <reference path="license.ts"/>
/// <reference path="format.ts"/>
/// <reference path="renderer.ts"/>

// Canvas Render backend

/**
 * Absolute insanity.
 * Creates a DOMMatrix for a triangle, assuming:
 * 1-2
 * |/
 * 3
 */
function pvaTriDOMMatrix(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number) {
	return new DOMMatrix([
		x2 - x1,
		y2 - y1,
		x3 - x1,
		y3 - y1,
		x1,
		y1
	]);
}

class PVACanvasBackend implements PVABackend {
	canvas: HTMLCanvasElement;
	c2d: CanvasRenderingContext2D;
	patternsW: number[];
	patternsH: number[];
	patternsR: CanvasPattern[];
	texturesI: number[];
	texturesF: boolean[];
	triState: number;
	triInvalid: boolean;
	triX1: number;
	triY1: number;
	triU1: number;
	triV1: number;
	triX2: number;
	triY2: number;
	triU2: number;
	triV2: number;
	textureIdx: number;
	constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
		let c2d = canvas.getContext("2d");
		if (c2d == null) {
			throw new Error("CV48");
		}
		this.c2d = c2d!;
		this.triState = 0;
		this.triInvalid = false;
		this.triX1 = 0;
		this.triY1 = 0;
		this.triU1 = 0;
		this.triV1 = 0;
		this.triX2 = 0;
		this.triY2 = 0;
		this.triU2 = 0;
		this.triV2 = 0;
		this.patternsW = [];
		this.patternsH = [];
		this.patternsR = [];
		this.texturesI = [];
		this.texturesF = [];
		this.textureIdx = -1;
	}
	reset(w: number, h: number): void {
		this.canvas.width = w;
		this.canvas.height = h;
		this.patternsW = [];
		this.patternsH = [];
		this.patternsR = [];
		this.texturesI = [];
		this.texturesF = [];
		this.textureIdx = -1;
	}
	createImage(img: ImageData): void {
		const img2 = document.createElement("canvas");
		img2.width = img.width;
		img2.height = img.height;
		const ctx = img2.getContext("2d")!;
		ctx.putImageData(img, 0, 0);
		this.patternsW.push(img.width);
		this.patternsH.push(img.height);
		this.patternsR.push(this.c2d.createPattern(img2, "repeat")!);
	}
	createTexture(mode: number, image: number): void {
		this.texturesI.push(image);
		this.texturesF.push((mode & 1) != 0);
	}
	setDepthTestEnabled(enabled: boolean): void {
	}
	clear(): void {
		this.c2d.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}
	setTexture(textureIdx: number): void {
		this.textureIdx = textureIdx;
	}
	flush(): void {
	}
	vertex(x: number, y: number, z: number, w: number, r: number, g: number, b: number, a: number, u: number, v: number): void {
		if (this.triInvalid) {
			// do nothing
		} else if (w <= 0) {
			// W clip
			this.triInvalid = true;
		} else if ((z / w) < -1) {
			// Z clip
			this.triInvalid = true;
		} else {
			x /= w;
			y /= w;
			x *= this.canvas.width / 2;
			y *= this.canvas.height / -2;
			x += this.canvas.width / 2;
			y += this.canvas.height / 2;
			if (this.triState == 0) {
				this.triX1 = x;
				this.triY1 = y;
				this.triU1 = u;
				this.triV1 = v;
			} else if (this.triState == 1) {
				this.triX2 = x;
				this.triY2 = y;
				this.triU2 = u;
				this.triV2 = v;
			}
		}
		this.triState++;
		if (this.triState == 3) {
			if (!this.triInvalid) {
				if (this.textureIdx != -1) {
					// Texture
					this.c2d.imageSmoothingEnabled = this.texturesF[this.textureIdx];
					let imgIdx = this.texturesI[this.textureIdx];
					let imgPtn = this.patternsR[imgIdx];
					let mtx = this.initTexMatrix(this.triX1, this.triY1, this.triU1, this.triV1, this.triX2, this.triY2, this.triU2, this.triV2, x, y, u, v, this.patternsW[imgIdx], this.patternsH[imgIdx]);
					imgPtn.setTransform(mtx);
					this.c2d.fillStyle = imgPtn;
				} else {
					// Colour
					this.c2d.fillStyle = "rgba(" + (r * 255) + "," + (g * 255) + "," + (b * 255) + "," + (a * 255) + ")";
				}
				this.c2d.strokeStyle = this.c2d.fillStyle;
				this.c2d.beginPath();
				this.c2d.moveTo(this.triX1, this.triY1);
				this.c2d.lineTo(this.triX2, this.triY2);
				this.c2d.lineTo(x, y);
				this.c2d.closePath();
				this.c2d.fill();
				if (this.textureIdx == -1) {
					this.c2d.stroke();
				}
			}
			this.triState = 0;
			this.triInvalid = false;
		}
	}
	initTexMatrix(x1: number, y1: number, u1: number, v1: number, x2: number, y2: number, u2: number, v2: number, x3: number, y3: number, u3: number, v3: number, imgW: number, imgH: number): DOMMatrix {
		u1 *= imgW;
		v1 *= imgH;
		u2 *= imgW;
		v2 *= imgH;
		u3 *= imgW;
		v3 *= imgH;
		let mat1 = pvaTriDOMMatrix(u1, v1, u2, v2, u3, v3);
		mat1.invertSelf();
		let mat2 = pvaTriDOMMatrix(x1, y1, x2, y2, x3, y3);
		mat2.multiplySelf(mat1);
		return mat2;
	}
}
