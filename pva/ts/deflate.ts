/// <reference path="license.ts"/>
/// <reference path="bitstream.ts"/>

// RFC 1951 3.2.6: Fixed Huffman tables
let pvaDeflateHFixedLit: PVAHuffmanNode;
let pvaDeflateHFixedDst: PVAHuffmanNode;
(function () {
	let lens = [];
	for (let i = 0; i < 144; i++) {
		lens.push(8);
	}
	for (let i = 144; i < 256; i++) {
		lens.push(9);
	}
	for (let i = 256; i < 280; i++) {
		lens.push(7);
	}
	for (let i = 280; i < 288; i++) {
		lens.push(8);
	}
	pvaDeflateHFixedLit = pvaHuffmanCompile(lens);
	lens = [];
	for (let i = 0; i < 32; i++) {
		lens.push(5);
	}
	pvaDeflateHFixedDst = pvaHuffmanCompile(lens);
})();

// RFC 1951 3.2.5: Compressed blocks

/**
 * This represents a base/bits table.
 * This is a set of (base, bits) pairs.
 * Some external mechanism selects which pair to use.
 */
class PVADeflateBaseBitsTable {
	bits: number[];
	base: number[];
	constructor(base: number[], bits: number[]) {
		this.bits = bits;
		this.base = base;
	}
	read(br: PVABitReader, idx: number): number {
		return this.base[idx] + br.getBits(this.bits[idx])
	}
}

let pvaDeflateBBLength = new PVADeflateBaseBitsTable([
	3, 4, 5, 6, 7, 8, 9, 10,
	11, 13, 15, 17, 19, 23, 27, 31,
	35, 43, 51, 59, 67, 83, 99, 115,
	131, 163, 195, 227, 258
], [
	0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 2, 2, 2, 2,
	3, 3, 3, 3, 4, 4, 4, 4,
	5, 5, 5, 5, 0
]);

let pvaDeflateBBDist = new PVADeflateBaseBitsTable([
	1, 2, 3, 4, 5, 7, 9, 13,
	17, 25, 33, 49, 65, 97, 129, 193,
	257, 385, 513, 769, 1025, 1537, 2049, 3073,
	4097, 6145, 8193, 12289, 16385, 24577
], [
	0, 0, 0, 0, 1, 1, 2, 2,
	3, 3, 4, 4, 5, 5, 6, 6,
	7, 7, 8, 8, 9, 9, 10, 10,
	11, 11, 12, 12, 13, 13
]);

// The Decompressor Itself

/**
 * The core state of the decompressor.
 * This maintains the window and the stream.
 */
class PVADeflateState {
	br: PVABitReader;
	total: number[];

	constructor(br: PVABitReader) {
		this.br = br;
		this.total = [];
	}

	/**
	 * For a distance from 1 to 32768, gets the byte there.
	 */
	windowGet(dst: number): number {
		return this.total[this.total.length - dst];
	}

	/**
	 * Reads a DEFLATE block.
	 * Returns false if this is the final block.
	 */
	readBlock(): boolean {
		// RFC 1951 3.2.3: Details of block format
		let res = this.br.getBits(3);
		switch (res & 6) {
		case 0:
			this.br.skipToByte();
			let len = this.br.getBits(16);
			this.br.getBits(16);
			let arr = this.br.getAlignedBytes(len);
			for (let i = 0; i < len; i++) {
				this.total.push(arr[i]);
			}
			break;
		case 2:
			this.readLZWithHuffman(pvaDeflateHFixedLit, pvaDeflateHFixedDst);
			break;
		case 4:
			this.deflateHDynamic();
			break;
		case 6:
			throw new Error("DF18");
		}
		return (res & 1) == 0;
	}

	/**
	 * Reads a main "compressed block".
	 */
	readLZWithHuffman(litTree: PVAHuffmanNode, dstTree: PVAHuffmanNode): void {
		// RFC 1951 3.2.5: Compressed blocks
		while (true) {
			let opc = litTree.read(this.br);
			if (opc < 256) {
				this.total.push(opc);
			} else if (opc == 256) {
				return;
			} else {
				// RFC 1951 doesn't bother to tell you about how the distance codes are used.
				// It just leaves you with a table and you kinda have to just guess.
				// Basically, though, it's the same as the length but without the literal/stop/length merging.
				let len = pvaDeflateBBLength.read(this.br, opc - 257);
				let dstPR = dstTree.read(this.br);
				let dst = pvaDeflateBBDist.read(this.br, dstPR);
				// It's worth noting the minimum distance is 1, not 0. This nicely follows into how windowGet is written.
				for (let i = 0; i < len; i++) {
					this.total.push(this.windowGet(dst));
				}
			}
		}
	}

	// RFC 1951 3.2.7: Dynamic Huffman codes
	deflateHDynamicSubcodes(metaTree: PVAHuffmanNode, count: number): number[] {
		let allLens: number[] = [];
		while (allLens.length < count) {
			let op = metaTree.read(this.br);
			if (op < 16) {
				allLens.push(op);
			} else if (op == 16) {
				let rpc = 3 + this.br.getBits(2);
				let last = allLens[allLens.length - 1];
				for (let i = 0; i < rpc; i++) {
					allLens.push(last);
				}
			} else if (op == 17) {
				let rpc = 3 + this.br.getBits(3);
				for (let i = 0; i < rpc; i++) {
					allLens.push(0);
				}
			} else if (op == 18) {
				let rpc = 11 + this.br.getBits(7);
				for (let i = 0; i < rpc; i++) {
					allLens.push(0);
				}
			}
		}
		return allLens;
	}

	/**
	 * Huffman dynamic decoder.
	 */
	deflateHDynamic(): void {
		// Read the Huffman code lengths used for the code lengths.
		let metaLensOrder = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
		let metaLens = [];
		for (let i = 0; i < 19; i++) {
			metaLens.push(0);
		}
		let hlit = this.br.getBits(5) + 257;
		let hdist = this.br.getBits(5) + 1;
		let hclen = this.br.getBits(4) + 4;
		for (let i = 0; i < hclen; i++) {
			metaLens[metaLensOrder[i]] = this.br.getBits(3);
		}
		let metaTree = pvaHuffmanCompile(metaLens);
		// The literal and distance alphabets are encoded as one "super-alphabet" (see the last paragraph of 3.2.7).
		// Follow past-me's idea of giving it a separate function.
		let allLens = this.deflateHDynamicSubcodes(metaTree, hlit + hdist);
		let litLens = [];
		let dstLens = [];
		for (let i = 0; i < hlit; i++) {
			litLens.push(allLens[i]);
		}
		for (let i = 0; i < hdist; i++) {
			dstLens.push(allLens[hlit + i]);
		}
		this.readLZWithHuffman(pvaHuffmanCompile(litLens), pvaHuffmanCompile(dstLens));
	}
}

function pvaZLibDecompress(arrayBuffer: ArrayBuffer): ArrayBuffer {
	let bitReader = new PVABitReader(arrayBuffer, 2);
	let dfs = new PVADeflateState(bitReader);
	while (dfs.readBlock()) {
		// spin wheels...
	}
	return (new Uint8Array(dfs.total)).buffer;
}
