/// <reference path="license.ts"/>
/// <reference path="deflate.ts"/>

/**
 * Decodes a CABS file into chunks.
 */
function pvaCABSDecode(arrayBuffer: ArrayBuffer): Array<DataView> {
	let dv = new DataView(arrayBuffer);
	let i = 0;
	let results = [];
	while (i < dv.byteLength) {
		let chunkLen = dv.getUint32(i, true);
		let chunkType = dv.getUint8(i + 4);
		// console.log("CABS:", i, chunkType);
		if (chunkType == 0) {
			results.push(new DataView(arrayBuffer, i + 5, chunkLen - 5));
		} else if (chunkType == 1) {
			let channels = dv.getUint8(i + 5);
			let records = ((chunkLen - 6) / channels) | 0;
			let ab = new ArrayBuffer(chunkLen - 6);
			let src = new Uint8Array(arrayBuffer, i + 6, chunkLen - 6);
			let dst = new Uint8Array(ab);
			let dstI = 0;
			// transpose
			for (let r = 0; r < records; r++) {
				for (let c = 0; c < channels; c++) {
					dst[dstI++] = src[(c * records) + r];
				}
			}
			results.push(new DataView(ab));
		} else {
			console.log("FM32", i, chunkType);
		}
		i += chunkLen;
	}
	return results;
}

/**
 * Decodes an array in a PVA file.
 */
function pvaDecArray<T>(dataView: DataView, layout: string): T[] {
	let ofs = 0;
	let res = [];
	while (ofs < dataView.byteLength) {
		let val = [];
		for (let i = 0; i < layout.length; i++) {
			const v = layout[i];
			if (v == "i") {
				val.push(dataView.getInt32(ofs, true));
				ofs += 4;
			} else if (v == "f") {
				val.push(dataView.getFloat32(ofs, true));
				ofs += 4;
			} else if (v == "C") {
				// Colour type (with overbright)
				val.push(dataView.getUint16(ofs, true) / 255.0);
				ofs += 2;
			} else if (v == "c") {
				// Colour type (no overbright)
				val.push(dataView.getUint8(ofs) / 255.0);
				ofs += 1;
			} else {
				throw new Error("FM64" + v);
			}
		}
		res.push(val);
	}
	return res as unknown as T[];
}

type PVAFile = {
	width: number;
	height: number;
	sequence: PVASeqElm[];
	frames: PVAFrameElm[][];
	triangles: PVATri[];
	matrices: PVAMat[];
	loops: PVALoop[];
	palette: PVACol[];
	vertices: PVAVtx[];
	textures: PVATex[];
	imageHeaders: PVAImgH[];
	imageData: DataView[];
};

type PVAHeader = [number, number, number];
type PVASeqElm = [number, number];
type PVAFrameElm = [number, number];
type PVATri = [number, number, number, number];
type PVAMat = [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number];
type PVALoop = [number, number, number, number];
type PVACol = [number, number, number, number];
type PVAVtx = [number, number, number];
type PVATex = [number, number];
type PVAImgH = [number, number];

/**
 * Decodes a PVA file.
 */
function pvaDecode(arrayBuffer: ArrayBuffer): PVAFile {
	arrayBuffer = pvaZLibDecompress(arrayBuffer);
	let chunks = pvaCABSDecode(arrayBuffer);
	let ci = 0;

	let header = pvaDecArray<PVAHeader>(chunks[ci++], "iii");
	let width = header[0][0];
	let height = header[0][1];
	let frameCount = header[0][2];

	let sequence = pvaDecArray<PVASeqElm>(chunks[ci++], "fi");

	let frames = [];
	for (let i = 0; i < frameCount; i++)
		frames.push(pvaDecArray<PVAFrameElm>(chunks[ci++], "ii"));

	let triangles = pvaDecArray<PVATri>(chunks[ci++], "iiii");
	let matrices = pvaDecArray<PVAMat>(chunks[ci++], "ffffffffffffffff");
	let loops = pvaDecArray<PVALoop>(chunks[ci++], "iiff");
	let palette = pvaDecArray<PVACol>(chunks[ci++], "CCCc");
	let vertices = pvaDecArray<PVAVtx>(chunks[ci++], "fff");
	let textures = pvaDecArray<PVATex>(chunks[ci++], "ii");

	let imageHeaders = pvaDecArray<PVAImgH>(chunks[ci++], "ii");
	let imageData: DataView[] = [];
	for (let i = 0; i < imageHeaders.length; i++)
		imageData.push(chunks[ci++]);

	return {
		width: width,
		height: height,
		sequence: sequence,
		frames: frames,
		triangles: triangles,
		matrices: matrices,
		loops: loops,
		palette: palette,
		vertices: vertices,
		textures: textures,
		imageHeaders: imageHeaders,
		imageData: imageData,
	};
}

