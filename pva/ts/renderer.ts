/// <reference path="license.ts"/>
interface PVABackend {
	/**
	 * Element to check visibility of.
	 * Not necessarily really a canvas.
	 */
	canvas: HTMLElement;

	/**
	 * Resets the PVABackend (clears images and textures).
	 */
	reset(w: number, h: number): void;

	/**
	 * Adds an image. Image indices start at 0.
	 */
	createImage(img: ImageData): void;

	/**
	 * Adds a texture. Texture indices start at 0.
	 */
	createTexture(mode: number, image: number): void;

	/**
	 * Enables/disables the depth test (if supported).
	 */
	setDepthTestEnabled(enabled: boolean): void;

	/**
	 * Clears the screen.
	 */
	clear(): void;

	/**
	 * Sets the texture.
	 */
	setTexture(textureIdx: number): void;

	/**
	 * Flushes vertex data to the screen.
	 */
	flush(): void;

	/**
	 * Inputs a vertex (3 vertices make a triangle).
	 * This must be called three times in sequence.
	 * Interrupting the sequence is undefined behaviour.
	 */
	vertex(x: number, y: number, z: number, w: number, r: number, g: number, b: number, a: number, u: number, v: number): void;
}

/**
 * Returns true if the given HTML element is potentially visible.
 * The user can use this to avoid rendering.
 */
function pvaHTMLElementIsPotentiallyVisible(element: HTMLElement): boolean {
	if (document.hidden) {
		return false;
	}
	if (document.visibilityState === "hidden") {
		return false;
	}
	let rect = element.getBoundingClientRect();
	if (rect.width == 0 || rect.height == 0)
		return false;
	try {
		let html = document.documentElement as HTMLElement;
		let cw = html!.clientWidth;
		let ch = html!.clientHeight;
		if (rect.left >= cw)
			return false;
		if (rect.right <= 0)
			return false;
		if (rect.top >= ch)
			return false;
		if (rect.bottom <= 0)
			return false;
	} catch (ignoreMe) {
		// this can just theoretically happen, fail-safe
	}
	return true;
}

/**
 * Resets a PVABackend for a new animation.
 */
function pvaPerformResetWH(vgl: PVABackend, anim: PVAFile, w: number, h: number): void {
	vgl.reset(w, h);
	for (let i = 0; i < anim.imageHeaders.length; i++) {
		let imgh = anim.imageHeaders[i];
		let imgb = anim.imageData[i];
		let imgData = new ImageData(new Uint8ClampedArray(imgb.buffer, imgb.byteOffset, imgb.byteLength), imgh[0], imgh[1]);
		vgl.createImage(imgData);
	}
	for (let tex of anim.textures) {
		vgl.createTexture(tex[0], tex[1]);
	}
}

/**
 * Resets a PVABackend for a new animation.
 */
function pvaPerformReset(vgl: PVABackend, anim: PVAFile, scale: number): void {
	pvaPerformResetWH(vgl, anim, Math.floor(anim.width * scale), Math.floor(anim.height * scale));
}

/**
 * Renders a single vertex of a triangle.
 */
function pvaRenderLoop(vgl: PVABackend, anim: PVAFile, mtx: PVAMat, loop: PVALoop): void {
	let vtx = anim.vertices[loop[0]];
	let pal = anim.palette[loop[1]];
	let x = vtx[0];
	let y = vtx[1];
	let z = vtx[2];
	// transform
	let xo, yo, zo, wo;
	// matrix format:
	// each group of 4 is the input weights for a given output
	xo = (x * mtx[0]) + (y * mtx[1]) + (z * mtx[2]) + mtx[3];
	yo = (x * mtx[4]) + (y * mtx[5]) + (z * mtx[6]) + mtx[7];
	zo = (x * mtx[8]) + (y * mtx[9]) + (z * mtx[10]) + mtx[11];
	wo = (x * mtx[12]) + (y * mtx[13]) + (z * mtx[14]) + mtx[15];
	// finally output
	vgl.vertex(xo, yo, zo, wo, pal[0], pal[1], pal[2], pal[3], loop[2], loop[3]);
}

/**
 * Renders a single frame of a PVA animation.
 */
function pvaRenderFrame(vgl: PVABackend, anim: PVAFile, frame: PVAFrameElm[]): void {
	vgl.clear();
	for (let entry of frame) {
		let mtx = anim.matrices[entry[0]];
		let tri = anim.triangles[entry[1]];
		// material
		vgl.setTexture(tri[0]);
		// content
		pvaRenderLoop(vgl, anim, mtx, anim.loops[tri[1]]);
		pvaRenderLoop(vgl, anim, mtx, anim.loops[tri[2]]);
		pvaRenderLoop(vgl, anim, mtx, anim.loops[tri[3]]);
	}
	vgl.flush();
}

/**
 * Class to manage timings.
 */
class PVATimer {
	sequence: PVASeqElm[];
	/**
	 * Duration in milliseconds.
	 */
	duration: number;
	constructor(seq: PVASeqElm[]) {
		this.sequence = seq;
		let total = 0.0;
		for (let e of seq) {
			total += e[0];
		}
		this.duration = total;
	}

	/**
	 * Given a time in milliseconds, returns the corresponding frame index. Returns -1 on failure.
	 */
	frameOf(time: number): number {
		let frameStartTime = 0.0;
		for (let seq of this.sequence) {
			let frameEndTime = frameStartTime + seq[0];
			// i.e. for 10fps, 0 to 99 inclusive, but not 100
			if (time < frameEndTime)
				return seq[1];
			frameStartTime = frameEndTime;
		}
		return -1;
	}

	/**
	 * Like frameOf, but will try to clamp frames (so won't fail unless the sequence is empty)
	 */
	frameOfClamped(time: number): number {
		let res = this.frameOf(time);
		if (res == -1 && this.sequence.length > 0) {
			return this.sequence[this.sequence.length - 1][1];
		}
		return res;
	}

	/**
	 * Like frameOf, but loops the animation.
	 * The modulo used here doesn't react well to negative times.
	 */
	frameOfLooped(time: number): number {
		return this.frameOfClamped(pvaModuloTime(time, this.duration));
	}
}

/**
 * Given a time and a length, returns the time relative to an endlessly repeating cycle starting at 0 of that length.
 * Things to check:
 * pvaModuloTime(-8, 8) == 0
 * pvaModuloTime(-2, 8) == 6
 * pvaModuloTime(0, 8) == 0
 * pvaModuloTime(2, 8) == 2
 * pvaModuloTime(8, 8) == 0
 */
function pvaModuloTime(time: number, length: number) {
	let base = time % length;
	if (time < 0) {
		// base in range -(length - ULP) to -0 inclusive
		base += length;
		// notably, this could happen after rounding if the check was done early
		if (base >= length) {
			base -= length;
		}
	}
	return base;
}

