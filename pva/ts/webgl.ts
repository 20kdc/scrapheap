/// <reference path="license.ts"/>
/// <reference path="format.ts"/>
/// <reference path="renderer.ts"/>

// WebGL Render backend

class PVAWebGLBackend implements PVABackend {
	canvas: HTMLCanvasElement;
	program: WebGLProgram;
	gl: WebGLRenderingContext;
	nullTexture: WebGLTexture;
	_images: ImageData[];
	_textures: WebGLTexture[];
	batchTex: number;
	batchVtx: number[];
	batchVtxC: number;
	attribV: GLint;
	attribC: GLint;
	attribU: GLint;
	constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
		let gl = canvas.getContext("webgl", {antialias: false, premultipliedAlpha: true});
		if (gl == null) {
			throw new Error("GL24");
		}
		this.gl = gl!;
		// WEBGL INIT...
		// clear
		gl.clearColor(0.0, 0.0, 0.0, 0.0);
		gl.clearDepth(1.0);
		// depth unit
		gl.depthMask(true);
		gl.depthFunc(gl.LEQUAL);
		gl.enable(gl.DEPTH_TEST);
		// blend unit
		gl.blendEquation(gl.FUNC_ADD);
		gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
		gl.enable(gl.BLEND);
		// shader
		function makeShader(gl: WebGLRenderingContext, t: GLenum, src: string) {
			let s = gl.createShader(t)!;
			gl.shaderSource(s, src);
			gl.compileShader(s);
			return s;
		}
		let shaderCA = "attribute highp vec4 V; attribute highp vec4 C; attribute highp vec2 U;";
		let shaderCUV = "";
		let shaderCUF = "uniform sampler2D TX;";
		let shaderCV = "varying highp vec4 CO; varying highp vec2 UV;";
		let shaderV = makeShader(gl, gl.VERTEX_SHADER, shaderCA + " " + shaderCUV + " " + shaderCV + " void main() { gl_Position = V; CO = C; UV = U; }");
		let shaderF = makeShader(gl, gl.FRAGMENT_SHADER, shaderCUF + " " + shaderCV + " void main() { mediump vec4 frag = texture2D(TX, UV) * CO; if (frag.a <= 0.0) discard; gl_FragColor = vec4(frag.rgb * frag.a, frag.a); }");
		let program = gl.createProgram()!;
		gl.attachShader(program, shaderV);
		gl.attachShader(program, shaderF);
		gl.linkProgram(program);
		gl.useProgram(program);
		gl.uniform1i(gl.getUniformLocation(program, "TX"), 0);
		this.program = program;
		this.attribV = gl.getAttribLocation(program, "V");
		this.attribC = gl.getAttribLocation(program, "C");
		this.attribU = gl.getAttribLocation(program, "U");
		gl.enableVertexAttribArray(this.attribV);
		gl.enableVertexAttribArray(this.attribC);
		gl.enableVertexAttribArray(this.attribU);
		// done!
		// texturing
		let uca = new Uint8ClampedArray([255, 255, 255, 255]);
		let ntid = new ImageData(uca, 1, 1);
		this.nullTexture = this._createTextureInt(0, ntid);
		this._textures = [];
		this._images = [];
		// batch data
		this.batchTex = -1;
		this.batchVtx = [];
		this.batchVtxC = 0;
	}
	reset(w: number, h: number): void {
		this.canvas.width = w;
		this.canvas.height = h;
		let gl = this.gl;
		this.setTexture(-1);
		for (let v of this._textures) {
			gl.deleteTexture(v);
		}
		this._textures = [];
		this._images = [];
	}
	createImage(img: ImageData): void {
		this._images.push(img);
	}
	_createTextureInt(mode: number, imgData: ImageData): WebGLTexture {
		let gl = this.gl;
		let glt = gl.createTexture()!;
		gl.bindTexture(gl.TEXTURE_2D, glt);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, imgData);
		if (mode & 1) {
			// filter, check for power of two for mipmap
			let wPOT = false;
			let hPOT = false;
			let potCheck = 65536;
			while (potCheck != 0) {
				potCheck >>= 1;
				if (imgData.width == potCheck)
					wPOT = true;
				if (imgData.height == potCheck)
					hPOT = true;
			}
			// alright, continue
			if (wPOT && hPOT) {
				gl.generateMipmap(gl.TEXTURE_2D);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
			} else {
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			}
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		} else {
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		}
		if (mode & 2) {
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
		} else {
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		}
		return glt;
	}
	createTexture(mode: number, image: number): void {
		this._textures.push(this._createTextureInt(mode, this._images[image]));
	}
	setDepthTestEnabled(enabled: boolean): void {
		let gl = this.gl;
		if (enabled) {
			gl.enable(gl.DEPTH_TEST);
		} else {
			gl.disable(gl.DEPTH_TEST);
		}
	}
	clear(): void {
		this.flush();
		let gl = this.gl;
		gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	}
	setTexture(textureIdx: number): void {
		if (this.batchTex != textureIdx) {
			this.flush();
			this.batchTex = textureIdx;
		}
	}
	flush(): void {
		if (this.batchVtxC != 0) {
			let gl = this.gl;
			// resolve texture
			let resolvedTex = this.nullTexture;
			if (this.batchTex >= 0 && this.batchTex < this._textures.length)
				resolvedTex = this._textures[this.batchTex];
			gl.bindTexture(gl.TEXTURE_2D, resolvedTex);
			// I AM AWARE THIS IS BAD
			let newBuf = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, newBuf);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.batchVtx), gl.STREAM_DRAW);
			gl.vertexAttribPointer(this.attribV, 4, gl.FLOAT, false, 40, 0);
			gl.vertexAttribPointer(this.attribC, 4, gl.FLOAT, false, 40, 16);
			gl.vertexAttribPointer(this.attribU, 2, gl.FLOAT, false, 40, 32);
			gl.drawArrays(gl.TRIANGLES, 0, this.batchVtxC);
			gl.deleteBuffer(newBuf);
			this.batchVtx = [];
			this.batchVtxC = 0;
		}
	}
	vertex(x: number, y: number, z: number, w: number, r: number, g: number, b: number, a: number, u: number, v: number): void {
		this.batchVtx.push(x, y, z, w, r, g, b, a, u, v);
		this.batchVtxC += 1;
	}
}

