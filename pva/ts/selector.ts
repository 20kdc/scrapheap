/// <reference path="license.ts"/>
/// <reference path="renderer.ts"/>
/// <reference path="webgl.ts"/>
/// <reference path="canvas.ts"/>

function pvaTrySetupBackend(canvas: HTMLCanvasElement): PVABackend {
	try {
		return new PVAWebGLBackend(canvas)
	} catch (ex) {
		console.log("SL10", ex);
		return new PVACanvasBackend(canvas)
	}
}

