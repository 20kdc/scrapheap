#!/usr/bin/env python3

import bpy
import bmesh
import math
import mathutils
import json
from . import libpva

# - material harvesting - (may be needed externally for debug UI)

class MaterialInfo():
	def __init__(self):
		# This is a Vector, not a Color, because you can't multiply Colors
		self.col = mathutils.Vector((1, 1, 1))
		self.alpha = 1
		self.image_id = None
		self.image_nearest = False
		self.image_repeat = False

ERROR_MATERIAL_INFO = MaterialInfo()
ERROR_MATERIAL_INFO.col = mathutils.Vector((1, 0, 1))

def node_input_or_none(node, input_id, expected_type):
	if input_id in node.inputs:
		ei = node.inputs[input_id]
		if ei.type == expected_type:
			if not ei.is_linked:
				return ei.default_value
	return None

def get_material_info(material):
	mi = MaterialInfo()
	if material == None:
		mi.col = mathutils.Vector((1, 0, 1))
	else:
		mi.col = mathutils.Vector(material.diffuse_color[0:3])
		if material.use_nodes:
			for node in material.node_tree.nodes:
				if type(node) == bpy.types.ShaderNodeTexImage:
					if not (node.image is None):
						mi.image_id = node.image.name
					mi.image_nearest = node.interpolation == "Closest"
					mi.image_repeat = node.extension == "REPEAT"
				elif (type(node) == bpy.types.ShaderNodeGroup) or (type(node) == bpy.types.ShaderNodeEmission):
					col = node_input_or_none(node, "Color", "RGBA")
					if not (col is None):
						mi.col = mathutils.Vector(col[0:3])
					alp = node_input_or_none(node, "Alpha", "VALUE")
					if not (alp is None):
						mi.alpha = alp
	return mi

# - vertex attribute abstraction to work around bad Blender API design -
# basically, Blender abstracts over the difference between loop and vertex
#  attributes. BUT BMesh doesn't, which means it has to be implemented here.
# this abstraction layer can convert any named attribute into a getter, similar
#  to how Geometry Nodes autoconvert attributes.

def vat_loop(layer):
	def getter(loop):
		return loop[layer]
	return getter

def vat_vert(layer):
	def getter(loop):
		return loop.vert[layer]
	return getter

def vat_get(mesh, name):
	# loop
	if name in mesh.loops.layers.float_vector:
		return vat_loop(mesh.loops.layers.float_vector[name])
	if name in mesh.loops.layers.float_color:
		return vat_loop(mesh.loops.layers.float_color[name])
	if name in mesh.loops.layers.color:
		return vat_loop(mesh.loops.layers.color[name])
	if name in mesh.loops.layers.uv:
		return vat_loop(mesh.loops.layers.uv[name])
	if name in mesh.loops.layers.float:
		return vat_loop(mesh.loops.layers.float[name])
	if name in mesh.loops.layers.int:
		return vat_loop(mesh.loops.layers.int[name])
	# vert
	if name in mesh.verts.layers.float_vector:
		return vat_vert(mesh.verts.layers.float_vector[name])
	if name in mesh.verts.layers.float_color:
		return vat_vert(mesh.verts.layers.float_color[name])
	if name in mesh.verts.layers.color:
		return vat_vert(mesh.verts.layers.color[name])
	# UV maps cannot be applied to verts
	if name in mesh.verts.layers.float:
		return vat_vert(mesh.verts.layers.float[name])
	if name in mesh.verts.layers.int:
		return vat_vert(mesh.verts.layers.int[name])
	return None

def vat_cast_v3(val):
	if isinstance(val, mathutils.Color):
		return mathutils.Vector(val[0:3])
	elif isinstance(val, mathutils.Vector):
		return val.resized(3)
	elif isinstance(val, (float, int)):
		return mathutils.Vector((val, val, val))
	elif isinstance(val, bmesh.types.BMLoopUV):
		return val.uv.resized(3)
	else:
		return mathutils.Vector((1.0, 1.0, 1.0))

def vat_cast_v2(val):
	if isinstance(val, bmesh.types.BMLoopUV):
		return val.uv
	elif isinstance(val, mathutils.Color):
		return mathutils.Vector(val[0:2])
	elif isinstance(val, mathutils.Vector):
		return val.resized(2)
	elif isinstance(val, (float, int)):
		return mathutils.Vector((val, val))
	else:
		return mathutils.Vector((1.0, 1.0))

def vat_cast_num(val):
	if isinstance(val, (float, int)):
		return val
	elif isinstance(val, mathutils.Color):
		return val[0]
	elif isinstance(val, mathutils.Vector):
		return val[0]
	elif isinstance(val, bmesh.types.BMLoopUV):
		return val.uv[0]
	else:
		return 1.0

# - the converter -

def pva_loop_bpy(anim, col_layer, val_layer, uv_layer, loop, mi):
	vtx = loop.vert
	# col
	col = mi.col
	if not (col_layer is None):
		# DO NOT use *= here
		# it breaks everything
		col = col * vat_cast_v3(col_layer(loop))
	# alpha
	alpha = mi.alpha
	if not (val_layer is None):
		alpha *= vat_cast_num(val_layer(loop))
	# uv
	u = 0
	v = 0
	if not (uv_layer is None):
		uvd = vat_cast_v2(uv_layer(loop))
		u = uvd[0]
		v = uvd[1]
	# done
	return libpva.PVALoop(anim, vtx.co[0], vtx.co[1], vtx.co[2], col[0], col[1], col[2], alpha, u, v)

def ensure_texture_bpy(anim, image_registry, material_info):
	mode = 0
	if not material_info.image_nearest:
		mode |= 1
	if material_info.image_repeat:
		mode |= 2
	image_idx = ensure_image_bpy(anim, image_registry, material_info.image_id)
	if image_idx == -1:
		return -1
	return anim.ensure_texture(mode, image_idx)

def ensure_image_bpy(anim, image_registry, image_id):
	if image_id is None:
		return -1
	if image_id in image_registry:
		return image_registry[image_id]
	res = create_image_bpy(anim, image_id)
	image_registry[image_id] = res
	if res == -1:
		return -1
	return res

def create_image_bpy(anim, image_id):
	if image_id in bpy.data.images:
		# ok, it's *vaguely* possible, so just do it...
		img = bpy.data.images[image_id]
		if img.channels == 4:
			return anim.add_image(img.size[0], img.size[1], img.pixels)
	return -1

def attach_mesh_to_fb(fb, image_registry, obj, mesh, matrix):
	material_infos = []
	for v in obj.material_slots:
		material_infos.append(get_material_info(v.material))
	pva_matrix = libpva.PVAMatrix(fb.anim, list(matrix[0]) + list(matrix[1]) + list(matrix[2]) + list(matrix[3]))
	# work out layers
	uv_layer = vat_get(mesh, "UVMap")
	col_layer = vat_get(mesh, "Color")
	val_layer = vat_get(mesh, "Alpha")
	# actually do the thing
	for tri in mesh.calc_loop_triangles():
		face = tri[0].face
		# get material and prepare alphas for discard
		if face.material_index >= len(material_infos):
			material_info = ERROR_MATERIAL_INFO
		else:
			material_info = material_infos[face.material_index]
		# ideally, we'd know if the triangle was discarded before generating the texture
		# ...but that creates something of a paradox
		texture_idx = ensure_texture_bpy(fb.anim, image_registry, material_info)
		# cancel UV layer if no texture
		# (this optimization may be a less-than-good idea, hm)
		local_uv_layer = uv_layer
		if texture_idx == -1:
			local_uv_layer = None
		# convert to builder vertices
		pxa = pva_loop_bpy(fb.anim, col_layer, val_layer, local_uv_layer, tri[0], material_info)
		pxb = pva_loop_bpy(fb.anim, col_layer, val_layer, local_uv_layer, tri[1], material_info)
		pxc = pva_loop_bpy(fb.anim, col_layer, val_layer, local_uv_layer, tri[2], material_info)
		# and add triangle
		fb.add_tri(pva_matrix, texture_idx, pxa, pxb, pxc)

def do_export(context, filepath, vtx_precision, mtx_precision, uv_precision):
	scene = context.scene
	delay = (1000 * scene.render.fps_base) / scene.render.fps

	pvab = libpva.PVABuilder(scene.render.resolution_x, scene.render.resolution_y, vtx_precision, mtx_precision, uv_precision)
	cam_rax = pvab.width
	cam_ray = pvab.height
	image_registry = {}

	for i in range(scene.frame_start, scene.frame_end + 1):
		fb = pvab.add_frame(delay)
		scene.frame_current = i
		depsgraph = context.evaluated_depsgraph_get()
		scene_ev = scene.evaluated_get(depsgraph)
		camera_matrix = mathutils.Matrix.Identity(4)
		cam = scene_ev.camera
		if not (cam is None):
			camera_matrix = cam.evaluated_get(depsgraph).calc_matrix_camera(depsgraph, x = cam_rax, y = cam_ray) @ cam.matrix_world.inverted()
		for obj in scene_ev.objects:
			if obj.type != "MESH":
				continue
			obj_ev = obj.evaluated_get(depsgraph)
			if obj_ev.hide_render:
				continue
			bmi = bmesh.new()
			bmi.from_object(obj_ev, depsgraph)
			final_matrix = camera_matrix @ obj_ev.matrix_world
			attach_mesh_to_fb(fb, image_registry, obj_ev, bmi, final_matrix)
			bmi.free()

	fn = open(filepath, "wb")
	pvab.build(fn)
	fn.close()
	return {"FINISHED"}

