#!/usr/bin/env python3

import struct
import math
import io
import zlib
from . import libcabs

# - Data Defs -

# w h frames_count
struct_hdr = struct.Struct("<iii")
struct_id = struct.Struct("<i")
# r g b a
struct_pal = struct.Struct("<HHHB")
# x y z
struct_vtx = struct.Struct("<fff")
# vertex pal u v
struct_loop = struct.Struct("<iiff")
# delay frame
struct_seq = struct.Struct("<fi")
# mode image
struct_tex = struct.Struct("<ii")
# 4x4
struct_mtx = struct.Struct("<ffffffffffffffff")
# w h
struct_imh = struct.Struct("<ii")
# material a b c
struct_tri = struct.Struct("<iiii")
# matrix tri
struct_fe = struct.Struct("<ii")

# - The Horror -

class PVAMatrix:
	"""
	Holder for a matrix that may not necessarily be used.
	"""
	def __init__(self, anim, matrix):
		self.anim = anim
		self.matrix = matrix
		self.idx = None
	def use(self):
		if self.idx is None:
			self.idx = self.anim.ensure_matrix(self.matrix)
		return self.idx

class PVALoop:
	"""
	Holder for a loop that may not necessarily be used.
	"""
	def __init__(self, anim, x, y, z, r, g, b, a, u, v):
		self.anim = anim
		self.x = x
		self.y = y
		self.z = z
		self.r = r
		self.g = g
		self.b = b
		self.a = a
		self.u = u
		self.v = v
		self.idx = None
	def transform(self, matrix):
		"""
		Using a PVAMatrix, transforms a PVALoop into OpenGL normalized window coordinates.
		"""
		mtx = matrix.matrix
		x = self.x
		y = self.y
		z = self.z
		tx = (x * mtx[0]) + (y * mtx[1]) + (z * mtx[2]) + mtx[3];
		ty = (x * mtx[4]) + (y * mtx[5]) + (z * mtx[6]) + mtx[7];
		tz = (x * mtx[8]) + (y * mtx[9]) + (z * mtx[10]) + mtx[11];
		tw = (x * mtx[12]) + (y * mtx[13]) + (z * mtx[14]) + mtx[15];
		if tw == 0:
			return None
		return (tx / tw, ty / tw, tz / tw)
	def use(self):
		if self.idx is None:
			self.idx = self.anim.ensure_loop(self.x, self.y, self.z, self.r, self.g, self.b, self.a, self.u, self.v)
		return self.idx

def transformed_to_clipflags(tc):
	flags = 0
	if tc[0] <= -1:
		flags |= 1
	if tc[0] >= 1:
		flags |= 2
	if tc[1] <= -1:
		flags |= 4
	if tc[1] >= 1:
		flags |= 8
	if tc[2] <= -1:
		flags |= 16
	if tc[2] >= 1:
		flags |= 32
	return flags

def key_ztagged_poly(ztp):
	return -ztp[2]

class PVAFrameBuilder:
	"""
	Frame builder.
	"""
	def __init__(self, anim, delay):
		self.anim = anim
		self.delay = delay
		self.polys = []

	def add_tri(self, pva_matrix, texture_index, va, vb, vc):
		# transform for clipping
		ta = va.transform(pva_matrix)
		tb = vb.transform(pva_matrix)
		tc = vc.transform(pva_matrix)
		# calculate clipflags and sorting Z
		clipflags = -1
		z = -math.inf
		for v in (ta, tb, tc):
			if v is None:
				continue
			clipflags &= transformed_to_clipflags(v)
			z = max(z, v[2])
		if clipflags != 0:
			# entire triangle was clipped
			return
		# check alpha
		if va.a <= 0 and vb.a <= 0 and vc.a <= 0:
			# entire triangle is transparent
			return
		# triangle confirmed
		tri_index = self.anim.triangles.add(struct_tri.pack(texture_index, va.use(), vb.use(), vc.use()))
		self.polys.append([pva_matrix.use(), tri_index, z])

	def build(self):
		polys2 = sorted(self.polys, key = key_ztagged_poly)
		# can't become a table because of inter-frame comparisons
		polys3 = libcabs.CABSTableWriter(struct_fe.size, True, True)
		for v in polys2:
			polys3.add(struct_fe.pack(v[0], v[1]))
		return polys3

struct_prec_f = struct.Struct("<f")

def precision_32(v):
	return v

def precision_24(v):
	return struct_prec_f.unpack(b"\x00" + struct_prec_f.pack(v)[1:4])[0]

def precision_16(v):
	return struct_prec_f.unpack(b"\x00\x00" + struct_prec_f.pack(v)[2:4])[0]

def precision_12(v):
	packed = struct_prec_f.pack(v)
	low = packed[2]
	high = packed[3]
	low &= 0xF0
	return struct_prec_f.unpack(b"\x00\x00" + bytes([low, high]))[0]

precisions = {
	"FLOAT32": precision_32,
	"BF24": precision_24,
	"BF16": precision_16,
	"BF12": precision_12
}

class PVABuilder:
	"""
	Animation builder.
	"""
	def __init__(self, width, height, vtx_precision, mtx_precision, uv_precision):
		self.vtx_precision = precisions[vtx_precision]
		self.mtx_precision = precisions[mtx_precision]
		self.uv_precision = precisions[uv_precision]
		self.width = int(width)
		self.height = int(height)
		self.textures = libcabs.CABSTableWriter(struct_tex.size, False, True)
		self.matrices = libcabs.CABSTableWriter(struct_mtx.size, False, True)
		self.loops = libcabs.CABSTableWriter(struct_loop.size, False, True)
		self.palette = libcabs.CABSTableWriter(struct_pal.size, False, True)
		self.vertices = libcabs.CABSTableWriter(struct_vtx.size, False, True)
		self.triangles = libcabs.CABSTableWriter(struct_tri.size, False, True)
		self.frames = []
		self.image_headers = libcabs.CABSTableWriter(struct_imh.size, True, True)
		self.image_data = []

	def ensure_texture(self, mode, image):
		return self.textures.add(struct_tex.pack(mode, image))

	def ensure_matrix(self, matrix):
		mtx2 = matrix
		if self.mtx_precision != precision_32:
			mtx2 = [self.mtx_precision(v) for v in matrix]
		return self.matrices.add(struct_mtx.pack(*mtx2))

	def ensure_loop(self, x, y, z, r, g, b, a, u, v):
		x = self.vtx_precision(x)
		y = self.vtx_precision(y)
		z = self.vtx_precision(z)
		u = self.uv_precision(u)
		v = self.uv_precision(v)
		vtx = self.vertices.add(struct_vtx.pack(x, y, z))
		r = max(min(int(r * 255), 65535), 0)
		g = max(min(int(g * 255), 65535), 0)
		b = max(min(int(b * 255), 65535), 0)
		a = max(min(int(a * 255), 255), 0)
		pal = self.palette.add(struct_pal.pack(r, g, b, a))
		return self.loops.add(struct_loop.pack(vtx, pal, u, v))

	def add_frame(self, delay):
		frame = PVAFrameBuilder(self, delay)
		self.frames.append(frame)
		return frame

	def add_image(self, w, h, contents):
		data = []
		for v in contents:
			data.append(min(255, max(0, int(v * 255))))
		self.image_data.append(bytes(data))
		return self.image_headers.add(struct_imh.pack(w, h))

	def build(self, fn_compressed):
		fn = io.BytesIO()
		# frame build/deduplicate loop
		frames2 = []
		frames_count = 0
		frames_lookup = {}
		seq_pre = []
		last_key = None
		for v in self.frames:
			tmp = v.build()
			key = tmp.to_key()
			if key == last_key:
				# identical frame
				seq_pre[len(seq_pre) - 1][0] += v.delay
			elif key in frames_lookup:
				# existing frame
				seq_pre.append([v.delay, frames_lookup[key]])
			else:
				# new frame
				frames_lookup[key] = frames_count
				seq_pre.append([v.delay, frames_count])
				frames2.append(tmp)
				frames_count += 1
			last_key = key
		# finish sequence
		sequence = libcabs.CABSTableWriter(struct_seq.size, True, True)
		for v in seq_pre:
			sequence.add(struct_seq.pack(*v))
		# perform actual write
		libcabs.cabs_write_raw(struct_hdr.pack(self.width, self.height, frames_count), fn)
		sequence.write(fn)
		for v in frames2:
			v.write(fn)
		self.triangles.write(fn)
		self.matrices.write(fn)
		self.loops.write(fn)
		self.palette.write(fn)
		self.vertices.write(fn)
		self.textures.write(fn)
		self.image_headers.write(fn)
		for v in self.image_data:
			libcabs.cabs_write_raw(v, fn)
		# and for our final trick today
		fn.flush()
		fn_compressed.write(zlib.compress(fn.getbuffer().tobytes(), level = 9))

