#!/usr/bin/env python3

# See PVA specification.
# This layer is mainly to offload some of the compression-helping tricks.

import struct

struct_hdr = struct.Struct("<IB")
struct_tphdr = struct.Struct("<B")

class CABSTableWriter():
	def __init__(self, channels, unique, packed):
		self.channels = channels
		self.channels_range = range(channels)
		self.unique = unique
		self.packed = packed
		self.count = 0
		if self.packed:
			self.buffers = []
			for i in self.channels_range:
				self.buffers.append(b"")
		else:
			self.buffers = [b""]
		if not self.unique:
			self.lookup = {}
	def add(self, data):
		if len(data) != self.channels:
			raise Exception("length of table record must be correct")
		if not self.unique:
			if data in self.lookup:
				return self.lookup[data]
		nid = self.count
		self.count += 1
		if not self.unique:
			self.lookup[data] = nid
		# actually add
		if self.packed:
			for i in self.channels_range:
				self.buffers[i] += data[i : i + 1]
		else:
			self.buffers[0] += data
		return nid
	def write(self, f):
		clen = struct_hdr.size + (self.count * self.channels)
		if self.packed:
			clen += struct_tphdr.size
			f.write(struct_hdr.pack(clen, 1))
			f.write(struct_tphdr.pack(self.channels))
		else:
			f.write(struct_hdr.pack(clen, 0))
		for b in self.buffers:
			f.write(b)
	def to_key(self):
		total = b""
		for b in self.buffers:
			total += b
		return total

def cabs_write_raw(data, f):
	f.write(struct_hdr.pack(len(data) + 5, 0))
	f.write(data)

