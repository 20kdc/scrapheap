#!/usr/bin/env python3

bl_info = {
	"name": "Portable Vector Animation",
	"version": (0, 1, 0),
	"blender": (3, 6, 0),
	"category": "Import-Export",
	"location": "File > Import/Export",
	"description": "portable vector animation format"
}

import bpy
from . import libpvabpy

# bl_info is parsed via Shenanigans so it takes precedence
BRAND = bl_info["name"]

from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, IntProperty, FloatProperty, BoolProperty, EnumProperty
from bpy.types import Operator

precision_items = [
	("FLOAT32", "float32", "32-bit floating point"),
	("BF24", "bfloat24", "Removes the 8 lowest bits of a float. Usually decently safe"),
	("BF16", "bfloat16", "Removes the 16 lowest bits of a float. Can work for small animations, particularly good for chopping off extraneous precision"),
	("BF12", "bfloat12", "Removes the 20 lowest bits of a float. Can sometimes be usable for vertex data, don't use for matrices")
]

class ExporterI2PVA(Operator, ExportHelper):
	bl_idname = "libpvabpy.export"
	bl_label = "Export " + BRAND

	filename_ext = ".pva"

	filter_glob: StringProperty(
		default ="*.pva",
		options = {'HIDDEN'},
		maxlen = 255
	)

	vtx_precision: EnumProperty(items = precision_items, name = "Vertex Precision", default = "FLOAT32")

	mtx_precision: EnumProperty(items = precision_items, name = "Matrix Precision", default = "FLOAT32")

	uv_precision: EnumProperty(items = precision_items, name = "UV Precision", default = "FLOAT32")

	def execute(self, context):
		return libpvabpy.do_export(context, self.filepath, self.vtx_precision, self.mtx_precision, self.uv_precision)

class MakeSceneSensibleI2PVA(Operator):
	bl_idname = "libpvabpy.sanitize"
	bl_label = BRAND + ": Sanitize Settings"
	bl_description = "Sanitizes Colour Management and Film settings to better match the operation of a " + BRAND + " renderer"

	def invoke(self, context, event):
		try:
			# before Blender 4.0
			context.scene.display_settings.display_device = "None"
			context.scene.view_settings.view_transform = "Standard"
		except:
			# after Blender 4.0
			context.scene.display_settings.display_device = "sRGB"
			context.scene.view_settings.view_transform = "Raw"
		# continue
		context.scene.view_settings.look = "None"
		context.scene.view_settings.exposure = 0.0
		context.scene.view_settings.gamma = 1.0
		context.scene.view_settings.use_curve_mapping = False
		context.scene.render.filter_size = 1.0
		context.scene.render.film_transparent = True
		context.scene.id_properties_ensure()["portablevectoranimation"] = True
		return {"FINISHED"}

class ImagesSetupI2PVA(Operator):
	bl_idname = "libpvabpy.images_setup"
	bl_label = "Fix All Image Colourspaces"
	bl_description = "Sets all images to match how " + BRAND + " will read them. Quick-fix for cross-use w/ Images as Planes"

	def invoke(self, context, event):
		for v in bpy.data.images:
			# not sure if this is right, but can always change it later
			try:
				v.colorspace_settings.name = "Linear"
			except:
				# so on versions of Blender where "Linear" gives bad results,
				#  you should in fact be using "sRGB" here, because Blender changed
				#  the semantics of the colour transform to make more sense
				v.colorspace_settings.name = "sRGB"
			v.alpha_mode = "STRAIGHT"
		return {"FINISHED"}

def menu_func_export(self, context):
	self.layout.operator(ExporterI2PVA.bl_idname, text = (BRAND + " (*" + ExporterI2PVA.filename_ext + ")"))

def menu_utilities(self, context):
	row = self.layout.row()
	row.operator(MakeSceneSensibleI2PVA.bl_idname)
	row.operator(ImagesSetupI2PVA.bl_idname)

def register():
	bpy.utils.register_class(ExporterI2PVA)
	bpy.utils.register_class(MakeSceneSensibleI2PVA)
	bpy.utils.register_class(ImagesSetupI2PVA)
	bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
	bpy.types.SCENE_PT_scene.append(menu_utilities)

def unregister():
	bpy.utils.unregister_class(ExporterI2PVA)
	bpy.utils.unregister_class(MakeSceneSensibleI2PVA)
	bpy.utils.unregister_class(ImagesSetupI2PVA)
	bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
	bpy.types.SCENE_PT_scene.remove(menu_utilities)

if __name__ == "__main__":
	register()

