## PVA recording node. Usually this script fits nicely on the root of a scene.
class_name PVAStudioRecorder
extends Node

## Save the resulting PVA to this file.
@export var output_path = "output.pva"
## Expected amount of frames of the PVA animation.
@export var frames = 100
