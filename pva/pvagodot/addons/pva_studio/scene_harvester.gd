## Harvests PVAInputTriangles for PVA from the scene.
## As an object, contains caches for things like rendered labels.
class_name PVASceneHarvester
extends RefCounted

## Harvests triangles from a node tree.
func harvest(target: Array[PVAInputTriangle], source: Node):
	pass
