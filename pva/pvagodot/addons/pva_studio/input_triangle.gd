## A triangle, harvested from Godot, and not yet optimized into PVA tables.
class_name PVAInputTriangle
extends RefCounted

var mtx_ix: Vector4 = Vector4(1, 0, 0, 0)
var mtx_iy: Vector4 = Vector4(0, 1, 0, 0)
var mtx_iz: Vector4 = Vector4(0, 0, 1, 0)
var mtx_iw: Vector4 = Vector4(0, 0, 0, 1)

var tex: Texture

var a_pos: Vector3
var b_pos: Vector3
var c_pos: Vector3

var a_uv: Vector2
var b_uv: Vector2
var c_uv: Vector2

var a_col: Vector4
var b_col: Vector4
var c_col: Vector4
