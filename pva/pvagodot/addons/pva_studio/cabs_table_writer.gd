## Utility class implementing the "compression-aware bulk storage" layer of PVA.
class_name PVACABSTableWriter
extends RefCounted

## Amount of records.
var count: int = 0

## Amount of 'channels' (byte-sized columns)
var channels: int

## If true, records are considered unique and will not be deduplicated.
var unique: bool

## If true, records will be stored in channel-major form.
## This is the main use of CABS.
var packed: bool

## Internal data buffers.
var buffers: Array[PackedByteArray] = []

## Lookup table for non-unique tables.
var lookup: Dictionary = {}

func _init(_channels: int, _unique: bool, _packed: bool):
	channels = _channels
	unique = _unique
	packed = _packed
	if packed:
		# packed format has one buffer per channel
		for i in range(channels):
			buffers.append(PackedByteArray())
	else:
		# unpacked format simply has one buffer
		buffers.append(PackedByteArray())

## Adds a record.
func add(data: PackedByteArray):
	assert(len(data) == channels)
	if not unique:
		var tmp = lookup.get(data)
		if tmp != null:
			return tmp
	var id = count
	if not unique:
		lookup[data] = id
	if packed:
		for i in range(channels):
			buffers[i].append(data[i])
	else:
		buffers[0].append_array(data)
	return id

## Writes to a little-endian StreamPeer.
## If the StreamPeer is not little-endian, then, uh.
## Uhoh.
func write(target: StreamPeer):
	if target.packed:
		target.put_u32(5 + (count * channels) + 1)
		target.put_u8(1)
		target.put_u8(channels)
	else:
		target.put_u32(5 + (count * channels))
		target.put_u8(0)
	for v in buffers:
		target.put_data(v)

func to_key() -> PackedByteArray:
	var key = PackedByteArray()
	for v in buffers:
		key.append_array(v)
	return key
