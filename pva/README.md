# PVA: Uhhhhh something something vector animation

For description/demos/etc. please see https://20kdc.gitlab.io/scrapheap/pva/ (or failing this, /public/pva in this repository)

## Licensing

The files here are under the MIT license at the root of the repository.
However, see COPYING of the /public/pva directory for some specifics of the HTML.

