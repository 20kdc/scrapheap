# Verification

In the unlikely event that someone actually decides to *use* this format, they may wish to use it to handle untrusted user data.

This document lists the various verification concerns to be aware of.

These should be roughly checked in-order.

## File Size

This one is pretty simple. Is the file too big for your server, or for the client?

For compressed files, some care should be taken. If at all possible, try to determine the decompressed size before decompression. Otherwise, decompress until a limit is hit.

## CABS Integrity Check

Confirm the CABS file does not have abnormal chunk types and that the file is not truncated.

For transposed data:

* Check packed channel count is non-zero.
* Check data lengths are divisible by the packed channel count.

(By this point, you have confirmed that the file is CABS-valid, but not PVA-valid.)

## Record Size Integrity Check

* Check the header record is the right size. Decode and extract frame count.
* Check chunk sizes make sense for the structures they contain (a 4-byte structure must be in data that is a multiple of 4 in size).
* Check that frames do not contain more than an application-defined polygon count.
* Check record counts are within application-defined boundaries.
* Check image sizes are within application-defined boundaries. *Particularly, ensure overflow errors do not occur.*
* Check image sizes match their data chunks.
* Check the total amount of texture data (Image Data for each Texture record) does not exceed an application-defined boundary.
* There should not be any unaccounted-for chunks.

(By this point, you have confirmed that the file is safe to finish loading, but not necessarily safe to display.)

## Detailed Check

* Confirm cross-record references are within bounds, i.e. that they do not refer to records past the end of their arrays, or IDs < 0 (except where -1 is acceptable, etc.)
* Confirm the file doesn't use any capabilities your renderer doesn't have.
* Sequence chunks should never have delays that are under or equal to zero.
* All floating-point numbers should be finite.

## Non-Programmable Safety Checks

Rapidly changing or flashing animations can be a health risk.

* Allow the user to disable display of animations. *This should be a global setting, and must not be lost without warning.*
* Allow controlling a "slow fading" effect, blending between the frames of every second or so. *This should be a global setting, and must not be lost without warning. Before attempting a solution like this, please consult a medical professional with test footage to confirm that several animations, including but not limited to worst-case 'attack' animations, are made safe by this effect.*
* If it's not possible to stop users from exposing each other to content that contains rapidly changing or flashing animations, the application should display a prominent warning.

Some animations may contain rude, unprofessional, offensive, hateful, or dangerous content.

* Actually moderate your platform.

## About Choosing Application-Defined Limits

Depending on your renderer, different limits make different amounts of sense.

The most important thing is not to allow memory safety to be violated, if relevant.

However, depending on the implementation strategy, some resources that can be cheaply defined by a PVA file may "multiply" into more resources on a GPU.

In particular, Frames, Triangles, Matrices, Loops, the Palette, and Vertices are all potential targets a renderer may "pre-send" to the GPU.

But an "immediate" renderer will not do this, so in that event the main thing to keep in mind is simply per-frame triangle count.

Textures and Images, however, will pretty much always be sent to the GPU in some capacity, so must be monitored.

In particular, it's possible to add 8 bytes to the file and use a lot of VRAM easily simply by defining redundant entries in the Textures table. Check the total VRAM usage.

