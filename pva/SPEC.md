# PVA Specification, v1.0

## What is PVA? (From A Reader's Or Writer's Perspective)

PVA is, in essence, an "animated metafile" format.

Unlike most metafile formats, aimed purely at a 2D renderer, PVA is written from the perspective of efficiently encoding a 3D animated scene, as might be rendered by, say, WebGL or OpenGL ES 1.

This implies native support for a decent handling of perspective-correct 3D rendering, but removes the ability to render, say, antialiased polygon outlines. This choice was made willingly and knowingly, and is a tradeoff. The benefit is added portability, and the ability to create animations using existing standard tooling.

Potential exporters for PVA include 3D animation & modelling software such as Blender, and "IDE game engines" such as Godot.
Potential importers for PVA include game engines, UI frameworks, web applications, and MSPFA comics.

## What is expected of a renderer?

Because PVA may be used in various circumstances where not all capabilities may be available, yet the specification and format may still be of use, the absolute minimum expected of a renderer is that it can render solid coloured 2D triangles, ideally without antialiasing (and the resulting artifacting).

While this renderer would be extremely crippled, PVA could still provide a useful service even with these limitations. Similarly, not all rendering mechanisms are capable of subpixel polygon rendering.

*In short, renderers are expected to be best-effort implementations.*

A fully featured PVA renderer has as ideals:

* A pre-multiplied alpha sRGB (perceptual-space) framebuffer. *Even if linear-light rendering or in-hardware conversion is available, it should not be used.*
* A depth buffer. This depth buffer would ideally use the `GL_LEQUAL` function and generally act according to OpenGL expectations. *Writers are expected to order polygons in such a way that disabling depth has as small a difference in the rendered output as possible.*
* The ability to render 3D perspective-correct texture-mapped and coloured triangles with interpolation of UVs and RGBA vertex colours, with subpixel precision. Texture mapping can choose between clamping or repeating.

Again, these are ideals. A renderer that does not meet these ideals is potentially of degraded quality, but is not non-compliant.

In practice, there are four "profiles":

* The Ideal Profile: See above.
* The Misuse Profile:
	* When exporting, the "camera" is the identity matrix.
	* When exporting, clipping and sorting mechanisms are disabled.
	* Very small textures of specific size may mean something to the engine. These may be invisible markers, or they may be replaced with different textures (possibly with a texture matrix applied).
	* This exists in case someone really liked early ID Software's model formats but believes they didn't go far enough. This is sort of the 3D counterpart to using PVA animations in a game world.
* The Inline Profile:
	* There is no depth buffer. If any point has `W <= 0` (pre-divide) or `Z < -1` (post-divide), the triangle is eliminated.
	* Perspective correction does not exist.
	* This profile is useful for performance reasons, when a 3D rendering backend exists but a large amount of PVA animations are being drawn at once. Resetting the depth buffer is thus not possible here.
* The Canvas Profile:
	* It cannot interpolate RGBA values, and will instead use the last vertex's colour. If a texture is present, it overrides colour completely (they cannot be mixed).
	* There is no depth buffer. If any point has `W <= 0` (pre-divide) or `Z < -1` (post-divide), the triangle is eliminated.
	* Perspective correction does not exist.
	* Textures always repeat.
	* Antialiasing will cause artifacting for diagonals or non-integer coordinates.
	* Certain UV arrangements might break UV handling, so don't use infinitely thin UV triangles for instance.
	* The Canvas Profile exists to ensure that significantly old hardware, or browsers not capable or willing to use WebGL, are still capable of viewing these animations.

## Coordinate System

The output coordinate system is OpenGL normalized device coordinates.

These use X, Y, and Z coordinates from -1 to 1.

Exporters may (and should) avoid exporting data that would be completely clipped.

Renderers must not display triangles for which all points have `Z < -1` after the perspective divide.

Renderers *may* refuse to display triangles for which any point has `Z < -1` or `Z > 1` after the divide, or `W <= 0` before it.

As stated before, the ideal here is a standard WebGL/OpenGL depth buffer setup.

## Colour Space

PVA uses the non-linear sRGB space.

No provision is made for colour profiles. There is no possible way adding colour profiles to the specification could add any conceivable benefit to a format that ultimately will be rendered by best-effort sRGB display anyway; it will simply add points of failure. (How many different sRGB ICC profiles are there? And why do sRGB ICC profiles exist?)

This is because hardware compatibility does not allow for the higher-quality framebuffers required for good linear compositing, or the built-in operations required for fast linear compositing.

Alpha is not pre-multiplied in the format. However, rendering should be a best-effort attempt at the Alpha Over operation. Therefore, the framebuffer should ideally be pre-multiplied alpha, and the shaders will simply have to convert.

At some later point, a texture flag may be introduced which allows explicit use of pre-multiplied alpha textures.

If the framebuffer is pre-multiplied alpha, triangles are drawn using the following blend equations:

```
Ad = As + (Ad * (1 - As))
Cd = Cs + (Cd * (1 - As))
```

## zlib

The outermost layer of the animation file is zlib-compressed as per RFC 1950.

zlib was chosen as opposed to the more common gzip because gzip has an annoying and hard to deal with series of header fields that add literally nothing but file size.

It was chosen over raw DEFLATE because command-line tooling for raw DEFLATE is hard to get ahold of, and because zlib compressed data can be identified by `file`.

An additional concern is that APIs for some languages (i.e. Godot 3.x GDScript) cannot read raw DEFLATE data, only zlib or gzip. In a language which does have raw DEFLATE, but not zlib/gzip, stripping the zlib header/footer is trivial. *Implementations are explicitly not required to check the Adler32 checksum, but when writing it must be correct for those implementations which don't have any choice in the matter.*

## Compression-aware bulk storage

The next layer of the animation file is made up of chunks.

Each chunk starts with a 32-bit little-endian total length (including the chunk header).

This is then followed by a single byte type.

There are two types:

* 0: Raw chunk. The remainder of the chunk is a raw block of data.
* 1: Transposed chunk. The next (unsigned) byte is a "channel count". The rest of the chunk is the actual data. The best way to explain this is that for 4 channels, the ASCII text `0123012301230123`, would become `0000111122223333`, and `ABCDEFGHIJKLMNOP` becomes `AEIMBFJNCGKODHLP`.

Readers must treat these chunk types as interchangable. (As such, the channel count should not be exposed.)

**Like the header, the contents of chunks are always little-endian.**

## Chunk Layout

* Header: 1
* Sequence: 1
* Frames: Chunk count is equal to frame count in header
* Triangles: 1
* Matrices: 1
* Loops: 1
* Palette: 1
* Vertices: 1
* Textures: 1
* Image Headers: 1
* Image Datas: Chunk count is equal to number of image headers

## Header

* I32 width (pixels)
* I32 height (pixels)
* I32 frame count

Note that the width and height do not *have* to be respected, just should be.

In particular, the aspect ratio is important, and the size represents the resolution the animation was intended to be rendered at for display.

The frame count has no direct relation to the length of the animation (that's the sequence's job), and exists so that the quantity of frame chunks is known.

## Sequence

This chunk is an array of the following struct:

* F32 delay (milliseconds)
* I32 frame index

## Frames

Each frame chunk is an array of the following struct:

* I32 matrix index
* I32 triangle index

*Triangles in a frame must be effectively drawn in-order.* 3D editors, assuming no more precise method is available, should order these based on the furthest depth of the triangle.

## Triangles

This chunk is an array of the following struct:

* I32 texture index (-1: No texture)
* I32 loop index 1
* I32 loop index 2
* I32 loop index 3

## Matrices

This chunk is an array of F32.

Each matrix is 16 floats long.

Each group of 4 floats represents a given output (X, Y, Z, and W) and each float represents a given input.

## Loops

This chunk is an array of the following struct:

* I32 vertex index
* I32 palette index
* F32 U coordinate
* F32 V coordinate

## Palette

This chunk is an array of the following struct:

* U16 R
* U16 G
* U16 B
* U8 Alpha

RGB are 16-bit for overbright tricks. Meanwhile Alpha doesn't make sense beyond the 0-255 range.

With this in mind, RGB is in the 0-255 range. Values outside of this range allow for up to 257x overbright, which is more than anyone should ever need.

## Vertices

This chunk is an array of the following struct:

* F32 X
* F32 Y
* F32 Z

## Textures

This chunk is an array of the following struct:

* I32 mode
* I32 image index

The mode flags are: 1 enables filtering (which may mipmap if supported for the image dimensions). 2 enables repeat.

Do not use repeat on textures with non-power-of-two dimensions.

## Image Headers

This chunk is an array of the following struct:

* I32 W
* I32 H

## Image Data

Each image data chunk contains the RGBA8888 data for a single image.

Formally speaking, images are stored upside-down. Nobody notices because the texture data comes out that way in Blender and goes in that way to OpenGL anyway.