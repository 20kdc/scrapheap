# PVA Blender Export Addon Notes

## Helpful Other Addons To Use

* For sprite animations: Import Images as Planes. Ships with Blender. Use it with Shadeless or Emit material types, be sure to fix colourspaces.
* ReSprytile.

## Scene Setup

The addon adds two buttons to the scene properties:

* Sanitize Settings: This sets up a scene to better match the results (by tweaking colour management and film settings).
* Fix All Image Colourspaces: Sanitize Settings brings the display out of sync with what Blender expects, so images are now being converted to linear but not back. This fixes that.

*Note that Sanitize Settings sets the Display Device setting to None. This is intentional: This causes Blender to operate in non-linear space, matching PVA's rendering.*

Another thing to be aware of is that images may need their colour space set as "Raw" or "Linear" to display properly.

## Meshes/Geom Nodes

The export addon exports every `MESH` object in the scene, unless `hide_render` has been set on that object.

The meshes are saved for every frame for the start to the end frame of the scene (inclusive). Importantly, these frames are treated as independent copies of the meshes, so advanced Geometry Node effects work.

*Beware:* Geometry Node "instancing", *such as that used by String To Curves,* isn't presently supported.

## Materials

The export addon looks for Node Group, Emission, and Image Texture nodes.

If it finds an Emission or Node Group node, the set value of the `Color` property (ignoring connections) is used as a base material colour, and the `Alpha` property is used as a base material alpha.

These properties are ignored if they are linked to anything.

If it finds an Image Texture node, the texture is sourced from that node. Keep in mind the notes about needing to use Raw/Linear colour space.

The export addon also looks for the following attributes on face corners or vertices:

* `Color` (Recommended: Byte Color or Float Color)

* `Alpha` (Recommended: Byte Color or Float Color or Float ; uses red channel if Color)

* `UVMap` (Recommended: UV)

The addon will try to guess type conversions if necessary, but this probably isn't too reliable.

## pvashader.blend

This is a utility library. It tries to simulate the PVA renderer in Eevee, and provides some geometry nodes utilities.

*Beware that this library assumes you have your Colour Management settings set up correctly as per Sanitize Settings.*

### Shader Node Groups

`20kdc.PVAShader`: Can be textured or not textured. For not textured, just leave Image & Image Alpha at white/1.
This requires Color/Alpha attributes, because otherwise they default to 0.

`20kdc.PVAShader.Core`: Use this if you want to have Color but not Alpha or vice versa.
Though, it doesn't matter for optimization, so it's probably not worth it.

`20kdc.PVAShader.NoAtts`: Ignores attributes.

### Geometry Node Groups

`20kdc.PVAShader.Spritesheet`: Given an index integer and a frames vector, adjusts UVMap to select a sprite from a spritesheet.
"Index" is the frame index. It is modulo'd by "Modulo" for animation loop convenience.
"Base" is added to "Index" post-modulo.
"Frames" describes the spritesheet. The spritesheet must be evenly cut and is assumed to be row-major, top-left to bottom-right.

`20kdc.PVAShader.LightToColour`: Multiplies Light with FC/Color. External input for ambient light. Outputs to FC/Color.

`20kdc.PVAShader.DirectionalLight`: Adds a directional light.
The light can be represented by a "Single Arrow" empty.
Light is written as a Point attribute. This is because normal smoothing doesn't seem to work here otherwise.
The "Clamp" option can be set to -1 to allow backfaces to subtract light.
This works remarkably well in keeping backfaces from being impossibly dark.

`20kdc.PVAShader.InitColourAlpha`: Init FC/Color and FC/Alpha.

## Optimization Tips

This is focused on size optimization.

* Object transforms are the cheapest way to move things around. *When possible, use these.*
* The exporter will not write an object if an object is hidden in one of these ways:
  * Out of frame
  * Behind the camera
  * Alpha 0
  * Disabled for rendering
* Reuse meshes whenever possible. It doesn't *have* to be a literal reuse of the datablock, but it usually helps. The data should be as similar as possible. In particular:
  * Identical *local* vertex positions are reused.
  * Identical colour/alpha pairs are reused (but note that this is measured by the output of the material/vertex multiplication)
  * If the local vertex position, colour/alpha, and the UV, are all as a group identical, everything will be reused.
    * This optimization will essentially work automatically in places where faces touch each other but don't have distinct properties, i.e. when defining 3D shapes.
* Images are the most costly resource you have.
* Animating things with interpolation over a lot of frames has different kinds of effects on different tables:
  * Camera Transforms:
    * One matrix per frame per visible object (where the object wasn't otherwise moving)
  * Object Transforms:
    * One matrix per frame per animated object
  * Heavy use of animated modifiers, geometry nodes, shape keys, or skeletal animation:
    * Unless you're very careful, doing any of this spams Triangles, Loops and Vertices.
    * This is roughly `spam = frames * (altered face corners + altered vertices)`.
  * Colours
    * Can easily spam Loops and Palette.

