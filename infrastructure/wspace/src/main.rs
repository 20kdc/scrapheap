#![forbid(unsafe_code)]

use std::env::args;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use std::process::exit;

mod core;
mod crypto;
mod ipspace;
mod make_wintun_debuggable;
mod tunnel;
mod smoke;

fn help() {
    println!("wspace <peer name> <config-file> [<bind address>]");
    println!("config file should look something like:");
    println!("{}", "{");
    println!("{}", " \"password\": \"dogbone\",");
    println!("{}", " \"peers\": {");
    println!(
        "{}",
        "  \"alice\": {\"internal\": 1, \"external\": \"123.123.123.123:3310\"},"
    );
    println!(
        "{}",
        "  \"bob\":   {\"internal\": 2, \"external\": \"321.321.321.321:3310\"}"
    );
    println!(
        "{}",
        "  \"edith\": {\"internal\": 2, \"sid\": \"0\"}"
    );
    println!("{}", " }");
    println!("{}", "}");
    println!("this should be the same file on all peers, though you can omit peers if you want (but keep it bi-directional! also, this means you can't connect to each other!)");
    println!("bind address defaults to 0.0.0.0:3310");
    println!("internal indexes are translated as so:");
    println!(" 1: {}", ipspace::encode_wsidx(1));
    println!(" 2: {}", ipspace::encode_wsidx(2));
    println!("internal indexes 0 and 255 are invalid");
    println!("if the password is empty, encryption is disabled (DEBUGGING ONLY, PLEASE!!!)");
}

fn main() {
    // init libs
    make_wintun_debuggable::start();

    // args...
    let mut args_iter = args();
    _ = args_iter.next();
    let peer_name = args_iter.next();
    let config_file = args_iter.next();
    let bind_spec = args_iter.next();
    if let None = peer_name {
        help();
        exit(1)
    } else if let None = config_file {
        help();
        exit(1)
    } else if let Some(_) = args_iter.next() {
        println!("only 3 args!");
        help();
        exit(1)
    }
    // -- done parsing args --
    let peer_name = peer_name.unwrap();
    let config_file = config_file.unwrap();
    let config_data = std::fs::read_to_string(&config_file)
        .expect("the config file is expected to be a file that actually exists");
    let config_parsed =
        json::parse(&config_data).expect("the config file is expected to be valid JSON");

    let mut bind: SocketAddr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, 3310));
    if let Some(ba) = bind_spec {
        bind = ba.parse().expect("bind address should parse");
    }

    let password: String = config_parsed["password"]
        .as_str()
        .expect("you must at least have a password attribute to prevent 'accidents'")
        .to_string();
    let mut peers: Vec<core::PeerInfo> = vec![];

    for (k, v) in config_parsed["peers"].entries() {
        let mut external: Option<core::PeerExternal> = None;
        // UDP
        if let Some(external_str) = v["external"].as_str() {
            external = Some(core::PeerExternal::UDP(external_str
                .parse()
                .expect("sid: all peer external addresses must parse")));
        }
        // Smoke
        if let Some(external_str) = v["sid"].as_str() {
            external = Some(core::PeerExternal::Smoke(external_str
                .parse()
                .expect("external: all peer external addresses must parse")));
        }
        // done
        peers.push(core::PeerInfo {
            name: k.to_string(),
            internal: v["internal"]
                .as_u8()
                .expect("all peer indices must be valid u8s"),
            external: external.expect("peers must have external addresses (\"external\" or \"sid\")"),
            is_local: k.eq(&peer_name),
        });
    }

    let config = core::Config {
        bind,
        password,
        peers,
    };
    core::run(config);
}
