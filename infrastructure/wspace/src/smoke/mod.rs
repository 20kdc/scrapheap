use std::sync::Arc;

/// Smoke implementation trait.
pub trait Smoke: Send + Sync {
    /// Sends a packet to a user.
    fn send_to(&self, who: u64, data: &[u8]);
    /// Gets the next packet from the Smoke API.
    /// This must be called decently often as it also handles callback management in the underlying API.
    fn get_next_packet(&self, data: &mut [u8]) -> Option<(u64, usize)>;
    /// Controls the existence of the receiver thread (it's kinda heavy and ugly and bad and no good)
    fn should_run_thread(&self) -> bool;
}

mod fake;

#[cfg(feature = "steamworks")]
mod real;

/// Creates the fake impl.
pub fn create_fake() -> Arc<dyn Smoke> {
    Arc::new(fake::Impl)
}

/// Creates the Smoke.
/// On failure, just returns the dummy.
#[cfg(feature = "steamworks")]
pub fn create_impl() -> Arc<dyn Smoke> {
    let ads: Arc<dyn Smoke> = create_fake();
    real::create_impl().unwrap_or(ads)
}

/// Creates the Smoke.
/// On failure, just returns the dummy.
#[cfg(not(feature = "steamworks"))]
pub fn create_impl() -> Arc<dyn Smoke> {
    create_fake()
}
