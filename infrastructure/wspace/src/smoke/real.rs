use std::sync::{Arc, Mutex};

use steamworks::{P2PSessionRequest, SteamId};

use super::Smoke;

struct Impl {
    client: Arc<steamworks::Client>,
    single_client: Mutex<steamworks::SingleClient>
}

impl Smoke for Impl {
    fn send_to(&self, who: u64, data: &[u8]) {
        self.client.networking().send_p2p_packet(SteamId::from_raw(who), steamworks::SendType::Unreliable, data);
    }
    fn get_next_packet(&self, data: &mut [u8]) -> Option<(u64, usize)> {
        let net = self.client.networking();
        if let Some(_) = net.is_p2p_packet_available() {
            net.read_p2p_packet(data).map(|v| (v.0.raw(), v.1))
        } else {
            self.single_client.lock().unwrap().run_callbacks();
            None
        }
    }
    fn should_run_thread(&self) -> bool {
        true
    }
}

pub fn create_impl() -> Option<Arc<dyn Smoke>> {
    match steamworks::Client::init() {
        Err(err) => {
            eprintln!("{:?}", err);
            None
        }
        Ok(ok) => {
            let cbarc = Arc::new(ok.0);
            let mcbarc = cbarc.clone();
            cbarc.register_callback(move |p2p: P2PSessionRequest| {
                mcbarc.networking().accept_p2p_session(p2p.remote);
            });
            Some(Arc::new(Impl {
                client: cbarc,
                single_client: Mutex::new(ok.1)
            }))
        }
    }
}
