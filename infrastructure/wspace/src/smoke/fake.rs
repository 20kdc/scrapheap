use super::Smoke;

pub struct Impl;

impl Smoke for Impl {
    fn send_to(&self, _who: u64, _data: &[u8]) {

    }
    fn get_next_packet(&self, _data: &mut [u8]) -> Option<(u64, usize)> {
        None
    }
    fn should_run_thread(&self) -> bool {
        false
    }
}
