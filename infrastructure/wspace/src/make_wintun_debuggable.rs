struct Logger15;
impl log::Log for Logger15 {
    fn enabled(&self, _: &log::Metadata) -> bool {
        true
    }
    fn log(&self, rec: &log::Record) {
        eprintln!("{:?}", rec.args());
    }
    fn flush(&self) {}
}
static INSTANCE: Logger15 = Logger15;
pub fn start() {
    log::set_logger(&INSTANCE).expect("logger should be settable");
    log::set_max_level(log::LevelFilter::Trace);
}
