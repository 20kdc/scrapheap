use std::net::{SocketAddr, UdpSocket};
use std::sync::atomic::AtomicBool;
use std::sync::Arc;
use std::{thread::sleep, time::Duration};

use crate::crypto::PacketCrypt;
use crate::ipspace::{self, AddressClass};
use crate::smoke::{self, Smoke};
use crate::tunnel::{create_tunnel, Tunnel};

/// External transport addresses.
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum PeerExternal {
    UDP(SocketAddr),
    Smoke(u64)
}

impl PeerExternal {
    pub fn wants_heartbeat(&self) -> bool {
        match self {
            Self::UDP(_) => true,
            Self::Smoke(_) => false
        }
    }
    pub fn wants_broadcast(&self) -> bool {
        match self {
            Self::UDP(_) => true,
            Self::Smoke(_) => false
        }
    }
}

#[derive(Clone)]
pub struct PeerInfo {
    pub name: String,
    pub internal: ipspace::PeerAddr,
    pub external: PeerExternal,
    pub is_local: bool,
}

pub struct Config {
    pub bind: SocketAddr,
    pub password: String,
    /// Peer information
    pub peers: Vec<PeerInfo>,
}

struct Routing {
    pub internal_addr_to_peer: [Option<PeerInfo>; ipspace::PEER_SLOTS],
    pub peers_seen: [AtomicBool; ipspace::PEER_SLOTS],
    pub peers: Vec<PeerInfo>,
    pub packetcrypt: PacketCrypt,
    pub udp: UdpSocket,
    pub smoke: Arc<dyn Smoke>,
    pub device: Box<dyn Tunnel + Send + Sync>,
}

impl Routing {
    fn send_to(&self, pkt_encrypted: &[u8], external: PeerExternal) {
        match external {
            PeerExternal::UDP(address) => {
                _ = self.udp.send_to(&pkt_encrypted, address);
            }
            PeerExternal::Smoke(id) => {
                self.smoke.send_to(id, pkt_encrypted);
            }
        }
    }
    fn route_packet(&self, pkt: &[u8], route: AddressClass, is_ingress: bool) {
        match route {
            ipspace::AddressClass::PeerIndex(target_index) => {
                let peer = &self.internal_addr_to_peer[target_index as usize];
                if let Some(peer) = peer {
                    if !peer.is_local {
                        let pkt_encrypted = self.packetcrypt.encrypt(&pkt);
                        self.send_to(&pkt_encrypted, peer.external);
                    } else {
                        self.device.send(pkt);
                    }
                } else {
                    println!(
                        "Received packet aimed for {} but couldn't find it.",
                        target_index
                    )
                }
            }
            ipspace::AddressClass::Broadcast => {
                let pkt_encrypted = self.packetcrypt.encrypt(&pkt);
                for peer in &self.peers {
                    if !peer.is_local {
                        if is_ingress {
                            // disjoint wspace networks are weird enough as it is
                            // sorry, no broadcast for you
                            continue;
                        }
                        if peer.external.wants_broadcast() {
                            self.send_to(&pkt_encrypted, peer.external);
                        }
                    } else {
                        self.device.send(pkt);
                    }
                }
            }
        }
    }
    fn handle_incoming(&self, peer: &PeerInfo, packet: &[u8]) {
        if !self.peers_seen[peer.internal as usize].swap(true, std::sync::atomic::Ordering::Relaxed) {
            println!("Confirmed receipt from {} for the first time", peer.name);
        }
        if packet.len() >= crate::crypto::NONCE_SIZE {
            // Not a dummy packet
            let decrypted = self.packetcrypt.decrypt(packet);
            //println!("DEBUG: Decrypted {}-byte packet", decrypted.len());
            if decrypted.len() != 0 {
                let pkt_parsed = ipspace::classify_packet(&decrypted);
                match pkt_parsed {
                    ipspace::PacketClass::Ok(route) => {
                        self.route_packet(&decrypted, route, true);
                    }
                    _ => {
                        if should_show_packet_errors() {
                            println!("Packet from {}: error {:?}", peer.name, pkt_parsed);
                        }
                    }
                }
            } else if should_show_packet_errors() {
                println!("Packet from {}: Decryption error", peer.name);
            }
        }
    }
}

#[cfg(windows)]
fn should_show_packet_errors() -> bool {
    // spams these on Windows
    true
}
#[cfg(not(windows))]
fn should_show_packet_errors() -> bool {
    false
}

pub fn run(cfg: Config) {
    // 'pure' peers table
    let peers = cfg.peers;
    // internal address to peers table
    let mut internal_addr_to_peer: [Option<PeerInfo>; ipspace::PEER_SLOTS] =
        [(); ipspace::PEER_SLOTS].map(|_| None);
    let mut using_smoke: bool = false;
    for v in &peers {
        if let PeerExternal::Smoke(_) = v.external {
            using_smoke = true;
        }
        internal_addr_to_peer[v.internal as usize] = Some(v.clone());
    }
    let smoke = if using_smoke {
        smoke::create_impl()
    } else {
        smoke::create_fake()
    };
    // -- peer mapping tables have been setup --
    let packetcrypt = PacketCrypt::setup(&cfg.password);
    if !packetcrypt.encryption {
        for _ in 0..8 {
            println!("!!! PACKET ENCRYPTION DISABLED !!! THIS IS ALSO AUTHENTICATION !!!");
        }
    } else {
        //println!("DEBUG: KEY: {:?}", packetcrypt.key_base);
    }
    println!("NETWORK TOPOLOGY");
    let mut local_peer: Option<PeerInfo> = None;
    for peer in &peers {
        if peer.is_local {
            println!(
                "\t{} = {} (you)",
                ipspace::encode_wsidx(peer.internal),
                peer.name
            );
            local_peer = Some(peer.clone());
        } else {
            println!(
                "\t{} = {} ({:?})",
                ipspace::encode_wsidx(peer.internal),
                peer.name,
                peer.external
            );
        }
    }
    let local_peer = local_peer.expect("Local peer must exist!");
    let local_peer_internal_addr = local_peer.internal;
    // ok, set it up
    let udp = UdpSocket::bind(cfg.bind).unwrap();
    let routing = Arc::new(Routing {
        device: create_tunnel(ipspace::encode_wsidx(local_peer_internal_addr)),
        internal_addr_to_peer,
        peers_seen: [false; ipspace::PEER_SLOTS].map(AtomicBool::new),
        peers,
        packetcrypt,
        smoke: smoke.clone(),
        udp,
    });
    // -- UDP Ingress Thread --
    let routing_for_ingress = routing.clone();
    std::thread::spawn(move || {
        let mut data = [0; 65536];
        let mut peers_port_mismatch: [bool; ipspace::PEER_SLOTS] = [false; ipspace::PEER_SLOTS];
        loop {
            let (plen, from) = routing_for_ingress.udp.recv_from(&mut data).unwrap();
            let packet = &data[0..plen];
            //println!("DEBUG: Received {}-byte packet", plen);
            let mut port_mismatch_peer = None;
            for (k, peer) in routing_for_ingress.peers.iter().enumerate() {
                if let PeerExternal::UDP(peer_external) = peer.external {
                    if peer_external.eq(&from) {
                        routing_for_ingress.handle_incoming(peer, packet);
                        port_mismatch_peer = None;
                        break;
                    } else if peer_external.ip().eq(&from.ip()) {
                        port_mismatch_peer = Some(k);
                        break;
                    }
                }
            }
            if let Some(port_mismatch_k) = port_mismatch_peer {
                let peer_info = &routing_for_ingress.peers[port_mismatch_k];
                if !peers_port_mismatch[peer_info.internal as usize] {
                    let name = &peer_info.name;
                    println!("Packet from IP/port {} matches IP with peer {} but not port. Unless this was an accident, their NAT is probably translating the port, so replace peer {} on all devices EXCEPT {} itself to {}.", from, name, name, name, from);
                    peers_port_mismatch[peer_info.internal as usize] = true;
                }
            }
        }
    });
    // -- TUN Egress/Routing Thread --
    let routing_for_egress = routing.clone();
    std::thread::spawn(move || loop {
        let mut data = [0; 65536];
        let plen = routing_for_egress.device.recv(&mut data);
        let pkt = &data[..plen];
        let target = ipspace::classify_packet(pkt);
        match target {
            ipspace::PacketClass::Ok(route) => {
                routing_for_egress.route_packet(pkt, route, false);
            }
            ipspace::PacketClass::BlockedInvalidIPVersion => {
                // do nothing!
            }
            _ => {
                if should_show_packet_errors() {
                    println!("Outbound packet: error {:?}", target);
                }
            }
        }
    });
    // -- Smoke Thread --
    if smoke.should_run_thread() {
        let routing_for_smoke = routing.clone();
        std::thread::spawn(move || {
            let mut next_sleep = std::time::Instant::now() + std::time::Duration::from_millis(1);
            let mut data = [0; 65536];
            loop {
                let rcpt = routing_for_smoke.smoke.get_next_packet(&mut data);
                if let Some((rcpt_smoke_id, plen)) = rcpt {
                    let packet = &data[0..plen];
                    for (_, peer) in routing_for_smoke.peers.iter().enumerate() {
                        if let PeerExternal::Smoke(smoke_id) = peer.external {
                            if smoke_id == rcpt_smoke_id {
                                routing_for_smoke.handle_incoming(peer, packet);
                                break;
                            }
                        }
                    }
                } else {
                    // We don't want to add too much latency but we also don't want to melt the processor.
                    let refpoint = std::time::Instant::now();
                    if let Some(time) = next_sleep.checked_duration_since(refpoint) {
                        std::thread::sleep(time);
                    }
                    next_sleep = refpoint + std::time::Duration::from_millis(1);
                }
            }
        });
    }
    // -- Heartbeat Targets --
    let mut heartbeat_targets = vec![];
    for peer in &routing.peers {
        if peer.is_local || !peer.external.wants_heartbeat() {
            continue
        }
        heartbeat_targets.push(peer.external);
    }
    // -- Heartbeat Loop --
    // Every 30 seconds, a new heartbeat opportunity occurs: If we are not already performing a heartbeat, we start one.
    // During a heartbeat, round-robin send ten packets per second, one to each peer.
    let mut countdown: u32 = 0;
    let mut round_robin: usize = 0;
    loop {
        if countdown == 0 {
            if round_robin >= heartbeat_targets.len() {
                round_robin = 0;
            }
            countdown = 300;
        }
        countdown -= 1;
        if round_robin < heartbeat_targets.len() {
            // don't send to self
            let peer = heartbeat_targets[round_robin];
            _ = routing.send_to(&[], peer);
            round_robin += 1;
        }
        sleep(Duration::from_millis(100));
    }
}
