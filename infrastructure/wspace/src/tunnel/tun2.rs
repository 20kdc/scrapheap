use super::Tunnel;
use std::net::Ipv4Addr;
use tun2::platform::Device;

pub struct TunnelImpl(Device);

impl Tunnel for TunnelImpl {
    fn send(&self, packet: &[u8]) {
        _ = self.0.send(packet);
    }
    fn recv(&self, data: &mut [u8]) -> usize {
        self.0.recv(data).unwrap()
    }
}

pub fn create_tunnel_impl(host: Ipv4Addr) -> TunnelImpl {
    let mut config = tun2::Configuration::default();
    config.address(host);
    config.netmask((255, 255, 255, 0));
    config.layer(tun2::Layer::L3);
    config.up();
    TunnelImpl(tun2::create(&config).unwrap())
}
