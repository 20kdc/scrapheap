use packet::{ip::Protocol, Packet, Size};
use std::net::Ipv4Addr;

/// 'compressed' peer address inside wspace
pub type PeerAddr = u8;
pub const PEER_SLOTS: usize = (PeerAddr::max_value() as usize) + 1;

#[derive(Debug, Clone, Copy)]
pub enum AddressClass {
    Broadcast,
    PeerIndex(PeerAddr),
}

pub fn classify_address(addr: Ipv4Addr) -> Option<AddressClass> {
    let octets = addr.octets();
    if octets[0] == 10 && octets[1] == 87 && octets[2] == 83 {
        if octets[3] == 0 || octets[3] == 255 {
            Some(AddressClass::Broadcast)
        } else {
            Some(AddressClass::PeerIndex(octets[3]))
        }
    } else if addr.is_link_local() || addr.is_multicast() || addr.is_broadcast() {
        Some(AddressClass::Broadcast)
    } else {
        None
    }
}

/// Encodes to an address
pub fn encode_wsidx(idx: PeerAddr) -> Ipv4Addr {
    Ipv4Addr::new(10, 87, 83, idx as u8)
}

/// Classifies the target of a packet
#[derive(Debug)]
pub enum PacketClass {
    BlockedInvalidIPPacket,
    BlockedSizeMismatch,
    BlockedInvalidIPVersion,
    BlockedInvalidSource,
    BlockedInvalidDestination,
    BlockedFailedDeepParse,
    BlockedSuspiciousPort,
    Ok(AddressClass),
}

/// Windows computers are full of holes, let's at least try to protect them a little
pub fn is_suspicious_port(port: u16) -> bool {
    match port {
        42 => true, // WINS (ancient Windows pre-Active Directory name resolver)
        53 => true, // DNS
        67 => true,
        88 => true,
        102 => true,
        135 => true, // <- Windows RPC is the most dangerous port here, if you keep any port here, keep this one
        137 => true, // NetBIOS
        138 => true, // NetBIOS
        139 => true, // NetBIOS
        161 => true, // SNMP
        162 => true, // SNMP Trap
        389 => true,
        445 => true, // SMB
        464 => true,
        500 => true,
        515 => true, // LPD
        593 => true, // <- MORE WINDOWS RPC
        647 => true,
        636 => true,
        1645 => true,
        1646 => true,
        1745 => true,
        1801 => true,
        1812 => true,
        1813 => true,
        1900 => true, // SSDP
        2101 => true,
        2105 => true,
        2107 => true,
        2171 => true,
        2172 => true,
        2173 => true,
        2175 => true,
        2460 => true, // "MS Theater": seemingly unknown protocol used by Windows Media Services, it's a security hazard
        2535 => true,
        2869 => true, // SSDP, UPnP
        3268 => true,
        3269 => true,
        3343 => true,
        3389 => true, // RDS (Just use VNC instead if you really want it)
        3527 => true,
        3847 => true,
        4011 => true, // PXE... shouldn't be relevant but let's just not, hmm?
        4500 => true,
        5000 => true, // SSDP
        5722 => true,
        5985 => true, // WinRM
        5986 => true, // WinRM
        6600 => true,
        9389 => true,
        _ => false,
    }
}

pub fn classify_packet(packet: &[u8]) -> PacketClass {
    let pkt_parsed = packet::ip::Packet::new(packet);
    match pkt_parsed {
        Ok(packet::ip::Packet::V4(pkt_parsed)) => {
            if pkt_parsed.size() != packet.len() {
                PacketClass::BlockedSizeMismatch
            } else {
                match classify_address(pkt_parsed.source()) {
                    None => PacketClass::BlockedInvalidSource,
                    Some(_) => {
                        let dest_class = classify_address(pkt_parsed.destination());
                        match dest_class {
                            None => PacketClass::BlockedInvalidDestination,
                            Some(route) => {
                                // UDP/TCP have a common "source/dest" port structure in the first 4 bytes.
                                // Keep in mind, the goal here isn't to do deep inspection on every packet.
                                // It's just to make absolutely sure nobody can connect to Windows RPC ports.
                                let must_inspect_ports = match pkt_parsed.protocol() {
                                    Protocol::Tcp => true,
                                    Protocol::Udp => true,
                                    Protocol::UdpLite => true,
                                    _ => false,
                                };
                                if must_inspect_ports {
                                    // Fragment offset is in units of 8 bytes.
                                    // As such it is not possible for a valid first fragment of a valid UDP or TCP packet to exist that does not include the source port and destination port.
                                    if pkt_parsed.offset() == 0 {
                                        if pkt_parsed.payload().len() < 4 {
                                            PacketClass::BlockedFailedDeepParse
                                        } else {
                                            let dest_port = u16::from_be_bytes(
                                                pkt_parsed.payload()[2..4].try_into().unwrap(),
                                            );
                                            if !is_suspicious_port(dest_port) {
                                                PacketClass::Ok(route)
                                            } else {
                                                PacketClass::BlockedSuspiciousPort
                                            }
                                        }
                                    } else {
                                        PacketClass::Ok(route)
                                    }
                                } else {
                                    PacketClass::Ok(route)
                                }
                            }
                        }
                    }
                }
            }
        }
        Err(_) => PacketClass::BlockedInvalidIPPacket,
        _ => PacketClass::BlockedInvalidIPVersion,
    }
}
