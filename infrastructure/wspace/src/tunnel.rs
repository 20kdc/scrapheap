use std::net::Ipv4Addr;

/// Implements the tunnel.
pub trait Tunnel {
    /// Sends a packet.
    fn send(&self, data: &[u8]);
    /// Receives a packet.
    fn recv(&self, data: &mut [u8]) -> usize;
}

// implementation
mod tun2;
use tun2::create_tunnel_impl;

/// Creates a tunnel.
/// This is a wrapper over the actual tunnel creation implementation.
pub fn create_tunnel(host: Ipv4Addr) -> Box<dyn Tunnel + Send + Sync> {
    Box::new(create_tunnel_impl(host))
}
