use sha3::{self, Digest};

pub const NONCE_SIZE: usize = 16;
const HASH_SIZE: usize = 32;

/// Hash algorithm used to ensure keys are clean and fixed-size.
type HashAlg = sha3::Sha3_256;

/// Shared-key packet cryptographic helper.
/// Basically, we can't just avoid padding the packets because that'd mean we're reencrypting with the same XOR key (this is very bad).
/// So we kinda have to send 16 bytes of nonce. That sucks.
/// Notably, nonce predictability isn't that important, just uniqueness, and being unique for identical data isn't an issue.
/// But transmitting mangled data would be a problem.
/// As such, the ed25519 approach is taken: the nonce is calculated from the first NONCE_SIZE bytes of the hash of the password followed by the contents.
/// (The password is included so that nonces do not trivially leak the hash of data.
///  Only authorized users can verify packet data with certainty, though guesses are possible.)
/// The actual encryption operation uses the XOR stream design from AES-256-CTR with Keccak as the generator.
/// The consistent nonce generation allows the nonce to double as a checksum.
/// A known weakness of this system is that repeated packets have repeated checksums, but this is okay within wspace's goals.
pub struct PacketCrypt {
    /// Key, loaded into hasher for easy inclusion
    pub key_base: HashAlg,
    pub key_hash: [u8; HASH_SIZE],
    /// on/off switch
    pub encryption: bool,
}

impl PacketCrypt {
    /// Sets up packet encryption.
    pub fn setup(password: &str) -> PacketCrypt {
        let key_base = HashAlg::new_with_prefix(password);
        let key_base2 = key_base.clone();
        PacketCrypt {
            key_base,
            key_hash: key_base2.finalize().try_into().unwrap(),
            encryption: password.len() != 0,
        }
    }

    /// Creates the XOR stream for decrypting/encrypting a packet.
    /// This is expensive, so, hey, sponge functions!
    fn pad(&self, nonce: &[u8; NONCE_SIZE], amount: usize) -> Vec<u8> {
        // init sponge
        let mut sponge_state: [u64; 25] = [0; 25];
        // load in key hash
        sponge_state[0] = u64::from_le_bytes(self.key_hash[0..8].try_into().unwrap());
        sponge_state[1] = u64::from_le_bytes(self.key_hash[8..16].try_into().unwrap());
        sponge_state[2] = u64::from_le_bytes(self.key_hash[16..24].try_into().unwrap());
        sponge_state[3] = u64::from_le_bytes(self.key_hash[24..32].try_into().unwrap());
        // load in nonce
        sponge_state[4] = u64::from_le_bytes(nonce[0..8].try_into().unwrap());
        sponge_state[5] = u64::from_le_bytes(nonce[8..16].try_into().unwrap());
        // salt a lil for taste
        sponge_state[6] = 0x4159314159314159;
        keccak::f1600(&mut sponge_state);

        let mut data = Vec::with_capacity(amount + 8);
        let mut sponge_access_index: usize = 0;
        while data.len() < amount {
            if sponge_access_index == 16 {
                keccak::f1600(&mut sponge_state);
                sponge_access_index = 0;
            }
            let val = sponge_state[sponge_access_index].to_le_bytes();
            data.extend_from_slice(&val);
            sponge_access_index += 1;
        }
        data.truncate(amount);
        data
    }

    /// Actual encryption/decryption operation.
    fn xorpad(&self, nonce: &[u8], buf: &[u8]) -> Vec<u8> {
        let mut res = self.pad(nonce.try_into().unwrap(), buf.len());
        if res.len() != buf.len() {
            panic!("res.len() != buf.len()");
        }
        for i in 0..res.len() {
            res[i] ^= buf[i];
        }
        res
    }

    /// Calculate the nonce for some unencrypted data.
    fn nonce_for(&self, input: &[u8]) -> [u8; NONCE_SIZE] {
        let mut nonce_hasher = self.key_base.clone();
        nonce_hasher.update(input);
        nonce_hasher.finalize()[0..NONCE_SIZE].try_into().unwrap()
    }

    /// Encrypts a packet, adding 16 bytes of nonce at the start.
    pub fn encrypt(&self, input: &[u8]) -> Vec<u8> {
        if !self.encryption {
            input.to_vec()
        } else {
            let nonce: [u8; NONCE_SIZE] = self.nonce_for(input);
            //println!("Enc:{:?}", nonce);
            let stream = self.xorpad(&nonce, input);
            let mut res = Vec::with_capacity(NONCE_SIZE + stream.len());
            res.extend_from_slice(&nonce);
            res.extend_from_slice(&stream);
            res
        }
    }

    /// Decrypts a packet.
    /// Packets smaller than the nonce size are decrypted to zero-bytes packets.
    /// (This is safe due to the usecase.)
    pub fn decrypt(&self, input: &[u8]) -> Vec<u8> {
        if !self.encryption {
            input.to_vec()
        } else if input.len() < NONCE_SIZE {
            vec![]
        } else {
            let nonce = &input[0..NONCE_SIZE];
            //println!("Dec:{:?}", nonce);
            let data = &input[NONCE_SIZE..];
            let res = self.xorpad(nonce, data);
            // Before we can release this data, we must verify it.
            let expected_nonce: [u8; NONCE_SIZE] = self.nonce_for(&res);
            if expected_nonce != nonce {
                // Failed!
                vec![]
            } else {
                res
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_roundtrip() {
        let vector: &[u8] =
            "umbrella and hat, clad in blue, in yellow and white, me and you".as_bytes();
        let key = "obfuscated by ignorance or by malice I do not know";
        let crypt = PacketCrypt::setup(&key);
        let encrypted = crypt.encrypt(&vector);
        let decrypted = crypt.decrypt(&encrypted);
        assert_eq!(vector, decrypted);
    }
}
