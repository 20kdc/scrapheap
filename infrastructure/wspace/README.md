# wspace: Trivially Configured Fully-Decentralized Application-Transparent Traversal Tool For Friend Groups With Port-Preserving NATs

Or, `wspace` for short.

_If you're seeing this file as part of a binary package, source code is at <https://gitlab.com/20kdc/scrapheap/-/tree/main/infrastructure/wspace>_

## Give me the TLDR.

Two computers, behind port-preserving NATs, `deusMachinist` and `chaosObserver`. External IPs are:

* `deusMachinist`: `198.51.100.95`
* `chaosObserver`: `203.0.113.47`

`deusMachinist` wants to play Minetest with `chaosObserver` on a private server hosted by `deusMachinist`, but neither can port-forward. They have agreed via some mechanism (your favorite chat app, in-game, pencil and paper) to run `wspace` to make the connection.

Their shared config:
```
{
 "bind": "0.0.0.0:3310",
 "password": "ThisIsNotAGoodPassword",
 "peers": {
  "deusMachinist": {"internal": 1, "external": "198.51.100.95:3310"},
  "chaosObserver": {"internal": 2, "external": "203.0.113.47:3310"}
 }
}
```

`deusMachinist` runs: `wspace deusMachinist myconfig.json`.

`chaosObserver` runs: `wspace chaosObserver myconfig.json`.

Assuming their networks are of the right kind, they connect without port forwarding. No external services are used.

`deusMachinist` now has the IP `10.87.83.1` and `chaosObserver` now has the IP `10.87.83.2`, and they can now access servers hosted on each other's computers. Only servers hosted on those computers are accessible to each other; the rest of their local networks are not. `chaosObserver` connects to `10.87.83.1` and they play together.

## Ok, but what is it?

wspace is a tool to allow a friend group to play a game together.

wspace is _not:_

* A 'hide your IP as you browse the internet' VPN, or software to run a VPN.
* Any form of anonymity tool.
* A grand overlay network project with encryption and IPv6 and grand ideas.
* A singular P2P network everybody using it's connected to with data relaying and stuff. \* Except given the fallback option. See "What if I can't use it?"
* Bait to get you to sign up for our Gaming Tunnel As A Service, blah blah blah blah...

wspace is a network tunnel with hardcoded safety rules that uses configuration _instead of_ a STUN server, and therefore, when it works, is completely decentralized.

_If you can setup your game server, run wspace with your friend's IP address, have your friend run it with yours, and have them connect to your in-wspace IP and join you, without any further configuration, then wspace is working as intended._

It is an intermediate solution, more annoying than "port forwarding/UPnP", but it's not trying to sell you something and it's not going to be misconfigured to turn into a VPN by accident or something. You won't relay packets unless they're from someone you authorized, and wspace won't relay them outside of your wspace network.

## Security?

It's important to keep in mind `wspace` is an alternative to port forwarding. It's not a VPN, not an anonymity layer, but also, there's no 'wspace server' which relays your packets.

wspace's cryptography is meant to provide some level of authentication _only._ No keypairs, just a shared password between all peers.

Any further security exists to just 'make attacks harder' by making, say, preimages harder. The assumption is wspace connections should be roughly on-par with open internet, though more measures have been taken here than should be strictly needed (better safe than sorry).

If they're more secure, that's a bonus -- if they're less secure that's a problem -- if they're equally secure, that's good enough.

## What's the protocol like?

See [PROTOCOL.md](PROTOCOL.md).

## Why not make it usable as a VPN?

Use [tinc](https://tinc-vpn.org/). Maybe use wspace to bootstrap the NAT traversal, IDK. Maybe use WireGuard and send UDP pings via `nc` to implement the punchthrough. Occasionally curl a gist, use that to schedule the punchthroughs. If you're trying to run your own VPN, you probably know what you're doing and already have plenty of options.

Software-wise, wspace uses VPN mechanisms because it's convenient and _will_ work with a large variety of applications. It comes with some risks, and wspace contains a firewall to mitigate those risks.

**wspace is designed to be trivial to setup without being too dangerous, and to work well with games, including games which react poorly to TCP proxying due to API misuse.** _This naturally limits the things it can be used for, and this is intentional._

## Why build it as a tunnel and end up having to firewall it?

Packet injection (using the UDP side-channel to arrange dummy return packets on a connection; essentially bolting punch-through onto the application rather than proxying traffic) was considered, but never got tested.

It feels like a better approach, and if Windows didn't exist or Npcap was open-source, I would have probably tried it. (It might still not have worked, but I would have *tried.*)

As it is, I can only pick two of the following:

* Build it without using a tunnel
* Support Windows
* _Not_ implicitly support Npcap's predatory licensing

In my opinion, Npcap violates the spirit of the GPL, and there's little you can do about it unless you work at Google or something, because of Windows driver signing requirements. And if you bypass _those,_ you're actually worse off than if you switched to Linux, because the anticheats will get... ravenous.

Npcap is used by Wireshark, and as of this writing, Npcap's "unlimited free tier" only counts if you are using it with Wireshark, Nmap, or "Microsoft Defender for Identity".

_I fail to see how making it essentially impossible to fork Wireshark in your organization, assuming it uses Windows, without potentially violating Npcap's unlimited free tier isn't an attack on software freedoms._ (The exact wording is precise enough that I am certain forks would count as a violation of this license.)

This is not a violation of the legal text of the GPL; it's a driver that Wireshark happens to use, and it's likely not linked in such a way that the GPL's linking provisions apply. But I am confident in my opinion that it is a violation of the spirit.

## What if I can't use it on my network?

**Ok, so, after finding out that the NAT situation of my friend groups absolutely and totally _sucks,_ I have decided to bite the bullet.** There is now a Cargo feature called `steamworks`. This Cargo feature, if someone were to _happen_ to enable it, and were to _happen_ to retrieve the correct version of the Steamworks redistributables from <https://github.com/Noxime/steamworks-rs/tree/v0.11.0/steamworks-sys/lib/steam/redistributable_bin>, and of course grabbed the `wintun` libraries if necessary and so on, allows a different kind of peer.

Rather than specifying `"external": "..."`, you can, in builds so enabled, specify `"sid": "SomeSteamID64Here"` (i.e. `"sid": "1234567890"`).

On builds that do not support this integration, this will create an inaccessible peer.

On builds that do support this integration, this will cause wspace to pretend to be a Steam game and to send data to/receive data from that peer via Steam. Broadcast packets and heartbeats are disabled for these peers.

In practice, the use of this network setup option is pretty either/or, as mixing the peer types will probably result in some kind of failure unless you have an explicitly opened port somewhere. Arguably it might ought to be a separate executable, or I should have some way to make peers prioritize based on settings or build type or something. IDK.

Fact is, this is a dodgy, quick & dirty workaround on top of a dodgy, quick & dirty workaround. I should really go refactor the application to allow for mixed networks which work properly without doing config-file-specific shenanigans. And allow auto-tuning the UDP port (i.e. for 'reverse server/client' situations). I should also do a hundred other things, and this is the change I can ship in a day which might make some stuff better for some people.
