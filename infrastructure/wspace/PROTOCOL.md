# wspace protocol

## Overview

The wspace protocol is pretty minimalistic. The 'configurative determinism' in the main README should give you the idea of how by the time this document becomes relevant, all protocol negotiations have been performed out-of-band.

The protocol is entirely based on a possibly-unreliable transport. It has some similarities to the Teredo protocol, and when encryption is disabled, is nearly identical in raw packet content (if IPv4 rather than IPv6), but it isn't the same thing.

## Glossary

Peer: A peer has a Peer Address and may have an External Address or be local. (The current implementation has an external address and a 'is local' flag, to keep the configuration consistent.)

Peer Address: Number from 1 to 254 inclusive. The current implementation allocates these based on the order of external addresses supplied on the command-line.

Internal Address: IPv4 address inside wspace network. Prefix `10.87.83.` to the peer address for this.

External Address: In theory, can be anywhere where you can send datagrams to and receive them from. In practice, there are two kinds of external address:
* UDP Transport: An IP socket address (IP/port pair) outside of wspace where it is assumed another wspace process is waiting.
* Smoke Transport: A 64-bit user ID.

## Constants

`NONCE_SIZE`: 16 (arbitrarily chosen to balance overhead vs. security)

`HASH_SIZE`: 32 (SHA3-256 output size)

## Transports

The wspace protocol has two defined transports:

* UDP: The UDP transport attempts to establish a connection by simply assuming the other side will respond. This can punch through simpler NATs.
* Smoke: The Smoke transport uses a well-known external API to shuffle packets. This external API has an externally assigned "app identifier" (which the user must provide) and packets are routed to users by their 64-bit user ID.

## UDP Transport: Duct-Taped NAT Punchthrough

UDP connections are arranged by "some external mechanism". In practice this is the config file to the `wspace` tool. The port has been arbitrarily chosen to be `3310`. _To be clear, the only inherent requirement of the protocol is that each peer already has the contact details of each peer it wishes to contact on startup, and all of these links are reciprocal._

I am quite aware that this doesn't handle NATs which don't preserve ports, and particularly, it _really_ can't deal with NATs which randomly reassign ports on a per-connection basis. There's only so far you can go without a STUN server.

## Smoke Transport: When In Doubt, Have Someone Else Figure It Out

The Smoke transport hands off establishment of NAT punchthrough; and, as a critical fallback, _possibly data relaying,_ to an external service. _For further details, please see the code._

## Packet Contents

The UDP packets are of two kinds:

* Any packet under `NONCE_SIZE` bytes in size is a 'heartbeat'. These are used to create and maintain NAT punchthrough. They presently serve no other purpose, except that they can trigger the program's alert of receipt of the first packet, which helps to indicate punchthrough success.
* IPv4 packets that _may_ be encrypted (see Encryption section).

## Encryption

If the password is not the empty string, a `NONCE_SIZE`-byte header is prepended to packets, and the packet contents (not including those `NONCE_SIZE` bytes) are XOR'd with a pseudorandom stream.

The added header is the first `NONCE_SIZE` bytes of `sha3_256(password+packet)`. This is referred to as `nonce` in the code. **This also acts as an integrity check. It is security-relevant to, post-decryption, verify the decrypted packet generates the same nonce.** (This stops malicious actors from injecting arbitrary mangled packets into the network. They can still replay any packets they like, but if they can do that then they could do that with a port-forwarded connection. Remember the security model here.)

The pseudorandom stream is more complex.

In the `keccak` crate, the state is represented as `[u64; 25]`. This state is mostly initialized with zeroes, but:

* The first 4 entries are initialized by treating the bytes of `sha3_256(password)` as 4 little-endian `u64`s.
* The next 2 entries are initialized by the the same principle on `nonce`.
* One further entry is initialized with `0x4159314159314159`.

The pseudorandom stream can conceptually be considered blocks of 128 bytes. These bytes are sourced from indexes 0 through 16 exclusive of the Keccak state, extracted as little-endian `u64`. `keccak::f1600(&mut sponge_state);` is run before each block is extracted. (As an optimization, this can, and is, modelled as blocks of 8 bytes with a round-robin scheme for extraction and updating the state at the start of every cycle.)

Once sufficient data has been extracted to cover the packet size, the pseudorandom stream is truncated to exactly fit.

## Packet Filtering

IPv6 packets are blocked pending further notice. There's nothing in the core protocol that makes them so much of a problem per-se, but there's nothing allocating them addresses, nothing to route them properly, and the IPv6 extension header mechanism is painful.

The host's core IP stack (any code which directly handles an IPv4, TCP, or UDP header) is _assumed secure_ against malformed packets (including broken fragmentation). Accepting conflicting fragment data does not break this assumption.

To prevent attacks against default services shipped with consumer OSes, any IPv4 packet with a zero fragment offset and the protocol numbers for TCP, UDP, or UDP-Lite undergoes an additional safety check.

TCP, UDP, and UDP-Lite have in common a big-endian `u16` "destination port" field at offset 2 in their headers.

Conveniently, due to 8-octet alignment, a fragment inherently cannot be placed to overlap this field without being at offset 0, making it mostly unnecessary to account for pathological fragmentation.

The main exception is a packet with fragment offset 0 and one of these protocol numbers have a payload less than 4 bytes long. These are dropped -- there is no _valid_ circumstance where such a fragment can create a complete packet. In invalid circumstances, a reassembly implementation might let undefined data seep into the intervening space, or overlapping fragments may be used: neither technically writes a banned port number into the field, but the composition of both do. For example, `00 00 FF 87` followed by `00 00 00` composing to `00 00 00 87`. For this reason, it can't be allowed.)

The ports which are considered invalid are subject to change, but the following are considered **critical** to block for user safety:
* The Windows RPC ports, 135 and 593.
	* _This is probably the most dangerous protocol a Windows computer can expose, and it's exposed by default. **If you can only block two ports, block these. In fact, how about you just never, ever unblock them, even if you're told to unblock all ports. Please.**_
* The NetBIOS ports, 137, 138, 139.
* The SMB port, 445.
* The DNS port, 53.
* The SNMP ports, 161, 162.
