FROM scratch
# alpine + miniupnpc, stunnel, curl, and busybox's tcpsvd
# (Alpine's busybox doesn't have it)
# stored as a rootfs due to issues caching it
# and due to failures in production
ADD alpine.tar.gz /

COPY nanomta/nanomta /nanomta
COPY nanomta/port25 /port25
COPY nanomta/nanomta-entry /entry
COPY nanomta/starttls /starttls
COPY nanomta/nanomta-stunnel.conf /stunnel.conf

# env file should contain, i.e.
# NANOMTA_SIGNON=example.org
# NANOMTA_RCPT=@example\.org>$|postmaster
# NANOMTA_SSL_CRT=/caddy-data/caddy/certificates/acme-v02.api.letsencrypt.org-directory/example.org/example.org.crt
# NANOMTA_SSL_KEY=/caddy-data/caddy/certificates/acme-v02.api.letsencrypt.org-directory/example.org/example.org.key

VOLUME /Maildir

CMD ["/entry"]
