# nanoMTA+nanoMDA+TLS: Dockerized

This exists because I've found that a Docker Compose-based infrastructure to be rather convenient to manage for devices where the overhead is acceptable.

But nanoMTA remains easier to administrate for me. I trust it in a way I could never trust a larger codebase where there's probably a hundred ways to turn it into an open relay.

The docker-compose.yml file here is an example, and the Dockerfiles require some added binaries (and, due to repeated random glitches building the containers, a manually supplied rootfs), but it seems to all work.

## Binaries

The missing binaries are `nanomta` and `nanomda`. The `./b` script in the parent directory compiles them.

There's also `alpine.tar.gz`, which is Alpine with the following packages installed:

* `stunnel`
* `miniupnpc` (not used here)
* `curl` (not used here)

And this binary installed:

* `busybox_TCPSVD` as `/usr/bin/tcpsvd`

## Configuration

Basically, once the missing binaries are dealt with, the containers expect the following configuration:

* `NANOMTA_SIGNON`: See parent README (Hostname, whatever)
* `NANOMTA_RCPT`: See parent README (TLDR: Regex to validate recipients as being of this server)
* `NANOMTA_SSL_CRT`: Path to SSL certificate file
* `NANOMTA_SSL_KEY`: Path to SSL key file
* `NANOMDA_USER`: See parent README (username)
* `NANOMDA_PASS`: See parent README (password)
* `NANOMDA_SALT`: See parent README (salt)

They expect a `/Maildir` volume (for storing the mail), it's designed assuming you're running Caddy on the same host, so you should have that.

The env file looks like this:

```
NANOMTA_SIGNON=example.com
NANOMTA_RCPT=@example\.com>$|postmaster
NANOMTA_SSL_CRT=/caddy-data/caddy/certificates/acme.example.org-directory/example.com/example.com.crt
NANOMTA_SSL_KEY=/caddy-data/caddy/certificates/acme.example.org-directory/example.com/example.com.key
NANOMDA_USER=notauser
NANOMDA_PASS=notapassword
NANOMDA_SALT=notasalt
```

## Results

The `nanomta` container exposes SMTP ingress on port 25.

The `nanomda` container exposes password-authenticated POP3 TLS on port 995. It *does not* expose unencrypted POP3 (there isn't really a reason to do so).
