FROM scratch
# alpine + miniupnpc, stunnel, curl, and busybox's tcpsvd
# (Alpine's busybox doesn't have it)
# stored as a rootfs due to issues caching it
# and due to failures in production
ADD alpine.tar.gz /

COPY nanomta/nanomda /nanomda
COPY nanomta/nanomda-entry /entry
COPY nanomta/nanomda-stunnel.conf /stunnel.conf

# env file should contain, i.e.
# NANOMDA_USER=admin
# NANOMDA_PASS=snowglobe
# NANOMDA_SALT=sodium

VOLUME /Maildir

CMD ["/entry"]
