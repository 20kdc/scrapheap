#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <regex.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>
#include <inttypes.h>

#ifndef ERROR_PREFIX
#define ERROR_PREFIX "221 "
#endif

// This is a workaround for a disagreement between glibc and musl.
char * crypt(const char * p, const char * s);

// command-line //

#define MAIN_MAX_CMDLINE_LEN 1024
static char cmdline[MAIN_MAX_CMDLINE_LEN + 1];

// Reads a command. Exits on failure.
static void cmd_read() {
	size_t len = 0;
	int last_char_was_13 = 0;
	while (len < MAIN_MAX_CMDLINE_LEN) {
		int ch = getchar();
		if (ch == -1) {
			// bye then
			printf(ERROR_PREFIX "eof\r\n");
			exit(1);
		} else if (ch == 0) {
			printf(ERROR_PREFIX "do not try to put NUL into cmdline\r\n");
			exit(1);
		} else if (ch == '\n') {
			// done!
			if (last_char_was_13)
				len--;
			cmdline[len] = 0;
			return;
		}
		last_char_was_13 = ch == '\r';
		cmdline[len++] = ch;
	}
	// too long, abandon
	printf(ERROR_PREFIX "command line way too long; abandoning\r\n");
	exit(1);
}

// Returns true if the commandline starts with the given string (case-insensitive).
// See for instance "rcpt to:<tester@example.com>" ; this wouldn't parse as a normal command.
// On success, param provides the parameter.
static int cmd_prefix(const char * cmd, char ** param) {
	int slc = strlen(cmd);
	if (strlen(cmdline) < slc)
		return 0;
	if (strncasecmp(cmdline, cmd, slc))
		return 0;
	if (param)
		*param = cmdline + slc;
	return 1;
}

// The commandline must either be the given string,
//  or must start with the given string followed by " ".
// On success, param provides the parameter.
static int cmd_normal(const char * cmd, char ** param) {
	size_t slc = strlen(cmd);
	size_t cmdline_len = strlen(cmdline);
	if (cmdline_len < slc)
		return 0;
	if (cmdline_len > slc)
		if (cmdline[slc] != ' ')
			return 0;
	if (strncasecmp(cmdline, cmd, slc))
		return 0;
	if (param)
		*param = cmdline_len > slc ? (cmdline + slc + 1) : (cmdline + slc);
	return 1;
}

// Kind of strdup(getenv(env)), except that would SIGSEGV if getenv were to return NULL.
// This instead returns NULL itself.
static char * dupenv(const char * env) {
	const char * val = getenv(env);
	if (!val)
		return NULL;
	return strdup(val);
}

// Kind of strdup(crypt(a, b)), except for two elements:
// 1. Errors return NULL instead of '*' prefix.
// 2. Always strdup'd.'
static char * dupcrypt(const char * a, const char * b) {
	const char * val = crypt(a, b);
	if (!val)
		return NULL;
	if (val[0] == '*')
		return NULL;
	return strdup(val);
}

// Like strdup, but a mem* variant of that.
static void * memdup(const void * s, size_t n) {
	void * res = malloc(n);
	if (!res)
		return NULL;
	memcpy(res, s, n);
	return res;
}
