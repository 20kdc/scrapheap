#include <stdio.h>
#define BRAND_ENV "NANOMTA"
#define BRAND "nanoMTA"

#define ERROR_PREFIX "221 "

#include "common.h"

// blob copy //

static int mail_putc(int fd, char ch) {
	if (write(fd, &ch, 1) <= 0)
		return 0;
	return 1;
}

static int mail_puts(int fd, const char * s) {
	while (*s)
		if (!mail_putc(fd, *(s++)))
			return 0;
	return 1;
}

// starts once we're committed to receiving the data
// returns non-zero on success
static int do_mail_copy(int fd, int max_size) {
	int ok = 1;
	const char * inf = getenv(BRAND_ENV "_RECEIVED");
	if (inf) {
		ok &= mail_puts(fd, "Received: ");
		ok &= mail_puts(fd, inf);
		ok &= mail_puts(fd, "\r\n");
	}
	// blob receiver
	int state = 0;
	while (state != -1) {
		int ch = getchar();
		if (!max_size--)
			return 0; // too large!
		if (ch == -1)
			return 1; // oh alright then
		// Lone CR is impossible at least as of RFC 2822.
		// Therefore we can treat LF as CRLF & ignore CR.
		if (ch == '\r')
			continue;
		int inhibit = 0;
		// state machine
		// has to be careful to try and recover from repeating sequences
		// i.e. "\n." or "\n\n.".
		// also needs to be careful to 'revive' inhibited chars.
		switch (state) {
			case 0: // ""
				if (ch == '\n')
					state = 1;
				break;
			case 1: // "\n"
				if (ch == '.') {
					// first dot is always inhibited, no matter what
					state = 2;
					inhibit = 1;
				} else {
					state = ch == '\n' ? 1 : 0;
				}
				break;
			case 2: // "\n."
				if (ch == '\n') {
					state = -1;
					inhibit = 1;
				} else {
					state = 0;
				}
				break;
		}
		if (!inhibit)
			ok &= mail_putc(fd, ch);
	}
	return ok;
}

// state machine //

#define STATE_DEFAULT 0
// MAIL command issued
#define STATE_MAIL 1
// has at least one valid recipient
#define STATE_RCPT 2

// the rest //

static regex_t rcpt_regex;
static regmatch_t rcpt_regex_match;

static char filename_buffer_a[64];
static char filename_buffer_b[64];
// filename_buffer_a + additional space for system() command
static char filename_buffer_c[128];

int main(int argc, char ** argv) {
	int has_filter = getenv(BRAND_ENV "_FILTER_CMD") != NULL;
	const char * sizestr = getenv(BRAND_ENV "_MAX_SIZE");
	int max_size = -1;
	if (sizestr)
		max_size = atoi(sizestr);

	const char * umbh = getenv(BRAND_ENV "_SIGNON");
	if (umbh)
		printf("220 %s ESMTP " BRAND "\r\n", umbh);

	const char * rcpt = getenv(BRAND_ENV "_RCPT");
	if (!rcpt) {
		printf("421 configuration error, missing " BRAND_ENV "_RCPT\r\n");
		return 1;
	}
	if (regcomp(&rcpt_regex, rcpt, REG_EXTENDED | REG_ICASE)) {
		printf("421 configuration error, " BRAND_ENV "_RCPT not valid\r\n");
		return 1;
	}

	int state = STATE_DEFAULT;

	// main command parsing loop
	while (1) {
		fflush(stdout);
		cmd_read();
		char * param;
		if (cmd_normal("ehlo", &param)) {
			state = STATE_DEFAULT;
			printf("250-OK\r\n");
			if (getenv(BRAND_ENV "_STARTTLS_CMD"))
				printf("250-STARTTLS\r\n");
			printf("250 8BITMIME\r\n");
		} else if (cmd_normal("helo", &param)) {
			state = STATE_DEFAULT;
			printf("250 OK\r\n");
		} else if (cmd_normal("rset", &param)) {
			state = STATE_DEFAULT;
			printf("250 OK\r\n");
		} else if (cmd_normal("noop", &param)) {
			printf("250 OK\r\n");
		} else if (cmd_normal("quit", &param)) {
			printf("221 OK\r\n");
			break;
		} else if (cmd_normal("vrfy", &param)) {
			printf("502 NO\r\n");
		} else if (cmd_normal("starttls", &param)) {
			const char * htls = getenv(BRAND_ENV "_STARTTLS_CMD");
			if (!htls) {
				printf("451 " BRAND_ENV "_STARTTLS_CMD disappeared\r\n");
			} else {
				printf("220 Starting TLS\r\n");
				fflush(stdout);
				execl("/bin/sh", "sh", "-c", htls, NULL);
				// nothing we can do now
				return 1;
			}
		} else if (cmd_normal("mail", &param)) {
			if (state != STATE_DEFAULT) {
				printf("503 transaction already in progress\r\n");
			} else {
				state = STATE_MAIL;
				printf("250 OK\r\n");
			}
		} else if (cmd_prefix("rcpt to:", &param)) {
			// notably, rcpt to: is parsed slightly differently due to the abnormal separator
			// just so we *don't* look like an open relay
			if (state != STATE_MAIL && state != STATE_RCPT) {
				printf("503 no active mail\r\n");
			} else if (regexec(&rcpt_regex, param, 1, &rcpt_regex_match, 0)) {
				printf("550 who?\r\n");
			} else {
				state = STATE_RCPT;
				printf("250 OK\r\n");
			}
		} else if (cmd_normal("data", &param)) {
			if (state == STATE_MAIL) {
				printf("554 no valid recipients\r\n");
			} else if (state != STATE_RCPT) {
				printf("503 invalid state\r\n");
			} else {
				// alright, here's the awkward part
				sprintf(filename_buffer_a, "tmp/%li-%i-XXXXXX", (long) time(NULL), (int) getpid());
				int fd = mkstemp(filename_buffer_a);
				// rely on directory security
				// better for debug that way
				fchmod(fd, 0666);
				if (fd < 0) {
					printf("451 tmp/XXXXXX file create fail\r\n");
				} else {
					printf("354 transmit at will, terminate with <CRLF>.<CRLF>\r\n");
					fflush(stdout);
					const char * mail_err = NULL;
					if (do_mail_copy(fd, max_size)) {
						// success!
						close(fd);
					} else {
						close(fd);
						unlink(filename_buffer_a);
						mail_err = "552 Failed to write - message discarded\r\n";
					}
					if (!mail_err) {
						// filter
						if (has_filter) {
							sprintf(filename_buffer_c, "$" BRAND_ENV "_FILTER_CMD %s", filename_buffer_a);
							if (system(filename_buffer_c)) {
								unlink(filename_buffer_a);
								mail_err = "550 Filter reject\r\n";
							}
						}
					}
					if (!mail_err) {
						// transfer file from tmp -> new
						strcpy(filename_buffer_b, filename_buffer_a);
						filename_buffer_b[0] = 'n';
						filename_buffer_b[1] = 'e';
						filename_buffer_b[2] = 'w';
						if (rename(filename_buffer_a, filename_buffer_b)) {
							unlink(filename_buffer_a);
							mail_err = "552 Failed to transfer - message discarded\r\n";
						}
					}
					if (!mail_err) {
						fputs("250 OK\r\n", stdout);
					} else {
						fputs(mail_err, stdout);
					}
					state = STATE_DEFAULT;
				}
			}
		} else {
			printf("500 unknown command\r\n");
		}
	}
	return 0;
}

