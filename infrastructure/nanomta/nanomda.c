#define BRAND_ENV "NANOMDA"
#define BRAND "nanoMDA"

#define ERROR_PREFIX "-ERR "

#include "common.h"

#define UIDL_LEN 70
typedef struct mail {
	// path and UIDL
	char path[UIDL_LEN + 1];
	// file descriptor
	int fd;
	// size (PRId64)
	int64_t size;
	// deletion flag
	int delete;
	// next mail
	struct mail * next;
} mail_t;

// Gets mail by a 1-based index.
mail_t * mail_get(mail_t * mail1, int index) {
	index--;
	mail_t * cursor = mail1;
	while (cursor && index) {
		index--;
		cursor = cursor->next;
	}
	if (index)
		return NULL;
	return cursor;
}
// Same, but handles the error response.
mail_t * mail_get_or_err(mail_t * mail1, int index) {
	mail_t * res = mail_get(mail1, index);
	if (!res)
		printf("-ERR not found\r\n");
	return res;
}

void transaction() {
	// maildir mandatory cleanup process
	DIR * dir = opendir("new");
	if (!dir) {
		printf("-ERR failed to list new mail\r\n");
		return;
	}
	while (1) {
		static char build_filename_src[PATH_MAX];
		static char build_filename_dst[PATH_MAX];
		struct dirent * dirent = readdir(dir);
		if (!dirent)
			break;
		if (!strcmp(".", dirent->d_name) || !strcmp("..", dirent->d_name))
			continue;
		// critical to check
		// this might be a byte too sharp, but that's fine'
		if (strlen(dirent->d_name) >= PATH_MAX - 5)
			continue;
		strcpy(build_filename_src, "new/");
		strcpy(build_filename_dst, "cur/");
		strcpy(build_filename_src + 4, dirent->d_name);
		strcpy(build_filename_dst + 4, dirent->d_name);
		rename(build_filename_src, build_filename_dst);
	}
	closedir(dir);
	// seal our fate
	if (chdir("cur")) {
		printf("-ERR misconfigured: unable to chdir to 'cur' mail directory\r\n");
		return;
	}
	// we now need to enumerate the maildir contents
	dir = opendir(".");
	if (!dir) {
		printf("-ERR failed to list mail\r\n");
		return;
	}
	mail_t * mail1 = NULL;
	int counter = 0;
	int64_t total = 0;
	while (1) {
		struct dirent * dirent = readdir(dir);
		if (!dirent)
			break;
		//printf("~DBG %s\r\n", dirent->d_name);
		if (!strcmp(".", dirent->d_name) || !strcmp("..", dirent->d_name))
			continue;
		// important to check
		if (strlen(dirent->d_name) > UIDL_LEN)
			continue;
		int mail_fd = open(dirent->d_name, O_RDONLY);
		if (mail_fd < 0)
			continue;
		off_t size = lseek(mail_fd, 0, SEEK_END);
		mail_t res;
		strcpy(res.path, dirent->d_name);
		res.fd = mail_fd;
		res.size = (int64_t) size;
		total += size;
		res.delete = 0;
		res.next = mail1;
		// Add to list
		void * newmail = memdup(&res, sizeof(res));
		if (newmail) {
			mail1 = newmail;
			counter++;
		}
	}
	closedir(dir);
	printf("+OK now in TRANSACTION state\r\n");
	while (1) {
		fflush(stdout);
		cmd_read();
		char * param;
		if (cmd_normal("uidl", &param)) {
			if (!*param) {
				printf("+OK\r\n");
				mail_t * cursor = mail1;
				int index = 1;
				while (cursor) {
					printf("%i %s\r\n", index++, cursor->path);
					cursor = cursor->next;
				}
				printf(".\r\n");
			} else {
				int index = atoi(param);
				mail_t * cursor = mail_get_or_err(mail1, index);
				if (cursor)
					printf("+OK %i %s\r\n", index, cursor->path);
			}
		} else if (cmd_normal("list", &param)) {
			if (!*param) {
				printf("+OK\r\n");
				mail_t * cursor = mail1;
				int index = 1;
				while (cursor) {
					printf("%i %" PRId64 "\r\n", index++, cursor->size);
					cursor = cursor->next;
				}
				printf(".\r\n");
			} else {
				int index = atoi(param);
				mail_t * cursor = mail_get_or_err(mail1, index);
				if (cursor)
					printf("+OK %i %" PRId64 "\r\n", index, cursor->size);
			}
		} else if (cmd_normal("stat", &param)) {
			printf("+OK %i %" PRId64 "\r\n", counter, total);
		} else if (cmd_normal("retr", &param)) {
			int index = atoi(param);
			mail_t * cursor = mail_get_or_err(mail1, index);
			if (cursor) {
				lseek(cursor->fd, 0, SEEK_SET);
				printf("+OK\r\n");
				fflush(stdout);
				// State machine for inserting the termination octet.
				char tmp = 0;
				static const char cr = '\r';
				int last_crlf = 0;
				while (read(cursor->fd, &tmp, 1) == 1) {
					// inhibit CR since we insert our own (due to Maildir convention)
					if (tmp == '\r')
						continue;
					// this dot gets elided; it acts as escaping, preventing <CRLF>.<CRLF>
					if (last_crlf && tmp == '.')
						write(1, &tmp, 1);
					last_crlf = tmp == '\n';
					if (last_crlf)
						write(1, &cr, 1);
					write(1, &tmp, 1);
				}
				// Done!
				if (!last_crlf)
					printf("\r\n");
				printf(".\r\n");
			}
		} else if (cmd_normal("dele", &param)) {
			int index = atoi(param);
			mail_t * cursor = mail_get_or_err(mail1, index);
			if (cursor) {
				if (cursor->delete) {
					printf("-ERR already marked\r\n");
				} else {
					cursor->delete = 1;
					printf("+OK marked\r\n");
				}
			}
		} else if (cmd_normal("rset", &param)) {
			mail_t * cursor = mail1;
			while (cursor) {
				cursor->delete = 0;
				cursor = cursor->next;
			}
			printf("+OK\r\n");
		} else if (cmd_normal("noop", &param)) {
			// do nothing!
			printf("+OK\r\n");
		} else if (cmd_normal("quit", &param)) {
			break;
		} else {
			printf("-ERR unknown/invalid command\r\n");
		}
	}
	// UPDATE state
	printf("+OK\r\n");
	fflush(stdout);
	while (mail1) {
		if (mail1->delete)
			unlink(mail1->path);
		mail1 = mail1->next;
	}
	exit(0);
}

int main(int argc, char ** argv) {
	char * userenv = dupenv(BRAND_ENV "_USER");
	if (!userenv) {
		printf("-ERR misconfigured: no username (" BRAND_ENV "_USER)\r\n");
		return 1;
	}
	char * passenv = dupenv(BRAND_ENV "_PASS");
	if (!passenv) {
		printf("-ERR misconfigured: no password (" BRAND_ENV "_PASS)\r\n");
		return 1;
	}
	char * saltenv = dupenv(BRAND_ENV "_SALT");
	if (!saltenv) {
		printf("-ERR misconfigured: no salt (" BRAND_ENV "_SALT)\r\n");
		return 1;
	}
	// Using a salted encrypted password reduces the effectiveness of a timing attack.
	char * passcrypt = dupcrypt(passenv, saltenv);
	free(passenv);
	free(saltenv);
	if (!passcrypt) {
		printf("-ERR failed to crypt() password\r\n");
		return 1;
	}
	printf("+OK POP3 " BRAND " ready\r\n");
	// Authorization loop
	int user_is_correct = 0;
	while (1) {
		fflush(stdout);
		cmd_read();
		char * param;
		if (cmd_normal("user", &param)) {
			printf("+OK\r\n");
			user_is_correct = !strcmp(param, userenv);
			// printf("~DBG user '%s' vs. '%s' res. %i\r\n", param, userenv, user_is_correct);
		} else if (cmd_normal("pass", &param)) {
			if (user_is_correct) {
				const char * chk = crypt(param, passcrypt);
				// printf("~DBG %s / %s\r\n", chk, passcrypt);
				if (!strcmp(chk, passcrypt)) {
					transaction();
				} else {
					printf("-ERR\r\n");
				}
			} else {
				printf("-ERR\r\n");
			}
		} else if (cmd_normal("quit", &param)) {
			printf("+OK quitting; didn't authorize\r\n");
			return 0;
		} else {
			printf("-ERR unknown/invalid command\r\n");
		}
	}
}
