# nanoMTA/MDA: The Dumbest Inbox

nanoMTA is hopefully the world's dumbest implementation of an SMTP server mail inbox. Or at least dumbest yet useful.

I will just take a moment to note: This is an **inbox.** It does not, as a rule, send mail. It seems, on the surface, like mail should be one unified server, but when you look into the antispam and open-relay-prevention requirements, the two basically just share protocol details and theoretical accounts.

nanoMDA, meanwhile, is an equally dumb implementation of a POP3 server (completing the inbox).

It uses filenames as UIDLs and deletes files straight from Maildir `new`. It's intended to be paired with nanoMTA for any case where Maildir either isn't an option or is inconvenient.

Paired together, these two provide an extremely low-overhead single-user mail inbox with nearly no administration overhead.

## Features

* Safe by default: I mean, kinda. **It will never make an outbound connection.** Budgeting disk space and IP banning anyone who abuses things is on you. But you won't have to worry about becoming an open relay due to not understanding how to configure some ancient mail server.
* TLS 'support': It supports enough of STARTTLS that you can plug stuff together and get a working setup.
  * Basically there's an environment variable, and you can use that to control what STARTTLS does. If not supplied, support is not advertised.
* Pretty much no default filtering: The out-of-band metadata must specify at least one valid recipient. From/To headers are ignored. That's pretty much it.
  * It is possible to provide your own filtering command.
* 'Kind of' 'maybe' supports Maildir. At least... enough of it that Evolution works if you create the `new`/`tmp`/`cur` directories yourself. nanoMTA properly creates files in `tmp` and moves them to `new`, and both nanoMDA and Evolution moves them to `cur`. That's good enough to me.
* Uses almost no resources when not in use, and tries to use few when in use! Works nicely with a systemd socket unit, inetd service, or ipsvd.
* Literally infinite accounts! ... because it has no idea what an account is. **Everything goes into one global mailbox.** Might need to work on improving the RCPT regex stuff.

## Why.

The mail servers I looked at either have a massive ton of configuration, or are still too big to quickly audit and too unmaintained for me to be strictly sure what's up with them (yes I mean qmail.) **This is not good for small self-hosted setups!**

In addition, I wanted a mail server I could actually run on my laptop without it draining meaningful resources while idle. I could always shut it down if it became a problem. That meant it had to be the size of a systemd socket unit. Literally.

It had to be old-fashioned enough to be run inetd-style, but following the new fad of privileging a particular use-case over literally anything else to create simpler and thus more secure (at least against the big attacks) code.

Since nanoMTA was made for single-user installations, it doesn't matter that it doesn't have accounts, and it's not like there's any tangible benefit from trying to tell if the mail server's lying about the RCPT TO field versus the From field.

## nanoMTA: Environment Variables / Configuration

### `NANOMTA_SIGNON` - 'Optional' (but not really)

'Sign-on message'/hostname.

If not present the initial message will not be sent by the server.

**This behaviour exists so that STARTTLS can be implemented by reinvoking nanoMTA, see below.**

Example setup:

```
export NANOMTA_SIGNON="example.com"
```

### `NANOMTA_STARTTLS_CMD` - Optional

If present, STARTTLS support is advertised.

When STARTTLS is requested it will `exec /bin/sh` with this command. 

The command is expected to reinvoke nanoMTA with a TLS wrapper, and with `NANOMTA_SIGNON` and `NANOMTA_STARTTLS_CMD` removed.

**Advice: A quick way to make this happen is to use stunnel in inetd mode. Read stunnel's manual for details. This also preserves, say, `NANOMTA_RECEIVED`. The catch is that it means nanoMTA has to have access to your certs.**

Example setup:
```
# not a real config envvar, just a stashing location
export NANOMTA_HERE="`pwd`"
# assumes you later CD
export NANOMTA_STARTTLS_CMD="`pwd`/starttls"
```

In that script:
```
#!/bin/sh
cd "$NANOMTA_HERE"
exec stunnel stunnel.conf
```

And then in `stunnel.conf`:

```
exec = ./srv2
cert = fullchain.pem
key = privkey.pem
```

And finally in `srv2`:

```
#!/bin/sh
unset NANOMTA_SIGNON
unset NANOMTA_STARTTLS_CMD
cd yourmaildir
exec "$NANOMTA_HERE/nanomta"
```

### `NANOMTA_RCPT` - Mandatory

Regex for the forward-path portion of a `RCPT TO:` line to match. Case-insensitive.

Without this regex, nanoMTA does not start. If the regex does not match, an error is given when the line is supplied. At least one valid `RCPT TO:` must be given for mail to be accepted.

Example setup:
```
export NANOMTA_RCPT="@example.com>$|postmaster"
```

This allows messages with recipients ending in `@example.com>` or containing `postmaster`.

### `NANOMTA_RECEIVED` - Optional

This is expected to be set by a script wrapping nanoMTA. If provided, it adds a Received header.

It's done this way so that Received headers survive through stunnel/etc.

Example setup:
```
export NANOMTA_RECEIVED="FROM $TCPREMOTEADDR BY `hostname` ; `date`"
```

This results in, say: `Received: FROM 209.85.167.50:57614 BY Trevize ; Sat 25 Nov 11:34:28 GMT 2023` - not completely compliant but close enough

### `NANOMTA_MAX_SIZE` - Optional

Limits the maximum size of a single mail.

Any other form of filtering, you'll need to either filter ocnnections or use `NANOMTA_FILTER_CMD` as appropriate.

Example setup:

```
export NANOMTA_MAX_SIZE=65536
```

### `NANOMTA_FILTER_CMD` - Optional

This can be used as a custom filter to reject or modify incoming mail.

The filter is executed as `system("$NANOMTA_FILTER_CMD tmp/somefilename")`; the shell is expected to expand the filter variable into the resulting command.

The command must not use the standard IO streams.

Example setup:

```
# becomes "grep -q spammersdontknowthisphrase tmp/somefilename"
export NANOMTA_FILTER_CMD="grep -q spammersdontknowthisphrase"
```

## nanoMDA: Environment Variables / Configuration

### `NANOMDA_USER` - Mandatory

Username for the POP3 account.

### `NANOMDA_PASS` - Mandatory

Password for the POP3 account.

### `NANOMDA_SALT` - Mandatory

`crypt` salt used internally for POP3.

This is used to provide some security against timing attacks.

## How to retrieve mail

1. If you have sshfs or are hosting your mail server locally, Mozilla Thunderbird can read Maildir directories directly. (tested)
2. nanoMDA provides a POP3 server. This is useful if Maildir is inconvenient for some reason.
3. solid-pop3d appears to be a decently simple way to network-share Maildir without any further side effects, though it seems to be designed assuming the user has a user account on the host. (untested)
