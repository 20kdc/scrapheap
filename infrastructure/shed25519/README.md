# shed25519: it's a signature tool but tiny

shed25519 is intended to be a tiny ed25519 signing/verification tool for use on Linux systems with shell scripts.

The goal here is for it to be "pure" in the functional programming sense.

It can sign data, verify signatures on data, and convert private keys to public keys. _That's it._

This means if you're writing a CGI shell script or something like that, and you need to take two uploaded files and verify signature A with pubkey B (presumably part of your application) and data C, you can do that.

shed25519 uses files containing binary data for everything, because it's easy enough to pass them into base64 or w/e if necessary, and otherwise there's no point. It doesn't have any encapsulation for the data, so it's very easy to write code that supports the resulting signatures assuming you have `libsodium` or equivalent.

**With this in mind, I use shed25519 just to sign data from my local shell scripts (all handling trusted data) -- it is not tested against untrusted data.** Said data can then be received by any application with an Ed25519 implementation.

## Help

```
shed25519 verify DATA SIGNATURE PUBKEY # writes 1 if ok, writes 0 if not ok, status 0 if ok, status 1 if not ok
shed25519 pubkey SEED                  # writes 32-byte public key for 32-byte seed
shed25519 sign DATA SEED               # writes 64-byte signature
```

