#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sodium/crypto_sign_ed25519.h>

typedef struct {
	int valid;
	unsigned char * data;
	size_t size;
} buf_t;

buf_t get_file(const char * name) {
	buf_t workspace = {};
	FILE * f = fopen(name, "rb");
	if (!f) {
		fprintf(stderr, "could not open %s\n", name);
		return workspace;
	}
	fseek(f, 0, SEEK_END);
	workspace.size = (size_t) ftell(f);
	fseek(f, 0, SEEK_SET);
	workspace.data = malloc(workspace.size);
	if (!workspace.data) {
		fprintf(stderr, "could not allocate memory for file %s\n", name);
		fclose(f);
		return workspace;
	}
	for (size_t i = 0; i < workspace.size; i++)
		workspace.data[i] = (unsigned char) getc(f);
	fclose(f);
	workspace.valid = 1;
	return workspace;
}

int get_seed(const char * name, unsigned char * pk, unsigned char * sk) {
	buf_t seed = get_file(name);
	if (!seed.valid)
		return 1;
	if (seed.size != crypto_sign_ed25519_SEEDBYTES) {
		fputs("seed not 32 bytes\n", stderr);
		return 1;
	}
	crypto_sign_ed25519_seed_keypair(pk, sk, seed.data);
	return 0;
}

int main(int argc, char ** argv) {
	if ((argc == 5) && (!strcmp(argv[1], "verify"))) {
		buf_t data = get_file(argv[2]);
		buf_t signature = get_file(argv[3]);
		buf_t pubkey = get_file(argv[4]);
		if (!data.valid)
			return 1;
		if (!signature.valid)
			return 1;
		if (!pubkey.valid)
			return 1;
		if (pubkey.size != crypto_sign_ed25519_PUBLICKEYBYTES) {
			fputs("pubkey not 32 bytes\n", stderr);
			return 1;
		}
		if (signature.size != crypto_sign_ed25519_BYTES) {
			fputs("signature not 64 bytes\n", stderr);
			return 1;
		}
		if (crypto_sign_ed25519_verify_detached(signature.data, data.data, data.size, pubkey.data)) {
			// invalid
			puts("0");
			return 1;
		} else {
			// valid
			puts("1");
			return 0;
		}
	} else if ((argc == 3) && (!strcmp(argv[1], "pubkey"))) {
		unsigned char pk[crypto_sign_ed25519_PUBLICKEYBYTES];
		unsigned char sk[crypto_sign_ed25519_SECRETKEYBYTES];
		if (get_seed(argv[2], pk, sk))
			return 1;
		fwrite(pk, crypto_sign_ed25519_PUBLICKEYBYTES, 1, stdout);
		return 0;
	} else if ((argc == 4) && (!strcmp(argv[1], "sign"))) {
		buf_t data = get_file(argv[2]);
		unsigned char pk[crypto_sign_ed25519_PUBLICKEYBYTES];
		unsigned char sk[crypto_sign_ed25519_SECRETKEYBYTES];
		if (get_seed(argv[3], pk, sk))
			return 1;
		if (!data.valid)
			return 1;
		unsigned char sig[crypto_sign_ed25519_BYTES];
		crypto_sign_ed25519_detached(sig, NULL, data.data, data.size, sk);
		fwrite(sig, crypto_sign_ed25519_BYTES, 1, stdout);
		return 0;
	}
	fputs("shed25519 verify DATA SIGNATURE PUBKEY # writes 1 if ok, writes 0 if not ok, status 0 if ok, status 1 if not ok\n", stderr);
	fputs("shed25519 pubkey SEED                  # writes 32-byte public key for 32-byte seed\n", stderr);
	fputs("shed25519 sign DATA SEED               # writes 64-byte signature\n", stderr);
	return 1;
}

