#!/bin/sh
# Initializes the global era3-sync installation.
set -e
if [ $# != 0 ]; then
	echo "don't provide any args!"
	exit 1
fi
echo "Ensuring directories exist..."
mkdir -p /media/era3-sync
mkdir -p /var/era3/sync/data/blob
mkdir -p /var/era3/sync/data/workspace
if [ ! -d /var/era3/sync/data ]; then
	echo "It is required that the directory or link /var/era3/sync/data is created to point to where sync files should be created."
	echo "This must be setup by the superuser."
	echo "/var/era3/sync should NOT be a symlink, as some contents are used for mounting internal filesystems which would appear in Caja."
	exit 1
fi
if [ ! -e /var/era3/sync/data/mountpoint ]; then
	ln -s /media/era3-sync /var/era3/sync/data/mountpoint
fi
echo "Preparing to initialize DB..."
if [ -e /var/era3/sync/data/metadata.db ]; then
	echo "The DB already exists."
	exit 1
fi
sqlite3 -bail /var/era3/sync/data/metadata.db < /opt/era3/sync/schema.sql
echo "Fixing permissions..."
era3 engine escalate sync-fix-perms
echo "Done! Remember to make your first commit to properly initialize your index and workspace."

# -- FINAL TREE --
# /media/era3-sync : final mount directory
# /var/era3/sync/data : changed to point to big data drive
# /var/era3/sync/data/metadata.db : the database
# /var/era3/sync/data/sandbox.db : the temp database
# /var/era3/sync/data/plan.json : see plan commands
# /var/era3/sync/data/blob : blobs
# /var/era3/sync/data/workspace : 'workspace'; managed by blobfs
# /var/era3/sync/data/mountpoint : used by commit so that we don't need a dedicated CowBarbeque extractor/etc.
