BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "blobs" (
	-- SHA-256 hash of data.
	"hash" BLOB NOT NULL UNIQUE,
	-- Length (in bytes) of data.
	"length" INTEGER NOT NULL,
	PRIMARY KEY("hash")
);
CREATE TABLE IF NOT EXISTS "paths" (
	-- Path. "/" is the root. "/example.txt" might be a file in said root.
	"path" TEXT NOT NULL UNIQUE,
	PRIMARY KEY("path")
);
CREATE TABLE IF NOT EXISTS "hosts" (
	-- Hostname (as from `hostname`).
	"host" TEXT NOT NULL UNIQUE,
	-- Latest update as a Unix time.
	-- This is used to determine when to import host data from an incoming database.
	-- As such, single metadata DB files now replace the server index directory.
	"update_time" INTEGER NOT NULL,
	PRIMARY KEY("host")
);
CREATE TABLE IF NOT EXISTS "index" (
	-- Hostname.
	"host" TEXT NOT NULL,
	-- Path.
	"path" TEXT NOT NULL,
	-- Blob, by hash. If NULL, this is a deletion record.
	"blob" BLOB,
	-- 1: Executable. 2: Symlink (blob gives readlink contents)
	"executable" INTEGER NOT NULL,
	-- Update time as a Unix time.
	"update_time" INTEGER NOT NULL,
	FOREIGN KEY("blob") REFERENCES "blobs"("hash"),
	FOREIGN KEY("host") REFERENCES "hosts"("host"),
	FOREIGN KEY("path") REFERENCES "paths"("path"),
	PRIMARY KEY("host", "path")
);

-- sitelen pi... pilin mute. sona ni li wile ala ante. ona li pini, taso mi awen. mi sona ala e nasin pona.
CREATE TABLE IF NOT EXISTS "stars" (
	-- freeform text to be permanently stored on all nodes, forever.
	"name" TEXT NOT NULL,
	PRIMARY KEY("name")
);

-- (20)KDC
PRAGMA application_id = 0x204b4443;
-- E3SI
PRAGMA user_version = 0x45335349;
COMMIT;
