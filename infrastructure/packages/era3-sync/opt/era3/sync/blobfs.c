#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64

#include <asm-generic/errno-base.h>
#include <unistd.h>
#define FUSE_USE_VERSION 31

#include <fcntl.h>
#include <sys/xattr.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fuse3/fuse.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include "fslowlevel.h"

// Can't rely on fslowlevel to be at all atomic.
// That in mind, accesses which fslowlevel will care about are protected with this.
// It is also important to protect in-memory structures that FUSE has access to (in other words, the FHs)
// Otherwise FUSE might, in theory, start a write request and then a close request before the write has completed.
pthread_mutex_t global_access_lock = PTHREAD_MUTEX_INITIALIZER;

// We need this because FUSE changes the current directory... and anyway, mallocing isn't considered 'cool' these days...!
int workspace_fd;

uid_t owner_uid;
gid_t owner_gid;

static void * blobfs_init(struct fuse_conn_info * conn, struct fuse_config * out) {
	conn->want |= FUSE_CAP_WRITEBACK_CACHE | FUSE_CAP_ATOMIC_O_TRUNC;
	out->kernel_cache = 1;
	return NULL;
}

static const char * fixname(const char * name) {
	if (!strcmp(name, "/"))
		return ".";
	return name + 1;
}

static mode_t fixmode(mode_t mode) {
	// prevent various ways of locking up the FS
	return mode | 0660;
}

static int blobfs_readdir(const char * name, void * data, fuse_fill_dir_t filler, off_t ofs, struct fuse_file_info * info, enum fuse_readdir_flags flags) {
	int dirfd = openat(workspace_fd, fixname(name), O_RDONLY | O_DIRECTORY);
	if (dirfd < 0)
		return -errno;
	DIR * dir = fdopendir(dirfd);
	// proxy error directly to caller
	if (!dir) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in readdir: %s = %i\n", name, en);
#endif
		return -en;
	}
	while (1) {
		struct dirent * res = readdir(dir);
		if (!res)
			break;
		filler(data, res->d_name, NULL, 0, 0);
	}
	closedir(dir);
	return 0;
}

static int blobfs_mkdir(const char * name, mode_t mode) {
	int res = mkdirat(workspace_fd, fixname(name), mode);
	return res ? -errno : 0;
}

static int blobfs_unlink(const char * name) {
	int res = unlinkat(workspace_fd, fixname(name), 0);
	return res ? -errno : 0;
}

static int blobfs_rmdir(const char * name) {
	int res = unlinkat(workspace_fd, fixname(name), AT_REMOVEDIR);
	return res ? -errno : 0;
}

static int blobfs_rename(const char * oldpath, const char * newpath, unsigned int flags) {
	int res = renameat2(workspace_fd, fixname(oldpath), workspace_fd, fixname(newpath), flags & (RENAME_EXCHANGE | RENAME_NOREPLACE));
	return res ? -errno : 0;
}

static int blobfs_chmod(const char * name, mode_t mode, struct fuse_file_info *fi) {
	int res = fchmodat(workspace_fd, fixname(name), fixmode(mode), 0);
	return res ? -errno : 0;
}

static int blobfs_chown(const char * name, uid_t uid, gid_t gid, struct fuse_file_info *fi) {
	// chown here *completely* silently fails, on purpose
	// see, we can't *actually* chown this, it'd let apps cause havoc even if we have the perms
	// we have to be able to read and write this stuff too, and also it breaks sync scripts
	// but, we have to keep up appearances...
	return 0;
}

static int blobfs_getattr(const char * name, struct stat * stat, struct fuse_file_info * fi) {
	int res = fstatat(workspace_fd, fixname(name), stat, AT_SYMLINK_NOFOLLOW);
	if (res) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in getattr: %s = %i\n", name, en);
#endif
		return -en;
	}
	stat->st_uid = owner_uid;
	stat->st_gid = owner_gid;
	if ((stat->st_mode & S_IFMT) == S_IFREG) {
		// cloak the file header
		stat->st_size -= sizeof(ws_fileheader_t);
		if (stat->st_size < 0)
			stat->st_size = 0;
	}
	return 0;
}

static int blobfs_getxattr(const char * name, const char * attr, char * value, size_t size) {
	int xfd = openat(workspace_fd, fixname(name), O_RDONLY | O_NOFOLLOW);
	if (xfd < 0) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in getxattr openat: %s = %i\n", name, en);
#endif
		return -en;
	}
	int res = fgetxattr(xfd, attr, value, size);
	close(xfd);
	if (res < 0) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in getxattr: %s = %i\n", name, en);
#endif
		return -en;
	}
	return res;
}

static int blobfs_listxattr(const char * name, char * value, size_t size) {
	int xfd = openat(workspace_fd, fixname(name), O_RDONLY | O_NOFOLLOW);
	if (xfd < 0) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in listxattr openat: %s = %i\n", name, en);
#endif
		return -en;
	}
	int res = flistxattr(xfd, value, size);
	close(xfd);
	if (res < 0) {
		int en = errno;
#ifndef PRODUCTION
		printf("error in listxattr: %s = %i\n", name, en);
#endif
		return -en;
	}
	return res;
}

static int blobfs_readlink(const char * name, char * buf, size_t bufsiz) {
	if (bufsiz == 0)
		return -EINVAL;
	ssize_t res = readlinkat(workspace_fd, fixname(name), buf, bufsiz - 1);
	if (res < 0)
		return -errno;
	buf[res] = 0;
	return 0;
}

static int blobfs_symlink(const char * contents, const char * name) {
	if (symlinkat(contents, workspace_fd, fixname(name)) < 0)
		return -errno;
	return 0;
}

static int blobfs_open(const char * name, struct fuse_file_info * fi) {
	int wsopenflags = 0;
	if ((fi->flags & O_ACCMODE) != O_RDONLY)
		wsopenflags |= WS_OPEN_WRITABLE;
	if (fi->flags & O_TRUNC)
		wsopenflags |= WS_OPEN_TRUNCATE | WS_OPEN_WRITABLE;
	pthread_mutex_lock(&global_access_lock);
	ws_session_t session;
	int res = ws_file_open(workspace_fd, fixname(name), wsopenflags, 0664, &session);
	pthread_mutex_unlock(&global_access_lock);
	if (res)
		return res;
	fi->fh = session.fh;
	return 0;
}

static int blobfs_create(const char * name, mode_t mode, struct fuse_file_info * fi) {
	pthread_mutex_lock(&global_access_lock);
	ws_session_t session;
	int res = ws_file_open(workspace_fd, fixname(name), WS_OPEN_CREATABLE | WS_OPEN_WRITABLE | WS_OPEN_TRUNCATE, fixmode(mode), &session);
	pthread_mutex_unlock(&global_access_lock);
	if (res)
		return res;
	fi->fh = session.fh;
	return 0;
}

static int blobfs_flush(const char * name, struct fuse_file_info * fi) {
	return 0;
}

static int blobfs_release(const char * name, struct fuse_file_info * fi) {
	pthread_mutex_lock(&global_access_lock);
	ws_file_close(fi->fh);
	pthread_mutex_unlock(&global_access_lock);
	return 0;
}

static int blobfs_fsync(const char * name, int flags, struct fuse_file_info * fi) {
	if (fi) {
		pthread_mutex_lock(&global_access_lock);
		ws_file_fsync(fi->fh);
		pthread_mutex_unlock(&global_access_lock);
	}
	return 0;
}

static int blobfs_utimens(const char * name, const struct timespec tv[2], struct fuse_file_info * fi) {
	int res = utimensat(workspace_fd, fixname(name), tv, AT_SYMLINK_NOFOLLOW);
	return res ? -errno : 0;
}

static int blobfs_read(const char * name, char * buf, size_t size, off_t ofs, struct fuse_file_info * fi) {
	if (ofs < 0)
		return 0;
	pthread_mutex_lock(&global_access_lock);
	ws_session_t session;
	int res = ws_session_open(&session, fi->fh);
	if (res) {
#ifndef PRODUCTION
		printf("session open error in read of %s\n", name);
#endif
		pthread_mutex_unlock(&global_access_lock);
		return res;
	}
	res = (int) ws_session_read(&session, buf, size, ofs);
	pthread_mutex_unlock(&global_access_lock);
	return res;
}

static int blobfs_write(const char * name, const char * buf, size_t size, off_t ofs, struct fuse_file_info * fi) {
	if (ofs < 0)
		return 0;
	pthread_mutex_lock(&global_access_lock);
	ws_session_t session;
	int res = ws_session_open(&session, fi->fh);
	if (res) {
#ifndef PRODUCTION
		printf("session open error in write of %s\n", name);
#endif
		pthread_mutex_unlock(&global_access_lock);
		return res;
	}
	res = (int) ws_session_write(&session, buf, size, ofs);
	pthread_mutex_unlock(&global_access_lock);
	return res;
}

static int blobfs_truncate(const char * name, off_t size, struct fuse_file_info * fi) {
	// apparently fi can be missing, so let's just do stuff 'normally'...
	if (size < 0)
		return -EINVAL;

	pthread_mutex_lock(&global_access_lock);

	ws_session_t session;
	int res = ws_file_open(workspace_fd, fixname(name), WS_OPEN_WRITABLE, 0644, &session);
	if (res) {
		pthread_mutex_unlock(&global_access_lock);
		return res;
	}
	res = ws_session_ftruncate(&session, size);
	ws_file_close(session.fh);
	pthread_mutex_unlock(&global_access_lock);
	return res;
}

static const struct fuse_operations blobfs_oper = {
	.init = blobfs_init,
	.readdir = blobfs_readdir,
	.mkdir = blobfs_mkdir,
	.unlink = blobfs_unlink,
	.rmdir = blobfs_rmdir,
	.rename = blobfs_rename,
	.chmod = blobfs_chmod,
	.chown = blobfs_chown,
	.getattr = blobfs_getattr,
	.getxattr = blobfs_getxattr,
	.listxattr = blobfs_listxattr,
	.readlink = blobfs_readlink,
	.symlink = blobfs_symlink,
	.open = blobfs_open,
	.create = blobfs_create,
	.flush = blobfs_flush,
	.release = blobfs_release,
	.fsync = blobfs_fsync,
	.utimens = blobfs_utimens,
	.read = blobfs_read,
	.write = blobfs_write,
	.truncate = blobfs_truncate
};

int main(int argc, char ** argv) {
	owner_uid = atoi(getenv("BLOBFS_UID"));
	owner_gid = atoi(getenv("BLOBFS_GID"));
	fprintf(stderr, "era3-sync 'blobfs' 2.0 'WHY'\n");
	fprintf(stderr, "WARNING: Untrusted input in the workspace/ directory may lead to chaos. FUSE is being trusted to sanitize paths.\n");
	workspace_fd = open("workspace", O_DIRECTORY);
	if (workspace_fd < 0) {
		fprintf(stderr, "workspace directory missing\n");
		return 1;
	}

	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
	if (fuse_opt_parse(&args, NULL, NULL, NULL) == -1) {
		// :(
		return 1;
	}
	return fuse_main(args.argc, args.argv, &blobfs_oper, NULL);
}
