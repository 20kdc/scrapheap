#define _GNU_SOURCE
#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>

int main(int argc, char ** argv) {
	// strip away 1st arg
	char * newargv[argc];
	for (int i = 1; i < argc; i++)
		newargv[i - 1] = argv[i];
	newargv[argc - 1] = NULL;
	// uhoh
	if (argc <= 1) {
		fprintf(stderr, "must have at least one arg (escalator-bin PROGRAM [ARG...])\n");
		return 1;
	}
	// fprintf(stderr, "getuid %i geteuid %i\n", getuid(), geteuid());
	// get group & user data
	struct group * c_group;
	struct passwd * c_user;
	c_group = getgrnam("era3-personnel");
	if (!c_group) {
		fprintf(stderr, "era3-personnel group does not exist\n");
		return 1;
	}
	gid_t era3_personnel_gid = c_group->gr_gid;
	c_user = getpwnam("era3-sync");
	if (!c_user) {
		fprintf(stderr, "era3-sync user does not exist\n");
		return 1;
	}
	uid_t era3_sync_uid = c_user->pw_uid;
	gid_t era3_sync_gid = c_user->pw_gid;
	if (getuid() != 0 && !group_member(era3_personnel_gid)) {
		fprintf(stderr, "user is not part of era3-personnel group\n");
		return 1;
	}
	// drop privileges in a very carefully ordered fashion
	if (initgroups("era3-sync", era3_sync_gid)) {
		fprintf(stderr, "setgroups somehow failed\n");
		return 1;
	}
	if (setgid(era3_sync_gid)) {
		fprintf(stderr, "setgid somehow failed\n");
		return 1;
	}
	if (setuid(era3_sync_uid)) {
		fprintf(stderr, "setuid somehow failed\n");
		return 1;
	}
	return execvp(newargv[0], newargv);
}
