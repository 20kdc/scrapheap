#pragma once
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/xattr.h>
#include <unistd.h>
#include <errno.h>

#include <assert.h>

#ifdef ALL_ERRORS_FATAL
#define WS_ERROR() assert(0)
#else
#define WS_ERROR() {}
#endif

static_assert(sizeof(off_t) == 8, "off_t must be 64-bit or data corruption is very likely.");

// -- core functions --

// Returns either a FUSE-style negative error, or the resulting amount.
static ssize_t ws_readall(int fd, off_t offset, void * data, size_t amount) {
	ssize_t successful = 0;
	char * datac = (char *) data;
	while (amount > 0) {
		ssize_t v = pread(fd, datac, amount, offset);
		if (v < 0) {
			WS_ERROR();
			return -errno;
		}
		if (v == 0)
			return successful;
		successful += v;
		datac += v;
		offset += v;
		amount -= v;
	}
	return successful;
}
// Returns 0 on success, or returns a FUSE-style negative error.
static int ws_writeall(int fd, off_t offset, const void * data, size_t amount) {
	const char * datac = (const char *) data;
	while (amount > 0) {
		ssize_t v = pwrite(fd, datac, amount, offset);
		if (v < 0) {
			int en = errno;
			WS_ERROR();
#ifndef PRODUCTION
			printf("ws_writeall failure = %i\n", en);
#endif
			return -en;
		}
		datac += v;
		offset += v;
		amount -= v;
	}
	return 0;
}


// -- ERA 3 SYNC FS low level stuff --
// BEWARE: THIS IS NOT ATOMIC! AT ALL!

// Up to 252 source extents can be defined.
// If more than 252 source extents are defined, the smaller extents must be pulled in.
#define WS_EXTENT_COUNT 248

typedef struct {
	// Start offset
	uint64_t start;
	// Length
	uint64_t len;
} ws_extent_t;
#define WS_EXTENT_END(extent) ((extent).start + (extent).len)

static inline ws_extent_t ws_extent_new(uint64_t start, uint64_t len) {
	ws_extent_t ws = {start, len};
	return ws;
}

#define WS_EXTENT_FT(start, end) (ws_extent_new((start), (end) - (start)))

#define WS_UNMODIFIED_MARK "user.e3scow"

// Should ideally be exactly 4096 bytes.
typedef struct {
	// Source filename. Opened as a 'co-file' with the main file.
	// Notably, the source file never meaningfully changes for a given main file identity.
	// Files can be renamed over each other and such, but for a given main file FD, the source file is never changed.
	char sourcefile[127];
	char zero;
	// Source extents
	ws_extent_t extents[WS_EXTENT_COUNT];
} ws_fileheader_t;

// Verifies file header integrity.
// Returns 0 on success, or returns -EIO.
static int ws_fileheader_check_integrity(ws_fileheader_t * header) {
	{
		// ok, we did that, now what about source
		if (header->zero) {
#ifndef PRODUCTION
			printf("integrity check error: unterminated sourcefile\n");
#endif
			goto error;
		}
		for (int i = 0; i < WS_EXTENT_COUNT; i++) {
			ws_extent_t ie = header->extents[i];
			if (!ie.len)
				continue;
			for (int j = i + 1; j < WS_EXTENT_COUNT; j++) {
				ws_extent_t je = header->extents[j];
				if (!je.len)
					continue;
				if (ie.start >= WS_EXTENT_END(je))
					continue;
				if (WS_EXTENT_END(ie) <= je.start)
					continue;
#ifndef PRODUCTION
				printf("integrity check error: extent collision between %i and %i\n", i, j);
#endif
				goto error;
			}
		}
	}
	return 0;
	// on error...
	error:
	WS_ERROR();
	return -EIO;
}

#define WS_FH_WRITABLE (0x8000000000000000ULL)

// Sessions split out the data from the FH integer.
// FHs are eternally valid, but sessions are only valid until another thread can modify the data.
typedef struct {
	// Original fh from which this session was created.
	uint64_t fh;
	// If this is supposed to be writable.
	int writable;
	// FD of 'real file'.
	int fd_real;
	// FD of 'source file'.
	int fd_source;
	// Cached/working file header.
	ws_fileheader_t header;
} ws_session_t;

#define WS_OPEN_WRITABLE 1
#define WS_OPEN_CREATABLE 2
#define WS_OPEN_EXCLUSIVE 4
#define WS_OPEN_TRUNCATE 8
// Opens a new session with a new FH.
// Returns 0 on success, or returns a FUSE-style negative error.
static int ws_file_open(int pathfd, const char * path, int flags, mode_t mode, ws_session_t * session) {
	// verify we are opening a regular file and not a pipe or something
	// this is NOT a foolproof guard and is not intended as one, but it might prevent accidents
	struct stat stat;
	if (!lstat(path, &stat))
		if ((stat.st_mode & S_IFMT) != S_IFREG)
			return -ENXIO;
	// set oflags according to caller open flags
	int oflags = O_NOFOLLOW;
	if (flags & WS_OPEN_WRITABLE) {
		session->writable = 1;
		oflags |= O_RDWR;
	} else {
		session->writable = 0;
		oflags |= O_RDONLY;
	}
	if (flags & WS_OPEN_CREATABLE)
		oflags |= O_CREAT;
	if (flags & WS_OPEN_EXCLUSIVE)
		oflags |= O_EXCL;
	if (flags & WS_OPEN_TRUNCATE)
		oflags |= O_TRUNC;
	// this is the only case where errno is preserved, as it is the actual open
	// other cases are E3S fs corruption or such and are thus -EIO
	session->fd_real = openat(pathfd, path, oflags, mode);
	if (session->fd_real < 0) {
#ifndef PRODUCTION
		printf("Error opening %s (flags %x) : %i in primary opener\n", path, flags, errno);
		system("pwd");
#endif
		WS_ERROR();
		return -errno;
	}
	// past here, must fail_with_real
	ssize_t err = ws_readall(session->fd_real, 0, &session->header, sizeof(session->header));
	if ((err == 0) && ((flags & WS_OPEN_CREATABLE) || (flags & WS_OPEN_TRUNCATE))) {
		// assume this is a new file, initialize it
		memset(&session->header, 0, sizeof(session->header));
		strcpy(session->header.sourcefile, "/dev/null");
		fremovexattr(session->fd_real, WS_UNMODIFIED_MARK);
		// and write (we're at the start of the file).
		err = ws_writeall(session->fd_real, 0, &session->header, sizeof(session->header));
		if (err) {
#ifndef PRODUCTION
			printf("Error opening %s : failure to write header!\n", path);
#endif
			return err;
		}
	} else if (err != sizeof(session->header)) {
#ifndef PRODUCTION
		printf("Error opening %s : did not read entire header!\n", path);
#endif
		WS_ERROR();
		close(session->fd_real);
		return -EIO;
	}
	err = ws_fileheader_check_integrity(&session->header);
	if (err) {
#ifndef PRODUCTION
		printf("Error opening %s : header failed integrity check\n", path);
#endif
		close(session->fd_real);
		return (int) err;
	}
	session->fd_source = openat(pathfd, session->header.sourcefile, O_RDONLY);
	if (session->fd_source < 0) {
#ifndef PRODUCTION
		printf("Error opening %s : %i in secondary opener\n", path, errno);
#endif
		WS_ERROR();
		close(session->fd_real);
		return -EIO;
	}
	// past here, must fail_with_real_and_source ; but I don't think we'll need to do that?
	uint64_t fh = flags & WS_OPEN_WRITABLE ? WS_FH_WRITABLE : 0;
	fh |= ((uint64_t) session->fd_real);
	fh |= ((uint64_t) session->fd_source) << 32;
	session->fh = fh;
	return 0;
}

// Closes the file handles.
static void ws_file_close(uint64_t fh) {
	close((int) (fh & 0xFFFFFFFFULL));
	close((int) ((fh >> 32) & 0x7FFFFFFFULL));
}

// fsyncs the 'real' file handle.
static void ws_file_fsync(uint64_t fh) {
	fsync((int) (fh & 0xFFFFFFFFULL));
}

// Returns 0 on success, or returns a FUSE-style negative error.
static int ws_session_open(ws_session_t * session, uint64_t fh) {
	session->writable = (fh & WS_FH_WRITABLE) != 0;
	session->fd_real = (int) (fh & 0xFFFFFFFFULL);
	session->fd_source = (int) ((fh >> 32) & 0x7FFFFFFFULL);
	session->fh = fh;
	ssize_t header = ws_readall(session->fd_real, 0, &session->header, sizeof(session->header));
	if (header != sizeof(session->header)) {
#ifndef PRODUCTION
		printf("session open error due to header oddity\n");
#endif
		WS_ERROR();
		return -EIO;
	}
	return 0;
}

// 'Closes off' writable sessions after writing operations.
// Returns 0 on success, or returns a FUSE-style negative error.
static int ws_session_whdr(const ws_session_t * session) {
	fremovexattr(session->fd_real, WS_UNMODIFIED_MARK);
	return ws_writeall(session->fd_real, 0, &session->header, sizeof(session->header));
}

// -- THE ACTUAL OPERATIONS START HERE --

#define WS_TRANSFER_AMOUNT 4096

// Migrates an extent to reality.
// Returns 0 on success, or returns a FUSE-style negative error.
// On success, the target extent is cleared, no other extents affected.
static int ws_session_migrate_by_id(ws_session_t * session, int id) {
	ws_extent_t ext = session->header.extents[id];
	if (lseek(session->fd_source, ext.start, SEEK_SET) < 0) {
		WS_ERROR();
		return -errno;
	}
	off_t dest_ptr = ext.start + sizeof(ws_fileheader_t);
	char transfer_buffer[WS_TRANSFER_AMOUNT];
	while (ext.len > 0) {
		size_t chunk_size = WS_TRANSFER_AMOUNT;
		if (ext.len < chunk_size)
			chunk_size = ext.len;
		int rl = read(session->fd_source, transfer_buffer, chunk_size);
		if (rl < 0) {
			// something went wrong :(
			WS_ERROR();
			return -errno;
		} else if (!rl) {
			// random premature EOF
			WS_ERROR();
			return -EIO;
		}
		int err = ws_writeall(session->fd_real, dest_ptr, transfer_buffer, chunk_size);
		if (err)
			return err;
		dest_ptr += rl;
		ext.start += rl;
		ext.len -= rl;
	}
	// clear the extent
	session->header.extents[id] = ws_extent_new(0, 0);
	return 0;
}

// Ensures a free extent ID exists, or returns a FUSE-style negative error.
// May migrate an arbitrary extent!
static int ws_session_ensure_free_extent(ws_session_t * session) {
	size_t smallest_len = ~0ULL;
	int smallest_extent = 0;
	for (int i = 0; i < WS_EXTENT_COUNT; i++) {
		size_t len = session->header.extents[i].len;
		if (!len) {
			// found without having to free up extents via migration
			return i;
		} else if (len < smallest_len) {
			smallest_len = len;
			smallest_extent = i;
		}
	}
	// if we got here, we MUST migrate an extent
	int res = ws_session_migrate_by_id(session, smallest_extent);
	if (res)
		return res;
	return smallest_extent;
}

// Migrates the given region to the real file.
// Returns 0 on success, or returns a FUSE-style negative error.
// Assuming your function succeeds, be sure to commit the resulting header changes.
// If preserve_contents is 0 and your function fails, you *shouldn't* commit the resulting header changes (as you're now in an indeterminate state).
static int ws_session_migrate_by_area(ws_session_t * session, ws_extent_t area, int preserve_contents) {
	if (area.len == 0)
		return 0;
	uint64_t area_end = WS_EXTENT_END(area);
	while (1) {
		int free_extent = ws_session_ensure_free_extent(session);
		if (free_extent < 0)
			return free_extent;
		// attempt to find *something* we need to get rid of
		int did_anything = 0;
		for (int i = 0; i < WS_EXTENT_COUNT; i++) {
			ws_extent_t extent = session->header.extents[i];
			if (extent.len == 0)
				continue;
			uint64_t extent_end = WS_EXTENT_END(extent);
			if (area.start <= extent.start && area_end >= extent_end) {
				// area [ extent {} ]
				// the extent must be completely migrated
				if (preserve_contents) {
					int res = ws_session_migrate_by_id(session, i);
					if (res)
						return res;
				} else {
					// if we don't need to preserve contents, we can simply delete the extent
					session->header.extents[i] = ws_extent_new(0, 0);
				}
				did_anything = 1;
				break;
			} else if (area_end >= extent_end && area.start > extent.start && area.start < extent_end) {
				// extent { area [ } ]
				// Split into one extent that terminates with the area's start,
				//  and one which covers the intersection.
				session->header.extents[i] = WS_EXTENT_FT(extent.start, area.start);
				session->header.extents[free_extent] = WS_EXTENT_FT(area.start, extent_end);
				did_anything = 1;
				break;
			} else if (area.start <= extent.start && area_end > extent.start && area_end <= extent_end) {
				// area [ extent { ] }
				// Similar splitting case.
				session->header.extents[i] = WS_EXTENT_FT(extent.start, area_end);
				session->header.extents[free_extent] = WS_EXTENT_FT(area_end, extent_end);
				did_anything = 1;
				break;
			} else if (extent.start < area.start && extent_end > area_end) {
				// extent { area [] }
				// ok, so, this is a problem because we can only ensure one extent is free at a time!
				// therefore, we must turn this into one of the other cases with a single split
				session->header.extents[i] = WS_EXTENT_FT(extent.start, area_end);
				session->header.extents[free_extent] = WS_EXTENT_FT(area_end, extent_end);
				did_anything = 1;
				break;
			}
		}
		if (!did_anything)
			break;
	}
	return 0;
}

// Implements read on a session.
// Returns the amount of bytes read on success, or returns a FUSE-style negative error.
static ssize_t ws_session_read(ws_session_t * session, char * buf, size_t size, size_t ofs) {
	ssize_t banked = 0;
	while (size) {
		// find source extent for 1st byte, if any
		int source_extent = -1;
		uint64_t source_extent_end = 0;
		uint64_t limit = ~0ULL;
		for (int i = 0; i < WS_EXTENT_COUNT; i++) {
			ws_extent_t extent = session->header.extents[i];
			if (!extent.len)
				continue;
			uint64_t extent_end = WS_EXTENT_END(extent);
			if (ofs >= extent.start && ofs < extent_end) {
				source_extent = i;
				source_extent_end = extent_end;
				limit = extent_end;
				break;
			}
			if (extent.start > ofs && extent.start < limit)
				limit = extent.start;
		}
		// figure out max. that can be read in this pass
		size_t max_read_size = limit - ofs;
		if (size < max_read_size)
			max_read_size = size;
		// figure out what we want to read
		int from_fd = (source_extent == -1) ? session->fd_real : session->fd_source;
		uint64_t from_pos = (source_extent == -1) ? sizeof(ws_fileheader_t) : 0;
		from_pos += ofs;
		// read it
		ssize_t res = ws_readall(from_fd, from_pos, buf, max_read_size);
		if (res < 0)
			return res;
		banked += res;
		ofs += res;
		buf += res;
		size -= res;
		if (res != max_read_size) {
			// mid-read EOF; this means we hit a file limit
			return banked;
		}
	}
	return banked;
}

// Implements write on a session.
// Returns the amount of bytes written on success, or returns a FUSE-style negative error.
// On success, automatically commits the resulting header changes.
static ssize_t ws_session_write(ws_session_t * session, const char * buf, size_t size, size_t ofs) {
	if (!session->writable)
		return -EBADF;
	// migrate no-preserve
	int res = ws_session_migrate_by_area(session, ws_extent_new(ofs, size), 0);
	if (res) {
#ifndef PRODUCTION
		printf("write failure due to migrator failure = %i\n", -res);
#endif
		return res;
	}
	res = ws_writeall(session->fd_real, ofs + sizeof(ws_fileheader_t), buf, size);
	if (res)
		return res;
	res = ws_session_whdr(session);
	if (res) {
#ifndef PRODUCTION
		printf("write failure due to whdr failure = %i\n", -res);
#endif
		return res;
	}
	return size;
}

// Implements ftruncate on a session.
// Returns 0 on success, or returns a FUSE-style negative error.
// On success, automatically commits the resulting header changes.
static int ws_session_ftruncate(ws_session_t * session, size_t size) {
	if (!session->writable) {
		WS_ERROR();
		return -EBADF;
	}
	// use a no-preserve migration for the areas we are discarding (all after the new size)
	// this can fail if we have to *actually* migrate some stuff due to extent clutter
	int res = ws_session_migrate_by_area(session, ws_extent_new(size, (~0ULL) - size), 0);
	if (res)
		return res;
	// and now we can actually truncate
	if (ftruncate(session->fd_real, size + sizeof(ws_fileheader_t))) {
		WS_ERROR();
		return -errno;
	}
	// update header to be sure
	return ws_session_whdr(session);
}
