# era3-sync common utilities

import os
import hashlib
import sqlite3
import uuid

# File flags

FLAG_EXECUTABLE = 1

# direct blob functions

def blob_local(blob_hash):
	try:
		assert type(blob_hash) == bytes
		os.stat("blob/" + blob_hash.hex())
		return True
	except:
		return False

def blob_size(blob_hash):
	try:
		assert type(blob_hash) == bytes
		return os.stat("blob/" + blob_hash.hex()).st_size
	except:
		return None

# prepare connection

def db_prep_ro(db, sandbox):
	if not sandbox:
		db.create_function("e3_blob_size", 1, blob_size)
		db.create_function("e3_blob_local", 1, blob_local)
	db.executescript("""
	PRAGMA foreign_keys = ON;
	PRAGMA trusted_schema = OFF;
	""")
	if sandbox:
		try:
			db.setlimit(sqlite3.SQLITE_LIMIT_ATTACHED, 0)
		except:
			# oh well, was worth a try...
			pass

def db_prep_rw(db, sandbox):
	db_prep_ro(db, sandbox)
	# this **SHOULD** catch the autocommit = False breakage
	db.execute("BEGIN;")

def db_connect_rw():
	# ok, so, Python developers are breaking things for everybody else for no reason again
	# in short, autocommit doesn't exist in 3.11, but as of 3.12 will become MANDATORY for consistent behaviour in Some Future Release.
	# a short reminder: NEVER DO THIS IN YOUR API, ESPECIALLY IF IT MIGHT CAUSE DATA LOSS.
	db = sqlite3.connect("file:metadata.db")
	db_prep_rw(db, False)
	return db

def db_connect_ro():
	db = sqlite3.connect("file:metadata.db?mode=ro")
	db_prep_ro(db, False)
	return db

# data manipulation

def db_ensure_path(db, path):
	db.execute("INSERT OR IGNORE INTO paths (path) VALUES (?)", [path])

# file into blob (assumes pwd = data)
def db_file2blob(db, path, progress = None):
	return db_fobj2blob(db, open(path, "rb"), progress)

# amount of memory that can be used reading at a time
BLOB_CHUNK = 0x1000000

# file object into blob (assumes pwd = data)
def db_fobj2blob(db, fobj, progress = None):
	try:
		hashobj = hashlib.sha256()
		filename_tmp = "blob/.incoming-" + str(uuid.uuid1())
		byte_count = 0
		try:
			fobj_tmp = open(filename_tmp, "xb")
			while True:
				if not (progress is None):
					progress(byte_count)
				chunk = fobj.read(BLOB_CHUNK)
				if chunk == b"":
					break
				fobj_tmp.write(chunk)
				hashobj.update(chunk)
				byte_count += len(chunk)
			fobj_tmp.close()
			# alright, writing done, what was the hash
			digest = hashobj.digest()
			# commit to blobstore
			os.rename(filename_tmp, "blob/" + digest.hex())
			# add to db
			db.execute("INSERT OR REPLACE INTO blobs (hash, length) VALUES (?, ?)", [digest, byte_count])
			return digest
		finally:
			# unlink temporary file if not already moved/unlinked
			try:
				os.unlink(filename_tmp)
			except:
				pass
	except Exception as exc:
		raise RuntimeError("during blob input") from exc

def db_index_rm(db, host, path):
	db.execute("DELETE FROM \"index\" WHERE host = ? AND path = ?", [host, path])

# Returns a list of all known hosts.
def db_all_known_hosts(db):
	hosts = []
	for host in db.execute("SELECT host FROM hosts"):
		hosts.append(host[0])
	return hosts

# -- planner --

# Automatic planner / desync detector.
# Returns:
#  path_to_host (dict: paths to recommended hosts), contested (dict: paths to 'bad' host list)
def planner(db, hosts):
	# dict: host to dict: path to modified time
	hosts_with_paths = {}
	path_to_host = {}
	involved_times = {}

	# index which hosts have entries
	# and find the best entry times
	for the_host in hosts:
		host_data = {}
		hosts_with_paths[the_host] = host_data
		for v in db.execute("SELECT path, update_time FROM \"index\" WHERE host = ?", [the_host]):
			path = v[0]
			host_data[path] = v[1]
			superior = True
			if path in involved_times:
				superior = v[1] > involved_times[path]
			if superior:
				path_to_host[path] = the_host
				involved_times[path] = v[1]

	contested = {}

	# put all hosts which don't have the "correct entry" into contested[path]
	for path in involved_times:
		true_time = involved_times[path]
		path_contested = []
		for the_host in hosts:
			host_data = hosts_with_paths[the_host]
			if not (path in host_data):
				path_contested.append(the_host)
			elif host_data[path] != true_time:
				path_contested.append(the_host)
		if len(path_contested) > 0:
			contested[path] = path_contested

	return path_to_host, contested
