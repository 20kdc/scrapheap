# Packages

Quick index:

* `busybox-static`: Upgraded `busybox-static` with more applets.
* `dumb-fan-booster`: Shell-script-based mechanism to run actions when the fan temperature gets too hot.
* `era3-rust`: This is a "model workspace" designed for development of the engine's Rust crate.
	* This uses Cargo to get the tooling configured the right way. The actual environment is based on system-packaged `rustc`.
* `era3-engine`: Underlying engine that runs on all Era3 systems. Functionality:
	* Responsible for recompiling the engine's Rust crate and managing the Rust script cache at `/opt/era3/rust/cache`.
	* Provides the hostname lookup beacon on port 20530.
	* Provides the era3 Python 3 package.
	* Provides the era3 command, including escalator/less-privileged shutdown.
	* Provides the management panel.
* `era3-essentials`: Right now, just utility commands.
* `era3-system-config`: Global system config alterations.
* `era3-installer-untrusted`: Untrusted installer. Only installs packages above.
* `era3-installer`: Trusted installer, can install everything (depending on machine config).
* `era3-private`: Private!
* `era3-sync`: Sync system.
* `era3-sync-test`: Not a package. Test scripts/resources.
* `era3-essentials-desktop`: Metapackage for Era3 infra for desktop systems (_presently Twilight, Trevize, and Kaede_)

## Management panel API

* `GET /api/report`: Returns hostname, `/proc/version_signature`, and era3 version, separated by newlines.
* `GET /api/primary`: Returns `1` or `0` to report the primary device status.
* `POST /api/primary?1` / `POST /api/primary?0`: Enables/disables the primary device flag remotely.

### Report

"kit net list" / synctech.py uses this to read out node information.

### Primary Device Flag

* Indicated by presence of file `/etc/era3-primary-device`.
* Represents the current device on which development occurs, etc.
* "kit sync connect": Uses this for filesystem feed.
* "kit sync mobile": mobile suite uses this to determine which device to sync with.

IMPORTANT: The mobile system can't run on the IPv6 network, so it MUST use the broadcast system.

There might be some way to fix this, but it'd have to be something like targetted RA, and the address space would need to be rearranged for it...

Can't use indiscriminate RA or the network will go haywire.

It ain't happenin'.
