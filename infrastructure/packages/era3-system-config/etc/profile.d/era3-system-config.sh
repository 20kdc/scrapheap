# This covers things that need to be universal to work properly
export DOTNET_ROOT=/usr/share/dotnet
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export VCPKG_DISABLE_METRICS=1
export FREETYPE_PROPERTIES="truetype:interpreter-version=35 autofitter:warping=1 cff:no-stem-darkening=0 autofitter:no-stem-darkening=0"
export XDG_CURRENT_DESKTOP=sway
