// https://bugzilla.mozilla.org/show_bug.cgi?id=1862182
// https://phabricator.services.mozilla.com/D192451
pref("gfx.font_rendering.fallback.unassigned_chars", true);
// cargo cult configuration
pref("gfx.font_rendering.fallback.always_use_cmaps", true);
