#!/usr/bin/python3
# Adjusts single-file HTML for use with KOReader

import os
import sys
import html
import html.parser
import base64
import mimetypes
import urllib.parse
import urllib.request

# -- PROXY --

class ProxyHTMLParser(html.parser.HTMLParser):
	def __init__(self, target):
		super().__init__()
		self.target_parser = target

	def handle_starttag(self, tag, attrs):
		self.target_parser.handle_starttag(tag, attrs)
	def handle_endtag(self, tag):
		self.target_parser.handle_endtag(tag)
	def handle_startendtag(self, tag, attrs):
		self.target_parser.handle_startendtag(tag, attrs)
	def handle_data(self, data):
		self.target_parser.handle_data(data)
	def handle_entityref(self, name):
		self.target_parser.handle_entityref(name)
	def handle_charref(self, name):
		self.target_parser.handle_charref(name)
	def handle_comment(self, data):
		self.target_parser.handle_comment(data)
	def handle_decl(self, data):
		self.target_parser.handle_decl(data)
	def handle_pi(self, data):
		self.target_parser.handle_pi(data)
	def unknown_decl(self, data):
		self.target_parser.unknown_decl(data)

# -- PRINTER --

class PrintHTMLParser(html.parser.HTMLParser):
	def handle_starttag(self, tag, attrs):
		sys.stdout.write("<" + tag)
		self.write_padded_attrs(attrs)
		sys.stdout.write(">")
	def handle_endtag(self, tag):
		sys.stdout.write("</" + tag + ">")
	def handle_startendtag(self, tag, attrs):
		sys.stdout.write("<" + tag)
		self.write_padded_attrs(attrs)
		sys.stdout.write("/>")

	def write_padded_attrs(self, attrs):
		for v in attrs:
			if v[0] is None:
				raise Exception("Attribute with None key")
			if v[1] is None:
				sys.stdout.write(" " + v[0])
			else:
				sys.stdout.write(" " + v[0] + "=\"" + html.escape(v[1]) + "\"")

	def handle_data(self, data):
		sys.stdout.write(html.escape(data, False))
	def handle_entityref(self, name):
		sys.stdout.write("&" + name + ";")
	def handle_charref(self, name):
		sys.stdout.write("&#" + name + ";")
	def handle_comment(self, data):
		sys.stdout.write("<!--" + data + "-->")
	def handle_decl(self, data):
		sys.stdout.write("<!" + data + ">")
	def handle_pi(self, data):
		sys.stdout.write("<?" + data + ">")
	def unknown_decl(self, data):
		sys.stdout.write("<![" + data + "]>")

# -- EMBED PASS --

class EmbedImagesHTMLParser(ProxyHTMLParser):
	def __init__(self, target, image_base):
		super().__init__(target)
		self.image_base = image_base

	def handle_starttag(self, tag, attrs):
		if tag == "img":
			attrs = self.modify_img_attrs(attrs)
		super().handle_starttag(tag, attrs)
	def handle_startendtag(self, tag, attrs):
		if tag == "img":
			attrs = self.modify_img_attrs(attrs)
		super().handle_startendtag(tag, attrs)

	def modify_img_attrs(self, attrs):
		res = []
		for v in attrs:
			key = v[0]
			value = v[1]
			if key == "src":
				if value.startswith("data:"):
					continue
				value_ue = urllib.parse.unquote(value)
				mimetype = mimetypes.guess_type(value_ue)[0]
				# determine path
				if value.startswith("http://") or value.startswith("https://"):
					sys.stderr.write("Downloading: " + value + "\n")
					value_file_content = urllib.request.urlopen(value).read()
				else:
					# attempt to load file
					value_file = open(self.image_base + "/" + value_ue, "rb")
					value_file_content = value_file.read()
					value_file.close()
				# embed
				if mimetype == None:
					if value_ue.endswith(".webp"):
						mimetype = "image/webp"
					else:
						raise Exception("cannot determine mime type for " + value_ue)
				value = "data:" + mimetype + ";base64," + base64.standard_b64encode(value_file_content).decode("ascii")
				# value = "data:" + mimetype + "," + urllib.parse.quote_from_bytes(value_file_content)
			res.append((key, value))
		return res

# -- URL REWRITE PASS --

class URLRewriteHTMLParser(ProxyHTMLParser):
	def __init__(self, target, src, tgt):
		super().__init__(target)
		self.url_src = src
		self.url_tgt = tgt

	def handle_starttag(self, tag, attrs):
		if tag == "a":
			attrs = self.modify_a_attrs(attrs)
		super().handle_starttag(tag, attrs)
	def handle_startendtag(self, tag, attrs):
		if tag == "a":
			attrs = self.modify_a_attrs(attrs)
		super().handle_startendtag(tag, attrs)

	def modify_a_attrs(self, attrs):
		res = []
		for v in attrs:
			key = v[0]
			value = v[1]
			if key == "href":
				if value.startswith(self.url_src):
					value = self.url_tgt + value[len(self.url_src):]
			res.append((key, value))
		return res

# -- actual program --

if len(sys.argv) < 3:
	raise Exception("alpacka INPUT MODE(none/embed/urlrw/fimfiction) ... > OUTPUT")

file = open(sys.argv[1], "r")
all_text = file.read()
file.close()
# heuristic because this keeps happening
base = os.path.dirname(sys.argv[1])
if base == "/":
	base = "."

if sys.argv[2] == "none":
	parser = PrintHTMLParser()
	parser.feed(all_text)
	parser.close()
elif sys.argv[2] == "embed":
	print("<!-- ALPACKA : Embedded images -->")
	parser = EmbedImagesHTMLParser(PrintHTMLParser(), base)
	parser.feed(all_text)
	parser.close()
elif sys.argv[2] == "urlrw":
	print("<!-- ALPACKA : URL rewrite -->")
	parser = URLRewriteHTMLParser(PrintHTMLParser(), sys.argv[3], sys.argv[4])
	parser.feed(all_text)
	parser.close()
elif sys.argv[2] == "fimfiction":
	print("<!-- ALPACKA : URL rewrite (fimfiction) & embedded images -->")
	parser = PrintHTMLParser()
	parser = EmbedImagesHTMLParser(parser, base)
	parser = URLRewriteHTMLParser(parser, "file:///", "https://fimfiction.net/")
	parser.feed(all_text)
	parser.close()
else:
	raise Exception("unknown mode: none / embed / urlrw A B / fimfiction")

