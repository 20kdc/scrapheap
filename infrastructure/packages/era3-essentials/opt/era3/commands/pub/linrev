#!/bin/sh
# Linear Revision System

set -e

# --- Repo file format ---
# Repositories contain a directory called .linrev
# This in turn contains:
# * `.linrev/tmp` (working directory)
# * `.linrev/sweep` (`linrev clean` data)
# * `.linrev/unlocked` (if present, the repo is unlocked)
# * `.linrev/revs/*` (all actual data is here)

# "mangled names" are sha256sum'd filenames of repo contents

# .linrev/revs contains numbered files `{REVISION}.zip`.
# each of these contains:
# * `base.{REVISION}`: parent revision or empty
#   this file is in case of future changes
# * `message.{REVISION}`: revision's log entry
# * `hash.{REVISION}.{MANGLEDNAME}`: hash for a particular file at a particular revision
# * `index.{REVISION}`: names of all files in the revision, one per line
# * `data.{REVISION}.tar.zst`: zstd patch from following TAR to older TAR.
#   importantly, decoding is first attempted as a non-patch
#   basically, the latest revision is stored as a non-patch, and older revisions are patches

# --- Globals ---
if [ "$LINREV" = "" ]; then
	# operations on other repos require self-invoke
	# otherwise, the code would get very complicated
	if which linrev > /dev/null; then
		LINREV="linrev"
	else
		LINREV="$0"
	fi
fi
# $LINREV_USE_BUSYBOX
# $current_revision : calculated by repo_current_revision
# $target_revision : input to and output of repo_target_revision_fixup
# $repo_locked : pwd where repo was locked

# --- Tempfiles ---
# * `.linrev/tmp/current`: current_revision is also stored here
# All files from `.linrev/revs` ZIPs can be stored in `.linrev/tmp` when resolved or during construction
# * `.linrev/tmp/data.{REVISION}.tar`: Revision data
# * `.linrev/tmp/foa_tree`: Figure Out Actions : all files in tree
# * `.linrev/tmp/foa_add`: Figure Out Actions : added
# * `.linrev/tmp/foa_update`: Figure Out Actions : updated
# * `.linrev/tmp/foa_delete`: Figure Out Actions : removed
# * `.linrev/tmp/foa_shadow/`: Figure Out Actions : shadow tree directory (for file scanning)

# --- Common Operations ---

# Use on anything for which busybox has an applet for testing.
# Unless it's a shell built-in specifically, anyway (echo, test)
if [ "$LINREV_USE_BUSYBOX" = 1 ]; then
	_s() {
		busybox "$@"
	}
else
	_s() {
		"$@"
	}
fi

# gzip -c with some extra opts
# for "linrev archive"
gzip_c() {
	gzip --best -n -c "$@"
}

zstd_compress() {
	zstd -q "$@"
}

zstd_decompress() {
	zstd --memory=2047MiB -d "$@"
}

# sha256sum pipe
sum_pipe() {
	_s sha256sum | _s head -c 64
	echo
}

# sha256sum file
sum_file() {
	_s sha256sum "$1" | _s head -c 64
	echo
}

# echo -n but portable (SC3037)
echon() {
	printf "%s" "$1"
}

# sha256sum parameter and output to stdout
sum_text() {
	echon "$1" | sum_pipe
}

# ensure sha256sum is installed
sum_text "a" > /dev/null

# gets size of file
filesize() {
	_s wc -c < "$1"
}

# if $1 is in setfile $2
in_setfile() {
	_s grep -q -x -F "$1" "$2"
}

arg_refuse() {
	if [ $# != 0 ]; then
		echo "too many args" > /dev/stderr
		exit 1
	fi
}

arg_expect() {
	if [ $# = 0 ]; then
		echo "needs arg" > /dev/stderr
		exit 1
	fi
}

# --- High-level Repository Operations (repo) ---

# find repository
repo_find() {
	while [ "$(_s pwd)" != "/" ]; do
		if [ -e .linrev ]; then
			break
		fi
		cd ..
	done
	if [ ! -e .linrev ]; then
		echo "unable to find repository" > /dev/stderr
		exit 1
	fi
}

# lock the repository
repo_lock() {
	until _s rm .linrev/unlocked 2>/dev/null; do
		_s sleep 1
		echo "linrev: retrying lock at $(pwd) (try linrev unlock?)" > /dev/stderr
	done
	_s rm -rf .linrev/tmp
	_s mkdir .linrev/tmp
	repo_locked="$(_s pwd)"
	trap "repo_unlock" EXIT
}

repo_unlock() {
	cd "$repo_locked"
	_s rm -rf .linrev/tmp
	_s touch .linrev/unlocked
}

# find current revision
repo_current_revision() {
	# get maximum revision number
	_s ls ".linrev/revs" | _s grep -E -x "0\.zip|[1-9][0-9]*\.zip" | _s grep -E -o "[0-9]+" | (
		current_revision="-1"
		while read -r file; do
			if [ "$current_revision" -lt "$file" ]; then
				current_revision="$file"
			fi
		done
		echo "$current_revision" > .linrev/tmp/current
	)
	current_revision="$(_s cat .linrev/tmp/current)"
}

repo_figure_out_actions_build_tree() {
	_s rm -rf .linrev/tmp/foa_shadow
	_s mkdir -p .linrev/tmp/foa_shadow
	_s find -L . -type f > .linrev/tmp/foa_shadow_tmp
	{
		while read -r file; do
			_s mkdir -p ".linrev/tmp/foa_shadow/$(dirname "$file")"
			_s touch ".linrev/tmp/foa_shadow/$file"
		done
	} < .linrev/tmp/foa_shadow_tmp
	cd .linrev/tmp/foa_shadow
	_s tar c -vh -X ../../../.linrevignore . -f /dev/null > ../foa_tree_unsorted
	cd ../../..
	{
		# filter to ensure only files get through
		while read -r file; do
			{ test -f "$file" && echo "$file"; } || true
		done
	} < .linrev/tmp/foa_tree_unsorted | _s sort > .linrev/tmp/foa_tree
}

# common between status & commit
repo_figure_out_actions() {
	repo_current_revision
	rev_unzip "$current_revision"
	_s rm -f .linrev/tmp/foa_add; _s touch .linrev/tmp/foa_add
	_s rm -f .linrev/tmp/foa_delete; _s touch .linrev/tmp/foa_delete
	_s rm -f .linrev/tmp/foa_update; _s touch .linrev/tmp/foa_update
	repo_figure_out_actions_build_tree
	_s cat .linrev/tmp/foa_tree ".linrev/tmp/index.$current_revision" | _s sort | _s uniq | {
		# Run for all files under consideration
		while read -r file; do
			if in_setfile "$file" .linrev/tmp/foa_tree; then
				if in_setfile "$file" ".linrev/tmp/index.$current_revision"; then
					# duplicate, possible update: check
					name_mangled="$(sum_text "$file")"
					if [ "$(sum_file "$file")" != "$(_s cat ".linrev/tmp/hash.$current_revision.$name_mangled")" ]; then
						# update file
						echo "$file" >> .linrev/tmp/foa_update
					fi
				else
					# new file
					echo "$file" >> .linrev/tmp/foa_add
				fi
			else
				# removed file
				echo "$file" >> .linrev/tmp/foa_delete
			fi
		done
	}
}

# resolve target revision refs
repo_target_revision_fixup() {
	target_revision_subtract="0"
	while { echo "$target_revision" | _s grep -qE "\^$"; }; do
		target_revision="$(echon "$target_revision" | _s head -c -1)"
		target_revision_subtract="$((target_revision_subtract + 1))"
	done
	if [ "$target_revision" = "HEAD" ]; then
		repo_current_revision
		target_revision="$current_revision"
	else
		target_revision="$((target_revision + 0))"
	fi
	while [ "$target_revision_subtract" -gt 0 ]; do
		target_revision="$(rev_unzip_base "$target_revision")"
		target_revision_subtract="$((target_revision_subtract - 1))"
	done
}

# --- Low-level rev operations (rev) ---

# Retrieves revision $1's ZIP contents if they have not already been retrieved
# WILL NOT OVERWRITE EXISTING FILES
rev_unzip() {
	cd .linrev/tmp
	# no overwrite to prevent waste while still allowing selective extraction
	_s unzip -n -qq "../revs/$1.zip"
	cd ../..
}

# Retrieves the base of revision $1
rev_unzip_base() {
	cd .linrev/tmp
	_s unzip -n -qq "../revs/$1.zip" "base.$1"
	cd ../..
	_s cat ".linrev/tmp/base.$1"
}

# Retrieves the message of revision $1
rev_unzip_message() {
	cd .linrev/tmp
	_s unzip -n -qq "../revs/$1.zip" "message.$1"
	cd ../..
	_s cat ".linrev/tmp/message.$1"
}

# Resolves revision $1 if it has not already been resolved
rev_resolve() {
	if [ ! -e ".linrev/tmp/data.$1.tar" ]; then
		rev_unzip "$1"
		rev_resolve_base="$(_s cat ".linrev/tmp/base.$1")"
		if [ "$rev_resolve_base" = "" ]; then
			# root
			zstd_decompress -q --rm ".linrev/tmp/data.$1.tar.zst"
		elif zstd_decompress -qq --rm ".linrev/tmp/data.$1.tar.zst"; then
			# checkpoint
			true
		else
			# resolve previous revision
			rev_resolve "$rev_resolve_base"
			# rev_resolve_base will get overwritten
			rev_resolve_base="$(_s cat ".linrev/tmp/base.$1")"
			zstd_decompress -q --rm --patch-from ".linrev/tmp/data.$rev_resolve_base.tar" ".linrev/tmp/data.$1.tar.zst"
		fi
	fi
}

# ZIPs all internal files in a revision.
# This doesn't handle "compiling" the revision (i.e. zstd stuff, hashing etc.)
rev_zip() {
	cd .linrev/tmp
	_s rm -f "revision.$1.zip"
	zip -qq "revision.$1.zip" "base.$1" "message.$1" "index.$1" "data.$1.tar.zst"
	{
		while read -r file; do
			echo "hash.$1.$(sum_text "$file")"
		done
	} < "index.$1" | zip -qq -@ "revision.$1.zip"
	_s mv "revision.$1.zip" "../revs/$1.zip"
	cd ../..
}

# Automatically compress revision $1 data file
rev_auto_compress() {
	auto_compress_prev_revision="$(($1 - 1))"
	if [ -e ".linrev/revs/$auto_compress_prev_revision.zip" ]; then
		# previous revision exists, so this revision should be made relative to it
		rev_resolve "$auto_compress_prev_revision"
		zstd_compress --patch-from ".linrev/tmp/data.$auto_compress_prev_revision.tar" ".linrev/tmp/data.$1.tar" -o ".linrev/tmp/data.$1.tar.zst"
	else
		zstd_compress ".linrev/tmp/data.$1.tar" -o ".linrev/tmp/data.$1.tar.zst"
	fi
}

# While building a revision:
# Writes commit message $2 for revision $1 to stdout
rev_commit_message() {
	echo "r$1: $(whoami)@$(hostname) / $(date) : $2"
}

# Rebuilds revision $1's tar, index, and hashes given the index on standard input
rev_build_tar() {
	cat - > ".linrev/tmp/index.$1"
	_s tar c -T ".linrev/tmp/index.$1" -f ".linrev/tmp/data.$1.tar"
	{
		while read -r file; do
			name_mangled="$(sum_text "$file")"
			# done this way to ensure we output what we got atomically
			# needs to succeed/fail
			_s tar x -Of ".linrev/tmp/data.$1.tar" "$file" > .linrev/tmp/hash_me || {
				echo "linrev: file $file was not extractable. this filename may not be supported" > /dev/stderr
				exit 1
			}
			sum_file .linrev/tmp/hash_me > ".linrev/tmp/hash.$1.$name_mangled"
		done
	} < ".linrev/tmp/index.$1"
	if [ "$(filesize ".linrev/tmp/data.$1.tar")" -gt 2146435072 ]; then
		echo "linrev: archive too big, would break zstd" > /dev/stderr
		exit 1
	fi
}

# --- High-level rev operations (repo_rev) ---

# Makes revision $1 a checkpoint
repo_rev_checkpoint() {
	rev_resolve "$1"
	cd .linrev/tmp
	zstd_compress "data.$1.tar" -o "data.$1.tar.zst"
	cd ../..
	rev_zip "$1"
}

# Recompresses revision $1 (presumably to deltaify it)
repo_rev_recompress() {
	rev_resolve "$1"
	rev_auto_compress "$1"
	rev_zip "$1"
}

# --- Display Utilties ---

dsp_prefix_list() {
	while read -r line; do
		echo "$1$line"
	done
}

# --- Commands ---

do_init() {
	arg_refuse "$@"

	# .linrev/archive etc. cannot have existed if this exists
	_s mkdir .linrev
	_s mkdir .linrev/tmp
	_s mkdir .linrev/revs
	if [ ! -e .linrevignore ]; then
		echo ".linrev" > .linrevignore
	fi
	# message
	{
		rev_commit_message 0 "(linrev init)"
		echo " + ./.linrevignore"
	} > ".linrev/tmp/message.0"
	# base
	echo > .linrev/tmp/base.0
	# index, hash, and data (decompressed)
	echo "./.linrevignore" | rev_build_tar 0 || exit 1
	rev_auto_compress 0
	rev_zip 0
	repo_unlock
}

do_status() {
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_figure_out_actions
	dsp_prefix_list "+ " < .linrev/tmp/foa_add
	dsp_prefix_list "~ " < .linrev/tmp/foa_update
	dsp_prefix_list "- " < .linrev/tmp/foa_delete
}

do_commit() {
	arg_expect "$@"
	commit_message="$1"
	shift
	arg_refuse "$@"

	repo_find
	if [ -e ./linrev-pre-commit-hook ]; then
		./linrev-pre-commit-hook "$commit_message"
	fi
	repo_lock
	repo_figure_out_actions
	new_revision="$((current_revision + 1))"
	echo "$current_revision" > ".linrev/tmp/base.$new_revision"
	{
		rev_commit_message "$new_revision" "$commit_message"
		dsp_prefix_list " + " < .linrev/tmp/foa_add
		dsp_prefix_list " ~ " < .linrev/tmp/foa_update
		dsp_prefix_list " - " < .linrev/tmp/foa_delete
	} > ".linrev/tmp/message.$new_revision"
	# add relevant files
	rev_build_tar "$new_revision" < .linrev/tmp/foa_tree || exit 1
	rev_auto_compress "$new_revision"
	rev_zip "$new_revision"
	_s cat ".linrev/tmp/message.$new_revision"
	# explicit unlock for post-commit-hook
	repo_unlock
	if [ -e ./linrev-pre-commit-hook ]; then
		./linrev-post-commit-hook "$commit_message" "$new_revision"
	fi
}

do_log() {
	target_revision="HEAD"
	if [ "$#" -gt 0 ]; then
		target_revision="$1"
		shift
	fi
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	cd .linrev/tmp
	log_revision="$target_revision"
	while [ "$log_revision" != -1 ]; do
		if [ ! -e "../revs/$log_revision.zip" ]; then
			echo "r$log_revision: missing / missing : missing"
		else
			_s unzip -n -qq "../revs/$log_revision.zip" "message.$log_revision"
			_s cat "message.$log_revision"
			echo
		fi
		log_revision="$((log_revision - 1))"
	done
	cd ../..
}

do_restore() {
	target_revision="HEAD"
	if [ "$#" -gt 0 ]; then
		target_revision="$1"
		shift
	fi
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	rev_resolve "$target_revision"
	_s tar x -vf ".linrev/tmp/data.$target_revision.tar"
	echo "linrev: r$target_revision"
}

do_archive() {
	arg_expect "$@"
	tgz_file="$1"
	shift
	target_revision="HEAD"
	if [ "$#" -gt 0 ]; then
		target_revision="$1"
		shift
	fi
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	rev_resolve "$target_revision"
	if [ "$tgz_file" = "-" ]; then
		gzip_c ".linrev/tmp/data.$target_revision.tar"
		echo "linrev: r$target_revision -> (stdout)" > /dev/stderr
	else
		gzip_c ".linrev/tmp/data.$target_revision.tar" > "$tgz_file"
		echo "linrev: r$target_revision -> $tgz_file" > /dev/stderr
	fi
}

do_backout() {
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_current_revision
	# make sure you can't backout root rev
	target_revision="$(rev_unzip_base "$current_revision")"
	if [ "$target_revision" = "" ]; then
		echo "no backouting root revision" > /dev/stderr
		repo_unlock
		exit 1
	fi
	_s rm ".linrev/revs/$current_revision.zip"
	rev_unzip_message "$target_revision" | _s head -n 1
}

do_checkpoint() {
	arg_expect "$@"
	target_revision="$1"
	shift
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	repo_rev_checkpoint "$target_revision"
}

do_recompress() {
	arg_expect "$@"
	target_revision="$1"
	shift
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	repo_rev_recompress "$target_revision"
}

clean_mv_files() {
	while read -r file; do
		_s mkdir -p ".linrev/sweep/$(_s dirname "$file")"
		_s mv -v "$file" "./.linrev/sweep/$file"
	done
}

do_clean() {
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_figure_out_actions
	clean_mv_files < .linrev/tmp/foa_add
}

do_unlock() {
	arg_refuse "$@"

	repo_find
	repo_unlock
}

do_getref() {
	target_revision="HEAD"
	if [ "$#" -gt 0 ]; then
		target_revision="$1"
		shift
	fi
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	echo "$target_revision"
}

do_hashref() {
	target_revision="HEAD"
	if [ "$#" -gt 0 ]; then
		target_revision="$1"
		shift
	fi
	arg_refuse "$@"

	repo_find
	repo_lock
	repo_target_revision_fixup
	rev_resolve "$target_revision"
	sum_file ".linrev/tmp/data.$target_revision.tar"
}

do_diverged() {
	arg_expect "$@"
	other_repo="$1"
	shift
	arg_refuse "$@"

	# do this first because of the pwd semantics
	repo_find

	rev_diverged_ptr="$(cd "$other_repo" ; linrev get-ref)"
	if [ "$rev_diverged_ptr" = "" ]; then
		echo "linrev: other repo HEAD missing (invalid?)" > /dev/stderr
		exit 1
	fi

	repo_lock
	# begin the loop
	while [ "$rev_diverged_ptr" != "" ]; do
		rev_diverged_parent_ptr="$(cd "$other_repo" ; linrev get-ref "$rev_diverged_ptr^")"
		# ok, so, firstly, do we have even a lookalike
		if [ ! -e ".linrev/revs/$rev_diverged_ptr.zip" ]; then
			# we don't! continue backing out
			rev_diverged_ptr="$rev_diverged_parent_ptr"
			continue
		fi
		other_repo_hash="$(cd "$other_repo" ; linrev hash-ref "$rev_diverged_ptr")"
		rev_resolve "$rev_diverged_ptr"
		our_repo_hash="$(sum_file ".linrev/tmp/data.$rev_diverged_ptr.tar")"
		# go back out
		if [ "$other_repo_hash" = "$our_repo_hash" ]; then
			break
		fi
		# continue backing out
		rev_diverged_ptr="$rev_diverged_parent_ptr"
	done
	echo "$rev_diverged_ptr"
	[ "$rev_diverged_ptr" != "" ] || exit 1
}

do_tree() {
	arg_refuse "$@"
	repo_find
	repo_lock
	repo_figure_out_actions_build_tree
	cat .linrev/tmp/foa_tree
}

# --- Dispatch ---

do_help() {
	arg_refuse "$@"
	echo "linrev - Linear Revision System"
	echo " an experiment in absurdity"
	echo ""
	echo "WARNING: zstd cannot handle delta archives above 2GB."
	echo "DO NOT LET A REPOSITORY GET ABOVE THIS SIZE!"
	echo ""
	echo "linrev is a single-user revision system built as a shell script."
	echo "It does not support, say, merging branches."
	echo "Backing up a linrev repository simply requires backing up '.linrev/revs'."
	echo "In normal use, linrev is append-only, so these files do not change."
	echo "It is possible to discard history at arbitrary points:"
	echo " checkpoint the first revision you wish to keep, then delete previous archives."
	echo ""
	echo "Ignore is handled using tar's -X 'ignore globs in file' on '.linrevignore'."
	echo "Wherever a REVISION is specified, 'HEAD' may instead be specified."
	echo "Suffixing with ^ will cause a lookup of the previous revision."
	echo ""
	echo "Commands:"
	echo "linrev init - Initialize new repository"
	echo "linrev status - Status of working tree"
	echo "linrev commit MESSAGE - Commits a change."
	echo " WARNING: Runs ./linrev-pre-commit-hook \"MESSAGE\" if it exists."
	echo "          This could be a security issue if you're doing something you shouldn't"
	echo "          with a linrev repository."
	echo "          If that program errors, does not perform the commit."
	echo "          Runs ./linrev-post-commit-hook \"MESSAGE\" \"REVISION\" if that exists."
	echo "linrev log [REVISION] - Shows the commit log"
	echo "linrev restore [REVISION] - Restore from backup / reset modified files"
	echo "linrev archive TGZ [REVISION] - make .tar.gz"
	echo "linrev backout - Deletes the current revision, but doesn't affect working tree."
	echo "linrev checkpoint REVISION - Turns REVISION into a checkpoint."
	echo " Checkpoints can be decompressed without the preceding revisions."
	echo "linrev recompress REVISION - The opposite of linrev checkpoint."
	echo "linrev clean - Moves files that would be added to .linrev/sweep"
	echo "linrev unlock - Unlocks repo after an error"
	echo "linrev get-ref [REVISION] - resolves the revision, writes TAR SHA-256 to standard output"
	echo "linrev hash-ref [REVISION] - resolves the revision, writes TAR SHA-256 to standard output"
	echo "linrev diverged OTHER_REPO - find common divergence point (for diff3, etc.)"
	echo "linrev tree - lists all non-ignored files in repo tree (including uncommitted)"
	echo "linrev help - You're reading it"
}

if [ $# = 0 ]; then
	# hmm
	do_help "$@" | pager
	exit
fi

linrev_cmd="$1"
shift

if [ "init" = "$linrev_cmd" ]; then
	do_init "$@"
	exit
fi

if [ "status" = "$linrev_cmd" ]; then
	do_status "$@"
	exit
fi

if [ "commit" = "$linrev_cmd" ]; then
	do_commit "$@"
	exit
fi

if [ "log" = "$linrev_cmd" ]; then
	do_log "$@" | pager
	exit
fi

if [ "restore" = "$linrev_cmd" ]; then
	do_restore "$@"
	exit
fi

if [ "archive" = "$linrev_cmd" ]; then
	do_archive "$@"
	exit
fi

if [ "backout" = "$linrev_cmd" ]; then
	do_backout "$@"
	exit
fi

if [ "checkpoint" = "$linrev_cmd" ]; then
	do_checkpoint "$@"
	exit
fi

if [ "recompress" = "$linrev_cmd" ]; then
	do_recompress "$@"
	exit
fi

if [ "clean" = "$linrev_cmd" ]; then
	do_clean "$@"
	exit
fi

if [ "unlock" = "$linrev_cmd" ]; then
	do_unlock "$@"
	exit
fi

if [ "get-ref" = "$linrev_cmd" ]; then
	do_getref "$@"
	exit
fi

if [ "hash-ref" = "$linrev_cmd" ]; then
	do_hashref "$@"
	exit
fi

if [ "diverged" = "$linrev_cmd" ]; then
	do_diverged "$@"
	exit
fi

if [ "tree" = "$linrev_cmd" ]; then
	do_tree "$@"
	exit
fi

if [ "help" = "$linrev_cmd" ]; then
	do_help "$@" | pager
	exit
fi

echo "Unknown sub-command: $linrev_cmd" > /dev/stderr
echo "Try 'linrev help'?" > /dev/stderr
exit 1
