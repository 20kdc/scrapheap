# era3 internal permissions manual

users added:

* era3-sync (w/group)

groups added:

* era3-personnel

## Engine/Escalator

era3-personnel is a group which, on era3 systems, is meant to be kind of 'wheel for era3 services'.

It's what grants access to sudoless shutdown, the sync drive, and whatever uses I come up with for it in future.

## Sync

era3-sync represents access to core sync data.

Mess this data up and the whole system could break, so it's locked.

era3-personnel users have access to the sync drive.

But they don't have access to the core sync data directly.

They can gain access to the core sync data using the era3-sync escalator (not to be confused with the era3-engine escalator).

The idea is that era3-sync does this automatically when necessary.

Above all, this reduces the amount of sudoing/etc. when using sync.
