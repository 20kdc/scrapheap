#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64

#include <fcntl.h>
#include "../../era3-sync/opt/era3/sync/fslowlevel.h"

#define TESTFILE_LEN 6144
char data_bse[TESTFILE_LEN];
char data_ref[TESTFILE_LEN];
char data_ws[TESTFILE_LEN];

static void bse_new(int * ref, ws_session_t * session) {
	for (int i = 0; i < TESTFILE_LEN; i++)
		data_bse[i] = (char) rand();
	int bse = open("/media/ramdisk/exerciser.bse.tmp", O_RDWR | O_CREAT | O_TRUNC, 0644);
	assert(bse >= 0);
	assert(!ws_writeall(bse, 0, data_bse, TESTFILE_LEN));
	*ref = open("/media/ramdisk/exerciser.ref.tmp", O_RDWR | O_CREAT | O_TRUNC, 0644);
	assert(*ref >= 0);
	assert(!ws_writeall(*ref, 0, data_bse, TESTFILE_LEN));

	// bse gets moved into the cow.tmp session
	assert(!ws_file_open(AT_FDCWD, "/media/ramdisk/exerciser.cow.tmp", WS_OPEN_WRITABLE | WS_OPEN_CREATABLE | WS_OPEN_TRUNCATE, 0644, session));
	close(session->fd_source);
	session->fd_source = bse;
	strcpy(session->header.sourcefile, "/media/ramdisk/exerciser.bse.tmp");
	session->header.extents[0] = WS_EXTENT_FT(0, TESTFILE_LEN);
	assert(!ftruncate(session->fd_real, TESTFILE_LEN + sizeof(ws_fileheader_t)));
	assert(!ws_session_whdr(session));
}

int main() {
	int ref;
	int max_extents = 0;
	ws_session_t session;
	bse_new(&ref, &session);

	// -- ready! we should now be consistent --
	printf(" -- iterating... --\n");
	for (int iteration = 0; iteration < 0x100000; iteration++) {
		if (!(iteration & 0xFFF))
			printf("IT %x (ME %i)\n", iteration, max_extents);
		// check extents
		int extents = 0;
		for (int i = 0; i < WS_EXTENT_COUNT; i++)
			if (session.header.extents[i].len != 0)
				extents++;
		if (extents > max_extents)
			max_extents = extents;
		// verify
		off_t ws_length = lseek(session.fd_real, 0, SEEK_END);
		ws_length -= sizeof(ws_fileheader_t);
		off_t ref_length = lseek(ref, 0, SEEK_END);
		if (ws_length != ref_length) {
			printf("FAILED: ws_length %i ref_length %i\n", (int) ws_length, (int) ref_length);
			return 1;
		}
		// check extent table integrity
		assert(!ws_fileheader_check_integrity(&session.header));
		// op
		int rcode = rand() & 255;
		int rscode = rcode % 3;
		if (rcode == 255) {
			// reset?
			close(ref);
			close(session.fd_real);
			close(session.fd_source);
			bse_new(&ref, &session);
		} else if (rscode == 0) {
			// read
			// needs to be calibrated to not run off of the end
			int offset = rand() & 4095;
			int length = rand() & 2047;
			assert(ws_readall(ref, offset, data_ref, length) == length);
			assert(ws_session_read(&session, data_ws, length, offset) == length);
			assert(!memcmp(data_ref, data_ws, length));
		} else if (rscode == 1) {
			// write
			// preinit data
			for (int i = 0; i < TESTFILE_LEN; i++)
				data_ref[i] = (char) rand();
			// and figure out what we do with it
			int offset = rand() & 4095;
			// this is chosen to maximize extent count
			// but to allow overlaps and such to occur
			int length = rand() & 7;
			assert(!ws_writeall(ref, offset, data_ref, length));
			assert(ws_session_write(&session, data_ref, length, offset) == length);
		} else if (rscode == 2) {
			// verify
			memset(data_ref, 0, TESTFILE_LEN);
			memset(data_ws, 0, TESTFILE_LEN);
			assert(ws_readall(ref, 0, data_ref, TESTFILE_LEN) == TESTFILE_LEN);
			assert(ws_session_read(&session, data_ws, TESTFILE_LEN, 0) == TESTFILE_LEN);
			assert(!memcmp(data_ref, data_ws, TESTFILE_LEN));
		}
	}
	printf("success\n");
	return 0;
}
