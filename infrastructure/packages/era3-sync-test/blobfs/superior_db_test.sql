.read ../era3-sync/opt/era3/sync/schema.sql
-- testdata should be superior relative to "adora", but not superior relative to "bartender"
INSERT INTO paths (path) VALUES ("/example");
INSERT INTO paths (path) VALUES ("/wineglass");
INSERT INTO hosts (host, update_time) VALUES ("adora", 10);
INSERT INTO hosts (host, update_time) VALUES ("bartender", 200);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("adora", "/example", NULL, 0, 10);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("bartender", "/wineglass", NULL, 0, 200);

ATTACH DATABASE "blobfs-test/test.db" AS "incoming";

