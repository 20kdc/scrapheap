.read ../../era3-sync/opt/era3/sync/schema.sql
INSERT INTO blobs (hash, length) VALUES (X'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 0);
INSERT INTO blobs (hash, length) VALUES (X'0b2246568a8a2b40b6f159d4e63b7d24e69af824cf3735d21412fa6fce6bf83c', 48);
INSERT INTO paths (path) VALUES ("/example");
INSERT INTO paths (path) VALUES ("/example/collider");

INSERT INTO hosts (host, update_time) VALUES ("adora", 100);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("adora", "/example", X'0b2246568a8a2b40b6f159d4e63b7d24e69af824cf3735d21412fa6fce6bf83c', 0, 100);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("adora", "/example/collider", X'0b2246568a8a2b40b6f159d4e63b7d24e69af824cf3735d21412fa6fce6bf83c', 0, 100);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("adora", "/deleted-scene", NULL, 0, 100);

INSERT INTO hosts (host, update_time) VALUES ("bartender", 100);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("bartender", "/example", X'0b2246568a8a2b40b6f159d4e63b7d24e69af824cf3735d21412fa6fce6bf83c', 0, 50);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("bartender", "/example/collider", X'0b2246568a8a2b40b6f159d4e63b7d24e69af824cf3735d21412fa6fce6bf83c', 0, 50);
INSERT INTO "index" (host, path, blob, executable, update_time) VALUES ("bartender", "/deleted-scene", NULL, 0, 50);
