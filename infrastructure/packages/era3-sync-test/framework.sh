# - test framework -

export BLOBFS_UID="`id -u`"
export BLOBFS_GID="`id -g`"

DURING_TEST=0
TEST_NAME=""

END_TEST() {
	DURING_TEST=0
	echo
	echo " $TEST_NAME OK"
	echo
}

TEST() {
	if [ "$DURING_TEST" = 1 ]; then
		END_TEST
	fi
	DURING_TEST=1
	TEST_NAME="$1"
	echo ""
	echo " $TEST_NAME:"
	echo ""
}

# - common utilities -

# NODE
node_enter() {
	cd "scratchpad/$1"
}

node_leave() {
	cd ../..
}

pad_reboot() {
	for v in `ls scratchpad`; do
		fusermount -u "scratchpad/$v/mountpoint" 2> /dev/null || true
	done
	rm -rf scratchpad
}

# NODE FILE TEXT
vfs_put() {
	echo "$3" > "scratchpad/$1/mountpoint/$2"
}

# NODE FILE
vfs_del() {
	rm "scratchpad/$1/mountpoint/$2"
}

# NODE
syn_init() {
	mkdir -p "scratchpad/$1/workspace" "scratchpad/$1/mountpoint" "scratchpad/$1/blob" || exit 1
	sqlite3 -bail "scratchpad/$1/metadata.db" < ../era3-sync/opt/era3/sync/schema.sql || exit 1
	syn_remount "$1"
}

# NODE
syn_remount() {
	node_enter "$1"
	fusermount -u mountpoint 2> /dev/null || true
	rm -rf workspace
	mkdir -p workspace
	era3 sync core db2index metadata.db "$1"
	/opt/era3/sync/fs mountpoint 2> /dev/null
	node_leave
	sleep 1.1
}

# NODE
syn_commit() {
	node_enter "$1"
	era3 sync core commit "$1" || exit 1
	node_leave
	syn_remount "$1"
	# ensures any further commits are perceived as logically later
	sleep 1.1
}

# NODE
syn_status() {
	node_enter "$1"
	era3 sync core status "$1" || exit 1
	node_leave
}

# NODE_FROM NODE_TO
syn_exchange() {
	node_enter "$2"
	sqlite3 "../../scratchpad/$1/metadata.db" .dump > import.sql || exit 1
	era3 sync core metadata-import "$1" < import.sql || exit 1
	node_leave
}

# NODE_INITIATOR NODE_RESPONDANT
syn_plan_create() {
	node_enter "$1"
	era3 sync core plan-create "$1" "$2" > plan.json || exit 1
	echo "PLAN: $1"
	cat plan.json
	node_leave
}

# NODE NODE_SOURCE
syn_plan_dl() {
	node_enter "$1"
	era3 sync core plan-needs-blobs > wanted || exit 1
	node_leave
	node_enter "$2"
	cd blob
	tar c -v -T "../../$1/wanted" > "../../$1/wanted.tar" || exit 1
	cd ..
	node_leave
	node_enter "$1"
	era3 sync core tar2blobs < wanted.tar || exit 1
	node_leave
}

# NODE
syn_plan_apply() {
	node_enter "$1"
	era3 sync core plan-apply "$1" || exit 1
	node_leave
	syn_remount "$1"
}

# NODE
syn_gc() {
	node_enter "$1"
	era3 sync core gc || exit 1
	node_leave
}

# NODE_INITIATOR NODE_RESPONDANT
syn_sync() {
	# initial metadata exchange
	# this exchange ensures DBs are in a consistent state
	#  for operations planning
	syn_exchange "$1" "$2" || exit 1
	syn_exchange "$2" "$1" || exit 1

	# create the initial plan based on reasonable sync principles
	syn_plan_create "$1" "$2" || exit 1
	# in a real flow, user would verify the plan here
	# initiator downloads any missing blobs from respondant
	syn_plan_dl "$1" "$2" || exit 1
	# initiator applies plan to initiator's own database
	syn_plan_apply "$1" || exit 1
	# plan files are host-independent
	# this mirrors plan-import to plan-export
	cp "scratchpad/$1/plan.json" "scratchpad/$2/plan.json" || exit 1
	# respondant downloads any missing blobs
	syn_plan_dl "$2" "$1" || exit 1
	# respondant can now apply the plan
	syn_plan_apply "$2" || exit 1

	# completion metadata exchange
	# this exchange is for GCs
	syn_exchange "$1" "$2" || exit 1
	syn_exchange "$2" "$1" || exit 1

	syn_gc "$1" || exit 1
	syn_gc "$2" || exit 1
}
