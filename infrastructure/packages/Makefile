# needs special handling but is copied so can be "cleaned" safely
CORE_BUILD_DEBS += dumb-fan-booster.deb
CORE_BUILD_DEBS += era3-essentials.deb era3-engine.deb era3-system-config.deb

CORE_INST_DEBS := $(CORE_BUILD_DEBS)

MAIN_BUILD_DEBS := $(CORE_BUILD_DEBS)
# normal
MAIN_BUILD_DEBS += era3-essentials-desktop.deb era3-sync.deb
# normal-ish
MAIN_BUILD_DEBS += era3-private.deb

# the installer doesn't actually install era3-installer-untrusted
# but it's convenient to package it this way for deploy-untrusted
MAIN_INST_DEBS := $(MAIN_BUILD_DEBS) era3-installer-untrusted.deb
# external debs
MAIN_INST_DEBS += kanri.deb

ALL_BUILD_DEBS := $(MAIN_BUILD_DEBS) era3-installer.deb era3-installer-untrusted.deb
ALL_DEPFILES := $(addsuffix .d, $(ALL_BUILD_DEBS))

DEB_BUILD_TOOLS := era3-pkg tree-deps

all: $(ALL_BUILD_DEBS)

install: era3-installer.deb
	sudo sh -c "apt reinstall ./era3-installer.deb ; /opt/era3/installer/install ./era3-installer.deb"
synctest: era3-sync.deb
	sudo sh -c "apt reinstall ./era3-sync.deb"

engtest: era3-engine.deb
	sudo sh -c "apt reinstall ./era3-engine.deb"

clean:
	rm -f $(ALL_BUILD_DEBS) $(ALL_DEPFILES)

%.deb: % %/DEBIAN/control $(DEB_BUILD_TOOLS)
	./era3-pkg build $<
	./era3-pkg deps $< > $*.deb.d

%.deb.d: % %/DEBIAN/control $(DEB_BUILD_TOOLS)
	./era3-pkg deps $< > $@

# These packages are external:

kanri.deb:
	echo "$@ : Missing. To bootstrap, copy from repositories/personal/synctech-bootstrap-backup"
	echo "External users: https://github.com/trobonox/kanri"
	exit 1

# Load depfiles

include $(ALL_DEPFILES)

# Locally built packages

era3-installer.deb: $(MAIN_INST_DEBS) $(DEB_BUILD_TOOLS)
	rm -rf era3-installer/opt/era3/installer/contents
	mkdir -p era3-installer/opt/era3/installer/contents
	cp $(MAIN_INST_DEBS) era3-installer/opt/era3/installer/contents/
	date "+%s" > era3-installer/opt/era3/installer/version
	./era3-pkg build era3-installer
	rm -f era3-installer/opt/era3/installer/version
	rm -rf era3-installer/opt/era3/installer/contents

era3-installer-untrusted.deb: $(CORE_INST_DEBS) $(DEB_BUILD_TOOLS)
	rm -rf era3-installer-untrusted/opt/era3/installer/contents
	mkdir -p era3-installer-untrusted/opt/era3/installer/contents
	cp $(CORE_INST_DEBS) era3-installer-untrusted/opt/era3/installer/contents/
	date "+%s" > era3-installer-untrusted/opt/era3/installer/version
	./era3-pkg build era3-installer-untrusted
	rm -f era3-installer-untrusted/opt/era3/installer/version
	rm -rf era3-installer-untrusted/opt/era3/installer/contents

ifeq ($(wildcard ./era3-private),)
# make this VERY NOISY
era3-private.deb: era3-private-dummy.deb
	@cat .era3-private-warning-notice && sleep 10 || exit 1
	@cp era3-private-dummy.deb era3-private.deb
endif
