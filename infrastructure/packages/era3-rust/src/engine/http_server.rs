//! HTTP parser/emitter. Adapted for standard-input only, because reasons.

use std::io::Read;

use super::http::{HTTPRequest, HTTPResponse};

/// Handles all early request handling in an app.
/// Auto-exits on error.
pub fn read_request() -> HTTPRequest {
    let mut lock = std::io::stdin().lock();
    // do this simply
    let mut buffer: Vec<u8> = Default::default();
    let mut current: u8 = 0;
    loop {
        let amount = if let Ok(amount) = lock.read(std::slice::from_mut(&mut current)) {
            amount
        } else {
            write_400("Early EOF");
            std::process::exit(0);
        };
        if amount == 0 {
            // early EOF?
            write_400("Early EOF");
            std::process::exit(0);
        }
        if current == 13 {
            continue;
        } else if current == 10 {
            break;
        }
        if buffer.len() >= 1024 {
            // surpassed valid line limit, terminate
            write_400("Line Too Long");
            std::process::exit(0);
        }
        buffer.push(current);
    }
    let res = String::from_utf8_lossy(&buffer);
    let trimmed = res.trim();
    let parts: Vec<&str> = trimmed.split(' ').collect();
    if parts.len() != 3 {
        let msg = format!("expected 3 parts, got {}: {}", parts.len(), trimmed);
        write_400(&msg);
        std::process::exit(0);
    }
    let fullpath = parts[1];
    if let Some(at) = fullpath.find('?') {
        let path = &fullpath[..at];
        let query = &fullpath[at + 1..];
        HTTPRequest {
            method: parts[0].to_string(),
            path: path.to_string(),
            query: query.to_string(),
            headers: vec![],
            body: vec![],
        }
    } else {
        HTTPRequest {
            method: parts[0].to_string(),
            path: fullpath.to_string(),
            query: "".to_string(),
            headers: vec![],
            body: vec![],
        }
    }
}

/// Writes a 400 bad request error.
pub fn write_400(info: &str) {
    write(400, info, "text/plain", info.as_bytes());
}

/// Writes a 404 not found error.
pub fn write_404() {
    write(
        404,
        "Not Found",
        "text/plain",
        "The resource was not found.".as_bytes(),
    );
}

/// Writes a 405 bad method error.
pub fn write_405() {
    write(
        405,
        "Method Not Allowed",
        "text/plain",
        "The method was not valid for the resource.".as_bytes(),
    );
}

/// Sends a response.
pub fn write(code: u32, message: &str, mimetype: &str, body: &[u8]) {
    let mut locked = std::io::stdout().lock();
    let response = HTTPResponse {
        code,
        message: message.to_string(),
        headers: vec![
            ("Content-Type".to_string(), mimetype.to_string()),
            ("Content-Length".to_string(), format!("{}", body.len())),
            ("Connection".to_string(), "close".to_string()),
        ],
        body: body.to_vec(),
    };
    response.write(&mut locked);
}

/// Sends a 200 OK.
pub fn write_200(mimetype: &str, body: &[u8]) {
    write(200, "OK", mimetype, body);
}
