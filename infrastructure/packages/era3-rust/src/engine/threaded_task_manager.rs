//! Rotates threads to increase throughput without exhausting PID space.

use std::{any::Any, cell::Cell, marker::PhantomData, rc::Rc, thread::JoinHandle};

/// Trait used to isolate task manager thread count / etc. from the rest
pub trait TaskManager {
    /// Spawns a task.
    fn spawn_untyped<F: FnOnce() -> TaskDynResult + Send + 'static>(&mut self, f: F) -> TaskAny;
    /// Spawns a task.
    fn spawn<T: Any + Send + 'static, F: FnOnce() -> T + Send + 'static>(
        &mut self,
        f: F,
    ) -> Task<T> {
        Task::from(self.spawn_untyped(move || Box::new(f())))
    }
}

pub type TaskDynResult = Box<dyn Any + Send + 'static>;

/// State for an active task.
pub enum TaskState {
    /// Busy / value consumed
    Consumed,
    Waiting(JoinHandle<TaskDynResult>),
    Received(TaskDynResult),
}

/// Thread pool, managed from one "controller" thread.
pub struct TaskPool<const THREADS: usize>([TaskSlot; THREADS], usize);
type TaskSlot = Option<TaskAny>;

/// Task
pub struct TaskData(Cell<TaskState>);

/// A typeless task.
pub type TaskAny = Rc<TaskData>;

/// A typed task. If the target isn't of the right type, a panic will occur on join.
pub struct Task<T: Any + Send + 'static>(pub TaskAny, PhantomData<T>);

impl<const THREADS: usize> Default for TaskPool<THREADS> {
    fn default() -> Self {
        Self([(); THREADS].map(|_| None), 0)
    }
}

impl TaskData {
    /// Joins and gets the task data. This can only be done once, and will panic if done twice.
    pub fn join(&self) -> TaskDynResult {
        let holder = Cell::new(TaskState::Consumed);
        self.0.swap(&holder);
        let ts = holder.into_inner();
        if let TaskState::Waiting(jh) = ts {
            jh.join().unwrap()
        } else if let TaskState::Received(v) = ts {
            v
        } else {
            panic!("task consumed twice");
        }
    }

    /// Ensures the task is either in Consumed or Received states; i.e. blocks so that join() will never block.
    pub fn ensure_complete(&self) {
        let holder = Cell::new(TaskState::Consumed);
        self.0.swap(&holder);
        let mut ts = holder.into_inner();
        if let TaskState::Waiting(jh) = ts {
            ts = TaskState::Received(jh.join().unwrap());
        }
        self.0.set(ts);
    }

    /// Queries if the task is complete (advancing it if necessary).
    pub fn poll_complete(&self) -> bool {
        let mut done = false;
        let holder = Cell::new(TaskState::Consumed);
        self.0.swap(&holder);
        let mut ts = holder.into_inner();
        if let TaskState::Waiting(jh) = ts {
            if jh.is_finished() {
                ts = TaskState::Received(jh.join().unwrap());
                done = true;
            } else {
                ts = TaskState::Waiting(jh);
            }
        } else if let TaskState::Consumed = ts {
            done = true;
        }
        self.0.set(ts);
        done
    }
}

impl<T: Sized + Send + 'static> From<TaskAny> for Task<T> {
    fn from(value: TaskAny) -> Self {
        Self(value, PhantomData)
    }
}

impl<T: Sized + Send + 'static> Task<T> {
    /// Joins and gets the task data. This can only be done once, and will panic if done twice.
    pub fn join(&self) -> T {
        *self.0.join().downcast::<T>().unwrap()
    }

    /// Ensures the task is either in Consumed or Received states; i.e. blocks so that join() will never block.
    pub fn ensure_complete(&self) {
        self.0.ensure_complete()
    }

    /// Queries if the task is complete (advancing it if necessary).
    pub fn poll_complete(&self) -> bool {
        self.0.poll_complete()
    }
}

impl<const THREADS: usize> TaskPool<THREADS> {
    /// Clears & joins completed tasks.
    pub fn clear_all_completed_tasks(&mut self) {
        for v in &mut self.0 {
            let mut done = false;
            if let Some(vs) = v {
                done = vs.poll_complete();
            }
            if done {
                *v = None;
            }
        }
    }

    /// Ensures the given slot's task will be in either Received or Consumed states and the slot is empty.
    fn free_slot(&mut self, slot: usize) {
        let slot_data = self.0[slot].take();
        if let Some(task) = slot_data {
            task.ensure_complete();
        }
    }

    /// Ensures a free task slot.
    fn ensure_free_task(&mut self) -> usize {
        // clean up
        self.clear_all_completed_tasks();
        // find already freed slots
        for k in 0..THREADS {
            if self.0[k].is_none() {
                return k;
            }
        }
        // rotate the barrel
        let slot = self.1;
        self.1 = (self.1 + 1) % THREADS;
        // return a free slot
        self.free_slot(slot);
        return slot;
    }
}

impl<const THREADS: usize> TaskManager for TaskPool<THREADS> {
    fn spawn_untyped<F: FnOnce() -> TaskDynResult + Send + 'static>(&mut self, f: F) -> TaskAny {
        let slot = self.ensure_free_task();
        let jh = std::thread::spawn(f);
        let task = Rc::new(TaskData(Cell::new(TaskState::Waiting(jh))));
        self.0[slot] = Some(task.clone());
        task
    }
}

impl<const THREADS: usize> Drop for TaskPool<THREADS> {
    fn drop(&mut self) {
        for slot in 0..THREADS {
            self.free_slot(slot);
        }
    }
}
