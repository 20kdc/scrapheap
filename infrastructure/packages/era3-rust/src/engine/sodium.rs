//! The libsodium wrapper you never knew you didn't want.

use std::mem::MaybeUninit;
use std::os::raw::c_ulonglong;

// --

pub const CRYPTO_SIGN_ED25519_SEEDBYTES: usize = 32;
pub const CRYPTO_SIGN_ED25519_PUBLICKEYBYTES: usize = 32;
pub const CRYPTO_SIGN_ED25519_SECRETKEYBYTES: usize = 64;
pub const CRYPTO_SIGN_ED25519_BYTES: usize = 64;

pub const CRYPTO_HASH_SHA256_BYTES: usize = 32;

// --

pub type ED25519Signature = [u8; CRYPTO_SIGN_ED25519_BYTES];
pub type ED25519Seed = [u8; CRYPTO_SIGN_ED25519_SEEDBYTES];
pub type ED25519PublicKey = [u8; CRYPTO_SIGN_ED25519_PUBLICKEYBYTES];
pub type ED25519SecretKey = [u8; CRYPTO_SIGN_ED25519_SECRETKEYBYTES];

pub type SHA256Hash = [u8; CRYPTO_HASH_SHA256_BYTES];

#[repr(C)]
#[derive(Clone, Copy)]
pub struct SHA256HashState {
    state: [u32; 8],
    count: u64,
    buf: [u8; 64],
}

// --

extern "C" {
    fn crypto_sign_ed25519_seed_keypair(
        pk: &mut MaybeUninit<ED25519PublicKey>,
        sk: &mut MaybeUninit<ED25519SecretKey>,
        seed: &ED25519Seed,
    ) -> i32;
    fn crypto_sign_ed25519_detached(
        sig: &mut MaybeUninit<ED25519Signature>,
        siglen: *const (),
        data: *const u8,
        data_len: c_ulonglong,
        sk: &ED25519SecretKey,
    ) -> i32;
    fn crypto_sign_ed25519_verify_detached(
        signature: &ED25519Signature,
        data: *const u8,
        data_len: c_ulonglong,
        pubkey: &ED25519PublicKey,
    ) -> i32;
    fn crypto_hash_sha256(
        hash: &mut MaybeUninit<SHA256Hash>,
        data: *const u8,
        data_len: c_ulonglong,
    ) -> i32;
    fn crypto_hash_sha256_init(state: &mut MaybeUninit<SHA256HashState>) -> i32;
    fn crypto_hash_sha256_update(
        state: &mut SHA256HashState,
        data: *const u8,
        data_len: c_ulonglong,
    ) -> i32;
    fn crypto_hash_sha256_final(
        state: &mut SHA256HashState,
        hash: &mut MaybeUninit<SHA256Hash>,
    ) -> i32;
}

pub fn ed25519_seed_keypair(seed: &ED25519Seed) -> (ED25519PublicKey, ED25519SecretKey) {
    let mut pk: MaybeUninit<ED25519PublicKey> = MaybeUninit::uninit();
    let mut sk: MaybeUninit<ED25519SecretKey> = MaybeUninit::uninit();
    unsafe {
        assert_eq!(crypto_sign_ed25519_seed_keypair(&mut pk, &mut sk, seed), 0);
        (pk.assume_init(), sk.assume_init())
    }
}

pub fn ed25519_detached(data: &[u8], sk: &ED25519SecretKey) -> ED25519Signature {
    let mut s: MaybeUninit<ED25519Signature> = MaybeUninit::uninit();
    unsafe {
        assert_eq!(
            crypto_sign_ed25519_detached(
                &mut s,
                std::ptr::null(),
                data.as_ptr(),
                data.len() as c_ulonglong,
                &sk
            ),
            0
        );
        s.assume_init()
    }
}

pub fn ed25519_verify_detached(s: &ED25519Signature, data: &[u8], pk: &ED25519PublicKey) -> bool {
    unsafe {
        crypto_sign_ed25519_verify_detached(&s, data.as_ptr(), data.len() as c_ulonglong, &pk) == 0
    }
}

pub fn sha256(data: &[u8]) -> SHA256Hash {
    let mut s: MaybeUninit<SHA256Hash> = MaybeUninit::uninit();
    unsafe {
        assert_eq!(
            crypto_hash_sha256(&mut s, data.as_ptr(), data.len() as c_ulonglong),
            0
        );
        s.assume_init()
    }
}

impl Default for SHA256HashState {
    fn default() -> Self {
        let mut state: MaybeUninit<Self> = MaybeUninit::uninit();
        unsafe {
            assert_eq!(crypto_hash_sha256_init(&mut state), 0);
            state.assume_init()
        }
    }
}

impl SHA256HashState {
    pub fn update(&mut self, data: &[u8]) {
        unsafe {
            assert_eq!(
                crypto_hash_sha256_update(self, data.as_ptr(), data.len() as c_ulonglong),
                0
            );
        }
    }
    pub fn finish(mut self) -> SHA256Hash {
        let mut hash: MaybeUninit<SHA256Hash> = MaybeUninit::uninit();
        unsafe {
            assert_eq!(crypto_hash_sha256_final(&mut self, &mut hash), 0);
            hash.assume_init()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hash_equivalence() {
        let mut a = SHA256HashState::default();
        a.update(b"Hello");
        a.update(b" world.");
        let hash_streamed = a.finish();
        let hash_unstreamed = sha256(b"Hello world.");
        assert_eq!(hash_streamed, hash_unstreamed);
    }
}
