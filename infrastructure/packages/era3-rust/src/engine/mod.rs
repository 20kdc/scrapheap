//! Core of era3. Provides utilities that will be used across pretty much everything.

use std::fmt::Display;
use std::fmt::Write;
use std::ops::Deref;

/// This is used to do some magic to ensure newlines don't infect the rest of the code.
/// As such, it should be used whenever the file is not expected to change without a Rust cache reset.
#[derive(Clone, Copy)]
pub struct BuildTimeFileString(&'static str);

impl AsRef<str> for BuildTimeFileString {
    fn as_ref<'a>(&'a self) -> &'a str {
        &self.0[..self.0.len() - 1]
    }
}

impl Deref for BuildTimeFileString {
    type Target = str;
    fn deref<'a>(&'a self) -> &'a str {
        &self.0[..self.0.len() - 1]
    }
}

impl Display for BuildTimeFileString {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.0[..self.0.len() - 1].fmt(fmt)
    }
}

/// era3 version (or at least, should be)
pub static VERSION: BuildTimeFileString =
    BuildTimeFileString(include_str!("/opt/era3/installer/version"));
/// Node hostname
pub static HOSTNAME: BuildTimeFileString = BuildTimeFileString(include_str!("/etc/hostname"));

/// Escapes HTML
pub fn escape_html(text: impl AsRef<str>) -> String {
    let mut s = String::new();
    for chr in text.as_ref().chars() {
        _ = write!(&mut s, "&#{};", chr as u32);
    }
    s
}

/// Args, not including program name, as a vec for easy access
/// This is used by E3S etc.
pub fn args() -> Vec<String> {
    std::env::args().skip(1).collect()
}

/// Args, not including program name, as a vec for easy access
/// This is used by E3S etc.
pub fn args_fixed<const ARGS: usize>(msg: &str) -> [String; ARGS] {
    let argsrc = args();
    assert_eq!(argsrc.len(), ARGS, "{}", msg);
    let mut res = [(); ARGS].map(|_| String::new());
    res.clone_from_slice(&argsrc);
    res
}

pub mod e3ms;
pub mod fs;
pub mod http;
pub mod http_server;
pub mod libc;
pub mod net;
pub mod prelude;
pub mod sodium;
pub mod sqlite3;
pub mod threaded_task_manager;
pub mod tui;
