//! The SQLite3 wrapper you never knew you didn't want.

use std::cell::Cell;
use std::ffi::{CStr, CString};
use std::os::raw::{c_char, c_uchar};
use std::ptr::NonNull;
use std::rc::Rc;

// --

pub const OPEN_READONLY: i32 = 0x00000001;
pub const OPEN_READWRITE: i32 = 0x00000002;
pub const OPEN_CREATE: i32 = 0x00000004;
pub const OPEN_URI: i32 = 0x00000040;
pub const OPEN_MEMORY: i32 = 0x00000080;
pub const OPEN_NOFOLLOW: i32 = 0x01000000;

/// Allowed bits (others will be masked off).
const OPEN_ALLOWED: i32 = 0x010000C7;
const OPEN_FULLMUTEX: i32 = 0x00010000;

const ROW: i32 = 100;
const DONE: i32 = 101;

pub const LIMIT_ATTACHED: i32 = 7;

// --

extern "C" {
    // open/close/errhandling
    fn sqlite3_open_v2(
        path: *const c_char,
        db: &mut *mut (),
        flags: i32,
        vfs: *const c_char,
    ) -> i32;
    fn sqlite3_close(db: *mut ()) -> i32;
    fn sqlite3_errstr(err: i32) -> *const c_char;
    // can only really be used for the opening stage, sadly...
    fn sqlite3_errmsg(db: NonNull<()>) -> *const c_char;
    fn sqlite3_limit(db: NonNull<()>, limit: i32, value: i32) -> i32;

    // statements
    fn sqlite3_prepare_v2(
        db: NonNull<()>,
        sql: *const c_char,
        sql_len: i32,
        stmt: &mut *mut (),
        tail: &mut *const c_char,
    ) -> i32;
    fn sqlite3_finalize(stmt: *mut ()) -> i32;
    fn sqlite3_reset(stmt: NonNull<()>) -> i32;
    fn sqlite3_step(stmt: NonNull<()>) -> i32;

    // values, out
    fn sqlite3_column_type(stmt: NonNull<()>, column: i32) -> i32;
    fn sqlite3_column_count(stmt: NonNull<()>) -> i32;
    fn sqlite3_column_blob(stmt: NonNull<()>, column: i32) -> *const u8;
    fn sqlite3_column_double(stmt: NonNull<()>, column: i32) -> f64;
    fn sqlite3_column_int64(stmt: NonNull<()>, column: i32) -> i64;
    fn sqlite3_column_text(stmt: NonNull<()>, column: i32) -> *const c_uchar;
    fn sqlite3_column_bytes(stmt: NonNull<()>, column: i32) -> i32;

    // values, in
    fn sqlite3_clear_bindings(stmt: NonNull<()>) -> i32;
    fn sqlite3_bind_null(stmt: NonNull<()>, column: i32) -> i32;
    fn sqlite3_bind_int64(stmt: NonNull<()>, column: i32, value: i64) -> i32;
    fn sqlite3_bind_double(stmt: NonNull<()>, column: i32, value: f64) -> i32;
    fn sqlite3_bind_blob64(
        stmt: NonNull<()>,
        column: i32,
        data: *const u8,
        len: u64,
        cb: usize,
    ) -> i32;
    fn sqlite3_bind_text64(
        stmt: NonNull<()>,
        column: i32,
        data: *const u8,
        len: u64,
        cb: usize,
        encoding: c_uchar,
    ) -> i32;
}

// --

#[derive(Clone, Debug)]
pub struct Error {
    pub message: String,
    pub code: i32,
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.message.fmt(fmt)
    }
}

// --

/// SQLite3 database connection type implementation.
pub struct ConnectionData(NonNull<()>);

/// SQLite3 database connection type.
pub type Connection = Rc<ConnectionData>;

impl Drop for ConnectionData {
    fn drop(&mut self) {
        _ = unsafe { sqlite3_close(self.0.as_ptr()) };
    }
}

/// Opens a new SQLite3 connection. The connection is always opened in `OPEN_FULLMUTEX` mode.
pub fn open(filename: impl AsRef<str>, mut flags: i32) -> Result<Connection, Error> {
    flags &= OPEN_ALLOWED;
    flags |= OPEN_FULLMUTEX;
    let cstring = CString::new(filename.as_ref().as_bytes()).unwrap();
    let mut db: *mut () = std::ptr::null_mut();
    // SAFETY: The flags assessment ensures this is a multithread-capable DB, and we own the CString.
    let res = unsafe { sqlite3_open_v2(cstring.as_ptr(), &mut db, flags, std::ptr::null()) };
    let db_cd = NonNull::new(db).map(|v| ConnectionData(v));
    match db_cd {
        Some(db) => {
            if res == 0 {
                Ok(Rc::new(db))
            } else {
                Err(db.errmsg(res))
            }
        }
        None => {
            let errptr = unsafe { sqlite3_errstr(res) };
            if errptr.is_null() {
                Err(Error {
                    message: format!("SQLite3 error {}", res),
                    code: res,
                })
            } else {
                Err(Error {
                    message: unsafe { CStr::from_ptr(errptr) }
                        .to_string_lossy()
                        .to_string(),
                    code: res,
                })
            }
        }
    }
}

impl ConnectionData {
    /// Error message.
    fn errmsg(&self, res: i32) -> Error {
        let errptr = unsafe { sqlite3_errmsg(self.0) };
        if errptr.is_null() {
            Error {
                message: format!("SQLite3 error {}", res),
                code: res,
            }
        } else {
            Error {
                message: unsafe { CStr::from_ptr(errptr) }
                    .to_string_lossy()
                    .to_string(),
                code: res,
            }
        }
    }
    /// Unit error
    fn errres(&self, res: i32) -> Result<(), Error> {
        if res == 0 {
            Ok(())
        } else {
            Err(self.errmsg(res))
        }
    }
    /// Prepares a statement for execution.
    /// Note that this discards any further statements.
    pub fn prepare(self: &Rc<Self>, sql: impl AsRef<str>) -> Result<Statement, Error> {
        let vtm = self.prepare_tail(sql.as_ref())?;
        match vtm {
            Some(v) => Ok(v.0),
            None => Err(Error {
                message: format!("era3 binding: No SQL statement"),
                code: 0,
            }),
        }
    }
    /// Prepares a statement for execution.
    /// Note that None can be returned if there's no statement.
    pub fn prepare_tail<'a>(
        self: &Rc<Self>,
        sql: &'a str,
    ) -> Result<Option<(Statement, &'a str)>, Error> {
        let sql_as_bytes = sql.as_bytes();
        if sql_as_bytes.len() > 0x7FFFFFFF {
            panic!("SQL statement of unusual size");
        }
        let mut stmt: *mut () = std::ptr::null_mut();
        let sql_as_bytes_ptr = sql_as_bytes.as_ptr() as *const c_char;
        let mut tail: *const c_char = std::ptr::null();
        let res = unsafe {
            sqlite3_prepare_v2(
                self.0,
                sql_as_bytes_ptr,
                sql_as_bytes.len() as i32,
                &mut stmt,
                &mut tail,
            )
        };
        let stmt_cd = NonNull::new(stmt).map(|v| Statement {
            db: self.clone(),
            stmt: v,
            finalized: Cell::new(false),
            columns: Cell::new(0),
        });
        match stmt_cd {
            Some(stmt) => {
                if tail.is_null() {
                    Ok(Some((stmt, "")))
                } else {
                    Ok(Some((
                        stmt,
                        &sql[unsafe { tail.offset_from(sql_as_bytes_ptr) } as usize..],
                    )))
                }
            }
            None => {
                if res == 0 {
                    Ok(None)
                } else {
                    Err(self.errmsg(res))
                }
            }
        }
    }
    /// See <https://www.sqlite.org/c3ref/limit.html>
    pub fn limit(&self, limit: i32, value: i32) -> i32 {
        unsafe { sqlite3_limit(self.0, limit, value) }
    }
    /// Executes a script.
    pub fn execute_script(self: &Rc<Self>, sql: impl AsRef<str>) -> Result<(), Error> {
        let mut sql = sql.as_ref();
        while sql.len() > 0 {
            if let Some((stmt, tail)) = self.prepare_tail(sql)? {
                stmt.finish()?;
                sql = tail;
            } else {
                // ran out of SQL
                break;
            }
        }
        Ok(())
    }
}

/// Prepared statement, used for executing SQL.
#[allow(dead_code)]
pub struct Statement {
    /// Database ref (keeps it valid).
    db: Connection,
    /// The statement itself.
    stmt: NonNull<()>,
    /// If this is finalized.
    finalized: Cell<bool>,
    /// Amount of columns.
    columns: Cell<usize>,
}

/// Column type. Values correspond to constants.
#[repr(i32)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum ColumnType {
    Integer = 1,
    Float = 2,
    Text = 3,
    Blob = 4,
    Null = 5,
}

/// Value. These match the column types, more or less.
#[derive(Clone, PartialEq, PartialOrd, Debug)]
pub enum Value {
    Integer(i64),
    Float(f64),
    Text(String),
    Blob(Vec<u8>),
    Null,
}

impl Drop for Statement {
    fn drop(&mut self) {
        if !self.finalized.get() {
            // if we haven't already been finalized, do that now
            _ = unsafe { sqlite3_finalize(self.stmt.as_ptr()) };
        }
    }
}

impl Statement {
    /// Finalizes the statement (destroying it, and returning the result).
    pub fn finalize(self) -> Result<(), Error> {
        // This prevents double-finalization.
        // Notably, finalization causes a dangling NonNull. Just have to accept that...
        self.finalized.set(true);
        self.columns.set(0);
        let res = unsafe { sqlite3_finalize(self.stmt.as_ptr()) };
        self.db.errres(res)
    }
    /// Resets the statement. This is fundamentally similar to finalize.
    pub fn reset(&self) -> Result<(), Error> {
        self.columns.set(0);
        let res = unsafe { sqlite3_reset(self.stmt) };
        self.db.errres(res)
    }
    /// 'Steps' the statement.
    /// This is written as an option unit (to represent the row) inside a result (to represent errors).
    pub fn step(&self) -> Result<Option<()>, Error> {
        self.columns.set(0);
        let res = unsafe { sqlite3_step(self.stmt) };
        if res == ROW {
            self.columns
                .set(unsafe { sqlite3_column_count(self.stmt) } as usize);
            Ok(Some(()))
        } else if res == DONE {
            Ok(None)
        } else {
            Err(self.db.errmsg(res))
        }
    }
    /// Repeatedly steps the statement until it is done.
    pub fn finish(&self) -> Result<(), Error> {
        loop {
            if self.step()?.is_none() {
                return Ok(());
            }
        }
    }
    /// Returns the amount of columns returned from this statement.
    pub fn column_count(&self) -> usize {
        (unsafe { sqlite3_column_count(self.stmt) }) as usize
    }
    /// Returns the type of a column's value.
    /// Beware: You should not call this after calling one of the retrieval functions.
    /// You will potentially get incorrect results. However, it is considered 'harmless'.
    pub fn column_type(&self, column: usize) -> ColumnType {
        assert!(column < self.columns.get());
        unsafe { std::mem::transmute(sqlite3_column_type(self.stmt, column as i32)) }
    }
    /// Returns the column value as a blob.
    /// Can coerce the type ; not marked mutable for convenience
    pub fn column_blob(&self, column: usize) -> Vec<u8> {
        assert!(column < self.columns.get());
        unsafe {
            let blob = sqlite3_column_blob(self.stmt, column as i32);
            let blob_size = sqlite3_column_bytes(self.stmt, column as i32);
            let data: &[u8] = std::slice::from_raw_parts(blob, blob_size as usize);
            data.to_vec()
        }
    }
    /// Returns the column value as text.
    /// Can coerce the type ; not marked mutable for convenience
    pub fn column_text(&self, column: usize) -> String {
        assert!(column < self.columns.get());
        unsafe {
            let blob = sqlite3_column_text(self.stmt, column as i32);
            let blob_size = sqlite3_column_bytes(self.stmt, column as i32);
            let data: &[u8] = std::slice::from_raw_parts(blob, blob_size as usize);
            String::from_utf8_lossy(data).into_owned()
        }
    }
    /// Returns the column value as a double.
    /// Can coerce the type ; not marked mutable for convenience
    pub fn column_double(&self, column: usize) -> f64 {
        assert!(column < self.columns.get());
        unsafe { sqlite3_column_double(self.stmt, column as i32) }
    }
    /// Returns the column value as an i64
    /// Can coerce the type ; not marked mutable for convenience
    pub fn column_int(&self, column: usize) -> i64 {
        assert!(column < self.columns.get());
        unsafe { sqlite3_column_int64(self.stmt, column as i32) }
    }

    /// Clears the bindings.
    pub fn clear_bindings(&self) -> Result<(), Error> {
        let res = unsafe { sqlite3_clear_bindings(self.stmt) };
        self.db.errres(res)
    }

    /// Binds a value to the given parameter.
    pub fn bind(&self, param: i32, value: Value) -> Result<(), Error> {
        let res = match value {
            Value::Integer(i) => unsafe { sqlite3_bind_int64(self.stmt, param, i) },
            Value::Float(d) => unsafe { sqlite3_bind_double(self.stmt, param, d) },
            Value::Text(t) => unsafe {
                sqlite3_bind_text64(self.stmt, param, t.as_ptr(), t.len() as u64, usize::MAX, 1)
            },
            Value::Blob(b) => unsafe {
                sqlite3_bind_blob64(self.stmt, param, b.as_ptr(), b.len() as u64, usize::MAX)
            },
            Value::Null => unsafe { sqlite3_bind_null(self.stmt, param) },
        };
        self.db.errres(res)
    }
}

// Implementing this on &Statement makes the for loop code work nicely.
impl<'t> Iterator for &'t Statement {
    type Item = Result<&'t Statement, Error>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.step() {
            Err(err) => Some(Err(err)),
            Ok(res) => match res {
                None => None,
                Some(_) => Some(Ok(self)),
            },
        }
    }
}

/// Represents an active transaction.
/// Note that there is no mutex since SQL could bypass it anyway.
/// Because an unexpected quit is equivalent to a rollback in SQLite3, dropping the transaction performs a rollback.
pub struct Transaction(Connection, bool);

impl Transaction {
    /// Creates a Transaction object.
    pub fn new(conn: &Connection) -> Result<Transaction, Error> {
        conn.execute_script("BEGIN")?;
        Ok(Transaction(conn.clone(), true))
    }

    /// Commits the transaction (or fails)
    pub fn commit(mut self) -> Result<(), Error> {
        // give up on rolling back
        self.1 = false;
        self.0.execute_script("COMMIT")
    }
}

impl Drop for Transaction {
    fn drop(&mut self) {
        if self.1 {
            if let Err(_) = self.0.execute_script("ROLLBACK") {
                // eprintln!("era3::engine::sqlite3 : rollback failed! {}", err);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn open_in_memory() {
        let db = open(":memory:", OPEN_READWRITE).unwrap();
        let res = db
            .prepare("create table test(value integer, name text)")
            .unwrap();
        assert!(res.step().unwrap().is_none());
        let res = db
            .prepare("insert into test (value, name) values (?, ?)")
            .unwrap();
        res.bind(1, Value::Integer(1234)).unwrap();
        res.bind(2, Value::Text("Moo".to_string())).unwrap();
        res.finish().unwrap();
        db.execute_script(
            "
        insert into test (value, name) values (4321, \"Baa\");
        insert into test (value, name) values (8471, \"Reh...\");
        ",
        )
        .unwrap();
        let res = db.prepare("select * from test order by value asc").unwrap();
        _ = res.step().unwrap().unwrap();
        assert_eq!(res.column_type(0), ColumnType::Integer);
        assert_eq!(res.column_int(0), 1234);
        assert_eq!(res.column_type(1), ColumnType::Text);
        assert_eq!(res.column_text(1), "Moo");
        _ = res.step().unwrap().unwrap();
        assert_eq!(res.column_type(0), ColumnType::Integer);
        assert_eq!(res.column_int(0), 4321);
        assert_eq!(res.column_type(1), ColumnType::Text);
        assert_eq!(res.column_text(1), "Baa");
        _ = res.step().unwrap().unwrap();
        assert_eq!(res.column_type(0), ColumnType::Integer);
        assert_eq!(res.column_int(0), 8471);
        assert_eq!(res.column_type(1), ColumnType::Text);
        assert_eq!(res.column_text(1), "Reh...");
        assert!(res.step().unwrap().is_none());

        let txtest = Transaction::new(&db).unwrap();
        txtest.commit().unwrap();

        let txtest2 = Transaction::new(&db).unwrap();
        drop(txtest2);
    }
}
