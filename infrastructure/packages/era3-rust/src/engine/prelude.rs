//! The prelude. Contains everything that should be in-scope in all files.

use std::{
    error::Error,
    fmt::{Display, Write},
    mem::{size_of, transmute},
    ptr::{slice_from_raw_parts, slice_from_raw_parts_mut},
    time::{Duration, SystemTime},
};

/// Error type for era3 Rust code.
#[derive(Debug, Clone)]
pub struct E3Err(pub String);

impl From<String> for E3Err {
    fn from(value: String) -> Self {
        Self(value)
    }
}
impl From<&str> for E3Err {
    fn from(value: &str) -> Self {
        Self(value.to_string())
    }
}
impl Display for E3Err {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}
impl Error for E3Err {}
impl E3Err {
    /// Converts error.
    pub fn of<V: Display>(v: V) -> E3Err {
        E3Err(v.to_string())
    }
    /// Converts result.
    pub fn result_of<V: Display, T>(v: Result<T, V>) -> E3Result<T> {
        v.map_err(|err| Self::of(err))
    }
    /// Wraps the E3Err with some more information.
    pub fn mark<T: Display>(&self, a: T) -> E3Err {
        E3Err(format!("{}: {}", a, self))
    }
}

/// Result type for era3 Rust code.
pub type E3Result<T> = Result<T, E3Err>;

/// Convenience syntax sugar stuff
pub trait ConvertibleToE3Result<T> {
    fn e3<D: Display>(self, at: D) -> E3Result<T>;
}

impl<T, E: Display> ConvertibleToE3Result<T> for Result<T, E> {
    fn e3<D: Display>(self, at: D) -> E3Result<T> {
        E3Err::result_of(self).map_err(|err| err.mark(at))
    }
}

/// Not-bitmuck transmutation enabler.
pub unsafe trait SafeCStruct: Copy + Sized {}

/// Converts a [SafeCStruct] to a byte slice.
pub fn safecstruct_as_bytes<T: SafeCStruct>(data: &T) -> &[u8] {
    unsafe {
        let transmuted = transmute::<&T, *const u8>(data);
        &*slice_from_raw_parts(transmuted, size_of::<T>())
    }
}

/// Converts a [SafeCStruct] to a mutable byte slice.
pub fn safecstruct_as_bytes_mut<T: SafeCStruct>(data: &mut T) -> &mut [u8] {
    unsafe {
        let transmuted = transmute::<&T, *mut u8>(data);
        &mut *slice_from_raw_parts_mut(transmuted, size_of::<T>())
    }
}

/// Converts a byte slice to hex.
pub fn bytes_to_hex(out: &mut dyn Write, sh: &[u8]) {
    for v in sh {
        write!(out, "{:02x}", v).unwrap();
    }
}

/// Converts a byte slice to a hex string.
pub fn bytes_to_hex_string(sh: &[u8]) -> String {
    let mut string = String::with_capacity(sh.len() * 2);
    for v in sh {
        write!(&mut string, "{:02x}", v).unwrap();
    }
    string
}

/// Converts a hex string to bytes.
pub fn hex_to_bytes(string: &str) -> E3Result<Vec<u8>> {
    if string.len() & 1 == 1 {
        Err(E3Err::of("Incorrect number of digits for byte string"))
    } else {
        let mut data = vec![0; string.len() >> 1];
        for i in 0..data.len() {
            data[i] = u8::from_str_radix(&string[i * 2..(i * 2) + 2], 16).e3("hex digit")?;
        }
        Ok(data)
    }
}

/// Converts a SystemTime to a Unix time.
pub fn systemtime_to_unix_time(time: SystemTime) -> i64 {
    if let Ok(later_than_epoch) = time.duration_since(SystemTime::UNIX_EPOCH) {
        later_than_epoch.as_secs() as i64
    } else if let Ok(before_epoch) =
        SystemTime::UNIX_EPOCH.duration_since(time - Duration::from_secs(1))
    {
        before_epoch.as_secs() as i64
    } else {
        panic!("Impossible time");
    }
}

/// Converts a Unix time to a SystemTime.
pub fn unix_time_to_systemtime(time: i64) -> SystemTime {
    if time >= 0 {
        SystemTime::UNIX_EPOCH + Duration::from_secs(time as u64)
    } else {
        SystemTime::UNIX_EPOCH - Duration::from_secs((-time) as u64)
    }
}

/// The e3q macro unwraps an expression, attaching backtrace information.
#[macro_export]
macro_rules! e3q {
    ($e:expr) => {
        $e.e3(concat!(file!(), ":", line!(), ":", column!()))?
    };
}
