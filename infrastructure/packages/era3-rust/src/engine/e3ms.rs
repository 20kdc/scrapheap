//! era3 management server interface module

use std::{
    collections::HashSet,
    net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, UdpSocket},
    time::Duration,
};

use super::{prelude::*, HOSTNAME};

/// Device information.
pub trait E3MSDevice {
    /// Gets the hostname of the device.
    /// This is assumed to be found during discovery.
    fn get_hostname(&self) -> String;
    /// Gets the IP address of the device.
    /// This is assumed to be found during discovery.
    fn get_ip(&self) -> IpAddr;
    /// Gets the E3MS version.
    fn get_version(&self) -> E3Result<String>;
    /// Gets the primary device flag.
    fn get_primary_device_flag(&self) -> E3Result<bool>;
    /// Sets the primary device flag.
    /// This may require root access for E3MSLocalRoot.
    fn set_primary_device_flag(&self, val: bool) -> E3Result<()>;
}

/// Local device (as root).
pub struct E3MSLocalRoot;

impl E3MSDevice for E3MSLocalRoot {
    fn get_hostname(&self) -> String {
        HOSTNAME.0.to_string()
    }
    fn get_ip(&self) -> IpAddr {
        IpAddr::V6(Ipv6Addr::LOCALHOST)
    }
    fn get_version(&self) -> E3Result<String> {
        Ok(crate::engine::HOSTNAME.to_string())
    }
    fn get_primary_device_flag(&self) -> E3Result<bool> {
        Ok(std::path::Path::new("/etc/era3-primary-device").exists())
    }
    fn set_primary_device_flag(&self, val: bool) -> E3Result<()> {
        if !val {
            E3Err::result_of(std::fs::remove_file("/etc/era3-primary-device"))
        } else {
            E3Err::result_of(std::fs::write("/etc/era3-primary-device", []))
        }
    }
}

/// Remote (or seemingly remote) device.
pub struct E3MSRemote(pub String, pub IpAddr);

impl E3MSDevice for E3MSRemote {
    fn get_hostname(&self) -> String {
        self.0.clone()
    }
    fn get_ip(&self) -> IpAddr {
        self.1
    }
    fn get_version(&self) -> E3Result<String> {
        Err(E3Err::of("Not yet implemented"))
    }
    fn get_primary_device_flag(&self) -> E3Result<bool> {
        Err(E3Err::of("Not yet implemented"))
    }
    fn set_primary_device_flag(&self, _val: bool) -> E3Result<()> {
        Err(E3Err::of("Not yet implemented"))
    }
}

impl E3MSRemote {
    /// Get the local device as a remote device.
    /// This is useful for non-elevated processes that want to invoke set_primary_device_flag via E3MS.
    pub fn local() -> E3MSRemote {
        E3MSRemote(E3MSLocalRoot.get_hostname(), E3MSLocalRoot.get_ip())
    }
    /// Gets a remote device by hostname using the IPv6 internal network.
    /// Beware: If a discovered address exists, you should prefer it.
    pub fn hostname_ipv6(hostname: &str) -> E3MSRemote {
        E3MSRemote(
            hostname.to_string(),
            IpAddr::V6(super::net::hostname_to_ipv6(hostname)),
        )
    }
}

/// Scans the network for E3MS devices.
/// For some reason this doesn't work right now (can't send beacons).
/// Might be due to issues with Rust stdlib (SO_REUSEADDR, anyone?)
pub fn scan() -> Vec<E3MSRemote> {
    // this is weirdly broken for no discernible reason
    let udp = UdpSocket::bind(SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, 0)));
    let mut res = vec![];
    let mut set: HashSet<String> = HashSet::new();
    let mut buf: [u8; 1024] = [0; 1024];
    if let Ok(udp) = udp {
        _ = udp.set_broadcast(true);
        _ = udp.set_read_timeout(Some(Duration::from_millis(100)));
        for _i in 0..15 {
            _ = udp.send_to(
                b"Request\n",
                SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::BROADCAST, 20530)),
            );
            for _j in 0..2 {
                loop {
                    if let Ok(rcv) = udp.recv_from(&mut buf) {
                        let data = &buf[0..rcv.0];
                        let hostname = String::from_utf8_lossy(data).trim().to_string();
                        if set.insert(hostname.clone()) {
                            res.push(E3MSRemote(hostname, rcv.1.ip()));
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }
    res
}
