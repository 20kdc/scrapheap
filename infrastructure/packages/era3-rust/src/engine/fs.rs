//! Filesystem utilties (and dragons)

use std::{ffi::CStr, os::fd::AsRawFd};

use super::libc;

use std::path::{Path, PathBuf};

use super::prelude::*;

/// Extended functions for file descriptors.
pub trait XAttr {
    fn set_xattr(&self, name: &CStr, value: &[u8], flags: i32) -> std::io::Result<()>;
    fn get_xattr(&self, name: &CStr, value: &mut [u8]) -> std::io::Result<usize>;
    fn remove_xattr(&self, name: &CStr) -> std::io::Result<()>;
    fn ftruncate(&self, len: i64) -> std::io::Result<()>;
}

impl<T: AsRawFd> XAttr for T {
    fn set_xattr(&self, name: &CStr, value: &[u8], flags: i32) -> std::io::Result<()> {
        unsafe {
            if libc::fsetxattr(
                self.as_raw_fd(),
                name.as_ptr(),
                value.as_ptr(),
                value.len(),
                flags,
            ) < 0
            {
                Err(std::io::Error::last_os_error())
            } else {
                Ok(())
            }
        }
    }
    fn get_xattr(&self, name: &CStr, value: &mut [u8]) -> std::io::Result<usize> {
        unsafe {
            let res = libc::fgetxattr(
                self.as_raw_fd(),
                name.as_ptr(),
                value.as_mut_ptr(),
                value.len(),
            );
            if res < 0 {
                Err(std::io::Error::last_os_error())
            } else {
                Ok(res as usize)
            }
        }
    }
    fn remove_xattr(&self, name: &CStr) -> std::io::Result<()> {
        unsafe {
            if libc::fremovexattr(self.as_raw_fd(), name.as_ptr()) < 0 {
                Err(std::io::Error::last_os_error())
            } else {
                Ok(())
            }
        }
    }
    fn ftruncate(&self, len: i64) -> std::io::Result<()> {
        unsafe {
            if libc::ftruncate64(self.as_raw_fd(), len) < 0 {
                Err(std::io::Error::last_os_error())
            } else {
                Ok(())
            }
        }
    }
}

/// When dropped, the associated path is deleted.
pub struct RemoveFileOnDrop(pub Option<PathBuf>);

impl RemoveFileOnDrop {
    /// Defuses this instance so that it drops without removing the file.
    pub fn defuse(mut self) {
        self.0 = None;
    }

    /// Renames the file to a target location *or* removes it.
    /// Asserts that this instance is not currently being defused (how would this happen idk)
    pub fn rename_or_remove(mut self, new_path: impl AsRef<Path>) -> E3Result<()> {
        // on failure, drop happens automatically
        std::fs::rename(self.0.as_ref().unwrap(), new_path.as_ref())
            .e3("while renaming file to destination")?;
        self.0 = None;
        Ok(())
    }
}

impl<T: AsRef<Path>> From<T> for RemoveFileOnDrop {
    fn from(value: T) -> Self {
        Self(Some(PathBuf::from(value.as_ref())))
    }
}

impl Drop for RemoveFileOnDrop {
    fn drop(&mut self) {
        if let Some(path) = &self.0 {
            _ = std::fs::remove_file(path);
        }
    }
}

/// Synchronizes all writes everywhere to disk.
pub fn sync() {
    unsafe {
        libc::sync();
    }
}
