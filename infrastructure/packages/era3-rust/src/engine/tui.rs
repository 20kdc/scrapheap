//! utilities for long-running tools and TUIs

use std::{
    cell::Cell,
    fs::File,
    io::{IsTerminal, Stderr, StderrLock, Stdin, StdinLock, Stdout, StdoutLock, Write},
    os::fd::{AsRawFd, BorrowedFd, OwnedFd, RawFd},
};

use super::libc;

/// Operations on terminals.
pub trait TerminalSize: IsTerminal {
    /// Size as (width, height).
    /// May provide an 'advisory' value on failure.
    fn terminal_size(&self) -> (usize, usize);

    /// Width / columns.
    fn terminal_width(&self) -> usize {
        self.terminal_size().0
    }

    /// Height / rows.
    fn terminal_height(&self) -> usize {
        self.terminal_size().1
    }
}

/// Operations on terminals.
pub trait TerminalOps: TerminalSize + Write {
    /// If the target is a terminal, attempts to clear it.
    fn cls(&mut self) {
        if self.is_terminal() {
            _ = self.write_all("\x1Bc".as_bytes());
            _ = self.flush();
        }
    }

    /// If the target is a terminal, attmepts to write a status indicator.
    /// Failing this, the status is written as a regular line.
    fn status(&mut self, text: &str) {
        if self.is_terminal() {
            let columns = self.terminal_width() - text_width(text);
            _ = self.write_fmt(format_args!(
                "\x1B[K\x1B[s\r\x1B[{}C{}\x1B[u",
                columns, text
            ));
            _ = self.flush();
        } else {
            _ = self.write_fmt(format_args!("{}\n", text));
            _ = self.flush();
        }
    }
}

pub fn text_width(text: &str) -> usize {
    text.chars().count()
}

const SPINNER_TABLE: &'static [&'static str] = &["  ", "󱥳 ", "󱥮 ", "󱥮󱥳", "󱥮󱥮"];

/// Spinner. This provides, essentially, reassurance.
#[derive(Clone)]
pub struct Spinner(Cell<usize>);

impl Spinner {
    pub fn new() -> Spinner {
        Spinner(Cell::new(0))
    }

    pub fn get(&self) -> &'static str {
        let mut num = self.0.get();
        let res = SPINNER_TABLE[num];
        num += 1;
        if num == SPINNER_TABLE.len() {
            num = 0;
        }
        self.0.set(num);
        res
    }
}

/// Gets the terminal size for a RawFd.
pub fn terminal_size_for_fd(fd: RawFd) -> (usize, usize) {
    let mut winsize = libc::Winsize::default();
    unsafe {
        if libc::ioctl(
            fd,
            libc::TIOCGWINSZ,
            (&mut winsize) as *mut libc::Winsize as usize,
        ) == 0
        {
            (winsize.ws_col as usize, winsize.ws_row as usize)
        } else {
            (80, 25)
        }
    }
}

// TerminalSize for all IsTerminal impls

impl TerminalSize for File {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for BorrowedFd<'_> {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for OwnedFd {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for Stderr {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for StderrLock<'_> {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for Stdin {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for StdinLock<'_> {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for Stdout {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

impl TerminalSize for StdoutLock<'_> {
    fn terminal_size(&self) -> (usize, usize) {
        terminal_size_for_fd(self.as_raw_fd())
    }
}

// TerminalOps where possible

impl TerminalOps for File {}
impl TerminalOps for StderrLock<'_> {}
impl TerminalOps for StdoutLock<'_> {}

/// Status in a more convenient package.
pub fn status(status: &str) {
    std::io::stderr().lock().status(status);
}
