//! since we can't use the actual libc crate, libc definitions go here

use std::ffi::c_char;

use super::prelude::*;

pub const AF_INET: i32 = 2;
pub const AF_INET6: i32 = 10;

pub const O_NOFOLLOW: i32 = 0400000;

pub const TIOCGWINSZ: i32 = 0x5413;

#[repr(C)]
#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub struct Winsize {
    pub ws_row: u16,
    pub ws_col: u16,
    pub ws_xpixel: u16,
    pub ws_ypixel: u16,
}

unsafe impl SafeCStruct for Winsize {}

extern "C" {
    pub fn socket(family: i32, socktype: i32, protocol: i32) -> i32;

    pub fn fsetxattr(
        fd: i32,
        name: *const c_char,
        value: *const u8,
        size: usize,
        flags: i32,
    ) -> i32;
    pub fn fgetxattr(fd: i32, name: *const c_char, value: *mut u8, size: usize) -> isize;
    pub fn fremovexattr(fd: i32, name: *const c_char) -> i32;
    pub fn ftruncate64(fd: i32, size: i64) -> i32;

    pub fn ioctl(fd: i32, kind: i32, arb: usize) -> i32;

    pub fn sync();
}
