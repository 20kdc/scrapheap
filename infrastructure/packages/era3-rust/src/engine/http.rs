//! HTTP structs and stuff.

use std::io::Write;

/// The HTTP request.
pub struct HTTPRequest {
    pub method: String,
    pub path: String,
    pub query: String,
    pub headers: IMFHeaders,
    pub body: Vec<u8>,
}

impl HTTPRequest {
    /// Sends the request. Failures are ignored.
    pub fn write(&self, target: &mut impl Write) {
        if self.query.is_empty() {
            _ = writeln!(target, "{} {} HTTP/1.1\r", self.method, self.path);
        } else {
            _ = writeln!(
                target,
                "{} {}?{} HTTP/1.1\r",
                self.method, self.path, self.query
            );
        }
        write_imf_headers(&self.headers, target);
        _ = target.write_all(&self.body);
    }
}

/// The HTTP response.
pub struct HTTPResponse {
    pub code: u32,
    pub message: String,
    pub headers: IMFHeaders,
    pub body: Vec<u8>,
}

impl HTTPResponse {
    /// Sends the response. Failures are ignored.
    pub fn write(&self, target: &mut impl Write) {
        _ = writeln!(target, "HTTP/1.1 {} {}\r", self.code, self.message);
        write_imf_headers(&self.headers, target);
        _ = target.write_all(&self.body);
    }
}

/// A single IMF header.
pub type IMFHeader = (String, String);

/// A list of IMF headers.
pub type IMFHeaders = Vec<IMFHeader>;

/// Sends IMF headers. Failures are ignored.
pub fn write_imf_headers(headers: &[IMFHeader], target: &mut impl Write) {
    for v in headers {
        _ = writeln!(target, "{}: {}\r", v.0, v.1);
    }
    _ = writeln!(target, "\r");
}
