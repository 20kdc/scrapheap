//! Networking utilities, including the IPv6 address arrangement and raw IP sockets.

use std::net::{IpAddr, Ipv6Addr, ToSocketAddrs, UdpSocket};
use std::os::fd::FromRawFd;

use super::libc;

/// Resolves an unknown host if it is a valid era3 hostname.
/// Otherwise, pass-through. This is a textual operation.
pub fn resolve(host: impl AsRef<str>) -> String {
    let host = host.as_ref();
    if host.contains('.') || host.contains(':') {
        host.to_string()
    } else {
        hostname_to_ipv6(host).to_string()
    }
}

/// Resolve, but to IP addresses.
pub fn resolve_ips(host: impl AsRef<str>) -> std::io::Result<Vec<IpAddr>> {
    let host = host.as_ref();
    if host.contains('.') || host.contains(':') {
        // This ":0" is a workaround for Rust weirdly being unable to lookup IP addresses.
        let expanded = format!("{}:0", host);
        expanded
            .to_socket_addrs()
            .map(|v| v.map(|v| v.ip()).collect())
    } else {
        Ok(vec![IpAddr::V6(hostname_to_ipv6(host))])
    }
}

/// Transforms a hostname to an IPv6 address.
pub fn hostname_to_ipv6(hostname: impl AsRef<str>) -> Ipv6Addr {
    let mut x = 0u8;
    let mut y = 0u8;
    for chr in hostname.as_ref().bytes() {
        for _round in 0..32 {
            x = (x >> 1) | (x << 7);
            x = x.wrapping_add(y);
            x ^= chr & 0xDF;
            y = (y << 1) | (y >> 7);
            y ^= x;
        }
    }
    let host_id = ((x as u16) << 8) | (y as u16);
    Ipv6Addr::new(0xFDE3, 0, 0, 0, 0, 0, host_id, 0x0001)
}

/// Runs the internet checksum operation on a 16-bit word.
pub fn checksum(i: u16, w: u16) -> u16 {
    let mut v = i.wrapping_add(w);
    if v < i {
        // can't wrap because i exists'
        v += 1;
    }
    v
}

/// Runs the internet checksum operation on data as per how it'd be done for e.g. UDP.
/// This means an extra zero padding byte is added if necessary.
pub fn checksum_data(mut i: u16, data: &[u8]) -> u16 {
    for word_idx in 0..(data.len() / 2) {
        let x = data[word_idx * 2];
        let y = data[(word_idx * 2) + 1];
        i = checksum(i, ((x as u16) << 8) | (y as u16));
    }
    if (data.len() & 1) == 1 {
        let x = data[data.len() - 1];
        i = checksum(i, (x as u16) << 8);
    }
    i
}

/// Invokes magicks to create a 'UDP' socket for sending IP packets.
/// [UdpSocket] is chosen here to avoid much ado about nothing.
/// The socket is effectively a 'UDP' socket; with port numbers ignored.
/// _Right now the actual binding is unimplemented._
pub fn bind_raw(addr: IpAddr, protocol: u8) -> std::io::Result<UdpSocket> {
    let af = match addr {
        IpAddr::V4(_) => libc::AF_INET,
        IpAddr::V6(_) => libc::AF_INET6,
    };
    // af, SOCK_RAW, protocol
    let res = unsafe { libc::socket(af, 3, protocol as i32) };
    if res < 0 {
        Err(std::io::Error::last_os_error())
    } else {
        Ok(unsafe { UdpSocket::from_raw_fd(res) })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test() {
        assert_eq!(
            hostname_to_ipv6(""),
            Ipv6Addr::new(0xFDE3, 0, 0, 0, 0, 0, 0, 1)
        );
        assert_eq!(
            hostname_to_ipv6("A"),
            Ipv6Addr::new(0xFDE3, 0, 0, 0, 0, 0, 0x56D4, 1)
        );
        assert_eq!(
            hostname_to_ipv6("Trevize"),
            Ipv6Addr::new(0xFDE3, 0, 0, 0, 0, 0, 0xB0D8, 1)
        );
    }
}
