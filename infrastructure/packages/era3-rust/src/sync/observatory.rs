//! The Observatory bridges the Workspace and the Database.
//! It contains the functions that perform the file-level operations to transfer data between the two (i.e. during commit and db2index phases).
//! It does *not* contain dir/file conflict resolution.

use std::{
    collections::{HashMap, HashSet},
    ffi::CString,
    fmt::Display,
    fs::{File, FileTimes, Permissions},
    io::Write,
    os::unix::{
        ffi::OsStrExt,
        fs::{MetadataExt, PermissionsExt},
    },
    path::{Path, PathBuf},
    str::FromStr,
    time::{Duration, SystemTime},
};

use crate::{
    e3q,
    engine::{
        fs::XAttr,
        prelude::*,
        threaded_task_manager::{Task, TaskManager, TaskPool},
    },
};

use super::{
    blobs::{SyncBlobIndex, SyncBlobPool, SyncHash, SYNC_HASH_LEN},
    cowbarbeque::{CowBarbequeHeader, COWBARBEQUE_HEADER_LEN},
    database::{SyncDBIndexEntry, SyncDBROOps, SyncDBRW, FLAG_EXECUTABLE, FLAG_SYMLINK},
};

// -- Observation --

/// Sync file path - always starts with / and only ends with / for the root
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct SyncPath(String);

impl FromStr for SyncPath {
    type Err = E3Err;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !s.starts_with("/") {
            Err(E3Err::of(format!(
                "SyncPath must start with '/' - '{}' does not",
                s
            )))
        } else if s.contains("//") {
            Err(E3Err::of(format!(
                "SyncPath must not contain // - '{}' does",
                s
            )))
        } else if s.contains("/../") {
            Err(E3Err::of(format!(
                "SyncPath must not contain /../ - '{}' does",
                s
            )))
        } else if s.contains("/./") {
            Err(E3Err::of(format!(
                "SyncPath must not contain /./ - '{}' does",
                s
            )))
        } else if s.ends_with("/.") {
            Err(E3Err::of(format!(
                "SyncPath must not end in /. - '{}' does",
                s
            )))
        } else if s.ends_with("/..") {
            Err(E3Err::of(format!(
                "SyncPath must not end in /.. - '{}' does",
                s
            )))
        } else {
            Ok(SyncPath(s.to_string()))
        }
    }
}

impl SyncPath {
    /// Creates the root SyncPath.
    pub fn root() -> SyncPath {
        SyncPath("/".to_string())
    }

    /// Returns the parent component.
    pub fn parent(&self) -> Option<SyncPath> {
        if self.0.eq("/") {
            None
        } else {
            let last_slash = self.0.rfind("/").expect("/ must be in a valid SyncPath");
            if last_slash == 0 {
                Some(SyncPath::root())
            } else {
                Some(SyncPath((&self.0[0..last_slash]).to_string()))
            }
        }
    }

    /// Gets the basename (last component text) of this SyncPath.
    pub fn basename(&self) -> &str {
        let last_slash = self.0.rfind("/").expect("/ must be in a valid SyncPath");
        &self.0[last_slash + 1..]
    }

    /// Appends a file component to a SyncPath.
    pub fn join(&self, component: &str) -> SyncPath {
        assert!(
            !component.contains("/"),
            "SyncPath components are not expected to contain slashes."
        );
        if self.0.eq(&"/") {
            SyncPath(format!("/{}", component))
        } else {
            SyncPath(format!("{}/{}", self.0, component))
        }
    }

    /// Concatenates SyncPaths.
    pub fn cat(&self, other: &SyncPath) -> SyncPath {
        if self.0.eq(&"/") {
            other.clone()
        } else {
            SyncPath(format!("{}{}", self.0, other.0))
        }
    }

    /// To a real path (given a target)
    pub fn to_real_path(&self, base: impl AsRef<Path>) -> PathBuf {
        base.as_ref().join(&self.0[1..])
    }
}

impl Display for SyncPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl AsRef<str> for SyncPath {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

/// Observed inode.
/// For DB observations, this structure is guaranteed to contain everything needed to project the inode in the workspace.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum SyncObservedInode {
    CleanFile {
        /// File's precise, known hash.
        blob: SyncHash,
        /// The file's flags. See FLAG_* constants.
        flags: i64,
        /// The update time. This is used during workspace realization.
        update_time: i64,
        /// The length. This is used during workspace realization.
        length: u64,
    },
    DirtyFile {
        /// The file's flags. See FLAG_* constants.
        flags: i64,
        /// The update time. This may be useful for UX but is not to be relied upon.
        update_time: i64,
        /// The length. May help with progress tracking.
        length: u64,
    },
    Symlink(PathBuf),
}

impl SyncObservedInode {
    /// If this Inode would import/create a blob.
    pub fn needs_blob_import(&self) -> bool {
        match self {
            Self::CleanFile {
                blob: _,
                flags: _,
                update_time: _,
                length: _,
            } => false,
            Self::DirtyFile {
                flags: _,
                update_time: _,
                length: _,
            } => true,
            Self::Symlink(_) => true,
        }
    }
    /// Gets the amount of bytes of progress for this record.
    pub fn progress_contribution(&self) -> u64 {
        match self {
            Self::DirtyFile {
                flags: _,
                update_time: _,
                length,
            } => *length,
            _ => 0,
        }
    }
    /// Exports the file to the workspace.
    pub fn project(&self, real_location: impl AsRef<Path>) -> E3Result<()> {
        match self {
            Self::CleanFile {
                blob,
                flags,
                update_time,
                length,
            } => {
                let update_time = SystemTime::UNIX_EPOCH + Duration::from_secs(*update_time as u64);
                let mut cb_header = CowBarbequeHeader::default();
                let text = format!("../blob/{}", bytes_to_hex_string(blob));
                cb_header.sourcefile[0..text.len()].copy_from_slice(text.as_bytes());
                cb_header.extents[0].len = *length;
                let mut file = File::create(real_location).e3("error creating file")?;
                file.write_all(safecstruct_as_bytes(&cb_header))
                    .e3("error writing header")?;
                file.ftruncate((length + COWBARBEQUE_HEADER_LEN) as i64)
                    .e3("error with ftruncate")?;
                file.set_times(
                    FileTimes::new()
                        .set_accessed(update_time)
                        .set_modified(update_time),
                )
                .e3("error setting times")?;
                let attr = CString::new("user.e3scow").unwrap();
                file.set_xattr(&attr, text.as_bytes(), 0)
                    .e3("error setting xattr")?;
                let mode: u32;
                if flags & FLAG_EXECUTABLE != 0 {
                    mode = 0o775;
                } else {
                    mode = 0o664;
                }
                file.set_permissions(Permissions::from_mode(mode))
                    .e3("error setting perms")?;
                Ok(())
            }
            Self::DirtyFile {
                flags: _,
                update_time: _,
                length: _,
            } => {
                panic!("Cannot project DirtyFile");
            }
            Self::Symlink(path) => std::os::unix::fs::symlink(path, real_location).e3("symlink"),
        }
    }
    /// Creates the DB record structure.
    /// TaskManager is used to pipeline syncs.
    pub fn add_as_db_record(
        &self,
        host: String,
        real_location_in_media_space: impl AsRef<Path>,
        virtual_location: SyncPath,
        update_time: i64,
        blobs: &SyncBlobPool,
        db: &SyncDBRW,
        progress: impl FnMut(u64),
        ttm: &mut impl TaskManager,
        tasks: &mut Vec<Task<E3Result<()>>>,
    ) -> E3Result<()> {
        let index_blob: SyncHash;
        let index_flags: i64;

        match self {
            Self::CleanFile {
                blob,
                flags,
                update_time: _,
                length: _,
            } => {
                index_blob = *blob;
                index_flags = *flags;
            }
            Self::DirtyFile {
                flags,
                update_time: _,
                length: _,
            } => {
                let f = File::open(real_location_in_media_space).e3("Opening mountpoint file")?;
                let blob_info = blobs.import_blob(f, progress, ttm, tasks)?;
                index_blob = blob_info.0;
                db.ensure_blob_length(blob_info.0, blob_info.1)?;
                index_flags = *flags;
            }
            Self::Symlink(path) => {
                index_flags = FLAG_SYMLINK;
                let blob_info = blobs
                    .import_blob_from_slice(path.clone().into_os_string().as_bytes(), ttm, tasks)
                    .e3("importing blob")?;
                db.ensure_blob_length(blob_info.0, blob_info.1)?;
                index_blob = blob_info.0;
            }
        }

        db.ensure_path(&virtual_location.0)?;

        let entry = SyncDBIndexEntry {
            host,
            path: virtual_location.0,
            blob: Some(index_blob),
            flags: index_flags,
            update_time,
        };

        db.write_index_entry(&entry)
    }
}

/// Observation set, mapping [SyncPath] to [SyncObservedInode].
/// This is enough data to reconstruct ([SyncObservedInode::project]) the entire workspace.
pub type SyncObservationSet = HashMap<SyncPath, SyncObservedInode>;

/// Observation results indicate what's different between the observations.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum SyncObservationResult {
    Untouched,
    /// Added (needs_blob_import) - added files which don't need blob import have been renamed
    Added(bool),
    /// Changed (needs_blob_import) - changed files which don't need blob import have been swapped
    Changed(bool),
    Removed,
}

/// Diff mapping [SyncPath] to [SyncObservationResult].
pub type SyncObservationDiff = HashMap<SyncPath, SyncObservationResult>;

/// Implements the observation diff algorithm used for planning.
/// See also sync.py observation_diffsets
pub fn observation_diffsets(
    from: &SyncObservationSet,
    to: &SyncObservationSet,
) -> SyncObservationDiff {
    let mut diff = SyncObservationDiff::new();
    for (path, r_from) in from.iter() {
        let r_to = to.get(path);
        if let Some(r_to) = r_to {
            if r_from.ne(r_to) {
                diff.insert(
                    path.clone(),
                    SyncObservationResult::Changed(r_to.needs_blob_import()),
                );
            } else {
                diff.insert(path.clone(), SyncObservationResult::Untouched);
            }
        } else {
            diff.insert(path.clone(), SyncObservationResult::Removed);
        }
    }
    for (path, r_to) in to.iter() {
        if !from.contains_key(path) {
            diff.insert(
                path.clone(),
                SyncObservationResult::Added(r_to.needs_blob_import()),
            );
        }
    }
    diff
}

/// Gets all blobs in the observation set.
pub fn observation_blobs(set: &SyncObservationSet) -> HashSet<SyncHash> {
    let mut blobs: HashSet<SyncHash> = HashSet::new();
    for v in set.values() {
        match v {
            SyncObservedInode::CleanFile {
                blob,
                flags: _,
                update_time: _,
                length: _,
            } => {
                blobs.insert(*blob);
            }
            _ => {}
        }
    }
    blobs
}

/// Much of the disk IO runs off-thread, but this means certain lookups like blob length have to run late.
/// So data is collated here before it's finally determined what is what.
enum ObservationFileTaskOut {
    File {
        length: u64,
        flags: i64,
        update_time: i64,
        unmodified_path: Option<String>,
    },
    Symlink(PathBuf),
}

/// Observes a workspace inode. See also sync.py observe_workspace_file
fn observe_workspace_inode_task(path: &Path) -> E3Result<ObservationFileTaskOut> {
    let info = std::fs::symlink_metadata(&path).e3(format!("file unstatable: {:?}", path))?;
    if info.is_symlink() {
        let read = std::fs::read_link(&path).e3(format!("symlink unreadable: {:?}", path))?;
        Ok(ObservationFileTaskOut::Symlink(read))
    } else {
        // since it's not a symlink, any symlink-following must precede this path (which wouldn't happen)
        let file = File::open(&path).e3(format!("file unopenable: {:?}", path))?;
        let st_mode = info.permissions().mode();
        let mut flags: i64 = 0;
        if (st_mode & 0o100) != 0 {
            flags |= FLAG_EXECUTABLE;
        }
        let mut source_path: [u8; 127] = [0; 127];
        let source_path = if let Ok(len) =
            file.get_xattr(&CString::new("user.e3scow").unwrap(), &mut source_path)
        {
            Some(String::from_utf8_lossy(&source_path[0..len]).into_owned())
        } else {
            None
        };
        Ok(ObservationFileTaskOut::File {
            length: info.len(),
            flags,
            update_time: info.mtime(),
            unmodified_path: source_path,
        })
    }
}

/// Observes a workspace inode. See also sync.py observe_workspace_file
fn observe_workspace_inode_end(
    res: ObservationFileTaskOut,
    blobs: &dyn SyncBlobIndex,
) -> SyncObservedInode {
    match res {
        ObservationFileTaskOut::File {
            length,
            flags,
            update_time,
            unmodified_path,
        } => {
            if let Some(path) = &unmodified_path {
                if let Some(blob_hash_hex) = path.strip_prefix("../blob/") {
                    let supposed_hash = hex_to_bytes(blob_hash_hex);
                    if let Ok(supposed_hash_data) = supposed_hash {
                        if supposed_hash_data.len() == SYNC_HASH_LEN {
                            let mut hash: SyncHash = SyncHash::default();
                            hash.copy_from_slice(&supposed_hash_data);
                            let expected_len = blobs.blob_length(hash);
                            if let Ok(expected_len) = expected_len {
                                if (expected_len + COWBARBEQUE_HEADER_LEN) == length {
                                    return SyncObservedInode::CleanFile {
                                        blob: hash,
                                        flags,
                                        update_time,
                                        length: expected_len,
                                    };
                                }
                            }
                        }
                    }
                }
            }
            SyncObservedInode::DirtyFile {
                flags,
                update_time,
                length: if length >= COWBARBEQUE_HEADER_LEN {
                    length - COWBARBEQUE_HEADER_LEN
                } else {
                    0
                },
            }
        }
        ObservationFileTaskOut::Symlink(target) => SyncObservedInode::Symlink(target),
    }
}

fn observe_workspace_inner(
    buffer: &mut Vec<Task<E3Result<(SyncPath, ObservationFileTaskOut)>>>,
    real_location: &Path,
    virtual_location: &SyncPath,
    blobs: &dyn SyncBlobIndex,
    ttm: &mut impl TaskManager,
) -> E3Result<()> {
    let metadata = std::fs::symlink_metadata(real_location).e3("during stat")?;
    if metadata.is_dir() {
        for file in std::fs::read_dir(real_location).e3("during read_dir")? {
            let file = file.e3("during read_dir loop")?;
            observe_workspace_inner(
                buffer,
                &file.path(),
                &virtual_location.join(file.file_name().to_str().unwrap()),
                blobs,
                ttm,
            )?;
        }
    } else {
        let vloc = virtual_location.clone();
        let rpath = real_location.to_path_buf();
        buffer.push(ttm.spawn(move || observe_workspace_inode_task(&rpath).map(|v| (vloc, v))));
    }
    Ok(())
}

/// Observes a workspace.
pub fn observe_workspace(
    root: impl AsRef<Path>,
    blobs: &dyn SyncBlobIndex,
) -> E3Result<SyncObservationSet> {
    let mut ttm: TaskPool<16> = Default::default();
    let mut tasks: Vec<Task<E3Result<(SyncPath, ObservationFileTaskOut)>>> = Vec::new();
    observe_workspace_inner(
        &mut tasks,
        root.as_ref(),
        &SyncPath::root(),
        blobs,
        &mut ttm,
    )?;
    let mut set = SyncObservationSet::new();
    for task in tasks {
        let out = e3q!(task.join());
        set.insert(out.0.clone(), observe_workspace_inode_end(out.1, blobs));
    }
    Ok(set)
}

/// Observes the state of the sync database for a given host.
/// The actual blob pool is needed for this to deal with symlinks.
pub fn observe_db(
    db: &impl SyncDBROOps,
    host: &str,
    pool: &SyncBlobPool,
) -> E3Result<SyncObservationSet> {
    let mut set = SyncObservationSet::new();
    let index = e3q!(db.read_index(Some(host), Some(false)));
    for stmt in index {
        let path: SyncPath = stmt
            .path
            .parse()
            .e3(&format!("observe_db path parse: {}", stmt.path))?;
        let blob = stmt.blob.unwrap();
        let inode = if stmt.flags & FLAG_SYMLINK != 0 {
            let blob_data = e3q!(pool.blob_data(blob));
            SyncObservedInode::Symlink(
                PathBuf::from_str(&String::from_utf8_lossy(&blob_data)).unwrap(),
            )
        } else {
            SyncObservedInode::CleanFile {
                blob,
                flags: stmt.flags,
                update_time: stmt.update_time,
                length: e3q!(db.blob_length(blob)),
            }
        };
        set.insert(path, inode);
    }
    Ok(set)
}

/// Regularizes conflicts in a sync observation set, creating a new set.
/// This is meant to process [observe_db] output before directories are cleaned up and [SyncObserveInode::project] is called.
pub fn regularize_db_conflicts(inset: SyncObservationSet) -> SyncObservationSet {
    let mut outset = SyncObservationSet::new();
    let mut paths: HashSet<SyncPath> = HashSet::new();
    paths.insert(SyncPath::root());

    // pass 1: dirs
    for (path, _) in &inset {
        let mut target = path.clone().parent().expect("must have a parent");
        while !paths.contains(&target) {
            paths.insert(target.clone());
            target = target.parent().unwrap();
        }
    }

    // pass 2: everything else
    for (path, value) in inset {
        let mut path = path.clone();
        while paths.contains(&path) {
            path = SyncPath::from_str(&format!("{}!", path)).unwrap();
        }
        paths.insert(path.clone());
        outset.insert(path, value);
    }

    outset
}
