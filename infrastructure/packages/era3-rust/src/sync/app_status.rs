//! Code for era3 sync core status.

use std::collections::HashSet;

use crate::{e3q, engine::prelude::*};

use super::{blobs::SyncHash, database::{index_to_hashmap, SyncDBROOps}};

pub fn contested_hosts(db: &impl SyncDBROOps, hostname: &str) -> E3Result<Vec<(String, u64)>> {
    let hosts: Vec<String> = e3q!(db.read_hosts()).into_keys().collect();
    let mut out_of_touch = Vec::new();
    let our_index = e3q!(index_to_hashmap(e3q!(db.read_index(Some(hostname), None))));
    for host in hosts {
        if host.eq(hostname) {
            continue;
        }
        // a host and it's not us
        let their_index = e3q!(index_to_hashmap(e3q!(db.read_index(Some(&host), None))));
        // index of blobs these hosts are known to have
        let mut blobs_ours: HashSet<SyncHash> = HashSet::new();
        let mut blobs_theirs: HashSet<SyncHash> = HashSet::new();
        for (_k, v) in &our_index {
            if let Some(blob) = v.blob {
                blobs_ours.insert(blob);
            }
        }
        for (_k, v) in &their_index {
            if let Some(blob) = v.blob {
                blobs_theirs.insert(blob);
            }
        }
        let mut any_to_transfer = false;
        // index of blobs that are important (i.e. that both hosts will have assuming the plan isn't modified)
        let mut blobs_important: HashSet<SyncHash> = HashSet::new();
        for (k, our_entry) in &our_index {
            let their_entry = their_index.get(k);
            if let Some(their_entry) = their_entry {
                if !our_entry.data_eq(their_entry) {
                    // guess what the planner will do
                    if our_entry.update_time > their_entry.update_time {
                        // our blob is ahead
                        if let Some(blob) = our_entry.blob {
                            blobs_important.insert(blob);
                        }
                    } else {
                        // their blob is ahead
                        if let Some(blob) = their_entry.blob {
                            blobs_important.insert(blob);
                        }
                    }
                    any_to_transfer = true;
                }
            } else {
                // our blob is ahead by default
                if let Some(blob) = our_entry.blob {
                    blobs_important.insert(blob);
                }
                any_to_transfer = true;
            }
        }
        // are we missing their entries?
        for (k, their_entry) in &their_index {
            if !our_index.contains_key(k) {
                // their blob is ahead by default
                if let Some(blob) = their_entry.blob {
                    blobs_important.insert(blob);
                }
                any_to_transfer = true;
            }
        }
        if any_to_transfer {
            // any blobs which aren't already shared between both machines probably need to be transferred
            // from this, estimate the upload/download
            let mut total_bytes_to_transfer: u64 = 0;
            for blob in blobs_important {
                if !(blobs_ours.contains(&blob) && blobs_theirs.contains(&blob)) {
                    total_bytes_to_transfer += e3q!(db.blob_length(blob));
                }
            }
            out_of_touch.push((host, total_bytes_to_transfer));
        }
    }
    Ok(out_of_touch)
}
