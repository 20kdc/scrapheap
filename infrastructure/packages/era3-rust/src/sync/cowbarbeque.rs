//! CowBarbeque format, used in the workspace.

use crate::engine::prelude::*;
use std::str::from_utf8;

pub const COWBARBEQUE_HEADER_LEN: u64 = 4096;
pub const COWBARBEQUE_WS_EXTENT_COUNT: usize = 248;

#[derive(Clone, Copy, PartialEq, Eq, Default)]
#[repr(C)]
pub struct CowBarbequeExtent {
    pub start: u64,
    pub len: u64,
}

#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct CowBarbequeHeader {
    pub sourcefile: [u8; 127],
    pub zero: u8,
    pub extents: [CowBarbequeExtent; COWBARBEQUE_WS_EXTENT_COUNT],
}

impl Default for CowBarbequeHeader {
    fn default() -> Self {
        Self {
            sourcefile: [0; 127],
            zero: 0,
            extents: [CowBarbequeExtent::default(); COWBARBEQUE_WS_EXTENT_COUNT],
        }
    }
}

unsafe impl SafeCStruct for CowBarbequeHeader {}

impl CowBarbequeHeader {
    /// Source file as a slice.
    pub fn source_file_slice(&self) -> &[u8] {
        &self.sourcefile[..self
            .sourcefile
            .iter()
            .enumerate()
            .find(|v| *v.1 == 0)
            .map(|v| v.0)
            .unwrap_or(127)]
    }

    /// Source file as a string.
    pub fn source_file_str(&self) -> &str {
        from_utf8(self.source_file_slice()).expect("corrupted CowBarbeque")
    }
}
