//! Kind-of an ORM for the database.

use std::collections::HashMap;

use crate::{
    e3q,
    engine::{prelude::*, sqlite3},
};

use super::{
    blobs::{SyncBlobIndex, SyncBlobPool, SyncHash},
    observatory::SyncPath,
};

// -- Flags --

/// The inode is marked executable.
pub const FLAG_EXECUTABLE: i64 = 1;
/// The inode is a symlink. This is DB-internal.
pub const FLAG_SYMLINK: i64 = 2;

// -- SyncDB --

/// Trait which contains the initialization logic for SyncDB.
pub trait SyncDBInit: Sized {
    const READONLY: bool;

    /// Creates from an initialized DB connection.
    fn from_initialized_connection(conn: sqlite3::Connection) -> Self;

    /// Sets up the DB.
    fn from_connection(conn: sqlite3::Connection, sandbox: bool) -> E3Result<Self> {
        e3q!(conn.execute_script(
            "
        PRAGMA foreign_keys = ON;
        PRAGMA trusted_schema = OFF;
        ",
        ));
        if sandbox {
            conn.limit(sqlite3::LIMIT_ATTACHED, 0);
        }
        Ok(Self::from_initialized_connection(conn))
    }

    /// Sets up the DB automatically.
    fn connect() -> E3Result<Self> {
        let conn = if Self::READONLY {
            e3q!(sqlite3::open("file:metadata.db", sqlite3::OPEN_READONLY))
        } else {
            e3q!(sqlite3::open("file:metadata.db", sqlite3::OPEN_READWRITE))
        };
        Self::from_connection(conn, false)
    }
}

/// Sync
#[derive(Clone)]
pub struct SyncDBIndexEntry {
    pub host: String,
    pub path: String,
    pub blob: Option<SyncHash>,
    pub flags: i64,
    pub update_time: i64,
}

impl SyncDBIndexEntry {
    const SELECT_SQL: &'static str =
        "SELECT host, path, blob, executable, update_time FROM \"index\"";
    fn from_stmt(stmt: &sqlite3::Statement) -> E3Result<Vec<SyncDBIndexEntry>> {
        let mut res = vec![];
        for step in stmt {
            let step = e3q!(step);
            let blob: Option<SyncHash> = if step.column_type(2) == sqlite3::ColumnType::Null {
                None
            } else {
                let mut blob: SyncHash = [0; 32];
                blob.copy_from_slice(&step.column_blob(2));
                Some(blob)
            };
            res.push(SyncDBIndexEntry {
                host: step.column_text(0),
                path: step.column_text(1),
                blob,
                flags: step.column_int(3),
                update_time: step.column_int(4),
            });
        }
        Ok(res)
    }

    /// If the data (not counting host/path) is equivalent.
    pub fn data_eq(&self, other: &SyncDBIndexEntry) -> bool {
        self.blob.eq(&other.blob)
            && self.flags == other.flags
            && self.update_time == other.update_time
    }
}

/// Read-only operations for SyncDB.
pub trait SyncDBROOps: SyncBlobIndex {
    /// Get at the DB connection.
    fn connection(&self) -> &sqlite3::Connection;

    /// Reads the host table.
    fn read_hosts(&self) -> E3Result<HashMap<String, i64>> {
        let mut map = HashMap::new();
        let stmt = e3q!(self
            .connection()
            .prepare("SELECT \"host\", \"update_time\" FROM hosts"));
        for step in &stmt {
            let step = e3q!(step);
            map.insert(step.column_text(0), step.column_int(1));
        }
        Ok(map)
    }

    /// Read the index with some conditions.
    fn read_index(
        &self,
        host: Option<&str>,
        deletion: Option<bool>,
    ) -> E3Result<Vec<SyncDBIndexEntry>> {
        let mut sql = String::from(SyncDBIndexEntry::SELECT_SQL);
        if let Some(_) = host {
            sql.push_str(" WHERE host = ?");
        }
        if let Some(deletion) = deletion {
            if host.is_some() {
                sql.push_str(" AND");
            } else {
                sql.push_str(" WHERE");
            }
            if deletion {
                sql.push_str(" blob NULL");
            } else {
                sql.push_str(" blob NOT NULL");
            }
        }
        // eprintln!("{}", sql);
        let stmt = e3q!(self.connection().prepare(sql));
        if let Some(host) = host {
            e3q!(stmt.bind(1, sqlite3::Value::Text(host.to_string())));
        }
        SyncDBIndexEntry::from_stmt(&stmt)
    }
}

/// Converts a list of database index entries to a hashmap.
/// This operation is useful when, for example, comparing database entries between hosts.
pub fn index_to_hashmap(
    src: Vec<SyncDBIndexEntry>,
) -> E3Result<HashMap<SyncPath, SyncDBIndexEntry>> {
    let mut map = HashMap::new();
    for v in src {
        map.insert(v.path.clone().parse()?, v);
    }
    Ok(map)
}

impl<T: SyncDBROOps> SyncBlobIndex for T {
    fn blob_length(&self, hash: SyncHash) -> E3Result<u64> {
        let stmt = e3q!(self
            .connection()
            .prepare("SELECT hash, length FROM blobs WHERE hash = ?"));
        e3q!(stmt.bind(1, sqlite3::Value::Blob(hash.to_vec())));
        if let Some(_) = e3q!(stmt.step()) {
            Ok(stmt.column_int(1) as u64)
        } else {
            Err(E3Err::of("get_blob_info: blob does not exist"))
        }
    }
}

/// Read-Only SyncDB
pub struct SyncDBRO(pub sqlite3::Connection);

impl SyncDBInit for SyncDBRO {
    const READONLY: bool = true;

    fn from_initialized_connection(conn: sqlite3::Connection) -> SyncDBRO {
        SyncDBRO(conn)
    }
}

impl SyncDBROOps for SyncDBRO {
    fn connection(&self) -> &sqlite3::Connection {
        &self.0
    }
}

/// Read/Write SyncDB
pub struct SyncDBRW(pub sqlite3::Connection);

impl SyncDBInit for SyncDBRW {
    const READONLY: bool = false;

    fn from_initialized_connection(conn: sqlite3::Connection) -> SyncDBRW {
        SyncDBRW(conn)
    }
}

impl SyncDBROOps for SyncDBRW {
    fn connection(&self) -> &sqlite3::Connection {
        &self.0
    }
}

impl SyncDBRW {
    /// Ensures a path exists.
    pub fn ensure_path(&self, path: &str) -> E3Result<()> {
        let stmt = e3q!(self
            .0
            .prepare("INSERT OR IGNORE INTO paths (path) VALUES (?)"));
        e3q!(stmt.bind(1, sqlite3::Value::Text(path.to_string())));
        e3q!(stmt.finish());
        Ok(())
    }
    /// Ensures a blob exists.
    /// To make sure this doesn't break things, it must be in a pool.
    pub fn ensure_blob(&self, blob: SyncHash, pool: &SyncBlobPool) -> E3Result<()> {
        self.ensure_blob_length(blob, pool.blob_length(blob)?)
    }

    /// Ensures a blob exists. Assumes the blob exists. Be careful.
    pub fn ensure_blob_length(&self, blob: SyncHash, length: u64) -> E3Result<()> {
        let stmt = e3q!(self
            .0
            .prepare("INSERT OR IGNORE INTO blobs (\"hash\", \"length\") VALUES (?, ?)"));
        e3q!(stmt.bind(1, sqlite3::Value::Blob(blob.to_vec())));
        e3q!(stmt.bind(2, sqlite3::Value::Integer(length as i64)));
        e3q!(stmt.finish());
        Ok(())
    }

    /// Puts a host entry in.
    pub fn put_host(&self, host: &str, nominal_time: i64) -> E3Result<()> {
        let stmt = e3q!(self
            .0
            .prepare("INSERT OR REPLACE INTO hosts (host, update_time) VALUES (?, ?)"));
        e3q!(stmt.bind(1, sqlite3::Value::Text(host.to_string())));
        e3q!(stmt.bind(2, sqlite3::Value::Integer(nominal_time)));
        e3q!(stmt.finish());
        Ok(())
    }

    /// Adds or changes an index entry.
    pub fn write_index_entry(&self, record: &SyncDBIndexEntry) -> E3Result<()> {
        let stmt = e3q!(self
            .0
            .prepare("INSERT OR REPLACE INTO \"index\" (host, path, blob, executable, update_time) VALUES (?, ?, ?, ?, ?)"));
        e3q!(stmt.bind(1, sqlite3::Value::Text(record.host.clone())));
        e3q!(stmt.bind(2, sqlite3::Value::Text(record.path.clone())));
        e3q!(stmt.bind(
            3,
            if let Some(blob) = record.blob {
                sqlite3::Value::Blob(blob.to_vec())
            } else {
                sqlite3::Value::Null
            },
        ));
        e3q!(stmt.bind(4, sqlite3::Value::Integer(record.flags)));
        e3q!(stmt.bind(5, sqlite3::Value::Integer(record.update_time)));
        e3q!(stmt.finish());
        Ok(())
    }
}
