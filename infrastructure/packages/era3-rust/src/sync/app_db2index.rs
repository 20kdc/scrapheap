//! Code for era3 sync core db2index.

use std::{fs::{DirEntry, Permissions}, os::unix::fs::PermissionsExt};

use crate::engine::{args_fixed, sqlite3};

use super::{
    blobs::SyncBlobPool,
    database::{SyncDBInit, SyncDBRO},
    observatory::{observation_diffsets, observe_db, observe_workspace, regularize_db_conflicts, SyncObservationResult, SyncPath},
};

/// db2index main function
/// here so that it's easier to write & maintain
pub fn main() {
    let args = args_fixed::<2>("DB HOSTNAME");
    let db = args[0].clone();
    let hostname = args[1].clone();

    let conn = sqlite3::open(format!("file:{}", db), sqlite3::OPEN_READONLY).expect("db connect 1");
    let conn = SyncDBRO::from_connection(conn, false).expect("db connect 2");

    let blobs = SyncBlobPool::new("blobs");

    let mut target_index = observe_db(&conn, &hostname, &blobs).expect("read index");

    // pass 1: regularize DB conflicts
    target_index = regularize_db_conflicts(target_index);

    // pass 2: create dirs & delete old conflicting files
    for (path, _) in &target_index {
        attach_dir(&path.parent().expect("must have a parent"));
    }

    // pass 3: observe the filesystem to figure out what needs updating
    let ob_ws = observe_workspace("workspace", &blobs).expect("observe_ws");
    let diff = observation_diffsets(&ob_ws, &target_index);

    // pass 4: project the DB into the filesystem
    for (path, mode) in &diff {
        let real_path = path.to_real_path("workspace");
        match mode {
            SyncObservationResult::Untouched => {}
            SyncObservationResult::Removed => {
                _ = std::fs::remove_file(&real_path);
            }
            SyncObservationResult::Added(_) => {
                let entry = &target_index[path];
                entry.project(real_path).expect("error in projector");
            }
            SyncObservationResult::Changed(_) => {
                let entry = &target_index[path];
                entry.project(real_path).expect("error in projector");
            }
        }
    }

    // pass 5: delete leaf directories
    for entry in std::fs::read_dir("workspace").expect("read_dir") {
        delete_leaf_dirs(entry.expect("read_dir"));
    }
}

/// Creates a directory, deleting existing files if they're in the way.
fn attach_dir(path: &SyncPath) {
    if let Some(parent) = path.parent() {
        attach_dir(&parent);
    }
    let real_path = path.to_real_path("workspace");
    if let Ok(metadata) = std::fs::symlink_metadata(&real_path) {
        if !metadata.is_dir() {
            _ = std::fs::remove_file(&real_path);
        }
    }
    _ = std::fs::create_dir(&real_path);
    std::fs::set_permissions(&real_path, Permissions::from_mode(0o775))
        .expect("permissions fixing");
}

/// Returns true if the target was deleted.
fn delete_leaf_dirs(path: DirEntry) -> bool {
    let metadata = path.metadata().unwrap();
    if metadata.is_dir() {
        let mut is_empty = true;
        for entry in std::fs::read_dir(path.path()).expect("read_dir inner") {
            if !delete_leaf_dirs(entry.expect("read_dir inner")) {
                // wasn't deleted so not empty'
                is_empty = false;
            }
        }
        if is_empty {
            _ = std::fs::remove_dir(path.path());
        }
        is_empty
    } else {
        false
    }
}
