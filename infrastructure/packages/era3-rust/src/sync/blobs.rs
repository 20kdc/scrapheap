//! Abstraction over the blob pool.
//! Immutable data is stored in the blob pool.

use std::{
    fs::{File, OpenOptions},
    io::{Read, Write},
    path::{Path, PathBuf},
};

use crate::{
    e3q,
    engine::{
        fs::RemoveFileOnDrop,
        prelude::*,
        sodium::{SHA256Hash, SHA256HashState, CRYPTO_HASH_SHA256_BYTES},
        threaded_task_manager::{Task, TaskManager},
    },
};

/// Size of [SyncHash].
pub const SYNC_HASH_LEN: usize = CRYPTO_HASH_SHA256_BYTES;
/// Typedef for sync hashes. These are likely going to always be SHA-256 since that's the existing data, but it's easier to change things later if its centrally defined.
pub type SyncHash = SHA256Hash;

/// Wrapper for a/the local blob pool.
pub struct SyncBlobPool(PathBuf);

/// A source for blob information. This can either be a DB or the disk.
pub trait SyncBlobIndex {
    /// Gets the length of a blob by hash.
    fn blob_length(&self, hash: SyncHash) -> E3Result<u64>;
}

const BLOB_CHUNK: usize = 0x1000000;

impl SyncBlobPool {
    /// Creates a SyncBlobPool from the blob pool.
    pub fn new(pb: impl AsRef<Path>) -> SyncBlobPool {
        Self(PathBuf::from(pb.as_ref()))
    }
    /// Gets the expected file path of a blob.
    pub fn blob_path(&self, blob: SyncHash) -> PathBuf {
        self.0.join(bytes_to_hex_string(&blob))
    }
    /// Reads an entire blob into memory.
    pub fn blob_data(&self, blob: SyncHash) -> E3Result<Vec<u8>> {
        std::fs::read(self.blob_path(blob)).e3("unable to read blob")
    }
    /// Creates a blob in the pool. Beware: This doesn't register it in the database!
    pub fn import_blob_from_slice(
        &self,
        mut src: &[u8],
        ttm: &mut impl TaskManager,
        tasks: &mut Vec<Task<E3Result<()>>>,
    ) -> E3Result<(SyncHash, u64)> {
        self.import_blob(&mut src, |_| {}, ttm, tasks)
    }
    /// Creates a blob in the pool. Beware: This doesn't register it in the database!
    /// TaskManager is used to pipeline syncs, so you should be sure to close it off.
    pub fn import_blob(
        &self,
        mut source: impl Read,
        mut progress: impl FnMut(u64),
        ttm: &mut impl TaskManager,
        tasks: &mut Vec<Task<E3Result<()>>>,
    ) -> E3Result<(SyncHash, u64)> {
        // try to ensure that different blob imports don't conflict
        let mut random_bytes: [u8; 16] = [0; 16];
        e3q!(e3q!(File::open("/dev/urandom")).read_exact(&mut random_bytes));
        // done
        let tmpfilepath = self.0.join(&format!(".incoming-{}", bytes_to_hex_string(&random_bytes)));
        let mut holding_file = e3q!(OpenOptions::new()
            .read(true)
            .write(true)
            .create_new(true)
            .open(&tmpfilepath));
        let remove_holding_file: RemoveFileOnDrop = (&tmpfilepath).into();
        // it'd be irresponsible to allocate this much data on stack... sadly
        let mut blob_chunk: Vec<u8> = Vec::with_capacity(BLOB_CHUNK);
        blob_chunk.resize(BLOB_CHUNK, 0);
        let mut total: u64 = 0;
        let mut hasher = SHA256HashState::default();
        loop {
            let us = e3q!(source.read(&mut blob_chunk));
            if us == 0 {
                break;
            }
            let read_chunk = &blob_chunk[0..us];
            hasher.update(read_chunk);
            e3q!(holding_file.write_all(read_chunk));
            total += read_chunk.len() as u64;
            progress(total);
        }
        let hash = hasher.finish();
        let finalpath = self.0.join(bytes_to_hex_string(&hash));
        tasks.push(ttm.spawn(move || {
            // make sure the file's properly saved, CPU latency isn't really the big issue in this part of E3S
            // and we *really* don't want to see corrupt files in blobstore
            e3q!(holding_file.sync_all());
            drop(holding_file);
            remove_holding_file.rename_or_remove(finalpath)?;
            Ok(())
        }));
        Ok((hash, total))
    }
}

impl SyncBlobIndex for SyncBlobPool {
    fn blob_length(&self, hash: SyncHash) -> E3Result<u64> {
        Ok(e3q!(std::fs::metadata(self.blob_path(hash))).len())
    }
}
