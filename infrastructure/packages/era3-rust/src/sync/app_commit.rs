//! Code for era3 sync core commit.

use std::{path::PathBuf, str::FromStr, time::SystemTime};

use crate::engine::{
    args_fixed,
    prelude::*,
    sqlite3::{self, Transaction},
    threaded_task_manager::{Task, TaskPool},
    tui,
};

use super::{
    blobs::SyncBlobPool,
    database::{SyncDBInit, SyncDBRW},
    observatory::{observation_diffsets, observe_db, observe_workspace, SyncPath},
};

/// commit main function
/// here so that it's easier to write & maintain'
pub fn main() {
    let args = args_fixed::<1>("HOSTNAME");
    let hostname = args[0].clone();

    // ok, so, cheeky trick here: this is to ensure that any FUSE ops are done
    crate::engine::fs::sync();

    let db = SyncDBRW::connect().expect("connecting to db");
    let blobs = SyncBlobPool::new("blob");

    let nominal_time = systemtime_to_unix_time(SystemTime::now());

    let tx = sqlite3::Transaction::new(&db.0).expect("opening tx");

    // we have to start with this statement in case this is our first commit
    // it's all a transaction anyway sooooooooo

    db.put_host(&hostname, nominal_time).expect("adding host");

    tui::status("scanning");

    let ob_db = observe_db(&db, &hostname, &blobs).expect("observe_db");
    let ob_ws = observe_workspace("workspace", &blobs).expect("observe_workspace");

    let ob_diffs = observation_diffsets(&ob_db, &ob_ws);

    let mut paths_all: Vec<SyncPath> = ob_diffs.keys().cloned().collect();
    paths_all.sort();

    tui::status("removing & collating");

    let mut total_progress: u64 = 0;
    let mut add_as_db_record: Vec<SyncPath> = Vec::new();

    for path in &paths_all {
        match ob_diffs[path] {
            super::observatory::SyncObservationResult::Untouched => {}
            super::observatory::SyncObservationResult::Added(_) => {
                total_progress += ob_ws[path].progress_contribution();
                add_as_db_record.push(path.clone());
            }
            super::observatory::SyncObservationResult::Changed(_) => {
                total_progress += ob_ws[path].progress_contribution();
                add_as_db_record.push(path.clone());
            }
            super::observatory::SyncObservationResult::Removed => {
                println!("- {}", path);
                db.write_index_entry(&crate::sync::database::SyncDBIndexEntry {
                    host: hostname.clone(),
                    path: path.to_string(),
                    blob: None,
                    flags: 0,
                    update_time: nominal_time,
                })
                .expect("writing index entry");
            }
        }
    }

    tui::status("importing");

    let mut ttm: TaskPool<16> = TaskPool::default();

    let mut progress_base: u64 = 0;
    let mut tasks: Vec<Task<E3Result<()>>> = Vec::new();
    for path in &add_as_db_record {
        println!("+ {}", path);
        let ob = &ob_ws[path];
        ob.add_as_db_record(
            hostname.clone(),
            path.to_real_path(PathBuf::from_str("mountpoint").unwrap()),
            path.clone(),
            nominal_time,
            &blobs,
            &db,
            |progress| {
                let full_progress = progress_base + progress;
                let percent: f64;
                if total_progress == 0 {
                    percent = 0.0;
                } else {
                    percent = ((full_progress as f64) / (total_progress as f64)) * 100.0;
                }
                tui::status(&format!("importing file ({:03})", percent.floor() as i64));
            },
            &mut ttm,
            &mut tasks,
        )
        .expect("db record add");
        progress_base += ob.progress_contribution();
    }
    for v in tasks {
        v.join().unwrap();
    }

    // This ensures all syncs complete
    drop(ttm);

    tui::status("committing");

    tx.commit().expect("committing");

    tui::status("optimizing");

    let tx = Transaction::new(&db.0).expect("creating optimization transaction");

    db.0.execute_script("PRAGMA optimize")
        .expect("while optimizing");

    tx.commit().expect("committing optimization transaction");

    tui::status("era3::engine::fs::sync()");

    crate::engine::fs::sync();

    tui::status("");
}
