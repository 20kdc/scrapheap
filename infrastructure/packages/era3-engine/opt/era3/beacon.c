// Still written in C because it's a background service and I can't get Rust to stop embedding panic code into everything.

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>

#define HOST_NAME_MAX 64

int main() {
	struct sockaddr_in6 addr = {};
	addr.sin6_family = AF_INET6;
	addr.sin6_port = htobe16(20530);
	int fd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr))) {
		return 1;
	}
	char hostname[HOST_NAME_MAX + 1] = {};
	gethostname(hostname, HOST_NAME_MAX);
	while (1) {
		char tmp;
		socklen_t addr_len = sizeof(addr);
		if (recvfrom(fd, &tmp, 1, 0, (struct sockaddr *) &addr, &addr_len) >= 0) {
			sendto(fd, hostname, strlen(hostname), 0, (struct sockaddr *) &addr, addr_len);
		}
	}
	return 0;
}
