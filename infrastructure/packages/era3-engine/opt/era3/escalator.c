#define _GNU_SOURCE
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>
#include <string.h>

char adjusted_program_name[4096] = "/opt/era3/escalated/";

// for numbers/etc.
char buffer_str[256];

int main(int argc, char ** argv) {
	// strip away 1st arg
	char * newargv[argc];
	for (int i = 1; i < argc; i++)
		newargv[i - 1] = argv[i];
	newargv[argc - 1] = NULL;
	// uhoh
	if (argc <= 1) {
		fprintf(stderr, "escalator: must have at least one arg (era3 engine escalate VERB [ARG...])\n");
		return 1;
	}
	// fprintf(stderr, "getuid %i geteuid %i\n", getuid(), geteuid());
	// get group & user data
	struct group * personnel_group = getgrnam("era3-personnel");
	if (!personnel_group) {
		fprintf(stderr, "escalator: era3-personnel group does not exist\n");
		return 1;
	}
	char * apn_concat = adjusted_program_name + strlen(adjusted_program_name);
	char * pnreader = newargv[0];
	if (!*pnreader) {
		fprintf(stderr, "escalator: verb name cannot be empty\n");
		return 1;
	}
	if (strlen(pnreader) > 128) {
		fprintf(stderr, "escalator: verb name too long\n");
		return 1;
	}
	while (*pnreader) {
		char c = *pnreader;
		if (!((c >= 'a' && c <= 'z') || c == '-')) {
			fprintf(stderr, "escalator: verb name is invalid\n");
			return 1;
		}
		*(apn_concat++) = c;
		pnreader++;
	}

	int original_uid = getuid();

	if (getuid() != 0 && !group_member(personnel_group->gr_gid)) {
		fprintf(stderr, "escalator: user is not part of era3-personnel group\n");
		return 1;
	}
	// we are root now!
	if (setuid(0)) {
		fprintf(stderr, "escalator: setuid failed\n");
		return 1;
	}
	if (setgid(0)) {
		fprintf(stderr, "escalator: setgid failed\n");
		return 1;
	}
	if (initgroups("root", 0)) {
		fprintf(stderr, "escalator: initgroups failed\n");
		return 1;
	}
	// ensure the environment is safe
	clearenv();
	// useful information
	setenv("PATH", "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", 1);
	sprintf(buffer_str, "%i", original_uid);
	setenv("CALLER_UID", buffer_str, 1);

	int res = execvp(adjusted_program_name, newargv);
	fprintf(stderr, "escalator: execvp failed for %s\n", adjusted_program_name);
	return res;
}
