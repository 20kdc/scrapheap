# utilities for long-running tools and TUIs

import os
import sys

def size():
	try:
		import termios
		return termios.tcgetwinsize(2)
	except:
		return 25, 80

def width():
	return size()[1]

def height():
	return size()[0]

def text_width(text):
	return len(text)

def cls(into=sys.stderr):
	into.write("\x1Bc")
	into.flush()

def status(text):
	try:
		if os.isatty(2):
			columns = width() - text_width(text)
			sys.stderr.write("\x1B[K\x1B[s\r\x1B[" + str(columns) + "C" + text + "\x1B[u")
			sys.stderr.flush()
			return
	except:
		pass
	sys.stderr.write(text + "\n")
	sys.stderr.flush()

_spinner_phases = (
	(["  ", "󱥳 ", "󱥮 ", "󱥮󱥳", "󱥮󱥮"] * 4) +
	(["· ", "--", " ·", "--"] * 5) +
	(["| ", "/ ", " -", " \\"] * 5) +
	(["00", "01", "10", "11"] * 5) +
	(["__", "--", "¹¹", "--"] * 5) +
	(["he", "el", "ll", "lo", "oh"] * 4)
)

_spinner_phases_len = len(_spinner_phases)

class Spinner():
	def __init__(self):
		self._i = 0
	def get(self):
		self._i = (self._i + 1) % _spinner_phases_len
		return _spinner_phases[self._i]
