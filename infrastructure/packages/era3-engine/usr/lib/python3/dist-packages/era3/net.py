# utilities for networking & such

import socket

def ipv6(name):
	x = 0
	y = 0
	for chr in name.encode("utf8"):
		for round in range(32):
			x = ((x >> 1) | (x << 7)) & 0xFF
			x = (x + y) & 0xFF
			x ^= chr & 0xDF
			y = ((y << 1) | (y >> 7)) & 0xFF
			y ^= x
	addr = bytes([0xFD, 0xE3, x, y, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
	return socket.inet_ntop(socket.AF_INET6, addr)

def resolve(name):
	if "." in name:
		return name
	if ":" in name:
		return name
	return ipv6(name)
