# era3-sync Design Document

era3-sync is built using a relatively modular structure, made up of interoperating formats connected via programs that manage the system.

You can think of it as four stores of data:

* `blob` (Blob Repository): Contains files. Each file is named by its SHA-256 sum.
* `metadata.db` (Database): The database provides the central, SQLite-based 'authoritative copy' of all indexes of all hosts.
* `workspace` (Workspace): Mirrors the structure of the data. This is used as the backend of the FUSE filesystem.
* `mountpoint` (Mountpoint): Projection of the workspace.
* `plan.json` (Plan): Temporary working file during sync. Dictates which index entries to trust over others.

Different tasks work with different areas of the system:

* Data transfer (i.e. `tar2blobs` and `blobs2tar`) work with the blob repository.
* Planning works with the database.
* FUSE / Moment to moment access (i.e. playing a music file with VLC) works with the workspace and blob repository, avoiding the database.
* Workspace reset, status and commit bring the whole thing together, as these access all three stores of data.
* Synchronization is essentially a checklist: workspace status check, planning, data transfer, workspace reset.

Key advantages of this design:

* Renaming or moving files changes very little in the database. This is as opposed to a big problem with Sync2 which sees it as delete/new even on-network.
* Independence between workspace / mountpoint and the database. This proved important i.e. with the changeover from blobfs to CowBarbeque.
	* blobfs had the same basic problem as Sync2, but in the form of large stalls in `unionfs-fuse`, not during cross-device sync.
* Index merging happens mostly through simple SQL code (see `era3-sync/opt/era3/commands/sync/core/metadata-import`).
* The indexes are the authoritative copy, not simply used for communication. Deriving the workspace from them (i.e. the undo button) is a cheap operation.
	* It is also possible to derive indexes from a backup and cheaply restore them, which offers a level of accident prevention.

## Versioning/DB Migration

...about that: There is no DB migration strategy right now apart from manually updating the DB when the schema changes.

This shouldn't matter as E3S is very close to the end of development (written March 3rd 2025).

Schema changelog:

* March 2nd, 2024: Initial version of schema.
* March 4th, 2024: Removed blob IDs in favour of using their hashes everywhere. ID numbers make import/export unworkable, but it was early days.
* March 7th, 2024: Added `application_id` and `user_version` fields to mark a sync database for fun. Added a foreign key constraint to `path` and give the `index` table the `host, path` primary key.
* March 11th, 2024: Added `local_plan` table (this was later ditched as plans make more sense as JSON files)
* May 3rd, 2024: `local_plan` deleted as the Plan is JSON-based now
* March 2nd, 2025: Added `stars` table for names that must be remembered forever

## Blob Repository

Blobs are stored in the blobs directory as so: `blobs/00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff`.

The blob names are based on SHA-256 sum.

## Workspace / `blobfs`

Basically, the current iteration of the workspace (aka blobfs) works like this:

The `workspace/` directory is made up of regular files, symlinks, and directories.

However, all regular files are, in fact, CowBarbeque files.

### CowBarbeque

CowBarbeque files have a 4096-byte header, made up of:

* a null-terminated 128-byte string area; this indicates the 'source filename' relative to the workspace directory.
	* era3-sync relies on these being of the form `../blob/BLOBHEX`.
* 248 (start, length) 64-bit number pairs for the extents that come from that source file.
* They may optionally, if and only if their contents are unmodified from the source file, have an xattr record `user.e3scow`.
	* The record contents are exactly equivalent to the CowBarbeque source filename. It may be _up to_ 127 bytes, and contains no zero bytes.
	* These are intended to be embedded into inodes.
	* The record *must* be removed if the file is modified or reinitialized (i.e. on truncation) for any reason. It is an assertion that the file is _exactly equivalent_ to the blob.
		* Notably, the use of these records removes the need to parse CowBarbeque headers throughout most processes.
		* It is recommended that applications reading this value also verify the size matches.

### Rationale

The goal of this mechanism is to essentially implement Copy-On-Write semantics between sync commits, reducing the amount of massive, file-manager-grinding-to-a-halt lagspikes (this was a problem that arose with the `unionfs-fuse`-based implementation).

The extent limit prevents the performance issues with this approach from getting too out-of-hand; if too many extents are present, the smallest extents will be migrated first.

The worst case scenario here is that the areas being changed are perfectly equally spaced, as this leads to the largest extents to migrate.

However, each extent will be roughly 1/248th of the file size, which should mitigate the practical danger immensely.

It's still much better than what was going on before, which put the whole project on hold.

Another problem that ended up cropping up was scanning for modified files.

This lead to the introduction of the "unmodified mark" `user.e3scow`, which exists to speed it up significantly by keeping the source file with the rest of the inode.
