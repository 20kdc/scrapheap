# (the public part of) 20kdc Infrastructure

So a lot of my infrastructure is too... terribad to be published.

However, it'd be nice if at least some of it was published, so it might go here in future.

At least there's a place for it now.

Quick index:

* packages: Debian packages for 20kdc systems
* nanomta: Tiny SMTP receiving daemon I'll probably end up using once I can be bothered to setup a server again
* shed25519: Tiny libsodium-based signature tool to allow shell scripts to do cool stuff without the issues GPG has
* scripts: aka `kit pub`, shell scripts and such small things
* wspace: NAT traversal (this maybe ought not to be in infrastructure???)