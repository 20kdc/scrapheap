# unofficial protocol notes & Linux client for the application 'WO Mic'

These are notes on the Wifi protocol and a custom Rust Linux JACK client for the application WO Mic, from <https://wolicheng.com/womic/index.html>.

The application already has an official Linux client, but it uses `snd-aloop` and that appears to have issues on some setups when interfacing with other applications.

All the monetization methods of the application (AdSense and subscribing to unlock volume control) are in the server, running on the tablet, so this shouldn't cause any issues.

To be clear, this unofficial client's buffering/packet loss recovery behaviour is... non-existent -- I wasn't sure how to deal with the edge cases. Hopefully Pipewire will just deal with it somehow when an underflow occurs, because I have no idea how to ensure that myself.

Protocol studies were done black-box with Wireshark. Not much effort was put into detailed probing of client or server behaviour; this was a quick project to get something working.

## Intro

All values in this protocol are big-endian.

Data is written here in hex, as so: `10` for decimal 16.

## TCP Session Protocol

The TCP session protocol goes as follows:

* Client: `65 00000006 04 04 06 02 00 00`
* Server: `65 00000002 00 04`
* Client: `66 00000006 02 02 00 00 1234` (Note: `0x1234` is the port number.)
* Server: `66 00000001 00`
* Client: `67 00000000`
* Server: `67 00000001 00`
* Client: `69 00000000` (The client sends this every second.)

The basic framing of this protocol is a type byte followed by a length, and the location of the port number is reasonably obvious. Other than this details are presently uncertain.

However, simply sending the client's side of the transcript seems to be acceptable for the server to send back audio data.

## UDP Media Protocol

UDP packets are laid out as follows:

* 00: bytes `04 00`
* 02: length, 16-bit (or possibly 24-bit)
* 04: sequence counter, 16-bit
* 06: unknown, increases by inconsistent values, 32-bit
* 0A: byte `05`
* 0B: Single Opus packet until end of UDP packet
