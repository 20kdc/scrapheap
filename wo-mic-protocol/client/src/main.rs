use std::thread::spawn;
use std::time::Duration;
use std::io::Write;
use std::net::{TcpStream, UdpSocket, SocketAddr, Ipv6Addr};
use std::sync::mpsc::channel;
use std::cell::RefCell;
use opus::Decoder;

/// UDP port used for the actual audio packets
const MEDIA_PORT_NUMBER: u16 = 49152;

/// Used by the JACK process handler to track position in the current audio buffer.
struct ReadingAudioBuffer {
	data: Vec<f32>,
	pos: usize
}

/// various stuff I just dumped here, also starts the media thread
/// since JACK starts its own thread the session thread now lives on the main thread
fn main() {
	let (samples_tx, samples_rx) = channel::<Vec<f32>>();
	let (client, _status) = jack::Client::new("wo_mic_protocol_client_rs", jack::ClientOptions::NO_START_SERVER).unwrap();
	let mut port: jack::Port<jack::AudioOut> = client.register_port("output", jack::AudioOut).unwrap();
	let sample_rate = client.sample_rate();
	eprintln!("system sample rate: {} (if not 48000, expect Fun!)", sample_rate);

	let offthread_holder: RefCell<Option<ReadingAudioBuffer>> = RefCell::new(None);
	let process_handler = jack::ClosureProcessHandler::new(move |_client, scope| {
		for v in port.as_mut_slice(scope) {
			let value: f32;
			loop {
				// try reading
				let mut rab_maybe = offthread_holder.take();
				if let Some(rab) = &mut rab_maybe {
					if rab.pos < rab.data.len() {
						value = rab.data[rab.pos];
						rab.pos += 1;
						offthread_holder.replace(rab_maybe);
						break;
					}
				}
				// if we got here, we need a new ReadingAudioBuffer
				offthread_holder.replace(Some(ReadingAudioBuffer {
					data: samples_rx.recv().unwrap(),
					pos: 0
				}));
			}
			*v = value;
		}
		jack::Control::Continue
	});

	let async_client = client.activate_async((), process_handler).unwrap();

	// let's make sure we have this *before* starting the media thread
	let udp = UdpSocket::bind(SocketAddr::from((Ipv6Addr::UNSPECIFIED, MEDIA_PORT_NUMBER))).unwrap();
	spawn(|| {
		media_thread(udp, samples_tx);
	});
	session_thread();

	async_client.deactivate().unwrap();
}

/// This function handles receiving UDP packets, decoding the Opus audio contained within, and pushing it to the JACK thread.
fn media_thread(udp: UdpSocket, tx: std::sync::mpsc::Sender<Vec<f32>>) {
	let mut dec = Decoder::new(48000, opus::Channels::Mono).unwrap();
	// if the tablet is sending jumbo packets, we have other problems
	let mut packet_buffer: [u8;65536] = [0;65536];
	loop {
		let res = udp.recv_from(&mut packet_buffer);
		if let Ok(res) = res {
			let dataslice = &packet_buffer[0x0B..res.0];
			if let Ok(samples) = dec.get_nb_samples(dataslice) {
				let mut output: Vec<f32> = Vec::with_capacity(samples);
				output.resize(samples, 0f32);
				if let Ok(_decoded) = dec.decode_float(dataslice, &mut output, false) {
					let _ = tx.send(output);
				} else {
					eprintln!("invalid packet in lower: {:?}", dataslice);
				}
			} else {
				eprintln!("invalid packet in upper: {:?}", dataslice);
			}
		} else {
			eprintln!("read error: {:?}", res);
		}
	}
}

/// This function reads the single command-line argument (the target address to connect to) and 'negotiates' the session.
/// This process uses the tried and true methodology of telling the client whatever it wants to hear.
fn session_thread() {
	let mut args = std::env::args();
	// skip self name
	args.next();
	let arg = args.next().unwrap();
	let mut tcp = TcpStream::connect(&arg).unwrap();
	tcp.write_all(b"\x65\x00\x00\x00\x06\x04\x04\x06\x02\x00\x00").unwrap();
	tcp.write_all(b"\x66\x00\x00\x00\x06\x02\x02\x00\x00").unwrap();
	tcp.write_all(&MEDIA_PORT_NUMBER.to_be_bytes()).unwrap();
	tcp.write_all(b"\x67\x00\x00\x00\x00").unwrap();
	eprintln!("session now in ping loop");
	loop {
		tcp.write_all(b"\x69\x00\x00\x00\x00").unwrap();
		std::thread::sleep(Duration::from_secs(1));
	}
}
